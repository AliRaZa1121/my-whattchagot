<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class trade_price extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('trade_prices')->insert([

            [
                'price' => '15',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')

            ]
        ]);
    }
}
