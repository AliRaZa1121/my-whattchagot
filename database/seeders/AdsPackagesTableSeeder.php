<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class AdsPackagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ads_packages')->insert([

            [
                'name' => 'Basic Ads Package',
                'type' => 'Basic',
                'price' => 25.00,
                'description' => 'This package includes 2 ads',
                'images' => 2,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],

            [
                'name' => 'Gold Ads Package',
                'type' => 'Gold',
                'price' => 50.00,
                'description' => 'This package includes 4 ads',
                'images' => 4,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],

            [
                'name' => 'Platinum Ads Package',
                'type' => 'Platinum',
                'price' => 95.00,
                'description' => 'This package includes 6 ads',
                'images' => 6,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],

            ]);
    }
}
