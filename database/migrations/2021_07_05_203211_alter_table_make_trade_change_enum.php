<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableMakeTradeChangeEnum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::table('make_trade', function (Blueprint $table) {
        //     $table->enum('status', ['Pending','Decline', 'Accept','Cancel'])->default('Pending')->change();
        // });
        DB::statement("ALTER TABLE make_trade MODIFY COLUMN status ENUM('Pending', 'Decline', 'Cancel') DEFAULT 'Pending'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('make_trade', function (Blueprint $table) {
            //
        });
    }
}
