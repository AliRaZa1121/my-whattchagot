<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->id();
            $table->string('name', 250);
            $table->float('monthly_price')->default(0.00);
            $table->float('yearly_price')->default(0.00);
            $table->text('description')->default(null);
            $table->enum('type', ['silver', 'gold', 'platinum'])->default('silver');
            $table->boolean('address_phone_fax')->default(0);
            $table->boolean('maps_and_directions')->default(0);
            $table->boolean('hours_of_operation')->default(0);
            $table->boolean('area_served')->default(0);
            $table->boolean('contact_us_form')->default(0);
            $table->boolean('website_link')->default(0);
            $table->boolean('about_us_desc')->default(0);
            $table->integer('categories');
            $table->boolean('social_media_link')->default(0);
            $table->integer('images');
            $table->float('featured')->nullable();
            $table->integer('menu_and_attachment');
            $table->boolean('manage_reviews')->default(0);
            $table->integer('video');
            $table->integer('coupons');
            $table->integer('events');
            $table->integer('priority_listing');
            $table->boolean('homepage_exposure')->default(0);
//            $table->enum('duration', ['weekly', 'monthly', 'yearly'])->default('yearly');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
