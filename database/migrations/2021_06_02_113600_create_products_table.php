<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('price');
            $table->string('currency_type');
            $table->integer('category_id');
            $table->integer('sub_category_id');
            $table->integer('length');
            $table->integer('height');
            $table->integer('width');
            $table->integer('quantity')->nullable();
            $table->string('description')->nullable();
            $table->string('policy')->nullable();
            $table->enum('status', ['Pending', 'Approved'])->default('Pending');
            $table->unsignedBigInteger('user_id');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
