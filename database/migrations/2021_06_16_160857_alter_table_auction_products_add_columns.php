<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableAuctionProductsAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auction_products', function (Blueprint $table) {
            $table->string('bid_price_update')->after('end_date')->nullable(); // use this for field after specific column.
            $table->integer('bid_count')->after('bid_price_update')->default(0)->nullable();
            $table->integer('bid_last_user_id')->after('bid_count')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auction_products', function (Blueprint $table) {
            //
        });
    }
}
