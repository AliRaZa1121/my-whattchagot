<?php

use Illuminate\Support\Facades\Route;


use App\Http\Controllers\UserController;
use App\Http\Controllers\Auth\LoginController;
use App\Events\FormSubmitted;
use App\Http\Controllers\FbController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;

//
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//login route
Auth::routes(['register' => false, 'logout' => false]);
Route::get('/login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('/login', [LoginController::class, 'loginUser'])->name('login.submit');
Route::post('logout', [LoginController::class, 'logoutUser'])->name('logout');
Route::post('/register', [LoginController::class, 'create'])->name('register');

Route::get('/switch-user/{type}/{id}', [HomeController::class, 'switchUser'])->name('switch-user');



Route::get('auth/facebook', [FbController::class, 'redirectToFacebook']);

Route::get('auth/facebook/callback', [FbController::class, 'facebookSignin']);

Route::get('auth/google', [LoginController::class, 'redirectToGoogle']);
Route::get('auth/google/callback', [LoginController::class, 'handleGoogleCallback']);

Route::get('check-zip/{zip}',[LoginController::class, 'checkZip'])->name('check.zip');

Route::get('profile-processing/{user}',[HomeController::class, 'profileProcess'])->name('profile.processing');

Route::middleware('auth')->group(function () {
    Route::get('/user/edit', [UserController::class, 'edit'])->name('user-edit');
    Route::post('/user-update', [UserController::class, 'update_user'])->name('user-update');
    Route::post('/user/attachment/upload/{type}', [UserController::class, 'uploadAttachment'])->name('user.attachment.upload');

    Route::namespace('App\Http\Controllers\Admin')->group(function () {
    Route::get('/dashboard', 'HomeController@index')->name('dashboard');

    //Products Routes
    Route::resource('products', ProductController::class);
    Route::get('/product/status/update/{product}/{status}', 'ProductController@updateStatus')->name('products.update.status');
    Route::post('/products-attahment/upload/{type}', 'ProductController@uploadAttachment')->name('products.attachment.upload');

    //orders
    Route::get('/orders', 'OrderController@index')->name('orders.index');
    Route::get('/ajax_get_billing_address', 'OrderController@ajax_get_billing_address')->name('ajax_get_billing_address');
    Route::get('/ajax_get_shipping', 'OrderController@ajax_get_shipping')->name('ajax_get_shipping');
    Route::get('/order_item/status/update/{order_item}/{status}', 'OrderItemController@updateStatus')->name('order_items.update.status');


    // advertise banners

    Route::get('advertise/banners', 'AdsPackageController@AdvertiseBanner')->name('advertise.banner.index');
    Route::get('advertise/banners/create', 'AdsPackageController@myAdvertiseBannerCreate')->name('advertise.banner.create');
    // Route::get('advertise/banner/edit/{id}', 'AdsPackageController@myAdvertiseBannerEdit')->name('advertise.banner.edit');
    Route::post('advertise/banner/store', 'AdsPackageController@myAdvertiseBannerStore')->name('advertise.banner.store');
    // Route::post('advertise/banner/update', 'AdsPackageController@myAdvertiseBannerUpdate')->name('advertise.banner.update');

    //chats
    Route::get('/chat', 'ChatController@admin_chat')->name('chat');
    Route::get('/users/chats', 'ChatController@index')->name('users.chat');
    Route::post('/ajax_get_message', 'ChatController@ajax_get_message')->name('ajax_get_message');
    Route::post('/ajax_save_message', 'ChatController@ajax_save_message')->name('ajax_save_message');
    Route::post('/ajax_show_message', 'ChatController@ajax_show_message')->name('ajax_show_message');
    Route::post('/chat_alreadylogin', 'ChatController@chat_alreadylogin')->name('chat_alreadylogin');
    Route::post('/ajax_get_message_admin', 'ChatController@ajax_get_message_admin')->name('ajax_get_message_admin');


    Route::resource('warehouse', WarehouseController::class);


});

    Route::namespace('App\Http\Controllers')->group(function () {
        //website interface
        Route::get('/product-details/{id}', 'HomeController@product_details')->name('product-details');
        Route::get('/auction-details/{id}', 'HomeController@auction_list')->name('auction-single');
        Route::get('/trading-detail/{id}', 'HomeController@trading_detail')->name('trading-single');

        Route::get('/checkout', 'HomeController@checkout')->name('checkout');
        Route::get('/checkout-single/{id}', 'HomeController@checkoutSingle')->name('checkout.single');
        Route::get('/cart', 'HomeController@cart')->name('cart');

        Route::get('/checkout_ajax', 'CheckoutController@checkout_ajax')->name('checkout_ajax');
        Route::get('/shipping_ajax', 'CheckoutController@shipping_ajax')->name('shipping_ajax');
        Route::get('/shipping_ajax/single/{id}/{zip}', 'CheckoutController@shippingAjaxSingle')->name('shipping_ajax.single');

        Route::get('/check_zipcode', 'CheckoutController@check_zipcode')->name('check_zipcode');

        Route::post('/checkout_chat_ajax', 'CheckoutController@checkout_chat_ajax')->name('checkout_chat_ajax');
        Route::get('/wishlist-ajax', 'CheckoutController@wishlist_ajax')->name('wishlist-ajax');
        Route::get('/wishlist', 'CheckoutController@wishlist')->name('wish-list');
        Route::get('/wishlish_delete/{id}', 'CheckoutController@wishlish_delete')->name('wishlish_delete');

        Route::get('/checkout_ajax_qty/', 'CheckoutController@checkout_ajax_qty')->name('checkout_ajax_qty');
        Route::get('/checkout_delete/{id}', 'CheckoutController@checkout_delete')->name('checkout_delete');
        Route::get('/buyer_login_id/{seller}/{product}', 'HomeController@buyer_login')->name('buyer_login_id');
        Route::get('/add_check_out/{seller}/{product}/{price}/{last_segment}', 'HomeController@buyer_login')->name('add_check_out');
        Route::get('/buyer_signup', 'HomeController@buyer_signup')->name('buyer_signup');
        Route::post('stripe', 'CheckoutController@stripePost')->name('stripe.post');

        Route::post('/checkout-single-post','CheckoutController@checkoutSingle')->name('checkout.single.post');

        Route::get('/contact-us', 'HomeController@contact_us')->name('contact-us');
        Route::post('/contact-us-submit', 'HomeController@contact_us_submit')->name('contact-us-submit');
        Route::get('/profile/{id}', 'HomeController@profile')->name('profile');

    });


});



Route::middleware(['auth', 'admin'])->group(function () {
   Route::get('users-list', [UserController::class, 'getusers'])->name('users');

   Route::namespace('App\Http\Controllers\Admin')->group(function () {

    Route::resource('category', CategoryController::class);
    Route::resource('sub-category', SubCategoryController::class);
    Route::resource('brand', BrandController::class);
    Route::post('/brand-attahment/upload/{type}', 'BrandController@uploadAttachment')->name('brand.attachment.upload');


    // Seller Packages
    Route::get('packages', 'SellerPackageController@index')->name('seller.packages');
    Route::get('packages/add', 'SellerPackageController@create')->name('seller.packages.create');
    Route::delete('packages/delete/{package}', 'SellerPackageController@destroy')->name('seller.packages.destroy');
    Route::get('packages/{sellerPackage}/edit', 'SellerPackageController@edit')->name('seller.packages.edit');
    Route::get('packages/{sellerPackage}/detail', 'SellerPackageController@show')->name('seller.packages.detail');
    Route::post('packages/store', 'SellerPackageController@store')->name('seller.packages.store');
    Route::post('packages/{sellerPackage}/update', 'SellerPackageController@update')->name('seller.packages.update');
    Route::delete('packages/{sellerPackage}/delete', 'SellerPackageController@destroy')->name('seller.packages.delete');


    Route::get('/newsletter', 'HomeController@Newsletterindex')->name('newsletters.index');
    Route::resource('website-banners', PageBannerController::class);

    //ad pacakges
    Route::get('ads-banner-packages', 'AdsPackageController@index')->name('ad-packages');
    Route::get('ads-banner-packages/add', 'AdsPackageController@create')->name('ad-packages.create');
    Route::delete('ads-banner-packages/delete/{id}', 'AdsPackageController@destroy')->name('ad-packages.destroy');
    Route::get('ads-banner-packages/{eventPackages}/edit', 'AdsPackageController@edit')->name('ad-packages.edit');
    Route::get('ads-banner-packages/{eventPackages}/detail', 'AdsPackageController@show')->name('ad-packages.detail');
    Route::post('ads-banner-packages/store', 'AdsPackageController@store')->name('ad-packages.store');
    Route::post('ads-banner-packages/{eventPackages}/update', 'AdsPackageController@update')->name('ad-packages.update');
    Route::get('ads-banner-packages/{eventPackages}/delete', 'AdsPackageController@destroy')->name('ad-packages.delete');

    // Route::get('ads-banner-packages/{packageId}/buyForm', 'AdsPackageController@buyAdPackageForm')->name('buy-ad-package-form');
    // Route::post('ads-banner-packages/buy', 'AdsPackageController@buyAdPackage')->name('buy-ad-package-store');

    Route::get('warehouse-sellers', 'WarehouseController@SellerWearhouse')->name('warehouse.index.seller');

    Route::resource('testimonials', TestimonialController::class);
    Route::post('/testimonials-attahment/upload/{type}', 'TestimonialController@uploadAttachment')->name('testimonials.attachment.upload');
    Route::get('/testimonials-delete/{id}', 'TestimonialController@destroy')->name('testimonials.delete');

    //not in use
    Route::resource('subscription', SubscriptionController::class);

    Route::get('contacts-listing', 'HomeController@getcontactus')->name('getcontactus');
    Route::get('contacts-delete/{id}', 'HomeController@contactsdelete')->name('contact-delete');

    Route::get('settings', 'SettingController@index')->name('settings');
    Route::post('settings/{setting}/update', 'SettingController@update')->name('settings.update');

    });

});







Route::prefix('admin')->group(function () {
    Route::namespace('App\Http\Controllers\Admin')->group(function () {
        Route::middleware('auth:admin')->group(function () {
            Route::get('/', 'HomeController@index')->name('admin.dashboard');


            Route::resource('seller', SellerProductController::class);
            Route::resource('financial_statement', SellerfinancialContoller::class);
            Route::get('transfer/{id}', 'SellerfinancialContoller@transfer')->name('financial_statement.transfer');
            Route::resource('auction', AuctionProductController::class);
            Route::resource('trading', TradingProductController::class);



            Route::post('/page-banner-attahment/upload/{type}', 'PageBannerController@uploadAttachment')->name('page.banner.upload');
            Route::post('/category-attahment/upload/{type}', 'CategoryController@uploadAttachment')->name('category.attachment.upload');
            // Route::post('/product-attahment/upload/{type}', 'ProductController@uploadAttachment')->name('product.attachment.upload');
            // Route::get('/product/status/update/{product}/{status}', 'ProductController@updateStatus')->name('products.update.status');
            // Route::get('/order_item/status/update/{order_item}/{status}', 'OrderItemController@updateStatus')->name('order_items.update.status');
            // Route::get('/chat', 'ChatController@admin_chat')->name('chat');
            Route::get('/user/status/update/{user}/{status}', 'SellerProductController@updatestatus')->name('user.update.status');
            // Route::get('seller_user', 'SellerProductController@getusers')->name('users.seller');
            Route::get('/order/{flag}', 'OrderController@index')->name('order.index');
            Route::get('/order_print', 'OrderController@order_print')->name('order_print');
            Route::get('buyer_user', 'SellerProductController@buyer_user')->name('users.buyer');
            Route::get('maketrade', 'SellerProductController@maketrade')->name('maketrade.admin');
            Route::get('/maketrade-detail/{request_product_id}/{user_id}/{maketradeDetail}', 'SellerProductController@maketradeDetail')->name('maketrade.detail.admin');
            Route::get('/bid_list_admin', 'OrderItemController@bid_list_admin')->name('bid.list.admin');

            Route::get('trade-price', 'TradingProductController@trade_price')->name('trade-price');
            Route::put('trade_update/{id}', 'TradingProductController@trade_update')->name('trade_update');
            //user





            Route::get('order-disputes/{order_id}', [App\Http\Controllers\Admin\DisputeController::class ,'index'])->name('disputes.index.admin');
            Route::get('disputes',[App\Http\Controllers\Admin\DisputeController::class ,'list'])->name('dispute.list.admin');
            Route::get('disputes-show/{dispute}',[App\Http\Controllers\Admin\DisputeController::class ,'show'])->name('dispute.show.admin');
            Route::get('disputes-change-status/{dispute}/{status}',[App\Http\Controllers\Admin\DisputeController::class ,'ChangeStatus'])->name('dispute.change.status.admin');





        });
    });

});

Route::prefix('seller')->group(function () {
    Route::namespace('App\Http\Controllers\Admin')->group(function () {
        Route::middleware('auth:seller')->group(function () {
            Route::resource('seller_warehouse', WarehouseController::class);
            Route::get('connect', 'SellerfinancialContoller@connect')->name('financial_statement.connect');

            Route::get('/', 'HomeController@index')->name('seller_dashboard');
            Route::resource('seller_seller', SellerProductController::class);
            Route::resource('seller_auction', AuctionProductController::class);
            Route::resource('seller_trading', TradingProductController::class);
            Route::resource('seller_category', CategoryController::class);
            Route::resource('seller_sub-category', SubCategoryController::class);



            Route::post('/seller_category-attahment/upload/{type}', 'CategoryController@uploadAttachment')->name('seller_category.attachment.upload');
            Route::post('/seller_product-attahment/upload/{type}', 'ProductController@uploadAttachment')->name('seller_product.attachment.upload');
            Route::get('/seller_chat', 'ChatController@index')->name('seller_chat');
            Route::get('/seller_product/status/update/{product}/{status}', 'ProductController@updateStatus')->name('seller_products.update.status');
            Route::get('/seller_order/{flag}', 'OrderController@index')->name('seller_order.index');
            Route::get('/seller_order_print', 'OrderController@order_print')->name('seller_order_print');
            Route::get('/seller_order_item/status/update/{order_item}/{status}', 'OrderItemController@updateStatus')->name('seller_order_items.update.status');
            Route::get('maketrade', 'SellerProductController@maketrade')->name('maketrade');
            Route::get('/maketrade-detail/{request_product_id}/{user_id}/{maketradeDetail}', 'SellerProductController@maketradeDetail')->name('maketrade.detail');
            Route::get('/maketrade/status/update/{request_product_id}/{user_id}/{status}', 'SellerProductController@updateStatus_trade')->name('maketrade.update.status');
            Route::post('stripe_trade', 'TradingProductController@stripePost')->name('stripe_trade.post');
            Route::get('/bid_list_seller', 'OrderItemController@bid_list_seller')->name('bid.list.seller');

            Route::get('create-banner/{package_id}/{user_package_id}', 'AdsPackageController@createbanner')->name('saller-create-banner');
            Route::post('banner-store', 'AdsPackageController@bannerstore')->name('seller_banner.store');


            Route::get('package-renew', 'SellerPackageController@PackageRenew')->name('package-renew');
            Route::post('package-renew', 'SellerPackageController@PackageRenewUpdate')->name('package-renew.update');


            Route::get('order-disputes/{order_id}', [App\Http\Controllers\Admin\DisputeController::class ,'index'])->name('disputes.index.seller');
            Route::get('disputes',[App\Http\Controllers\Admin\DisputeController::class ,'list'])->name('dispute.list.seller');
            Route::get('disputes-show/{dispute}',[App\Http\Controllers\Admin\DisputeController::class ,'show'])->name('dispute.show.seller');




        });
    });

});
Route::prefix('buyer')->group(function () {
    Route::namespace('App\Http\Controllers\Admin')->group(function () {
        Route::middleware('auth:buyer')->group(function () {
            Route::get('/', 'HomeController@index')->name('buyer_dashboard');
            Route::get('/buyer_chat', 'ChatController@index')->name('buyer_chat');
            Route::get('/buyer_order/{flag}', 'OrderController@index')->name('buyer_order.index');
            Route::get('/buyer_review', 'ReviewController@index')->name('buyer_review.index');
            Route::get('/buyer_order_print', 'OrderController@order_print')->name('buyer_order_print');
            Route::post('/store', 'ReviewController@store')->name('review.store');
            Route::get('/bid_list', 'OrderItemController@bid_list')->name('bid.list');
            Route::get('/buyer_order_item/status/update/{order_item}/{status}', 'OrderItemController@updateStatus')->name('buyer_order_items.update.status');

            // RequestTradeProduct
            Route::get('request-trading', 'RequestTradeProductController@index')->name('request.trading');
            Route::get('request-trading/add/{id}', 'RequestTradeProductController@create')->name('request.trading.create');
            Route::delete('request-trading/delete/{id}', 'RequestTradeProductController@destroy')->name('request.trading.destroy');
            Route::get('request-trading/{id}/edit', 'RequestTradeProductController@edit')->name('request.trading.edit');
            Route::get('request-trading/{id}/detail', 'RequestTradeProductController@show')->name('request.trading.detail');
            Route::post('request-trading/store', 'RequestTradeProductController@store')->name('request.trading.store');
            Route::post('request-trading/update', 'RequestTradeProductController@update')->name('request.trading.update');
            Route::get('request-trading/{id}/delete', 'RequestTradeProductController@destroy')->name('request.trading.delete');
            Route::post('/seller_product-attahment/upload/{type}', 'ProductController@uploadAttachment')->name('request.trading.attachment.upload');


            Route::get('order-disputes/{order_id}', [App\Http\Controllers\Admin\DisputeController::class ,'index'])->name('disputes.index.buyer');
            Route::get('dispute-form/{order_id}', [App\Http\Controllers\Admin\DisputeController::class ,'create'])->name('disputes.create.buyer');
            Route::post('dispute-form/submit', [App\Http\Controllers\Admin\DisputeController::class ,'store'])->name('disputes.store.buyer');


            Route::get('/checkout-list','HomeController@checkoutList')->name('checkout.list');


        });
    });

    Route::get('/add_bid', 'App\Http\Controllers\HomeController@add_bid')->name('add_bid');

});



//userinterface
Route::namespace('App\Http\Controllers')->group(function () {
    Route::get('get/sub-categories/{id}','HomeController@GetCategories');

    Route::get('/sitemap', 'HomeController@Sitemap')->name('site.map');
    Route::get('/', 'HomeController@home')->name('index');
    Route::get('/categories', 'HomeController@categories')->name('categories');
    Route::get('/news', 'HomeController@news')->name('news');
    Route::get('/news-single', 'HomeController@news_single')->name('news-single');
    Route::get('/about-us', 'HomeController@about_us')->name('about-us');
    Route::get('/privacy-policy', 'HomeController@PrivacyPolicy')->name('privacy-policy');
    Route::get('/auction', 'HomeController@auction')->name('auction-list');

    Route::post('/post_make_trade', 'HomeController@post_make_trade')->name('post-make-trade');



    Route::get('/shop/products', 'HomeController@products')->name('shop.products');
    Route::get('/new-arrivals', 'HomeController@newArrival')->name('new.arrival');
    Route::get('/newsletter-submit', 'HomeController@newsletterSubmit');

    Route::get('/top-traders', 'HomeController@topTraders')->name('top.traders');
    Route::get('/top-trades', 'HomeController@topTrades')->name('top.trades');


Route::get('money-back-guarantee', function () {
    return view('static-pages.money');
});


Route::get('bidding-buying-help', function () {
    return view('static-pages.bidding');
});

Route::get('stores', function () {
    return view('static-pages.store');
});

Route::get('whattchagot-local', function () {
    return view('static-pages.local');
});

Route::get('start-selling', function () {
    return view('static-pages.start-sell');
});

Route::get('learn-to-sell', function () {
    return view('static-pages.learn-sell');
});

Route::get('how-to-buy', function () {
    return view('static-pages.how-it-buy');
});

Route::get('business-sellers', function () {
    return view('static-pages.bus-sellers');
});


Route::get('affiliates', function () {
    return view('static-pages.affialiates');
});


Route::get('faqs', function () {
    return view('static-pages.faqs');
});


Route::get('careers', function () {
    return view('static-pages.career');
});


Route::get('advertise-with-us', function () {
    return view('static-pages.adevertise');
});


Route::get('help-center', function () {
    return view('static-pages.help');
});


Route::get('seller-information-center', function () {
    return view('static-pages.seller-info');
});


Route::get('terms-and-condition', function () {
    return view('static-pages.term');
});


});
Route::namespace('App\Http\Controllers\Admin')->group(function () {
    // Route::post('/ajax_get_message', 'ChatController@ajax_get_message')->name('ajax_get_message');
    // Route::post('/ajax_save_message', 'ChatController@ajax_save_message')->name('ajax_save_message');
    // Route::post('/ajax_show_message', 'ChatController@ajax_show_message')->name('ajax_show_message');
    // Route::post('/chat_alreadylogin', 'ChatController@chat_alreadylogin')->name('chat_alreadylogin');
    // Route::post('/ajax_get_message_admin', 'ChatController@ajax_get_message_admin')->name('ajax_get_message_admin');
    // Route::get('/ajax_get_billing_address', 'ChatController@ajax_get_billing_address')->name('ajax_get_billing_address');
    // Route::get('/ajax_get_shipping', 'ChatController@ajax_get_shipping')->name('ajax_get_shipping');
});

Route::get('/getsub', 'App\Http\Controllers\HomeController@getsub')->name('get.sub');
Route::get('/sender', function () {
    return view('sender');
});
Route::post('/sender', function () {
    $text = request()->text;
    $sender_id = request()->sender_id;
    $receiver_id = request()->receiver_id;
    $product_id = request()->product_id;
    $sender_name = request()->sender_name;
    event(new FormSubmitted($text, $sender_id, $receiver_id, $product_id, $sender_name));
});

Route::get('/get-auction', 'App\Http\Controllers\HomeController@AuctionCron')->name('auction.cron');



