// (function () {
//   const second = 1000,
//         minute = second * 60,
//         hour = minute * 60,
//         day = hour * 24;

//   let birthday = "May 30, 2050 00:00:00",
//       countDown = new Date(birthday).getTime(),
//       x = setInterval(function() {    

//         let now = new Date().getTime(),
//             distance = countDown - now;

//         document.getElementById("days").innerText = Math.floor(distance / (day)),
//           document.getElementById("hours").innerText = Math.floor((distance % (day)) / (hour)),
//           document.getElementById("minutes").innerText = Math.floor((distance % (hour)) / (minute)),
//           document.getElementById("seconds").innerText = Math.floor((distance % (minute)) / second);

//         //do something later when date is reached
//         if (distance < 0) {
//           let headline = document.getElementById("headline"),
//               countdown = document.getElementById("countdown"),
//               content = document.getElementById("content");

//           headline.innerText = "It's my birthday!";
//           countdown.style.display = "none";
//           content.style.display = "block";

//           clearInterval(x);
//         }
//         //seconds
//       }, 0)
//   }());



jQuery(document).ready(function() {
    jQuery('.mm-toggle').click(function() {
        if (jQuery('#mmb').hasClass('on')) {
            jQuery('#mmb').removeClass('on');
        } else {
            jQuery('#mmb').addClass('on');
        }
        jQuery('#mm ul').slideToggle('slow');
    });
});

jQuery(document).ready(function() {
    jQuery(".home_main_slider").owlCarousel({
        loop: true,
        items: 1,
        margin: 10,
        dots: false,
        nav: true,
        navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
        autoplay: false,
        autoplayTimeout: 5000,
        autoplaySpeed: 2000,
    });


    jQuery(".live_trade_slider").owlCarousel({
        loop: true,
        items: 1,
        margin: 10,
        dots: false,
        nav: true,
        navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
        autoplay: true,
        autoplayTimeout: 3000,
        responsiveClass: true,
    });

    jQuery(".trending_products_slider").owlCarousel({
        loop: true,
        items: 1,
        margin: 10,
        dots: false,
        nav: true,
        navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
        autoplay: true,
        autoplayTimeout: 9000,
        responsiveClass: true,
        autoplaySpeed: 2000,
        responsive: {
            0: {
                items: 1,
                autoHeight: true,
                nav: true
            },
            600: {
                items: 1,
                autoHeight: true,
                nav: false
            },
            1000: {
                items: 1,
                nav: true,
                autoHeight: true
            }
        }
    });







    jQuery(".profile-page-trade .trending_products_slider").owlCarousel({
        loop: true,
        items: 2,
        margin: 10,
        dots: false,
        nav: true,
        navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
        autoplay: false,
        autoplayTimeout: 3000,
        responsiveClass: true,
        autoplaySpeed: 2000,
        responsive: {
            0: {
                items: 1,
                autoHeight: true,
                nav: true
            },
            600: {
                items: 1,
                autoHeight: true,
                nav: false
            },
            1000: {
                items: 1,
                nav: true,
                autoHeight: true
            }
        }
    });



    jQuery("#top-selling-products .trending_products_slider").owlCarousel({
        loop: true,
        items: 2,
        margin: 10,
        dots: false,
        nav: true,
        navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
        autoplay: false,
        autoplayTimeout: 3000,
        responsiveClass: true,
        autoplaySpeed: 2000,
        responsive: {
            0: {
                items: 1,
                autoHeight: true,
                nav: true
            },
            600: {
                items: 1,
                autoHeight: true,
                nav: false
            },
            1000: {
                items: 1,
                nav: true,
                autoHeight: true
            }
        }
    });


    jQuery(".auction_slider_wrapper").owlCarousel({
        loop: true,
        infinite: true,
        items: 1,
        margin: 10,
        dots: false,
        nav: true,
        navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
        autoplay: false,
        autoplayTimeout: 3000,
        responsiveClass: true,
        autoplaySpeed: 2000,
    });

    jQuery(".best_selling_slider").owlCarousel({
        loop: true,
        items: 1,
        margin: 10,
        dots: false,
        nav: true,
        navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
        autoplay: false,
        autoplayTimeout: 3000,
        responsiveClass: true,
        autoplaySpeed: 2000,
        responsive: {
            0: {
                items: 1,
                autoHeight: true,
                nav: true
            },
            600: {
                items: 1,
                autoHeight: true,
                nav: false
            },
            1000: {
                items: 1,
                nav: true,
                autoHeight: true
            }
        }
    });

    // jQuery(".best_selling_row home-best-selling.home-best-sellings").owlCarousel({
    //     loop: false,
    //     items: 1,
    //     margin: 10,
    //     dots: false,
    //     nav: true,
    //     navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
    //     autoplay: false,
    //     autoplayTimeout: 3000,
    //     responsiveClass: true,
    //     autoplaySpeed: 2000,

    // });

    jQuery(".nav_sidebar").click(function() {
        jQuery(".side_cat_navbar").slideToggle("slow", function() {
            // Animation complete.
        });
    });


    jQuery("#news-slider").owlCarousel({
        loop: true,
        items: 3,
        rows: 3,
        margin: 10,
        dots: false,
        nav: true,
        navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
        autoplay: false,
        autoplayTimeout: 3000,
        autoplaySpeed: 2000,

        // responsive: {
        //   0: {
        //     items: 1,
        //     rows: 2 //custom option not used by Owl Carousel, but used by the algorithm below
        //   },
        //   768: {
        //     items: 2,
        //     rows: 3 //custom option not used by Owl Carousel, but used by the algorithm below
        //   },
        //   991: {
        //     items: 2,
        //     rows: 3 //custom option not used by Owl Carousel, but used by the algorithm below
        //   }
        // }

    });



});