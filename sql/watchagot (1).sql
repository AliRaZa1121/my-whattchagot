-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 10, 2021 at 11:29 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `watchagot`
--

-- --------------------------------------------------------

--
-- Table structure for table `auction_products`
--

CREATE TABLE `auction_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `auction_products`
--

INSERT INTO `auction_products` (`id`, `product_id`, `brand`, `start_date`, `end_date`, `start_time`, `end_time`) VALUES
(1, '4', 'Neque maxime dolor q', '06/04/2021', '06/04/2021', '12:00:00 AM', '12:00:00 AM');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `banner_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `banner_image`, `created_at`, `updated_at`) VALUES
(1, 'AAAA', '0', '2021-06-02 09:55:46', '2021-06-02 09:55:46'),
(2, 'bbbbaaccc', '0', '2021-06-02 09:57:04', '2021-06-02 11:24:33'),
(3, 'tuji@mailinator.com', 'category/ZEv8FzdByPR6dPsJFtzuhJAmnf5pVtyZf24eFMWA.jpg', '2021-06-03 05:40:53', '2021-06-03 05:40:53'),
(4, 'tuji@mailinator.com', 'category/ZEv8FzdByPR6dPsJFtzuhJAmnf5pVtyZf24eFMWA.jpg', '2021-06-03 05:40:58', '2021-06-03 05:40:58'),
(5, 'tuji@mailinator.com', 'category/ErHlJNArsX4hElJyBVCtIHt74YSjw89eCvBEMJwP.jpg', '2021-06-03 05:41:20', '2021-06-03 05:41:20'),
(6, 'Golden', 'category/jZ80gmdEp90GOHVsN3IsLjWoT1ScPL9vzxMIlaeH.jpg', '2021-06-03 05:42:14', '2021-06-03 05:42:14'),
(7, 'aaa', 'category/EN7yFhdjPJWfAhkZwDMw6ElU44wqFHERGn04C2lX.jpg', '2021-06-03 06:44:28', '2021-06-03 06:44:28');

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--

CREATE TABLE `chats` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chats`
--

INSERT INTO `chats` (`id`, `sender_id`, `receiver_id`, `product_id`, `message`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 0, 0, 'eyJpdiI6IjF4dlIyZ3dOcXkrWEtxb0tlZHJudEE9PSIsInZhbHVlIjoiTHU5OUZsNy9NU1lnTThyVmY2Qk9wdz09IiwibWFjIjoiMjMzZWE0ZDkxOTljNTVkNmQ3YjMzMmY3OWYyMmRlMjhiNWFkMWYxMjkwMDQ0MjliNGUwOGJlZjVkYWIwMDUyMSJ9', 0, '2021-06-09 07:38:51', '2021-06-09 12:23:20'),
(2, 13, 1, 7, '7', 1, '2021-06-09 12:24:43', '2021-06-09 12:24:43'),
(3, 13, 1, 11, '11', 1, '2021-06-09 12:25:42', '2021-06-09 12:25:42'),
(4, 13, 11, 12, '12', 1, '2021-06-09 13:19:17', '2021-06-09 13:19:17'),
(5, 13, 11, 12, 'asdasd', 1, '2021-06-09 15:29:31', '2021-06-09 15:29:31'),
(6, 13, 11, 12, 'asdasd', 1, '2021-06-09 15:29:34', '2021-06-09 15:29:34'),
(7, 13, 11, 12, 'asdasd', 1, '2021-06-09 15:29:34', '2021-06-09 15:29:34'),
(8, 13, 11, 12, 'asdasd', 1, '2021-06-09 15:29:40', '2021-06-09 15:29:40'),
(9, 13, 11, 12, 'asdasd', 1, '2021-06-09 15:29:40', '2021-06-09 15:29:40'),
(10, 13, 11, 12, 'asdasd', 1, '2021-06-09 15:30:16', '2021-06-09 15:30:16'),
(11, 13, 11, 12, 'asdasd', 1, '2021-06-09 15:36:26', '2021-06-09 15:36:26'),
(12, 13, 11, 12, 'asdasd', 1, '2021-06-09 15:46:44', '2021-06-09 15:46:44'),
(13, 13, 11, 12, 'asdasd', 1, '2021-06-09 15:47:17', '2021-06-09 15:47:17'),
(14, 13, 11, 12, 'asdasd', 1, '2021-06-09 15:50:04', '2021-06-09 15:50:04'),
(15, 13, 11, 12, 'asdasd', 1, '2021-06-09 15:50:04', '2021-06-09 15:50:04'),
(16, 11, 11, 12, 'qqqq', 1, '2021-06-09 15:52:44', '2021-06-09 15:52:44'),
(17, 11, 11, 12, 'qqqq', 1, '2021-06-09 15:52:47', '2021-06-09 15:52:47'),
(18, 11, 11, 12, 'asd', 1, '2021-06-09 15:56:23', '2021-06-09 15:56:23'),
(19, 11, 11, 12, 'asdasd', 1, '2021-06-09 15:56:53', '2021-06-09 15:56:53'),
(20, 11, 11, 12, 'asdasd', 1, '2021-06-09 15:57:06', '2021-06-09 15:57:06'),
(21, 11, 1, 7, 'asdasd', 1, '2021-06-09 16:27:26', '2021-06-09 16:27:26'),
(22, 13, 11, 12, 'asdasd', 1, '2021-06-09 16:28:39', '2021-06-09 16:28:39'),
(23, 11, 11, 12, 'asdasd', 1, '2021-06-09 16:28:39', '2021-06-09 16:28:39'),
(24, 13, 11, 12, 'asdasd', 1, '2021-06-09 16:28:45', '2021-06-09 16:28:45'),
(25, 11, 11, 12, 'asdasd', 1, '2021-06-09 16:28:45', '2021-06-09 16:28:45'),
(26, 13, 11, 12, 'asdasd', 1, '2021-06-09 16:33:00', '2021-06-09 16:33:00'),
(27, 13, 11, 12, 'asdasd', 1, '2021-06-09 16:33:00', '2021-06-09 16:33:00'),
(28, 11, 11, 12, 'asdasd', 1, '2021-06-09 16:33:00', '2021-06-09 16:33:00'),
(29, 11, 11, 12, 'asdasd', 1, '2021-06-09 16:33:00', '2021-06-09 16:33:00'),
(30, 13, 11, 12, 'asdasd', 1, '2021-06-09 16:33:19', '2021-06-09 16:33:19'),
(31, 11, 11, 12, 'asdasd', 1, '2021-06-09 16:33:19', '2021-06-09 16:33:19'),
(32, 11, 1, 7, 'asdasd', 1, '2021-06-09 16:34:24', '2021-06-09 16:34:24'),
(33, 13, 1, 7, 'asdasd', 1, '2021-06-09 16:34:24', '2021-06-09 16:34:24');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2021_06_02_120120_create_categories_table', 2),
(8, '2021_06_02_121003_create_sub_categories_table', 3),
(21, '2021_06_02_113600_create_products_table', 4),
(22, '2021_06_03_121915_create_tradings_table', 4),
(24, '2021_06_04_101530_create_auction_products_table', 5),
(26, '2021_06_04_104102_create_seller_products_table', 6),
(27, '2021_06_04_131819_create_orders_table', 7),
(29, '2021_06_07_111725_product_image', 8),
(30, '2021_06_09_115302_create_chats_table', 9);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `length` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Pending','Approved','Canceled') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending',
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `currency_type`, `category_id`, `sub_category_id`, `length`, `height`, `width`, `quantity`, `description`, `policy`, `status`, `user_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Miranda Rasmussen', '3', 'US Dollor- USD($)', 1, 1, 9, 6, 80, NULL, 'Dolores ullamco non', 'Rerum cumque eum mol', 'Pending', 1, NULL, '2021-06-03 16:30:55', '2021-06-03 16:30:55'),
(2, 'Sybil Hayden', '352', 'US Dollor- USD($)', 3, 1, 7, 89, 28, NULL, 'Ut perspiciatis tem', 'Harum sed laborum mo', 'Pending', 1, NULL, '2021-06-03 16:38:34', '2021-06-03 16:38:34'),
(3, 'Dorothy Farley', '315', 'US Dollor- USD($)', 3, 1, 36, 87, 26, 12, 'Est commodo qui sint', 'Quia consequatur ex', 'Pending', 1, NULL, '2021-06-03 16:44:38', '2021-06-03 16:44:38'),
(4, 'Rae Farley', '931', 'US Dollor- USD($)', 4, 1, 70, 65, 98, NULL, 'Voluptas ea dolores', 'Nostrum similique ad', 'Pending', 1, NULL, '2021-06-04 06:16:23', '2021-06-04 06:16:23'),
(7, 'Yolanda Wade', '578', 'US Dollor- USD($)', 3, 1, 92, 39, 2, 12, 'Esse ut rem culpa si', 'Magni sit corrupti', 'Pending', 1, NULL, '2021-06-04 06:22:30', '2021-06-04 06:22:30'),
(10, 'Arden Key', '722', 'US Dollor- USD($)', 7, 1, 94, 17, 82, NULL, 'Et labore sit qui mo', 'Ratione aut ut nemo', 'Approved', 1, NULL, '2021-06-07 12:41:13', '2021-06-08 09:12:15'),
(11, 'aaaccccaa', '11', 'US Dollor- USD($)', 1, 1, 1, 11, 11, NULL, 'asd', 'asd', 'Approved', 1, NULL, '2021-06-08 06:43:47', '2021-06-08 09:12:24'),
(12, 'Yasir Morin', '87', 'US Dollor- USD($)', 5, 1, 26, 34, 62, NULL, 'Culpa libero est re', 'Perferendis ab sunt', 'Approved', 11, NULL, '2021-06-08 07:41:03', '2021-06-08 09:12:46'),
(13, 'Colin Hill', '405', 'US Dollor- USD($)', 7, 1, 55, 61, 17, NULL, 'Vero pariatur Quia', 'Lorem fugiat incidi', 'Approved', 11, NULL, '2021-06-08 07:42:29', '2021-06-08 09:12:42'),
(14, 'Sharon Alvarez', '874', 'US Dollor- USD($)', 2, 1, 9, 30, 75, NULL, 'Excepteur adipisci v', 'Anim obcaecati sunt', 'Approved', 11, NULL, '2021-06-08 08:56:00', '2021-06-08 13:04:43'),
(15, 'Buffy Randall', '533', 'US Dollor- USD($)', 4, 1, 30, 77, 85, NULL, 'Maxime aliquip irure', 'Voluptate molestias', 'Canceled', 11, NULL, '2021-06-08 13:03:46', '2021-06-09 13:18:18'),
(16, 'asdasd', '223', 'US Dollor- USD($)', 2, 1, 123, 123, 123, NULL, 'asd', 'asd', 'Pending', 1, NULL, '2021-06-09 13:17:45', '2021-06-09 13:17:45');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `image`) VALUES
(1, 10, 'product/OZ5kK92ciMyKPwIxo0DkC0KSj97ApuFxn77nmjUe.jpg'),
(2, 10, 'product/OZ5kK92ciMyKPwIxo0DkC0KSj97ApuFxn77nmjUe.jpg'),
(3, 10, 'product/OZ5kK92ciMyKPwIxo0DkC0KSj97ApuFxn77nmjUe.jpg'),
(4, 10, 'product/OZ5kK92ciMyKPwIxo0DkC0KSj97ApuFxn77nmjUe.jpg'),
(5, 10, 'product/OZ5kK92ciMyKPwIxo0DkC0KSj97ApuFxn77nmjUe.jpg'),
(6, 12, 'product/wfZP1OGddYiIF9JLajnMph1gpyJ5s0aJ3yvU2EDr.jpg'),
(7, 12, 'product/wfZP1OGddYiIF9JLajnMph1gpyJ5s0aJ3yvU2EDr.jpg'),
(8, 13, 'product/iY83FR1HwRW0zgTL7brTaH361fvDuEke1QQrch2V.jpg'),
(9, 13, 'product/iY83FR1HwRW0zgTL7brTaH361fvDuEke1QQrch2V.jpg'),
(10, 13, 'product/iY83FR1HwRW0zgTL7brTaH361fvDuEke1QQrch2V.jpg'),
(11, 13, 'product/iY83FR1HwRW0zgTL7brTaH361fvDuEke1QQrch2V.jpg'),
(12, 13, 'product/iY83FR1HwRW0zgTL7brTaH361fvDuEke1QQrch2V.jpg'),
(13, 13, 'product/iY83FR1HwRW0zgTL7brTaH361fvDuEke1QQrch2V.jpg'),
(14, 14, 'product/NpWgzwf6bmQ2qh2s8lp0ZJZJ8rnY8Zhzlr6w8pJb.jpg'),
(15, 15, 'product/DwTh3r4vQ3fDDBYP88ZFOqxnb7xYrgaTOzP5DVPc.jpg'),
(16, 15, 'product/DwTh3r4vQ3fDDBYP88ZFOqxnb7xYrgaTOzP5DVPc.jpg'),
(17, 15, 'product/DwTh3r4vQ3fDDBYP88ZFOqxnb7xYrgaTOzP5DVPc.jpg'),
(18, 15, 'product/DwTh3r4vQ3fDDBYP88ZFOqxnb7xYrgaTOzP5DVPc.jpg'),
(19, 16, 'product/Cq3vp0kBGDyNJoiMR8ztiRvgKOlTyLoaAqbnXKj0.jpg'),
(20, 16, 'product/Cq3vp0kBGDyNJoiMR8ztiRvgKOlTyLoaAqbnXKj0.jpg'),
(21, 16, 'product/Cq3vp0kBGDyNJoiMR8ztiRvgKOlTyLoaAqbnXKj0.jpg'),
(22, 16, 'product/Cq3vp0kBGDyNJoiMR8ztiRvgKOlTyLoaAqbnXKj0.jpg'),
(23, 16, 'product/Cq3vp0kBGDyNJoiMR8ztiRvgKOlTyLoaAqbnXKj0.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `seller_products`
--

CREATE TABLE `seller_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `seller_products`
--

INSERT INTO `seller_products` (`id`, `product_id`, `brand`, `start_date`, `end_date`, `start_time`, `end_time`) VALUES
(1, '7', 'Dolorem ducimus opt', '06/04/2021', '06/04/2021', '12:00:00 AM', '12:00:00 AM'),
(4, '10', 'Quisquam enim possim', '06/07/2021', '06/07/2021', '12:00:00 PM', '12:00:00 AM'),
(5, '11', '11', '06/22/2021', '06/08/2021', '4:43:10 PM', '12:00:00 AM'),
(6, '12', 'Soluta quis quis id', '06/17/2021', '06/08/2021', '5:40:37 PM', '12:00:00 AM'),
(7, '13', 'Qui dolorem ea susci', '06/08/2021', '06/08/2021', '5:41:55 PM', '12:00:00 AM'),
(8, '14', 'Et ipsum ipsum cum', '06/08/2021', '06/08/2021', '12:00:00 AM', '12:00:00 AM'),
(9, '15', 'Voluptas maiores rer', '06/25/2021', '06/23/2021', '12:00:00 AM', '12:00:00 AM'),
(10, '16', 'asda', '06/09/2021', '06/09/2021', '11:16:29 PM', '12:00:00 AM');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `name`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'Golden', 4, '2021-06-03 10:40:44', '2021-06-03 10:40:44');

-- --------------------------------------------------------

--
-- Table structure for table `trading_products`
--

CREATE TABLE `trading_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `trading_products`
--

INSERT INTO `trading_products` (`id`, `product_id`, `brand`, `start_date`, `end_date`, `start_time`, `end_time`) VALUES
(1, '1', 'Nihil eaque doloribu', NULL, NULL, NULL, NULL),
(2, '2', 'Aliquid voluptates q', '06/04/2021', '06/04/2021', '2:38:16 AM', '12:00:00 AM'),
(3, '3', 'Corrupti velit dol', '06/04/2021', '06/04/2021', '12:00:00 AM', '12:00:00 AM');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_type` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Pending','Approved') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `user_type`, `status`) VALUES
(1, 'Admin ', 'admin@watchagot.com', NULL, '$2y$10$v.2Y6PfvLqCJPscqeCM5WOuXt9VkmKK6eEWVx8K2ohEAsxq.FBXry', NULL, NULL, NULL, 'Admin', 'Approved'),
(4, 'seller2', 'seller3@water.com', NULL, '$2y$10$PV73HLq5jeow3cB2oCKWW.gGIEOoKCqF.BSkr/MuwBsl7cFuyK9bC', NULL, '2021-06-04 09:59:15', '2021-06-04 09:59:15', 'Seller', 'Pending'),
(5, 'Levi Hahn', 'rugymiricu@mailinator.com', NULL, '$2y$10$jEkqdC/ZHfE8FRvpQRGy3.OrXhRzXdtKaQm7qu6XOmUyPp80wg1ku', NULL, '2021-06-04 16:43:40', '2021-06-04 16:43:40', 'Seller', 'Pending'),
(6, 'Roanna Higgins', 'donusucolu@mailinator.com', NULL, '$2y$10$bAwCrcpUsjFQbRjSAQ2ua.NlAv9BWLg/d6.cHkZgAwodk3bMqeA8y', NULL, '2021-06-04 16:48:37', '2021-06-04 16:48:37', 'Seller', 'Pending'),
(7, 'Kyle Bowen', 'bezyxedyqu@mailinator.com', NULL, '$2y$10$jVabg3FKdPEmn5APJTZh6O18Bm8NE3FFFk75NZA.HDnIhxCUzoy/S', NULL, '2021-06-04 16:50:24', '2021-06-04 16:50:24', 'Seller', 'Pending'),
(8, 'Jameson Parks', 'munovy@mailinator.com', NULL, '$2y$10$r1F0DyYGhPRzH3Dnej8t3.wY..N3wx7QlOLSTOF4H.DJpX/EaQDrS', NULL, '2021-06-04 16:50:48', '2021-06-04 16:50:48', 'Seller', 'Pending'),
(9, 'Barclay Mckenzie', 'lelysyce@mailinator.com', NULL, '$2y$10$OyUmzslTMwIYKQVG2sUS1eOkqB3S7BwEWzn92fzwwv1Jg7tg3fAGC', NULL, '2021-06-04 16:51:04', '2021-06-04 16:51:04', 'Seller', 'Pending'),
(10, 'buyer1', 'buyer1@gmail.com', NULL, '$2y$10$QljkY2Y4xUACiiU/8FkBM.Dm7nDJAqHUxP0JvOgqeXPWOXSvq10Py', NULL, '2021-06-08 04:05:56', '2021-06-08 04:05:56', 'Buyer', 'Approved'),
(11, 'sellerteest1', 'sellertest1@gmail.com', NULL, '$2y$10$Kugz/MQxGvAb4Zgl9s8souT3nP2rAtiT4onxQNa8e5hTmpodcOf8e', NULL, '2021-06-08 04:14:40', '2021-06-08 04:14:40', 'Seller', 'Approved'),
(12, 'abc', 'buyertest1@gmail.com', NULL, '$2y$10$R5Csfal9/DxLvnusKvfD8Oqi7P3E/EZmZpWCtEaKTtnnDBVuPj.HS', NULL, '2021-06-09 06:33:40', '2021-06-09 06:33:40', 'Buyer', 'Approved'),
(13, 'abc', 'buyertest2@gmail.com', NULL, '$2y$10$Kugz/MQxGvAb4Zgl9s8souT3nP2rAtiT4onxQNa8e5hTmpodcOf8e', NULL, '2021-06-09 06:34:31', '2021-06-09 06:34:31', 'Buyer', 'Approved');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auction_products`
--
ALTER TABLE `auction_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_user_id_foreign` (`user_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seller_products`
--
ALTER TABLE `seller_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trading_products`
--
ALTER TABLE `trading_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auction_products`
--
ALTER TABLE `auction_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `chats`
--
ALTER TABLE `chats`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `seller_products`
--
ALTER TABLE `seller_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `trading_products`
--
ALTER TABLE `trading_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
