 @extends('layouts.app2')

@section('content')
<div class="full-width-banner about-us-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Cart</h2>
            </div>

        </div>
    </div>

</div>




    <!--Cart Page Start here -->
    <section class="cartForm">
        <div class="container pt-2">

            @if($errors->any())
                <div class="alert alert-success" role="alert">
                    {{$errors->first()}}
                </div>
            @endif
            <table id="cart" class="table table-hover table-condensed">
                <thead>
                <tr>
                    <th style="width:50%">Product</th>
                    <th style="width:10%">Price</th>
                    <th style="width:10%">Color</th>
                    <th style="width:10%">Size</th>
                    <th style="width:15%">Quantity</th>
                    <th style="width:22%" >Subtotal</th>
                    <th style="width:10%"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($checkout as $key)
{{--                    {{$key->products_images->image}}--}}
                    <tr class="borderRow">
                        <td data-th="Product">
                            <div class="row">
                                <div class="col-sm-2 hidden-xs ">
                                    <img style="width: 100%;"
                                         src="{{ isset($key->products_images->image) ? asset('/storage/'.$key->products_images->image): asset('/storage/product/default.jpg')}}"

                                         alt="..."
                                         class="img-responsive"/></div>
                                <div class="col-sm-10">
                                    <h4 class="nomargin">{{$key->products->name}}</h4>
                                </div>
                            </div>
                        </td>

                        @if($key->products->flags !='trade')
                            <td data-th="Price">${{$key->price}}</td>
                        @else
                            <td data-th="">${{$key->price}}</td>
                        @endif
                            <td data-th="">{{$key->color ?? 'NONE'}}</td>
                            <td data-th="">{{$key->size ?? 'NONE'}}</td>



                            @if($key->products->flags =='trade')
                            <td data-th="Price">Trade</td>
                           @elseif($key->products->flags === 'request-trade')
                           <td data-th="Price">Request Trade</td>
                           @else
                           <td data-th="Quantity my-quantity-btn">
                            <div class="input-group">
                <span class="input-group-btn my-quantity-btn1">
                    <button type="button" class="quantity-left-minus btn btn-danger btn-number"
                            onclick="min_qty({{$key->id}},{{$key->product_id}},{{$key->price}})" data-type="minus"
                            data-field="">
                    -
                    </button>
                </span>
                                <input type="number" id="quantity{{$key->id}}" name="quantity"
                                        onchange="number_change({{$key->id}},{{$key->product_id}},{{$key->price}})"
                                        class="form-control input-number"
                                        value="{{$key->qty}}" min="1" max="100">
                                <span class="input-group-btn my-quantity-btn2">
                    <button type="button" class="quantity-right-plus btn btn-success btn-number"
                            onclick="plus_qty({{$key->id}},{{$key->product_id}},{{$key->price}})" data-type="plus"
                            data-field="">
                        +
                    </button>
                </span>
                            </div>
                        </td>
                            @endif


                            @if($key->products->flags == 'trade' || $key->products->flags == 'request-trade')
                                <td data-th="Subtotal">
                                    <div class="seconds" id="qtyprice_total{{$key->id}}">{{($key->price)}}</div>

                                </td>
                            @else
                            {{-- <td data-th="Subtotal" class="seconds">
                            {{$key->products->price*0.05}}</td>
                            </td> --}}
                            <td data-th="Subtotal">
                                <div class="seconds" id="qtyprice_total{{$key->id}}">{{($key->qty*$key->price)}}</div>

                            </td>
                            @endif

                            @if($key->products->flags == 'trade' || $key->products->flags == 'request-trade')
                            <td class="actions" data-th="">
                               <p></p>
                            </td>
                            @else
                            <td class="actions" data-th="">
                                <a href="{{route('checkout_delete',$key->id)}}">
                                    <button class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i>
                                    </button>
                                </a>
                            </td>
                            @endif
                    </tr>
                @endforeach

                </tbody>
                <tfoot>
                <tr>
                    <td style="padding-left: 0px;"><a href="{{route('index')}}" class="btn  cart-btn"><i
                                    class="fa fa-angle-left"></i>
                            Continue Shopping</a></td>
                    <td colspan="2" class="hidden-xs"></td>
                    <td class="hidden-xs "><strong>Total $<span id="subtotalcart">{{ $checkout_total }}</span></strong></td>
                    <td style="padding-right: 0px;"><a href="{{route('checkout')}}" class="btn  btn-block cart-btn">Checkout
                            <i
                                    class="fa fa-angle-right"></i></a></td>
                </tr>
                </tfoot>
            </table>
            {{ $checkout->withQueryString()->links() }}

        </div>
    </section>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script>


        function min_qty($id, $product_id, price) {

            var sum = 0;
            var qty = $("#quantity" + $id).val();
            if (qty > 1) {
                qty = parseInt(qty) - 1;
                $("#quantity" + $id).val(qty);
                var total = parseInt(price) * parseInt(qty)
                $("#qtyprice_total" + $id).text(total);
                $.ajax({
                    url: "{{url('/checkout_ajax_qty')}}",
                    type: "GET",
                    data: {id: $id, product_id: $product_id, qty: qty},
                    success: function (response) {
                        // alert("dollor");
                    }
                });

                $('.seconds').each(function() {
					sum += parseInt($(this).text());
				});
               $('#subtotalcart').html(sum)

            } else {
                $.ajax({
                    url: "{{url('/checkout_ajax_qty')}}",
                    type: "GET",
                    data: {id: $id, product_id: $product_id, qty: 1},
                    success: function (response) {
                        // alert("dollor");
                    }
                });
                swal({
                    title: "Warning!",
                    text: "You cannot decrease more or negative value minimum value will be 1",
                    type: "danger",
                });
                $("#quantity" + $id).val(1);




            $('.seconds').each(function() {
					sum += parseInt($(this).text());

				});
               $('#subtotalcart').html(sum)
            }
        };

        function number_change($id, $product_id, $price) {
            // alert('number_change');
            var sum = 0;
            var qty = $("#quantity" + $id).val();

            if (qty > 1) {
                var total = parseInt($price) * parseInt(qty);
                $("#qtyprice_total" + $id).text(total);

                $.ajax({
                    url: "{{url('/checkout_ajax_qty')}}",
                    type: "GET",
                    data: {id: $id, product_id: $product_id, qty: qty},
                    success: function (response) {
                        // alert("dollor");


                    }

                });
            } else {
                $.ajax({
                    url: "{{url('/checkout_ajax_qty')}}",
                    type: "GET",
                    data: {id: $id, product_id: $product_id, qty: 1},
                    success: function (response) {
                        // alert("dollor");


                    }
                });
                swal({
                    title: "Warning!",
                    text: "You cannot decrease more or negative value minimum value will be 1",
                    type: "danger",
                    // timer: 1000
                });
                var total = parseInt($price) * 1;
                $("#qtyprice_total" + $id).text(total);
                $("#quantity" + $id).val(1);

            }

            $('.seconds').each(function() {
					sum += parseInt($(this).text());

				});
               $('#subtotalcart').html(sum)

        };

        function plus_qty($id, $product_id, $price) {
            // alert('plus_qty');
            var sum = 0;
            var qty = $("#quantity" + $id).val();
            qty = (parseInt(qty) + 1);
            var total = parseInt($price) * parseInt(qty);
            $("#qtyprice_total" + $id).text(total);

            $("#quantity" + $id).val(qty);


            $.ajax({
                url: "{{url('/checkout_ajax_qty')}}",
                type: "GET",
                data: {id: $id, product_id: $product_id, qty: qty},
                success: function (response) {
                    // alert("dollor");


                }
            });

            $('.seconds').each(function() {
					sum += parseInt($(this).text());

				});
               $('#subtotalcart').html(sum)
        }


    </script>
    <!--Cart Page End here-->
@stop




