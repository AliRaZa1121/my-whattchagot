@extends('layouts.app2')

@section('content')
    @include('layout.nav_header.nav_slider_bar.nav_slider_bar')

    <!-- Full With banner -->
    <div class="full-width-banner about-us-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>About Us</h2>
                </div>

            </div>
        </div>

    </div>


    <!-- Full With Banner End here -->

    <!-- Who We Are Section Start here-->
    <div class="who-we-are-about">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="who-we-inner">
                        <div class="who-we-are-title">
                            <h2>Who we are</h2>
                            <p>
                                WhattchaGot.com is an online marketplace that facilitates transactions between sellers and
                                buyers securely and efficiently. We provide a platform to our customers to trade, auction,
                                and buy products without a second thought. Our foremost goal is to establish a safe place
                                where anyone can buy, sell, trade, and auction their goods.
                                <br>
                                WhattchaGot is open for all types of goods, from electronic items to handicrafts and other
                                vintage treasures. As a company, we struggle to lead with our guiding principles and help
                                spread ideas of responsibility and sustainability so that the impact can reach far beyond
                                our business.
                            </p>
                        </div>
                        <div class="who-we-are-bottom-img">
                            <img src="./images/about-title-img.jpg">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="who-we-inner who-we-inner-right">
                        <div class="who-we-are-title">
                            <p>Our platform connects sellers and traders with millions of potential buyers worldwide. We make our business a vibrant community of people connecting to trade goods.
                                <br>
                                With low cost, powerful tools, a friendly user interface, and support, we help entrepreneurs
                                start, manage, and scale their businesses through auction and trading. We also assist
                                independent creators in selling their products with peace of mind.
                            </p>
                        </div>
                        <div class="who-we-are-bottom-img">
                            <img src="./images/about-title-img2.jpg">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row about-quote-row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <div class="about-quote-div">
                        <p style="font-size:18px">
                            “WattchaGot is a community that pushes small businesses, people, and planet towards positive
                            change with trading and auction of goods!”
                        </p>
                        <img src="./images/quote-img.png">
                    </div>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
    </div>

    <!-- Who we are Section end here-->

    <!-- About Us Mission Section Start here -->
    <div class="about-mission-row">
        <div class="container">
            <div class="row">
                <div class="col-md-6 about-mission-col-left">
                    <div class="about-mission-left">
                        <img src="./images/about-mission.jpg">
                    </div>
                </div>
                <div class="col-md-6 about-mission-col-right">
                    <div class="about-mission-right">
                        <h2>Mission Statement</h2>
                        <p>Our mission is to keep the connection between seller and buyer at the heart of E-commerce. That’s
                            why we have built this platform where trading and auction thrive with customers.
                            We help our
                            community of sellers and buyers by connecting them to grow their business with little to no
                            barrier. We aim to create a safe community where our customers can trade, sell, buy, and auction
                            safely under one platform. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ABout Us mission Section end here-->




    <!-- Our Brands Section Start Here -->
    <!--trade Products Row Start here-->
    <div class="Our-brands-wrapper client-stories-row" id="client-stories">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="trending_products_wrapper">
                        <div class="tr_p_head">
                            <h3>Client Stories</h3>
                        </div>
                        @if(count($testimonials) > 0)


                        <div class="trending_products_slider owl-carousel owl-theme">
                             @foreach($testimonials as $t )
                            <div class="item">

                                <div class="client-stories-wrapper">
                                    <div class="client-thumbnail">
                                        <a data-remodal-target="bidding-vid" href="javascript:void(0)"
                                            class="lang-an">
                                            <img src="{{ asset('/storage/'.$t->banner_image ?? './images/client-stories.jpg') }}">
                                        </a>
                                    </div>
                                    <div class="client-stories-txt">
                                        <img src="./images/light-blue-shape.png">
                                        <img class="dark-shape" src="./images/dark-blue-shape.png">
                                        <p>“{{ $t->description }}”

                                        </p>
                                        <h2>{{ $t->name }}</h2>
                                    </div>
                                </div>
                            </div>


                                @endforeach

                            <!--item-->
                        </div>
                        @else
                        <h3 class="text-center">No Data Found</h3>
                        @endif
                    </div>

                </div>



            </div>
        </div>
    </div>

    <!--trade Products Row End here-->

    <!-- Our Brands Section end here -->

    @if(count($products_trade) > 0)

    <!-- About Us Last Slider Start here -->
    <div class="about-last-slider-wrapper">
        <div class="container">
            <div class="row">
                <div class="auction_slider">

                    <div class="auction_slider_wrapper owl-carousel owl-theme">
                        @foreach ($products_trade as $val)
                        <div class="item">
                            <div class="slider-side-image">
                                <img src="{{ isset($val->product_images->image) ? asset('/storage/' . $val->product_images->image) : asset('images/gaming-pc.png') }}">
                            </div>
                            <div class="gammming-pc-right-txt">
                                <h3>Trade Offer</h3>

                                <h2>Find<span class="gamming-span">Best Trading Products</span></h2>
                                <p>This is dummy copy. It is not meant to be read. It has been placed here solely to
                                    demonstrate the look and feel.</p>
                                <a href="{{ route('trading-single',$val->id) }}">Shop Now</a>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif


    <!-- ABout Us Last Slider End here -->





    <script>
        jQuery("#client-stories .trending_products_slider").owlCarousel({
            loop: false,
            items: 3,
            margin: 10,
            dots: false,
            nav: true,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            autoplay: true,
            autoplayTimeout: 3000,
            // responsiveClass: true,
            responsive: {
            0: {
                items: 1,
                autoHeight: true,
                nav: true
            },
            768: {
                items: 2,
                autoHeight: true,
                nav: false
            },
            1000: {
                items: 3,
                nav: true,
                autoHeight: true
            }
        }
        });

        jQuery(".about-last-slider-wrapper .auction_slider_wrapper").owlCarousel({
            loop: true,
            items: 1,
            margin: 10,
            dots: true,
            nav: false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            autoplay: false,
            autoplayTimeout: 3000,
            responsiveClass: true,
        });
    </script>
@stop
