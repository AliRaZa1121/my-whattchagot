@extends('layouts.app2')

@section('content')
@include('layout.nav_header.nav_slider_bar.nav_slider_bar')

<!-- Full With banner -->
<div class="full-width-banner about-us-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Bidding & Buying Help
                </h2>
            </div>

        </div>
    </div>

</div>


<!-- Full With Banner End here -->

<div class="priuvacy-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center">Bidding & Buying Help</h2>
                <p class="text-center"><strong>Whattchagot</strong> strives to give its users reliable and comfortable bidding and
                    purchasing
                    experience. We take every precaution to protect the buyer's and bidder's safety. Check out our
                    purchasing and bidding procedure for further assistance.</p>

                <h2>Bidding Process</h2>
                <p>If you wish to make your bidding and buying process effortless, Whattchagot presents you with a
                    five-step simple method to guide your journey. </p>
                </p>

                <strong>Step 1: Create An Account With Whattchagot</strong>
                <ul>
                    <li>Create an account and log in with your Whattchagot credentials. </li>
                </ul>

                <strong>Step 2: Classify The Kinds Of Products You Wish To Place Bids On</strong>
                <ul>
                    <li>Go to our bidding section and select the kinds of goods/items/products/ you wish you place a bid
                        on.</li>
                </ul>

                <strong>Step 3: Place Bids On Whattchagot Trusted Suppliers And Businesses</strong>
                <ul>
                    <li>Once shortlisted, you can now start bidding on your preferred items and products. </li>
                </ul>

                <strong>Step 4: Check All Possible Requirements</strong>
                <ul>
                    <li>See if you are the first to bid on that item or if that item already has a current offer. </li>
                </ul>

                <strong>Step 5: Wait For The Bid To Get Approved</strong>
                <ul>
                    <li>After placing your bid, wait for it to get approved from the seller's end.</li>
                </ul>

                <strong>Step 6: Congratulations You've Won The Bid</strong>
                <ul>
                    <li>Once you win the bid, you can now proceed to buy the product. </li>
                </ul>

            </div>
        </div>
    </div>
</div>



@stop
