@extends('layouts.app2')

@section('content')
    @include('layout.nav_header.nav_slider_bar.nav_slider_bar')

    <!-- Full With banner -->
    <div class="full-width-banner about-us-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Whattchagot Local

                    </h2>
                </div>

            </div>
        </div>

    </div>


    <!-- Full With Banner End here -->

    <div class="priuvacy-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center">Whattchagot Local</h2>
                    <br>
                    <br>
                    <h4 class="text-center">*We Connect Local Traders to Local Buyers*</h4>

                    <p class="text-center">At Wattchagot, we help you connect with local buyers and traders under one
                        platform with little to no
                        barrier of background, size, or geographic location. You can find and purchase every item from
                        electronic goods to handicrafts and other vintage treasures locally.
                        <br>
                        <br>
                        Take a look at our local electronic auction or variety of products promotions, and you may find
                        something you have been looking for a long time!
                        <a href="{{ route('products') }}?type=list" class="shop-now-btn"> Click Here</a>
                    </p>



                </div>
            </div>

            <div class="row">
                <div class="col-md-4 local-col">
                    <div class="who-we-are-title local-page-title-content">
                        <img src="{{ asset('images/brandedwatchauctions.jpg') }}" alt="">

                        <h3>Branded Watch Auctions</h3>
                        <p>
                            Find luxurious watch brands such as Omega, Rolex, Piguet, and many more from your local traders.
                        </p>
                    </div>
                </div>
                <div class="col-md-4 local-col">
                    <div class="who-we-are-title local-page-title-content">
                        <img src="{{ asset('images/electron.jpg') }}" alt="">

                        <h3>Luxurious Electronic Auctions</h3>
                        <p>
                            Extravaganza electronic auction where you will find everything from laptops and mobile phones to
                            home appliances.
                        </p>
                    </div>
                </div>
                <div class="col-md-4 local-col">
                    <div class="who-we-are-title local-page-title-content">
                        <img height="" src="{{ asset('images/designer.jpg') }}" alt="">

                        <h3>Designer Bags Auctions</h3>
                        <p>
                            Connect with local traders offering high-end designer bags such as Gucci, Chanel, Herms, etc.
                        </p>
                    </div>
                </div>

            </div>

            <div class="row">
            <div class="col-md-12">
                <h4 class="text-center">Helping local Buyers and Suppliers Connect Under One Platform!</h4>
            </div>
        </div>


        </div>
    </div>



@stop
