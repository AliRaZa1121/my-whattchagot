@extends('layouts.app2')

@section('content')
@include('layout.nav_header.nav_slider_bar.nav_slider_bar')

<!-- Full With banner -->
<div class="full-width-banner about-us-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Terms & Conditions
                </h2>
            </div>

        </div>
    </div>

</div>


<!-- Full With Banner End here -->

<div class="priuvacy-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <h2 class="text-center">WHATTCHAGOT – TERMS AND CONDITIONS</h2>
                <p class="text-center">Welcome to <strong>WHATTCHAGOT</strong> , an online platform that facilitates
                    its customers to trade, auction, and buy products securely and efficiently. </p>

                <p>The active Terms and Conditions of Use <strong>("Terms")</strong> are between You (the
                    "<strong>Customer</strong>") and <strong>WHATTCHAGOT</strong>
                    ("Company," "we" or "us") and aims to govern your access and use of
                    <strong>http:/www.whattchagot.com/</strong> (the
                    "<strong>Website</strong>"), whether you use it as a registered user or a guest, or any other way to
                    interact with us
                    through any of our customer channels..
                </p>

                <h4>Consent:</h4>
                <p>Carefully read the presented "Terms and Conditions" before visiting, accessing, or using any of the
                    services given on the website. By accessing/entering this Website, we presume you comply with the
                    terms and conditions stated on this webpage. Kindly stop using <strong>WHATTCHAGOT.COM</strong> if
                    you do not comply
                    with or confess to accepting all user agreements listed on this page.</p>


                <h2>Collecting & Using Information</h2>
                <p>This Website's material is generally accessible or may be used without providing us with any
                    information. Certain services, on the other hand, may need the acquisition of personal information
                    from the user. We utilize the user-provided information to enhance our service and goods,
                    particularly to preserve internal records, improve our services and our Website, notify you of newly
                    added businesses and product listings, provide auction updates and information regarding any
                    discount codes or offers.</p>


                <h4>Login/Registration</h4>
                <p>In order to use our services or purchase and other goods from the available businesses, the user must
                    register/submit personal data on our Website. The required data needed to complete the order
                    includes your name, phone number, home address, postal code, and email address.</p>

                <h4>Termination Of Registration</h4>
                <p><strong>WHATTCHAGOT.COM</strong> retains the right to block or cancel a user registration or seize
                    log-in access at any
                    point in case of infringement or contravention of the organization's
                    rules.</p>


                <h2>DATA PRIVACY</h2>
                <p><strong>WHATTCHAGOT</strong>guarantees the confidentiality of any information given. We never utilize
                    or mishandle your private information, including Personal Data. We do not distribute or trade
                    personal data to other organizations, associations, or people in line with this policy.</p>

                <h4>Security</h4>
                <p><strong>WHATTCHAGOT</strong>places utmost importance on securing the personal
                    information/details/data provided to us. We take all necessary precautions and steps to secure your
                    Personal Information from unlawful access or use. However, we cannot assure that the Private Data
                    provided, transmitted, or disclosed by you (the user) via this Website or by email is entirely
                    secure and protected.</p>



                <h2>Unauthorized Use Of Information </h2>
                <p>You do not have the right to copy, amend, send, publish, distribute, license, sell, create, or
                    transfer any of the information, products, services, software, or sources used on this site without
                    written permission.</p>

                <h2>Third-Party Links</h2>
                <p><strong>WHATTCHAGOT</strong> makes no warranty and accepts no liability or duty for any third-party
                    materials or other
                    materials, products, or services provided by third parties. <strong>WHATTCHAGOT</strong> will not be
                    entertaining any
                    complaints, claims, concerns, or questions regarding third-party products. They should be directed
                    to a third party. </p>


                <h2>PAYMENT POLICY</h2>
                <p>
                    By using our services and buying products from our platform, you allow us to charge your debit or
                    credit card or conduct other medians of payment (such as direct debit, PayPal, or mobile
                    wallet).<br>
                    When you make a purchase, you affirm to use a legitimate or authorized cash method set by
                    <strong>WHATTCHAGOT</strong>. If the required payment method fails and the product still gets
                    delivered, you agree to pay us the corresponding fees within the next <span style="color: red">
                        thirty (30) days.</span><br>
                    <strong>WHATTCHAGOT</strong> maintains the right to remove, cancel, or disable access to any bought
                    goods for which we have not received sufficient payment.
                </p>


                <h2>Order Confirmation And Email Notification</h2>
                <p>When you place an order with <strong>WHATTCHAGOT,</strong> we send an email notification. However,
                    we are not liable for any delays caused by email filters that prevent our communications from
                    reaching you. Please change your spam filter settings so that you may receive emails from our
                    <strong>WHATTCHAGOT,</strong> email addresses.
                </p>




                <h2>Pricing</h2>
                <p><strong>WHATTCHAGOT</strong>makes every effort to price our products competitively. Our pricing
                    varies with the market and the business. We are not required to honor pricing that appears in print
                    or is a mistake. We retain the right to change product pricing as the market dictates.</p>




                <h2>Product Information</h2>
                <p><strong>WHATTCHAGOT</strong> makes every effort to correctly portray the product via photo, pricing,
                    and text. </p>


                <h2>Cookies</h2>
                <p>By accessing or using the <strong>WHATTCHAGOT</strong> website, you agree to use cookies in agreement
                    with <strong>WHATTCHAGOT</strong> Terms & Conditions. <br> Our Website incorporates the use of
                    cookies to facilitate the functionality of certain areas. Cookies may be used by some of the
                    affiliate/advertising members. </p>


                <h2>License</h2>
                <p>Unless stated in writing, <strong>WHATTCHAGOT</strong> and/or its licensors own exclusive
                    intellectual property rights for all services and products, including data or any other form of
                    information on WHATTCHAGOT. All rights are reserved and may not be utilized unless
                    <strong>WHATTCHAGOT</strong>
                    restrictions are followed.<br>
                    You may access this from <strong>WHATTCHAGOT</strong> for your own personal use, subject to
                    restrictions set in these
                    terms and conditions.
                </p>
                <strong>You should not:</strong>
                <ul>
                    {{-- <li>Resell any of the products purchased from <strong>WHATTCHAGOT</strong>;</li> --}}
                    <li>Sub-lease or rent out products from <strong>WHATTCHAGOT</strong>;</li>
                    <li>Replicate, reproduce, or steal products from <strong>WHATTCHAGOT</strong>; and</li>
                    <li>Reallocate products from <strong>WHATTCHAGOT</strong></li>
                </ul>
                <p>The following Agreement shall begin on the date hereunder. </p>

                <h2>Disclaimer</h2>
                <p><strong>WHATTCHAGOT</strong> excludes certain representations, claims, and terms pertaining to our
                    Website, including our website's use to the fullest extent permissible by the law applicable.
                    Nothing in this disclaimer is intended to:
                </p>

                <ul>
                    <li>Restrict or exclude our or your liability for death or personal harm;</li>
                    <li>Limit or eliminate our or your responsibility for fraud or repudiatory breach;</li>
                    <li>Restrict our or your liability in any way that is not authorized by relevant law.; or</li>
                    <li>Exclude any of our or your responsibilities that are not expressly prohibited by applicable law.
                    </li>
                </ul>
                <p>
                    The liability restrictions and prohibitions set out in this Section and elsewhere in this
                    disclaimer: <br> (a) are susceptible to the previous paragraph; and <br>
                    (b) regulate all liabilities emerging
                    underneath the disclaimer, especially liabilities deriving in the contract, tort, and violation of
                    statutory duty.
                    <strong>WHATTCHAGOT</strong> shall not be held responsible for any loss or damage of any kind as
                    long as the Website, its information, and services are offered free of charge.
                </p>

                <h2>Rights To Change Terms</h2>
                <p>
                    <strong>WHATTCHAGOT</strong> reserves the right to change these Terms and Conditions, as well as our
                    Privacy Policy
                    and Disclaimers, at any time. Changes in our Terms and Conditions occur immediately. You will be
                    notified by publishing the updated Terms on our Website.
                </p>

                <p>
                    <strong>WHATTCHAGOT</strong> retains the authority to make decisions to any part of our Website,
                    including these Terms, at any moment. If you proceed to use our Website after we have made changes,
                    you agree to the updated Terms and Conditions.
                </p>

            </div>
        </div>
    </div>
</div>



@stop
