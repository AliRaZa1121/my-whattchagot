@extends('layouts.app2')

@section('content')
    @include('layout.nav_header.nav_slider_bar.nav_slider_bar')

    <!-- Full With banner -->
    <div class="full-width-banner about-us-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Stores


                    </h2>
                </div>

            </div>
        </div>

    </div>


    <!-- Full With Banner End here -->

    <div class="priuvacy-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center">Stores</h2>
                    <p class="text-center">Welcome to Whattchagot stores. With thousands of local and global online store
                        options available, you will definitely find one that suits your current needs. Whatever you are
                        looking for, whether big or small, local or international brand, electronics or fashion accessories,
                        Whattchagot guarantees to connect you to a relevant, trustworthy store. </p>




                    <h2>Local Store</h2>
                    <p>What’s better than shopping local? Whattchagot connects you with thousands of local businesses and
                        suppliers. Our objective is to make selling, buying, and auctioning more convenient and enable
                        anybody to engage with customers through our platform, from a small local company owner to a large
                        local enterprise.</p>
                    </p>

                    <h2>Global Store</h2>
                    <p>International shopping made easy with us. Shop from the best global stores available at Whattchagot.
                        We make worldwide shopping and shipping convenient for everyone. We let you shop like a local at all
                        international stores. With thousands of global stores, businesses, and suppliers available, you can
                        now easily find your everyday essentials and luxury items.</p>

                   <strong>Let’s search and shop from your favorite stores at Whattchagot!</strong>

                </div>
            </div>
        </div>
    </div>



@stop
