@extends('layouts.app2')

@section('content')
    @include('layout.nav_header.nav_slider_bar.nav_slider_bar')

    <!-- Full With banner -->
    <div class="full-width-banner about-us-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>FAQs

                    </h2>
                </div>

            </div>
        </div>

    </div>


    <!-- Full With Banner End here -->

    <div class="priuvacy-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center">FREQUENTLY ASKED QUESTIONS</h2>



                    <strong>Q:1 What benefits does Whattchagot give to its users?</strong>
                    <p><strong>Ans:</strong>
                        At Whattchagot, we help users connect with global and local traders at one platform where you can
                        buy
                        branded items like electronic appliances, luxuries watches, designer bags, and shoes, etc., on
                        auction. You can choose products according to your price range from the list of auction items.
                        Moreover, we only allow certified and authentic traders to place their products on our website for
                        auction to avoid any fraud.</p>


                    <strong>Q:2 How can I contact the Customer Service/Help center?</strong>
                    <p><strong>Ans:</strong>
                        If you have any questions or queries regarding Whattchagot services, you can contact us by clicking
                        the Help Center button in the lower right-hand corner of the Whattchagot page or email us your
                        queries at <a href="mailto:info@whattchagot.com">info@whattchagot.com</a> You will receive the
                        response at your registered email promptly.
                    </p>


                    <strong>Q:3 How do I register on Whattchagot?</strong>
                    <p><strong>Ans:</strong>
                        You need to make an account on Whattchagot to get register. All you have to do is move your cursor
                        on the right-top of our website, and you will find a button named ‘My Account.’ Here you need to
                        enter your details such as name, email, and password for a sign-up.</p>


                    <strong>Q:4 How can I pay on Whattchagot?</strong>
                    <p><strong>Ans:</strong>
                        You can pay for all your purchases with Visa, PayPal, MasterCard, and Discover on Whattchagot. </p>


                    <strong>Q:5 Is Whattchagot give a money-back guarantee on the items I purchased?</strong>
                    <p><strong>Ans:</strong>
                        Whattchagot believes in keeping all terms transparent with its users. Every business available on
                        our platform has its own set of money-back guarantee rules to protect its business from falling prey
                        to online fraud. If you want to acquire information about the money-back policy, </p>

                    <p> <strong>you may reach our Whattchagot customer support team at <a href="emailto:info@whattchagot.com">info@whattchagot.com</a></strong></p>


                </div>
            </div>
        </div>
    </div>



@stop
