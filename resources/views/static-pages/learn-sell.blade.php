@extends('layouts.app2')

@section('content')
    @include('layout.nav_header.nav_slider_bar.nav_slider_bar')

    <!-- Full With banner -->
    <div class="full-width-banner about-us-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Learn To Sell
                    </h2>
                </div>

            </div>
        </div>

    </div>


    <!-- Full With Banner End here -->

    <div class="priuvacy-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center">Learn To Sell</h2>
                    <p class="text-center" >Make profits by learning the basics of selling your items and products with Whattchagot.</p>

                    <h2>Selling Basics</h2>
                    <p>Here’s are some pointers you need to know before you start trading with Whattchagot.</p>


                    <strong>Create A Seller Account</strong>
                    <p>Create a Whattchagot seller account and log in to explore seller account features.</p>

                    <strong>Request Seller protection</strong>
                    <p>Contact our team at <a href="mailto:info@whattchagot.com">info@whattchagot.com</a> Our customer staff team will provide you with the necessary seller policies to keep you safe from online fraud.</p>

                    <strong>List Your Item On Whattchagot</strong>
                    <p>List your items on Whattchagot. A successful listing makes for a successful launch for a store.</p>

                    <strong>Add Product Details</strong>
                    <p>You must supply the necessary information and description about the product/item you are selling.</p>

                    <strong>You Are Ready To Sell</strong>
                    <p>Make your store online and start selling.
                        Once you are done with all these pointers, you are good to enter the Whattchagot marketplace,<br>
                        where we warmly welcome you!
                        </p>



                </div>
            </div>
        </div>
    </div>



@stop
