@extends('layouts.app2')

@section('content')
    @include('layout.nav_header.nav_slider_bar.nav_slider_bar')

    <!-- Full With banner -->
    <div class="full-width-banner about-us-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Money Back Guarantee

                    </h2>
                </div>

            </div>
        </div>

    </div>


    <!-- Full With Banner End here -->

    <div class="priuvacy-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h2 class="text-center">Whattchagot - Money Back Guarantee!</h2>
                    <p class="text-center">Whattchagot believes in keeping all terms transparent with its users. Every
                        business available on our platform has its own set of money-back guarantee rules aimed to protect
                        its business from falling prey to online fraud. </p>



                    <h2>Providing Safe & Secure Global Transaction</h2>
                    <p>Being an online selling, buying, and auctioning platform, Whattchagot takes every possible action to
                        ensure a safe and secure transaction of money and goods. As a platform that connects buyers to
                        sellers, we urge users to read the refund/return policy before getting involved in a purchase. </p>
                    </p>
                    <h2>Your Money Back Process</h2>
                    <p>You either get your order, or you sent a money-back request.
                        Whattchagot simplifies the process for their users. We provide you with an easy 3 step process that
                        you can follow if you wish to undergo the money-back policy. However, we would like to establish
                        that Whattchagot does not limit individual businesses' "Money Back Guarantee" policy.
                    </p>

                    <strong>Step 1: Contact Your Seller</strong>
                    <p>If your ordered product does not arrive or is not as advertised, go to your order history, pick the
                        item, and contact your seller.
                    </p>


                    <strong>Step 2: Issue Not Resolved?</strong>
                    <p>Please contact us if you've spoken with your vendor and the problem hasn't been resolved after three
                        working days. </p>

                    <strong>Step 3: Let Whattchagot Know</strong>
                    <p>If you acquire information about the money-back policy, you may reach our Whattchagot customer
                        support team at <a href="mailto:info@whattchagot.com">info@whattchagot.com</a> We will get back to you in the next 48 hours. </p>
                    <p><strong>Please note </strong>excessive use or misuse of any of our policies can result in various
                     actions. <br> The following actions can include;</p>

                     <ul>
                         <li>Warnings against the buyer</li>
                         <li>Blocking buyers' profiles</li>
                         <li>Blocking buyers from requesting further returns/refunds; and </li>
                         <li>Account suspension.</li>


                     </ul>


                </div>
            </div>
        </div>
    </div>



@stop
