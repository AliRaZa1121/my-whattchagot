@extends('layouts.app2')

@section('content')
    @include('layout.nav_header.nav_slider_bar.nav_slider_bar')

    <!-- Full With banner -->
    <div class="full-width-banner about-us-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Help Center

                    </h2>
                </div>

            </div>
        </div>

    </div>


    <!-- Full With Banner End here -->

    <div class="priuvacy-wrapper help-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center">Help Center</h2>
                    <p class="text-center">Got some questions? Write us away, and one of our team members will get in touch with you to resolve your query.</p>

                    <strong>Call</strong>
                    <p><strong><a href="callto:123456789">123456789</a></strong></p>
                    <strong>Email</strong>
                    <p><strong><a href="mailto:info@whattchagot.com">info@whattchagot.com</a></strong></p>


                </div>
            </div>
        </div>
    </div>



@stop
