@extends('layouts.app2')

@section('content')
    @include('layout.nav_header.nav_slider_bar.nav_slider_bar')

    <!-- Full With banner -->
    <div class="full-width-banner about-us-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Seller Information Center

                    </h2>
                </div>

            </div>
        </div>

    </div>


    <!-- Full With Banner End here -->

    <div class="priuvacy-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center">Seller Information Center</h2>
                    <p class="text-center">We welcome you to the <strong>Whattchagot Seller Information Center.</strong>
                        <br>

                        Find here the information you need to understand every aspect of selling faster on Whattchagot.

                    </p>

                    <h2>How To List Products</h2>
                    <p>We have a wide array of categories where you can list the items on Whattchagot. It starts with
                        becoming a seller by setting up a Whattchagot seller account. The Auction Section is a massive
                        territory where you can easily search, sell, buy and trade. Next comes the Products Section, where
                        you can find all the listings of all product categories. There, you will also see the section of
                        Trade, where, as a trader or seller, you can trade your items.</p>
                    </p>


                    <p>You may:</p>
                    <ul>
                        <li>Describe your items clear and concisely so they can get more views.</li>
                        <li>Some sellers prefer initiating from auction-style to gain buyers quickly.</li>
                        <li>After submitting your items, it becomes visible to countless buyers.</li>
                    </ul>
                    <h2>How To Handle Dispute</h2>
                    <p>We encourage buyers and sellers to work together to avoid the occurrence of any grave
                        misunderstandings. It will assist in enabling us to provide a hassle-free experience to both parties
                        through our platform. Buyers and sellers should follow Whattchagot’s policies to avoid the cause of
                        any inconvenience on both ends. Whattchagot may interfere with bringing about a resolution if any
                        misconduct is suspected.</p>
                        <p> <strong>You may connect with us at  <a href="emailto:info@whattchagot.com">info@whattchagot.com</a> for more details.</strong></p>


                </div>
            </div>
        </div>
    </div>



@stop
