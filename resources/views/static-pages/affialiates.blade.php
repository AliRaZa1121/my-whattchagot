@extends('layouts.app2')

@section('content')
    @include('layout.nav_header.nav_slider_bar.nav_slider_bar')

    <!-- Full With banner -->
    <div class="full-width-banner about-us-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Affiliates

                    </h2>
                </div>

            </div>
        </div>

    </div>


    <!-- Full With Banner End here -->

    <div class="priuvacy-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center">Affiliates</h2>
                    <p  class="text-center"><strong>Whattchagot</strong> provides an affiliate program through which anyone can become partners
                        and earn money.</p>

                    <h2>Here's How You Can Become Our Affiliate</h2>
                    <p>Becoming our Affiliate is super simple. Follow these steps to learn how.</p>
                    </p>

                    <strong>Step 1: Apply To Become An Affiliate</strong>
                    <ul>
                        <li>Apply to become an affiliate with Whattchagot.</li>
                    </ul>

                    <strong>Step 2: Create Affiliate Links</strong>
                    <ul>
                        <li>Create affiliate links for your desired product/item. Using specific links, you can direct your
                            following to a particular product listed in Whattchagot.</li>
                    </ul>

                    <strong>Step 3: Promote On Your Platform</strong>
                    <ul>
                        <li>You can add your Whattchagot affiliate link on your personal blog, website, social media, or
                            online profile. There is no conclusive limit to affiliate links you may post at once. Looking at
                            your audience's engagement, you can also choose to focus and promote certain items. </li>
                    </ul>

                    <strong>Step 4: Earn Commission</strong>
                    <ul>
                        <li>When your visitors click on your provided links and win or purchase an item, you will immediately receive a commission. Commissions differ depending on the product type and category. </li>
                        <li>Whattchagot affiliates can generate revenue on every purchase made by a consumer within 24 hours of clicking on the affiliate link.</li>
                    </ul>

                    <p> <strong>For more information regarding our affiliate application program, contact our team at <a href="mailto:info@whattchagot.com">info@whattchagot.com</a></strong></p>

                </div>
            </div>
        </div>
    </div>



@stop
