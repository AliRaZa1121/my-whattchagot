@extends('layouts.app2')

@section('content')
    @include('layout.nav_header.nav_slider_bar.nav_slider_bar')

    <!-- Full With banner -->
    <div class="full-width-banner about-us-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Advertise With Us
                    </h2>
                </div>

            </div>
        </div>

    </div>


    <!-- Full With Banner End here -->

    <div class="priuvacy-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center">ADVERTISE WITH US</h2>
                    <p class="text-center">With Whattchagot, you get the exciting opportunity to advertise your business
                        on a platform serving millions of customers worldwide. An audience so grand, you can connect with a
                        diverse range of buyers who are constantly seeking out brands like you.</p>

                    <h2>Become Visible On The E-Commerce Radar</h2>
                    <p>Whattchagot is a safe sanctuary for online traders, entrepreneurs, and sellers to showcase their
                        products to a vibrant community of people. We connect you with buyers that help maximize your
                        revenue and boost your visibility. We will help you deliver a cohesive customer experience that
                        converts fast.</p>
                    </p>
                    <h2>How Do We Help You Achieve Your Business Goals?</h2>
                    <p>Grow your business through our leading-edge branding solutions. Featuring your products on
                        Whattchagot enables you to connect with high-intent buyers giving you the needed exposure for brand
                        success. You can highlight your story, showcase excellent product offerings on a platform people
                        trust.</p>

                    <h2>Sell Anywhere, Anytime </h2>
                    <strong>
                        Want to sell in multiple places? Whattchagot has got your back!
                    </strong>
                    <p>We believe in bringing down the barrier of limiting our traders and sellers to auction and sell their
                        products. Build your international market and expand your business far and wide. We will guide you
                        at every step, from getting started to accelerating in the race towards progress.</p>




                </div>
            </div>
        </div>
    </div>



@stop
