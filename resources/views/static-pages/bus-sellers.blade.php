@extends('layouts.app2')

@section('content')
    @include('layout.nav_header.nav_slider_bar.nav_slider_bar')

    <!-- Full With banner -->
    <div class="full-width-banner about-us-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Business Sellers

                    </h2>
                </div>

            </div>
        </div>

    </div>


    <!-- Full With Banner End here -->

    <div class="priuvacy-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center">Business Sellers</h2>
                    <p class="text-center">Learn how you can become a business seller at Whattchagot and run your business smoothly and
                        efficiently.</p>

                    <h2>Become A Business Seller</h2>
                    <p><strong> Whattchagot </strong> allows you to register either as an individual seller or a business
                        seller. If you have a
                        registered business, you can select to create a business seller account and become a business
                        seller.<br>
                        <br>
                        Whattchagot Business Seller works best for industrial and commercial goods; you can earn a good
                        profit on other commodities as long as there is a bulk market for them.
                    </p>

                    <h2>Register As A Business Seller And Get:</h2>

                    <ul>
                        <li>Higher selling limits</li>
                        <li>Access to business tools</li>
                        <li>Access to discount deals and promotions</li>
                        <li>Low taxes</li>
                        <li>Legal business protection</li>
                    </ul>

                    <p><strong> Whattchagot </strong> business seller account makes your business stand out from the
                        competition. It helps you locate better facilities, increase sale units, and give access to
                        unlimited business tools.
                        Register yourself today as a business seller with Whattchagot and avail all privileges that we
                        offer!
                    </p>

                    <p> <strong>You may connect with us at  <a href="mailto:info@whattchagot.com">info@whattchagot.com</a> for more details.</strong></p>

                </div>
            </div>
        </div>
    </div>



@stop
