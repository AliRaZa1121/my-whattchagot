@extends('layouts.app2')

@section('content')
    @php use Carbon\Carbon; @endphp

    <style>
        nav.flex.items-center.justify-between {
        }

        nav.flex.items-center.justify-between svg {
            width: 20px !important;
            height: 17px !important;
        }

        nav.flex.items-center.justify-between a {
            width: 20px !important;;
            height: 17px !important;;
        }

        nav.flex.items-center.justify-between > div {
            text-align: center !important;
        }

        .flex.justify-between.flex-1.sm\:hidden {
            display: none !important;
        }

        nav.flex.items-center.justify-between .hidden.sm\:flex-1.sm\:flex.sm\:items-center.sm\:justify-between span.relative.z-0.inline-flex.shadow-sm.rounded-md a:hover {
            background-color: skyblue !important;
            color: white !important;
            font-size: 18px !important;
            cursor: pointer !important;
        }

        nav.flex.items-center.justify-between .hidden.sm\:flex-1.sm\:flex.sm\:items-center.sm\:justify-between span.relative.z-0.inline-flex.shadow-sm.rounded-md span:hover span {
            background-color: skyblue !important;
            color: white !important;
            font-size: 18px !important;
            cursor: pointer !important;
        }

        nav.flex.items-center.justify-between .hidden.sm\:flex-1.sm\:flex.sm\:items-center.sm\:justify-between span.relative.z-0.inline-flex.shadow-sm.rounded-md a {
            color: black !important;
            font-size: 18px !important;
        }

        nav.flex.items-center.justify-between .hidden.sm\:flex-1.sm\:flex.sm\:items-center.sm\:justify-between span.relative.z-0.inline-flex.shadow-sm.rounded-md span {
            color: black !important;
            font-size: 18px !important;
        }

        nav.flex.items-center.justify-between .hidden.sm\:flex-1.sm\:flex.sm\:items-center.sm\:justify-between span.relative.z-0.inline-flex.shadow-sm.rounded-md [aria-current] span {
            font-weight: bold;
            background-color: skyblue !important;
            color: white !important;
        }

        /*[aria-current] span {*/
        /*    font-weight: bold;*/
        /*    background-color: skyblue !important;*/
        /*    color: white !important;*/
        /*}*/


    </style>
@include('layout.nav_header.nav_slider_bar.nav_slider_bar')
    <!-- Full With banner -->
    <div class="full-width-banner auction-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Auctions</h2>
                </div>

            </div>
        </div>

    </div>


    <!-- Full With Banner End here -->

    <!-- Search Off Acution Start here -->
    <div class="auction-fillter-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="filter-inner-wrapper">

                        <form action="{{ url('auction') }}">
                            <div class="fomr-div location-form">
                                <label for="fname">Brands</label><br>
                                <div class="border-u">
                                    <select class="form-control1 form-control-sm" name="brand" id="categories_for_redirection">
                                        <option value="">Select Brand</option>
                                        @foreach($brand as $brands)
                                         <option value="{{$brands->id}}"  @if(isset($_GET['brand']) && $_GET['brand']==$brands->id) selected @endif>{{$brands->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div><!--form div-->

                            <div class="fomr-div cate-form">
                                <label for="fname">Category</label><br>
                                <div class="border-u">
                                    <select class="form-control1 form-control-sm" name="category" id="categories_for_redirection">
                                        <option value="">Select Category</option>
                                        @foreach($category as $cat)
                                         <option value="{{$cat->id}}" @if(isset($_GET['category']) && $_GET['category']==$cat->id) selected @endif>{{$cat->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div><!--form div-->

                            <div class="fomr-div show-form">
                                <label for="fname">Sub Category</label><br>
                                <div class="border-u">
                                    <select class="form-control1 form-control-sm" name="sub_category" id="categories_for_redirection">
                                        <option value="">Select </option>
                                        @foreach($sub_category as $sub_cat)
                                         <option value="{{$sub_cat->id}}" @if(isset($_GET['category']) && $_GET['category']==$sub_cat->id) selected @endif>{{$sub_cat->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div><!--form div-->

                            <div class="fomr-div sort-form">
                                <label for="fname">Sort By</label><br>
                                <div class="border-u">
                                    <select class="form-control1 form-control-sm" name="sort" id="categories_for_redirection">
                                        <option value="">Select </option>
                                        <option value="asc"  @if(isset($_GET['sort']) && $_GET['sort']=='asc') selected @endif>Asc</option>
                                        <option value="desc" @if(isset($_GET['sort']) && $_GET['sort']=='desc') selected @endif>Desc</option>

                                    </select>
                                </div>
                            </div><!--form div-->

                            <input class="single-input-btn" type="submit" value="Send Inquiry">
                        </form>
                    </div>
                </div>
            </div>

            <div class="row sorting-div">
                <div class="col-md-6 sorting-col1">
                    <div class="all-show-items same-line-style">
                        <p>All {{ $auctions->count() }}</p>
                    </div>
                    <div class="result-found same-line-style">
                        <p>Result Found</p>
                    </div>
                </div>

                <div class="col-md-6 sorting-col1">
                    <div class="show-drop-down same-line-style">
                        <p>Showing:</p>
                        <div class="border-u">
                        <form class="form aution-page-form" id="form" method="GET"  action="{{ url('auction') }}" >
                            <select class="form-control1 form-control-sm" name="perpage" id="categories_for_redirection"  onchange="this.form.submit()">
                                <option value="2">02</option>
                                <option value="4">04</option>
                                <option value="6">06</option>
                                <option value="8">08</option>
                                <option value="10">10</option>
                              <select>
                            </form>
                        </div>
                    </div>
                    <!-- <div class="sort-drop-down same-line-style">
                        <p>Sort By:</p>
                        <div class="border-u">
                            <select class="form-control1 form-control-sm" name="" id="categories_for_redirection">
                                <option value="">Select Category</option>
                                @foreach($category as $cat)
                                         <option value="{{$cat->id}}">{{$cat->name}}</option>
                               @endforeach
                            </select>
                        </div>
                    </div> -->
                </div>
            </div>

            <div class="row auction-page-products" id="auction-page-slider">
                <div class="best_selling_row">
                    <div class="container">
                        <div class="row">
                            <div class="best_selling_left">

                                <div class="best_selling_slider owl-carousel owl-theme">
                                    <div class="item">

                                     @if(!empty($auctions))
                                     @foreach($auctions as $p)
                                        <div class="best_selling_single">
                                            <div class="best_selling_slider_img">
                                                <img src="{{isset($p->product_images->image) ? asset('/storage/'.$p->product_images->image): asset('/storage/default.jpg')}}" style="max-width: 130px;">
                                            </div>
                                            <div class="auction_slider_text best_selling_slider_text">
                                                <div class="auction-page-cat">
                                                    <p>Auction Category</p>
                                                </div>
                                                <div class="auction_text">
                                                    <h2>{{  $p->name }}</h2>
                                                    <p>{{$p->description}}</p>
                                                    <div class="current-biding-acution">
                                                        <h4>Base Price: <span class="current-biding-acution-price">${{$p->price}}</span></h4>
                                                    </div>
                                                    <?php
                                                    //     $formatted_dt1= \Carbon\Carbon::parse($categorydatas->start_date." ".$categorydatas->start_time);
                                                    //   $formatted_dt2= \Carbon\Carbon::parse($categorydatas->end_date." ".$categorydatas->end_time);
                                                    $formatted_dt1=Carbon::now();
                                                    $formatted_dt2=Carbon::parse($p->end_date." ".$p->end_time);


                                                    $date_diff=$formatted_dt1->diffInDays($formatted_dt2);

                                                    $week_diff=$formatted_dt1->diffInWeeks($formatted_dt2);


                                                    $hours_diff = $formatted_dt1->diffInHours($formatted_dt2);
                                                    //  echo $hours_diff.' Hours '; //48 Hours


                                                    $Minutesdiff = $formatted_dt1->diffInMinutes($formatted_dt2);
                                                    // echo $Minutesdiff.' Minutes '; //2880 Minutes



                                                    $seconddiff = $formatted_dt1->DiffInSeconds($formatted_dt2);
                                                    // echo $seconddiff.' Seconds '; //172800  Seconds
                                                    // $period =  \Carbon\CarbonPeriod::create($start,$end);

                                                    // dump($period);
                                                    //     // Iterate over the period
                                                    //     foreach ($period as $date) {
                                                    //         echo $date->format('Y-m-d');
                                                    //     }

                                                    //     // Convert the period to an array of dates
                                                    //     $dates = $period->toArray();
                                                    ?>
                                                    <div class="auction-products-page-days">
                                                        <div class="c-same weeks-auction">
                                                            <div class="weeks-txt">
                                                                <p class="notranslate"><span class="ccounter">{{ $date_diff }}</span></p>
                                                                <h6>Days</h6>
                                                            </div>
                                                        </div>
                                                        <div class="c-same weeks-auction">
                                                            <div class="weeks-txt">
                                                                <p class="notranslate"><span class="ccounter">{{ $week_diff }}</span></p>
                                                                <h6>Weeks</h6>
                                                            </div>
                                                        </div>

                                                        <div class="c-same weeks-auction">
                                                            <div class="weeks-txt">
                                                                <p><span class="ccounter">{{ $hours_diff-($date_diff*24)  }}</span></p>
                                                                <h6>Hours</h6>
                                                            </div>
                                                        </div>
                                                        <div class="c-same weeks-auction">
                                                            <div class="weeks-txt">
                                                                <p><span class="ccounter">{{ $Minutesdiff-(($hours_diff*60)) }}</span></p>
                                                                <h6 class="notranslate">Minutes</h6>
                                                            </div>
                                                        </div>
                                                        <div class="c-same weeks-auction">
                                                            <div class="weeks-txt">
                                                                <p class="notranslate"><span class="ccounter">{{ $seconddiff-($Minutesdiff*60)  }}</span></p>
                                                                <h6>Seconds</h6>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tr_single_overlay best_selling_overlay">
                                                <div class="auction_wishlist tr_s_wishlist best_selling_wishlist">
                                                    <a href="#"><i class="far fa-heart"></i></a>
                                                </div>
                                                <div class="main_slider_btns tr_single_btns best_selling_btns">
                                                    <a href="{{ route('auction-single', ['id' => $p->id]) }}">Bid Now</a>
                                                    <!--  -->
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                       @endif
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            {{ $auctions->withQueryString()->links() }}
        </div>
    </div>

    <!-- Search Of Auction end here -->


    <!--How To Bid Sectio Start here -->
    <div class="how-to-bid-section">
        <div class="container">
            <div class="how-to-bid-inner-wrapper">
                <div class="how-to-bid-title">
                    <h2>How To Bid</h2>
                </div>
                <div class="row boxes-wrapper">
                    <div class="col-md-4">
                        <div class="bid-process-wrapper">
                            <div class="how-to-bid-icon">
                                <img src="./images/how-to-bid-icon-2.png">
                            </div>
                            <div class="how-to-bid-icon-txt">
                                <h3>Register With Us</h3>
                                <p>Create an account with Whattchagot and classify the products you wish to buy or sell.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="bid-process-wrapper">
                            <div class="how-to-bid-icon">
                                <img src="./images/how-to-bid-icon-2.png">
                            </div>
                            <div class="how-to-bid-icon-txt">
                                <h3>Place Your Bid</h3>
                                <p>Go to our bidding section and select the kinds of goods/items/products/ you wish you place a bid on.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="bid-process-wrapper">
                            <div class="how-to-bid-icon">
                                <img src="./images/how-to-bid-icon-2.png">
                            </div>
                            <div class="how-to-bid-icon-txt">
                                <h3>Confirm Your Order</h3>
                                <p>Make your offer, choose the best bid, and start earning!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <script>
        jQuery(".best_selling_slider").owlCarousel({
            loop:false,
            items: 1,
            margin:10,
            dots: false,
            nav: true,
            navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
            autoplay: false,
            autoplayTimeout: 3000,
            responsiveClass:true,
        });
    </script>
@stop





