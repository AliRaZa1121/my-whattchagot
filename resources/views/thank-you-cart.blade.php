@extends('layouts.app2')

@section('content')


    <!-- Full With banner -->
    <div class="full-width-banner about-us-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Thankyou</h2>
                </div>

            </div>
        </div>

    </div>


    <!-- Full With Banner End here -->


    <!--checkout Page Start here-->
    <div class="thankyou-page">
        <div class="container">
            <h2>Thankyou For Order</h2>
            <a href="{{route('index')}}">Continue Shopping</a>
        </div>
    </div>
    <!--checkout page end here -->

    <!--trade Products Row End here-->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $("#form_make_btn").on('click', function (e) {
            submitradeform();
        });
        function submitradeform() {
            setTimeout(function () {
                $("#form_make").ajaxSubmit(options);

            }, 500);


            var options = {

                dataType: 'json',
                async: true,
                success: function (response)
                {

                    console.log(response);
                    if (response.error === 1) {
                        var errorsHtml = '<div class="alert alert-danger alert-dismissable"><a href="#" class="pull-right" data-dismiss="alert" aria-label="close">&times;</a><ul>';
                        $.each(response.errors, function (key, value) {
                            errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
                        });
                        errorsHtml += '</ul></div>';
                        $('#errors_review').html(errorsHtml).show();
                        setInterval(function(){
                            $('#errors_review').html('');
                        }, 3000);//run this thang every 2 seconds
                    }
                    if(response.success==1){
                        swal({
                            title: "Success!",
                            icon: "success",
                            text: response.msg,
                            type: "success",
                            //  timer: 1000
                        }).then((result) => {
                            // Reload the Page
                            location.reload();
                        });;

                    }
                }




            };
        }
        jQuery("#client-stories .trending_products_slider").owlCarousel({
            loop:true,
            items: 3,
            margin:10,
            dots: false,
            nav: true,
            navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
            autoplay: true,
            autoplayTimeout: 3000,
            responsiveClass:true,
        });

        jQuery(".about-last-slider-wrapper .auction_slider_wrapper").owlCarousel({
            loop:true,
            items: 1,
            margin:10,
            dots: true,
            nav: false,
            navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
            autoplay: false,
            autoplayTimeout: 3000,
            responsiveClass:true,
        });

        function increment() {
            document.getElementById('demoInput').stepUp();
        }
        function decrement() {
            document.getElementById('demoInput').stepDown();
        }

    </script>
    <script>
        function openPage(pageName,elmnt,color) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablink");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].style.backgroundColor = "";
            }
            document.getElementById(pageName).style.display = "block";
            elmnt.style.backgroundColor = color;
        }

        // Get the element with id="defaultOpen" and click on it
        document.getElementById("defaultOpen").click();
    </script>

    <script>
        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: true,
            asNavFor: '.slider-nav'
        });
        $('.slider-nav').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            vertical:true,
            asNavFor: '.slider-for',
            dots: false,
            arrows: false,
            focusOnSelect: true
        });

        $('a[data-slide]').click(function(e) {
            e.preventDefault();
            var slideno = $(this).data('slide');
            $('.slider-nav').slick('slickGoTo', slideno - 1);
        });


        jQuery("#auction-details-biding .best_selling_slider").owlCarousel({
            loop:true,
            items: 1,
            margin:10,
            dots: false,
            nav: true,
            navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
            autoplay: true,
            autoplayTimeout: 3000,
            responsiveClass:true,
        });
        jQuery("#auction-you-like .best_selling_slider").owlCarousel({
            loop:true,
            items: 1,
            margin:10,
            dots: false,
            nav: true,
            navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
            autoplay: true,
            autoplayTimeout: 3000,
            responsiveClass:true,
        });


        jQuery("#top-selling-products .trending_products_slider").owlCarousel({
            loop:true,
            items: 1,
            margin:10,
            dots: false,
            nav: true,
            navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
            autoplay: true,
            autoplayTimeout: 3000,
            responsiveClass:true,
        });

        jQuery("#products-you-can-trade .trending_products_slider").owlCarousel({
            loop:true,
            items: 1,
            margin:10,
            dots: false,
            nav: true,
            navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
            autoplay: false,
            autoplayTimeout: 3000,
            responsiveClass:true,
        });

        jQuery(".animated-progress span").each(function () {
            jQuery(this).animate(
                {
                    width: jQuery(this).attr("data-progress") + "%",
                },
                1000
            );
            jQuery(this).text(jQuery(this).attr("data-progress") + "%");
        });
    </script>



    <script src="{{asset('js/malsup.github.io-jquery.form.js')}}"></script>

@stop




