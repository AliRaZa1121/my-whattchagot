<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@php
    $setting = get_setting();
@endphp
<head>

    <link rel="shortcut icon" href="{{asset('media\logos\Fav icon 32 x32-01.png')}}">


    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- <title>{{ config('app.name', 'Laravel') }}</title>--}}
    <?php if(request()->segment(1)=='buyer_login' || request()->segment(1)=='add_check_out'|| request()->segment(1)=='buyer_login_id' ){ ?>
    <title style="text-transform: capitalize">Login | {{ config('app.name') }}</title>
    <?php } else { ?>
    <title style="text-transform: capitalize">{{ !empty(ucwords(str_replace('-',' ',request()->segment(1)))) ?
        ucwords(str_replace('-',' ',request()->segment(1))) : 'Do What Winners Do – Auction & Trade!' }} | {{
        config('app.name') }}</title>
    <?php } ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js">
    </script>
    <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js">
    </script>
    <script src="{{ asset('js/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/custom.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/slick.min.js')}}"></script>
    <script defer src="{{ asset('js/remodal.min.js')}}"></script>


    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Roboto:wght@400;500;700;900&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/fontawesome/css/all.css')}}">
    <link rel="stylesheet" type="text/css"
        href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.3/css/fontawesome.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Roboto:wght@400;500;700;900&display=swap"
        rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('fonts/fontawesome/css/all.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
        integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
        crossorigin="anonymous" />

    <!-- Remodal -->
    <link rel="stylesheet" href="{{ asset('css/remodal.css')}}">
    <link rel="stylesheet" href="{{ asset('css/remodal-default-theme.css')}}">
    <!-- Slick CSS -->
    <link rel="stylesheet" href="{{ asset('css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slick-theme.css') }}">


    <script>

    </script>
    @yield('styles')

</head>
<style>
    .goog-tooltip {
        display: none !important;
    }

    .goog-tooltip:hover {
        display: none !important;
    }

    .goog-text-highlight {
        background-color: transparent !important;
        border: none !important;
        box-shadow: none !important;
    }

    div#preloader {
        position: fixed;
        left: 0;
        top: 0;
        z-index: 9999999 !important;
        width: 100%;
        height: 100%;
        overflow: visible;
        background-color: #fff;
    }

    .fixed .top_header,
    .fixed .search-section {
        display: none;
    }

    header.mainHeader.fixed {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        z-index: 99999;
        transition: 0.5s;
    }
</style>


<script>
    $(document).ready(function (){

        $('#goog-gt-tt').remove();

        var down = $('.current-acc-detail-title h2').html();


        var res = down.charAt(0).toUpperCase() + down.slice(1);
        // var res =  res.charAt(0).toUpperCase() + res.toLocaleLowerCase().substring(1);
        const words = res.split(" ");
        var html = "";

        for(var i=0;i< words.length;i++){
            html += catalize(words[i]);
        }
        function catalize(string){
          return ' '+string.charAt(0).toUpperCase() + string.slice(1);
        }


        document.title =html + ' | Whattchagot';

    });


</script>

<style>
    .main_slider_btns.tr_single_btns.best_selling_btns {
        text-transform: capitalize;
    }
</style>


<body>
{{-- {{dd(App\Helpers\Cart::get_cart_count())}} --}}
    {{-- <div id="loader-wrapper bg-white">
        <div class="loader-center">
            <div class="dot-1"></div>
            <div class="dot-2"></div>
            <div class="dot-3"></div>
            <img src="" alt="">
        </div>
    </div> --}}

    {{-- <div id="preloader">
        <video width="100%" autoplay muted preload="false" id="myVideo">
            <source src="{{asset('images/loader.mp4')}}" type="video/mp4">
            WhattchaGot.
        </video>
    </div> --}}


    <div class="wrapper">
        <header class="mainHeader" id="myheader">
            @include('layout.nav_header.top_header')
            @include('layout.nav_header.logo_bar')
            @include('layout.nav_header.nav_bar')
        </header>

        <div>

            @yield('content')
        </div>
        @include('layout.newsletter')
        @include('layout.footer')
        @include('layout.copy_right')

    </div>

</body>

</html>
@yield('scripts')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    document.addEventListener("DOMContentLoaded", function(event) {
      url = "{{url('get-auction')}}"
      $.ajax({
            url :url,
            type:'GET',
            success:function(data){
                if(data == 1){
                    console.log('sucess');
                }
                else{
                    console.log('error');
                }
            }
        })
      });

    //   $(window).scroll(function(){
    //     var sticky = $('header'),
    //         scroll = $(window).scrollTop();

    //     if (scroll >= 250) sticky.addClass('fixed');
    //     else sticky.removeClass('fixed');
    //     });
    window.onscroll = function() {myFunction()};

var header = document.getElementById("mynav");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("m-sticky");
  } else {
    header.classList.remove("m-sticky");
  }
}


    // $(document).ready(function() {
    // // Animate loader off screen
    // setTimeout(() => {
    //     jQuery("#loader-wrapper").fadeOut("slow");
    // }, 4000);
    // });

    ///loader script

    jQuery(document).ready(function(){

    jQuery("#preloader").show();

    setTimeout(() => {
        jQuery("#preloader").fadeOut('slow');
    }, 5000);

    // jQuery('#myVideo').on('load',function(){
    //     jQuery(this).hide();
    //     jQuery("#preloader").fadeOut('slow');
    // });


    });


    $("#news-letter").on('submit', (e) => {
        e.preventDefault();
        const email =  $("#newsletter_email").val();
        $.ajax({
                url: "{{url('/newsletter-submit')}}?email="+email,
                type: "GET",
                success: function (response) {

                    if (response == 200) {
                        swal({
                        title: "Success!",
                        text: "Thank you for subscribing",
                        type: "success",
                        timer: 2000
                    });
                    }
                    else if(response == 422){
                        swal({
                        title: "Success!",
                        text: "You already subscribed",
                        type: "success",
                        timer: 2000
                    });
                    }
                    else{
                        swal({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Something went wrong!',
                        });
                    }


                }
            });

    });

    $(".ajax_add_cart").on("submit", function (event) {
            event.preventDefault();

            var size  = $("input[name='product_size']:checked").val();
            var color = $("input[name='color']:checked").val();

            if (color != undefined && size != undefined) {
                 var data_input = $(this).serialize()+`&color=${color}&size=${size}`;
            }
            else{
            var data_input = $(this).serialize();
            }

            $.ajax({
                url: "{{url('/checkout_ajax')}}",
                type: "GET",
                data: data_input,
                success: function (response) {
                    if (response == 422) {
                    swal({
                        title: "Warning!",
                        text: "Your can't add your product in your cart.",
                        type: "success",
                        timer: 1000
                    });
                }
                    else if (response != 419) {
                    $("#cart_count").html(JSON.stringify(response.cart_item))
                    swal({
                        title: "Success!",
                        text: "Your product is added.",
                        type: "success",
                        timer: 1000
                    });
                }
                else{
                    swal({
                        title: "Warning!",
                        text: 'You have some trade items in cart, please complete checkout process',
                        type: "success",
                    });
                }
                }

            });

        }
    );

    $(".wishlist_ajax").click(function () {
        $.ajax({
            url: "{{url('/wishlist-ajax')}}",
            type: "GET",
            data: {id: $(this).data('id')},
            success: function (response) {
                var obj =response;
                if(obj.type=="success")
                {
                swal({
                    title: "Success!",
                    text: obj.message,
                    type: "success",
                    timer: 1000
                });
               }
               if(obj.type=="error")
                {
                swal({
                    title: "",
                    icon: "warning",
                    text: obj.message,
                    type: "error",
                    timer: 1000
                });
               }

            }
        });
    })





</script>
<!-- Scripts -->
