@extends('layouts.app2')

@section('content')
    <!-- Full With banner -->
    <div class="full-width-banner news-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>News</h2>
                </div>

            </div>
        </div>

    </div>


    <!-- Full With Banner End here -->





    <div class="best_selling_row news-main-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="news-single-page-inner">
                        <div class="news-single-thumbnail">
                            <img src="./images/new-single-thumnail.jpg">
                        </div>
                        <div class="current-acc-detail-title news-single-title ">
                            <h2>Dummy Title of Blogs and News title</h2>
                        </div>
                        <div class="news-single-content">
                            <p>This is dummy copy. It is not meant to be read. It has been placed here solely to demonstrate the look and feel. This is dummy copy. It is not meant to be read. It has been placed That being the case, there is really no point in your continuing to read them. These words are here to provide the reader with a basic impression of how actual text will appear in its final presentation. Think of them merely as actors on a paper stage This is dummy copy. It is not meant to be read. It has been placed here solely to demonstrate the look and feel of finished, typeset text. Only for show. He who searches for meaning here will be sorely disappointed. These words are here to provide the reader with a basic impression of how actual text will appear in its final presentation. Think of them merely as actors on a paper stage, in a performance devoid of content yet rich in form. That being the case, there is really no point in your continuing to read them. After all, you have many other things you should be doing. This is dummy copy. It is not meant to be read. It has been placed here solely to demonstrate the look and feel of finished, typeset text. Only for show. He who searches for meaning here will be sorely disappointed. These words are here to provide the reader with a basic impression of how actual text will appear in its final presentation. Think of them merely as actors on a paper stage, in a performance devoid of content yet rich in form. That being the case, there is really no point in your continuing to read them. After all, you have many other things you should be doing. Who's paying you to waste this time, anyway?</p>
                            <p>These words are here to provide the reader with a basic impression of how actual text will appear in its final presentation. Think of them merely as actors on a paper stage, in a performance devoid of content yet rich in form. That being the case, there is really no point in your continuing to read them. After all, you have many other things you should be doing. This is dummy copy. It is not meant to be read. It has been placed here solely to demonstrate the look and feel of finished, typeset text. Only for show. He who searches for meaning here will be sorely disappointed.</p>
                        </div>
                        <div class="row no-gutters news-single-images-row">
                            <div class="col-md-4">
                                <div class="news-single-single-img">
                                    <img src="./images/news-single-images.jpg">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="news-single-single-img">
                                    <img src="./images/news-single-images.jpg">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="news-single-single-img">
                                    <img src="./images/news-single-images.jpg">
                                </div>
                            </div>

                        </div>

                        <div class="news-single-content">
                            <p>This is dummy copy. It is not meant to be read. It has been placed here solely to demonstrate the look and feel. This is dummy copy. It is not meant to be read. It has been placed That being the case, there is really no point in your continuing to read them. These words are here to provide the reader with a basic impression of how actual text will appear in its final presentation. Think of them merely as actors on a paper stage This is dummy copy. It is not meant to be read. It has been placed here solely to demonstrate the look and feel of finished, typeset text. Only for show. He who searches for meaning here will be sorely disappointed. These words are here to provide the reader with a basic impression of how actual text will appear in its final presentation.</p>
                        </div>

                    </div><!-- news-single-page-inner -->
                    <div class="new-single-form">
                        <h2>Leave a reply</h2>
                        <p>Your Email Address wil not be published. Required Fields are Marked*</p>
                        <form class="sin-form" action="#">


                            <!-- <label for="fname">First name:</label><br> -->
                            <input type="text" id="fname" name="fname" placeholder="Your Name *">
                            <!-- <label for="lname">Last name:</label><br> -->
                            <input type="email" id="lname" name="lname" placeholder="Your Email *">
                            <textarea type="text" id="msgh" name="fname" placeholder="Your Message *"></textarea>
                            <input  class="single-input-btn" id="sbt-btn" type="submit" value="Submit">
                        </form>
                    </div>

                </div>

                <div class="col-md-3">
                    <div class="news-side-br">
                        <div class="news-side-br-inner">
                            <h2>Topics</h2>
                            <ul>
                                <li><img src="./images/side-icon.png">All Topics</li>
                                <li><img src="./images/side-icon.png">Announcements</li>
                                <li><img src="./images/side-icon.png">Payments</li>
                                <li><img src="./images/side-icon.png">Purpose</li>
                            </ul>
                        </div>
                    </div>
                    <div class="news-side-br">
                        <div class="news-side-br-inner recent-news-wrapper">
                            <h2>Recent News</h2>
                            <ul>
                                <li><img src="./images/recent-icons.png">This is dummy copy. It is not meant to be read.</li>
                                <li><img src="./images/recent-icons.png">This is dummy copy. It is not meant to be read.</li>
                                <li><img src="./images/recent-icons.png">This is dummy copy. It is not meant to be read.</li>
                                <li><img src="./images/recent-icons.png">This is dummy copy. It is not meant to be read.</li>
                                <li><img src="./images/recent-icons.png">This is dummy copy. It is not meant to be read.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="side-br-twitter">
                        <div class="twitter-img">
                            <img src="./images/white-logo-twitter.png">
                        </div>
                        <div class="twitter-txt">
                            <p>This is dummy copy. It is not meant to be read. It has been placed here solely to demonstrate the look and feel of finished, typeset text. </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop




