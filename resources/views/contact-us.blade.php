@extends('layouts.app2')
@php
    $setting = get_setting();
@endphp
@section('content')



    <!-- Full With banner -->
    <div class="full-width-banner contact-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Contact Us</h2>
                </div>

            </div>
        </div>

    </div>


    <!-- Full With Banner End here -->

    <!--- Contact Us Form Section Start here -->
    <div class="row contact-main-title">
        <div class="col-md-12">
            <div class="contact-tilte">
                <h2>Get In Touch</h2>
            </div>
        </div>

    </div>

    <div class="contact-form-main">
        <div class="container">


            <div class="row">
                <div class="col-md-6">
                    <div class="contact-us-side">
                        <h2>Contact Us Today</h2>
                        <div class="email-box">
                            <div class="emil-icon-wrapper">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                            </div>
                            <div class="email-icon-txt">
                                <h4>Email:</h4>
                                <ul>
                                    <li><a href="mailto:{{$setting->email}}">{{ $setting->email }}</a></li>
                                    <li><a href="#">{{ $setting->address }}</a></li>
                                    <li><a href="#">{{ $setting->phone }}</a></li>

                                    {{-- <li><a href="mailto:Support@whattchagot.com">Support@whattchagot.com</a></li> --}}
                                </ul>
                            </div>
                        </div>
                        <div class="contact-us-follow">
                            <h4>Follow Us</h4>
                            <div class="newsletter3">
                                <ul>
                                <li><a href="{{ $setting->facebook_link }}"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="{{ $setting->twitter_link }}"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="{{ $setting->instagram_link }}"><i class="fab fa-instagram"></i></a></li>
                                {{-- <li><a href="https://www.pinterest.com/"><i class="fab fa-pinterest"></i></a></li> --}}
                                <li><a href="{{ $setting->tiktok_link }}"><i class="fab fa-tiktok fa-2x"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="contact-form-wrapper">
                        @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                        <form action="{{route('contact-us-submit')}}" method="POST">
                                @csrf
                            <h3>Send us a Message</h3>
                            <p>For any inquiries and concerns, fill out the contact form below. Our expert support will get
                                back to you at the earliest.</p>

                            <input type="text" id="name" name="name" placeholder="Name" required>

                            <input type="email" id="email" name="email" placeholder="Email Address" required>

                            <input type="tel" id="phone" name="phone" placeholder="Phone Number" required>

                            <div class="border-u">
                                <select class="form-control1 form-control-sm" name="category" id="categories_for_redirection" required>
                                    <option value="">Select Category</option>
                                    @forelse ($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                    @empty
                                        <p>No Categories Found</p>
                                    @endforelse
                                </select>
                            </div>
                            <textarea type="text" id="message" name="message" placeholder="Your Message" required></textarea>
                            <input class="single-input-btn" type="submit" value="Submit">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- <div class="map-row">
        <div class="map-row-div">
            <div class="inner-wrapper">
                <div class="map-icon">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                </div>
                <p>Find Our Stores On</p>
                <a href="#">Google Map</a>
            </div>
        </div>
        <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d26360909.888257876!2d-113.74875964478716!3d36.242299409623534!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54eab584e432360b%3A0x1c3bb99243deb742!2sUnited%20States!5e0!3m2!1sen!2s!4v1622563687841!5m2!1sen!2s"
            width="100%" height="450px" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </div> --}}



    <!-- Our Brands Section Start Here -->
    <!--trade Products Row Start here-->
    <div class="Our-brands-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="trending_products_wrapper">
                        <div class="tr_p_head">
                            <h3>Our Brands</h3>
                        </div>
                        <div class="trending_products_slider owl-carousel owl-theme">
                            <div class="item">
                                @if(count($brands) > 0)
                                @foreach ($brands as $b )
                                <div class="trending_single_wrapper logo-brand-box">
                                    <div class="logo-inner-box">
                                        <a href="{{ url('products') }}?type=list&&brand={{$b->id}}">
                                        <img src="{{ asset('/storage/'.$b->banner_image ?? '') }}">
                                    </a>
                                    </div>
                                </div>
                                @endforeach
                                @else
                                <h4 class="text-center">No Data Found</h4>
                                @endif
                            </div>

                        </div>
                    </div>

                </div>


            </div>
        </div>
    </div>

@stop
