@extends('layouts.app2')

@section('content')
    @include('layout.nav_header.nav_slider_bar.nav_slider_bar')

    <!-- Full With banner -->
    <div class="full-width-banner about-us-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Privacy Policy</h2>
                </div>

            </div>
        </div>

    </div>


    <!-- Full With Banner End here -->

<div class="priuvacy-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p><strong>WHATTCHAGOT</strong>, together with its affiliates (we), (us), has made the following privacy statement you see before you. We are confidently devoted to the protection and safeguard of your (Site User) privacy.</p>
<p>By using this website, you agree to accept the Privacy Policy set by WHATTCHAGOT. If you may not accept or agree to the set privacy statement, please discontinue using this website immediately.</p>

<h2>General Data</h2>
<p><strong>WHATTCHAGOT </strong>uses General Information to support the Website, making it more useful to visitors (including to help us assess the content of interest to visitors). We collect information for other purposes, including determining the Websites’ technical design specifications and identifying system performance or problem areas. Along with tracking activity on the Website, preparing reports, assessing trends, improving usability, and otherwise monitoring the way in which the Website is being used – none of which would be in a way that personally identifies any users.</p>
<h2>Personal Data</h2>
<p><strong>WHATTCHAGOT</strong> uses Personal Information for a range of purposes, including: account registration (e.g., your name, telephone number, and email address); billing details, transaction and credit card information; and information regarding your interests, demographics, experiences and contact preferences. We acquire such info/data to provide you with further correspondence about your purchase of our services.</p>
      <p>If you contact WHATTCHAGOT via the Website or the provided email address set out on the Website, we are permitted to keep a record or a copy of that correspondence.</p>

<strong>We use the data we collect through your use of our Services to:</strong>
<ul>
    <li>Process your requests and orders for content, products, specific services, information, or features;</li>
    <li>Communicate with you about your account by:</li>
    <li>Responding to your questions and concerns;</li>
    <li>Sending you administrative messages and information, including announcements from customers and service providers, notifications about changes/modifications to our Service, and updates to our agreements.</li>
    <li>Sending you information, such as by email or text messages, about your progress in the project, new services, new features, promotions, newsletters, and other available content (which you can opt-out of at any time);</li>
    <li>Sending push notifications to convey updates and other relevant messages </li>
    <li>Manage your account and account preferences;</li>
    <li>Assist with Services’ technical functioning, which includes troubleshooting and resolving issues, securing the Services, and preventing fraud and abuse;</li>
    <li>Solicit feedback from users;</li>
    <li>Market products, services, and promotions;</li>
    <li>Market Subscription Plans to potential customers;</li>
    <li>Learn more about you by linking your data with additional data through third-party data providers and/or analyzing the data with the help of analytics service providers;</li>
    <li>Advance in our Services and progress new products, services, and features;</li>
    <li>Analyze trends and traffic, track purchases, and track usage data;</li>
    <li>Advertise the Services on third-party websites and applications;</li>
    <li>As required or permitted by law; or</li>
    <li>As we, in our sole discretion, otherwise determine to be necessary to ensure the safety or integrity of our users, employees, third parties, the public, or our Services.</li>
</ul>

<h2>DATA PRIVACY</h2>
<p><strong>WHATTCHAGOT </strong> assures that all the data/information provided remains confidential & private. We do not use or misuse private data, including Personal Data. In accordance, we do not distribute or sell any personal data to other organizations, groups, or individuals. </p>
<h2>Security </h2>
<p>Keeping your Personal Information/Details/Data safe is of utmost priority to us. We take all steps reasonably necessary to protect your Personal Information against any unauthorized access or use. However, we cannot give assurance that the Personal Information given, forwarded, or transmitted by you (the user) through this Website or by e-mail is totally secure and safe.</p>
<h2>Terms of Use & Changes to this Statement</h2>
<p>Your use of the <strong>WHATTCHAGOT</strong> Website is subject to the Privacy Statement you see before you and governed by Terms of Use set by us, including its choice of law provisions. We reserve the right to change, modify, and update the Policy, and these changes/ modifications will be made when WHATTCHAGOT.COM deems appropriate, promptly. </p>
<p>To safeguard that you are aware and updated on any changes we may have made, we encourage you to observe our Policy from time to time.</p>

<h2>Contact Us</h2>
<p>If you have any complaints, recommendations, or questions about the set Privacy Statement, feel free to contact us at <strong>info@whattchagot.com</strong>, and we will get back to you.</p>



</div>
        </div>
    </div>
</div>




    <script>
        jQuery("#client-stories .trending_products_slider").owlCarousel({
            loop: true,
            items: 3,
            margin: 10,
            dots: false,
            nav: true,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            autoplay: true,
            autoplayTimeout: 3000,
            responsiveClass: true,
        });

        jQuery(".about-last-slider-wrapper .auction_slider_wrapper").owlCarousel({
            loop: true,
            items: 1,
            margin: 10,
            dots: true,
            nav: false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            autoplay: false,
            autoplayTimeout: 3000,
            responsiveClass: true,
        });
    </script>
@stop
