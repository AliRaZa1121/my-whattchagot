@extends('layouts.app2')

@section('content')
    <div class="Shop-categories-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="trending_products_wrapper">
                        <div class="tr_p_head">
                            <h3>Shop By Categories</h3>
                        </div>
                        <div class="trending_products_slider owl-carousel owl-theme">
                            <div class="item">
                                <div class="trending_single_wrapper Shop-categoreis-item">
                                    <div class="shop-cat-wrapper">
                                        <div class="left-cate grid-container">
                                            <div class="grid-item item1">
                                                <div class="inn-wrapper">
                                                    <img src="./images/left-cat-1.jpg">
                                                    <div class="cate-txt">
                                                        <h2>Fashion</h2>
                                                        <a href="#">Shop Now</a>
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="grid-item item2">
                                                <div class="inn-wrapper">
                                                    <img src="./images/left-cat-2.jpg">
                                                    <div class="cate-txt">
                                                        <h2>Electronics</h2>
                                                        <a href="#">Shop Now</a>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="grid-item item3">
                                                <div class="inn-wrapper">
                                                    <img src="./images/left-cat-3.jpg">
                                                    <div class="cate-txt">
                                                        <h2>Home & garden</h2>
                                                        <a href="#">Shop Now</a>
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="grid-item item4">
                                                <div class="inn-wrapper">
                                                    <img src="./images/left-cat-4.jpg">
                                                    <div class="cate-txt">
                                                        <h2>Printers</h2>
                                                        <a href="#">Shop Now</a>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="right-cate">
                                            <div class="inner-cat-shop">
                                                <div class="inn-wrapper">
                                                    <img src="./images/right-big-cat.jpg">
                                                    <div class="cate-txt">
                                                        <h2>SPORTS</h2>
                                                        <a href="#">Shop Now</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>




                            </div>
                            <div class="item">
                                <div class="trending_single_wrapper Shop-categoreis-item">
                                    <div class="shop-cat-wrapper">
                                        <div class="left-cate grid-container">
                                            <div class="grid-item item1">
                                                <div class="inn-wrapper">
                                                    <img src="./images/left-cat-1.jpg">
                                                    <div class="cate-txt">
                                                        <h2>Fashion</h2>
                                                        <a href="#">Shop Now</a>
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="grid-item item2">
                                                <div class="inn-wrapper">
                                                    <img src="./images/left-cat-2.jpg">
                                                    <div class="cate-txt">
                                                        <h2>Electronics</h2>
                                                        <a href="#">Shop Now</a>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="grid-item item3">
                                                <div class="inn-wrapper">
                                                    <img src="./images/left-cat-3.jpg">
                                                    <div class="cate-txt">
                                                        <h2>Home & garden</h2>
                                                        <a href="#">Shop Now</a>
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="grid-item item4">
                                                <div class="inn-wrapper">
                                                    <img src="./images/left-cat-4.jpg">
                                                    <div class="cate-txt">
                                                        <h2>Printers</h2>
                                                        <a href="#">Shop Now</a>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="right-cate">
                                            <div class="inner-cat-shop">
                                                <div class="inn-wrapper">
                                                    <img src="./images/right-big-cat.jpg">
                                                    <div class="cate-txt">
                                                        <h2>SPORTS</h2>
                                                        <a href="#">Shop Now</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>




                            </div>
                        </div>
                    </div>

                </div>



            </div>
        </div>
    </div>

    <!--tShop By Categories Page Page end here-->


    <!-- Lcd Row Start here -->
    <div class="lcd-row-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="lcd-img">
                        <img src="./images/lcd-image.png">
                    </div>
                </div>
                <div class="col-md-5 middle-txt">
                    <div class="lcd-content">
                        <h3>Trade Your Product</h3>
                        <h2>Save Up To 30% By making a trade with your product</h2>
                        <a href="#">Click Here</a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="lcd-img watch-img">
                        <img src="./images/watch-img.png">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--lcd row end here -->

    <!--Why Shop With what chagot-->
    <div class="shop_with_row cate-page-why-shop">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>WHY SHOP WITH WHATTCHAGOT?</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="same_four_box">
                        <div class="same_four_box_img">
                            <img src="images/icon1.png">
                        </div>
                        <h3>Fair Return Policy</h3>
                        <p>This is dummy copy. It is not meant to be read. It has been placed here solely to demonstrate the look</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="same_four_box">
                        <div class="same_four_box_img">
                            <img src="images/icon2.png">
                        </div>
                        <h3>Technical Support</h3>
                        <p>This is dummy copy. It is not meant to be read. It has been placed here solely to demonstrate the look</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="same_four_box">
                        <div class="same_four_box_img">
                            <img src="images/icon3.png">
                        </div>
                        <h3>Secure Payments</h3>
                        <p>This is dummy copy. It is not meant to be read. It has been placed here solely to demonstrate the look</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="same_four_box">
                        <div class="same_four_box_img">
                            <img src="images/icon4.png">
                        </div>
                        <h3>Sell Worldwide</h3>
                        <p>This is dummy copy. It is not meant to be read. It has been placed here solely to demonstrate the look</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop




