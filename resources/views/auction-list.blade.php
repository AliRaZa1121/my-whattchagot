@extends('layouts.app2')
@php
use App\Models\Setting;
    $setting = Setting::first();
@endphp
<style>
    /**
 *	This element is created inside your target element
 *	It is used so that your own element will not need to be altered
 **/
.time_circles {
    position: relative;
    width: 100%;
    height: 100%;
}
#auction-you-like .best_selling_single {
    width: 48.5% !important;
}
#auction-you-like .row.auction-page-products {
    display: block;
}
#auction-you-like .best_selling_single:hover .best_selling_overlay {
    opacity: 1;
    visibility: visible;
    width: 38%;
    height: 97%;
    left: 1.5%;
    top: 1.5%;
    box-shadow: 0 2px 5px 0 rgb(0 0 0 / 10%);
    border-radius: 3px;
    z-index: 999;
}
#auction-details-biding .best_selling_single:hover .best_selling_overlay {
    opacity: 1;
    visibility: visible;
    width: 38%;
    height: 97%;
    left: 1.5%;
    top: 1.5%;
    box-shadow: 0 2px 5px 0 rgb(0 0 0 / 10%);
    border-radius: 3px;
    z-index: 999;
}
#auction-details-biding .auction_slider_text.best_selling_slider_text {
    width: 60%;
}
#auction-you-like .auction_slider_text.best_selling_slider_text {
    width: 60%;
}

#auction-details-biding .best_selling_single {
    width: 48.5% !important;
}
#auction-details-biding .row.auction-page-products {
    display: block;
}
/**
 *	This is all the elements used to house all text used
 * in time circles
 **/
.time_circles > div {
    position: absolute;
    text-align: center;
}

/**
 *	Titles (Days, Hours, etc)
 **/
.time_circles > div > h4 {
    margin: 0;
    padding: 0;
    text-align: center;
    text-transform: uppercase;
    font-family: 'Century Gothic', Arial;
    line-height: 1;
}

/**
 *	Time numbers, ie: 12
 **/
.time_circles > div > span {
    margin: 0;
    padding: 0;
    display: block;
    width: 100%;
    text-align: center;
    font-family: 'Century Gothic', Arial;
    line-height: 1;
    font-weight: bold;
}
</style>
@section('content')
@php use Carbon\Carbon;  @endphp

@include('layout.nav_header.nav_slider_bar.nav_slider_bar')

    <!--Progile Page Badge End here-->

    <!--auction detail slider section start here-->
    <!-- current auction detail section start here-->
    <div class="current-auction-detail-main container acution-de-slider">
        <div class="row">
            <div class="col-md-6 col-lg-6">
                <div class="preview-pic tab-content slider-for">
                    <div class="tab-pane active" id="pic-1"><img src="{{isset($auctions->product_images->image) ? asset('/storage/'.$auctions->product_images->image): asset('/storage/default.jpg')}}" /></div>

                    @if(isset($auctions->product_images_many) && !empty($auctions->product_images_many))
                     @foreach($auctions->product_images_many as $images)

                    <div class="tab-pane" id="num{{ $images->id }}"><img src="{{asset('/storage/'.$images->image)}}" /></div>
                    @endforeach
                    @endif
                    <!-- <div class="tab-pane" id="pic-3"><img src="./images/auction-details-products-slider.jpg" /></div>
                    <div class="tab-pane" id="pic-4"><img src="./images/auction-details-products-slider.jpg" /></div> -->

                </div>
                <div class="for-mob-scrol">
                <ul class="preview-thumbnail nav nav-tabs slider-nav auction_single">
                    <div>
                    {{-- <li class="active"><a data-target="#pic-1" data-toggle="tab"><img src="{{isset($auctions->product_images->image) ? asset('/storage/'.$auctions->product_images->image): asset('/storage/default.jpg')}}" /></a></li> --}}
                    @if(isset($auctions->product_images_many) && !empty($auctions->product_images_many))
                     @foreach($auctions->product_images_many as $key => $images)
                    <li class="{{ $key == 0 ? 'active':''}}"><a data-target="#num{{ $images->id }}" data-toggle="tab"><img src="{{asset('/storage/'.$images->image)}}" /></a></li>
                    @endforeach
                    @endif
                    <!-- <li><a data-target="#pic-3" data-toggle="tab"><img src="./images/auction-details-products-slider.jpg" /></a></li>
                    <li><a data-target="#pic-4" data-toggle="tab"><img src="./images/auction-details-products-slider.jpg" /></a></li> -->
                    </div>
                </ul>
                </div>
            </div>

            <div class="col-md-6 col-lg-6">
                <div class="auction-liosting-detail-seller-info">
                    <h2>Seller information</h2>
                    <div class="auction-liosting-detail-seller-info-inner">
                        <div class="seller-info-inner-thumbnail">
                            <div class="seller-thumbnail">
                                <img src="<?php   echo isset(user_image($auctions->user_id)->image) ? asset('/storage/' . user_image($auctions->user_id)->image) : url('/images/auction-detail-seller-img.png') ?>">
                            </div>
                            <div class="seller-txt">
                                <h3><a href="{{ url('profile') }}/{{ $auctions->users->id}}">{{ $auctions->users->name }}</a></h3>
                                <!-- <p>Member Since 05 Jan 2021</p> -->
                            </div>
                        </div>
                        <!-- <a class="detail-contact-seller" href="#">Chat With Seller</a> -->
                    </div>
                </div>
                <div class="shipment-process-auction-detail">
                    <ul>
                        <li><i class="fa fa-check-circle" aria-hidden="true"></i>Ready to Ship</li>
                        <li><i class="fa fa-check-circle" aria-hidden="true"></i>In stock</li>
                        <li><i class="fa fa-check-circle" aria-hidden="true"></i>Fast Dispatch</li>
                        <li><i class="fa fa-check-circle" aria-hidden="true"></i>Verified</li>
                    </ul>
                </div>
                <div class="current-acc-detail-title">
                    <div class="porduct-uploaded-time">
                        <p><i class="fas fa-clock"></i>Uploaded On {{ $auctions->updated_at }}</p>
                    </div>

                    <h2>{{  $auctions->name }}</h2>
                    <div class="auction-details-reviews">
                        {{-- <div class="auction_star_rating slider_star_rating">
                            <div class="rate">
                                <input type="radio" id="star5" name="rate" value="5">
                                <label for="star5" title="text">5 stars</label>
                                <input type="radio" id="star4" name="rate" value="4">
                                <label for="star4" title="text">4 stars</label>
                                <input type="radio" id="star3" name="rate" value="3">
                                <label for="star3" title="text">3 stars</label>
                                <input type="radio" id="star2" name="rate" value="2">
                                <label for="star2" title="text">2 stars</label>
                                <input type="radio" id="star1" name="rate" value="1">
                                <label for="star1" title="text">1 star</label>
                            </div>
                            <div class="auction_total_reviews slider_total_reviews">
                                <a href="#">3 Review(s)</a>
                            </div>
                        </div> --}}
                        {{-- <div class="review-btnss">
                            <a href="#">Add a Review</a>
                        </div> --}}
                        <div class="single-products-share">
                            <div class="newsletter3">
                                <p>Share:</p>

<div id="social-links"></div>
	<ul>
		{{-- <li><a href="https://www.facebook.com/sharer/sharer.php?u={{url()->current()}}" class="social-button " id=""><i class="fab fa-facebook-f"></i></a></li> --}}
		{{-- <li><a href="https://twitter.com/intent/tweet?text=&amp;url={{url()->current()}}" class="social-button " id=""><i class="fab fa-twitter"></i></a></li> --}}
          <li><a href="{{ $setting->facebook_link }}" class="social-button"><i class="fab fa-facebook-f"></i></a></li>
                    <li><a href="{{ $setting->twitter_link }}" class="social-button"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="{{ $setting->instagram_link }}" class="social-button"><i class="fab fa-instagram"></i></a></li>
                    <li><a href="{{ $setting->tiktok_link }}" class="social-button"><i class="fab fa-tiktok fa-2x"></i></a></li>
	</ul>
</div>
            <?php
            //     $formatted_dt1= \Carbon\Carbon::parse($categorydatas->start_date." ".$categorydatas->start_time);
            //   $formatted_dt2= \Carbon\Carbon::parse($categorydatas->end_date." ".$categorydatas->end_time);
            $formatted_dt1=Carbon::now();
            $formatted_dt2=Carbon::parse($auctions->end_date." ".$auctions->end_time);


            //   $date_diff=$formatted_dt1->diffInDays($formatted_dt2);

            //   $week_diff=$formatted_dt1->diffInWeeks($formatted_dt2);


            //   $hours_diff = $formatted_dt1->diffInHours($formatted_dt2);
            // //  echo $hours_diff.' Hours '; //48 Hours


            //   $Minutesdiff = $formatted_dt1->diffInMinutes($formatted_dt2);
            //  // echo $Minutesdiff.' Minutes '; //2880 Minutes



            $seconddiff = $formatted_dt1->DiffInSeconds($formatted_dt2);

                    ?>

@if($auctions->end_auction === 0)
<div id="CountDownTimer" data-timer="{{ $seconddiff }}" style="width: 500px; height: 125px; float: left"></div>
@else
<div>
    <h2 class="text-center">This auction has been ended.</h2>
</div>
@endif
                            </div>

                        </div>
                    </div>
                    <p class="auction-product-des">{{  $auctions->description}}</p>
                    <div class="auction-products-det-auc auction-de">
                        <div class="row no-gutters detial-timei">
                            <div class="col-md-6">
                                <div class="auction-products-det-auc-inner">
                                    <h3>Auction Starts:</h3>
                                    <p>{{  $auctions->start_date}}</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="auction-products-det-auc-inner">
                                    <h3>Auction Ends:</h3>
                                    <p>{{  $auctions->end_date}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row no-gutters detial-timei">
                            <div class="col-md-6">
                                <div class="auction-products-det-auc-inner">
                                    <h3>Count:</h3>
                                    <p>{{  $acutioncount->count() }}</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="auction-products-det-auc-inner">
                                    <h3>Auction ID:</h3>
                                    <p>{{  $auctions->id}}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="bidding-price-auc-single">
                        <h5>Base Price: ${{$auctions->price}}</h5>
                    </div>
                <p>

                LAST BIDDING PRICE: $<span class="bidding_price">{{ ($auctions->bid_price_update!='') ? ($auctions->bid_price_update) : ($auctions->price) }}</span>
                </p>


                    @if($auctions->end_auction === 0)
                    <div class="woo-commerce-btn">
                        <button class="inci dec" onclick="decrement()">-</button>
                        <input id=demoInput type=number min=1 max="" value="{{ ($auctions->bid_price_update!='') ? ($auctions->bid_price_update) : ($auctions->price) }}">
                        <button class="inci incc" onclick="increment()">+</button>

                    </div>

                    <div class="current-acc-place-bid">
                        @if (Auth()->guard('buyer')->check() ||Auth()->guard('seller')->check())
                        <a href="javascript:void(0)"id="add_bid">PLACE A BID</a>
                      @else
                      <a href="{{ route('login')}}" >PLACE A BID</a>
                      @endif

                    </div>

                    <div class="wishlist-single">
                        <a href="javascript:;" class="wishlist_ajax"
                           data-id="{{$auctions->id}}"><i
                                    class="far fa-heart"></i></a>
                    </div>
                    @endif








                </div>
            </div>

        </div>
    </div>

    <!-- current auction detail section End here-->

    <!-- auction details slider section end here-->



    <!-- Auction Products details tabs section-->
    <!-- prdocut description -->
    <div class="container products-details auction-de-tabs">
        <div class="product-inner-wrapper">
            <div class="product-btn">

            </div>

            <div class="col-md-12 product-info">
                <ul id="myTab" class="nav nav-tabs nav_tabs">

                    <li class="active"><a href="#service-one" class="active show" data-toggle="tab">Description</a></li>
                    <li><a href="#service-two" data-toggle="tab">Shipping Information</a></li>
                    <li><a href="#service-three" data-toggle="tab">Review</a></li>
                    <li><a href="#service-four" data-toggle="tab">Return Policy</a></li>


                </ul>
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade active show" id="service-one">

                        <section class="auction-product-info-deta">
                            <p>{{  $auctions->description}}</p>
                        </section>
                        <div class="featureds-auction-details">
                            <h2>Features:</h2>
                            <div class="auction-products-det-auc">
                                <div class="row no-gutters detial-timei">
                                    <div class="col-md-6">
                                        <div class="auction-products-det-auc-inner">
                                            <h3>Auction Starts:</h3>
                                            <p>{{  $auctions->start_date}}</p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="auction-products-det-auc-inner">
                                            <h3>Auction Ends:</h3>
                                            <p>{{  $auctions->end_date}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row no-gutters detial-timei">
                                    <div class="col-md-6">
                                        <div class="auction-products-det-auc-inner">
                                            <h3>Count:</h3>
                                            <p>{{  $acutioncount->count() }}</p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="auction-products-det-auc-inner">
                                            <h3>Auction ID:</h3>
                                            <p>{{  $auctions->id }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="service-two">

                        <section class="auction-product-info-deta">
                            <p>{{ $setting->shipping_deatils}}</p>
                        </section>
                        <div class="featureds-auction-details">
                            <h2>Features:</h2>
                            <div class="auction-products-det-auc">
                                <div class="row no-gutters detial-timei">
                                    <div class="col-md-6">
                                        <div class="auction-products-det-auc-inner">
                                            <h3>Auction Starts:</h3>
                                            <p>{{  $auctions->start_date}}</p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="auction-products-det-auc-inner">
                                            <h3>Auction Ends:</h3>
                                            <p>{{  $auctions->end_date}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row no-gutters detial-timei">
                                    <div class="col-md-6">
                                        <div class="auction-products-det-auc-inner">
                                            <h3>Count:</h3>
                                            <p>{{  $acutioncount->count() }}</p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="auction-products-det-auc-inner">
                                            <h3>Auction ID:</h3>
                                            <p>{{  $auctions->id }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    </div>
                    </div>
                    <div class="tab-pane fade" id="service-three">

                             <div class="bg-white rounded shadow-sm p-4 mb-4 restaurant-detailed-ratings-and-reviews">
                                {{-- <a href="#" class="btn btn-outline-primary btn-sm float-right">Top Rated</a> --}}
                                <h5 class="mb-1">All Ratings and Reviews</h5>
                                @if(!empty($reviewall) &&  count($reviewall)!=0)
                                @foreach ($reviewall as $reviewdata)
                                <div class="reviews-members pt-4 pb-4">
                                    <div class="media">
                                        <a href="#"><img alt="Generic placeholder image" src="<?php   echo isset(user_image($reviewdata->user_id)->image) ? asset('/storage/'.user_image($reviewdata->user_id)->image) : url('/images/auction-detail-seller-img.png') ?>" class="mr-3 rounded-pill"></a>
                                        <div class="media-body">
                                            <div class="reviews-members-header">
                                                <div class="auction_star_rating slider_star_rating">

                                                <h6 class="mb-1"><a class="text-black" href="{{ url('profile') }}/{{ $reviewdata->user_id}}">{{ $reviewdata->users->name }}</a></h6>
                                                <p class="text-gray">{{ $reviewdata->created_at }}</p>
                                                <p>
                                                  <div class="rate">
                                                    @for($i=1;$i<=5;$i++)
                                                    <input type="radio" id="star{{ $i }}" name="rate" value="{{ $i }}" @if(round($reviewdata->rate)==$i) checked  @endif>
                                                    <label for="star{{ $i }}" title="text">{{ $i }} stars</label>
                                                    @endfor
                                                      </span>
                                                    </div>
                                                </p>
                                            </div>
                                            <div class="reviews-members-body">
                                                <p>{{ $reviewdata->comment }}</p>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @else
                            <p>No Review found</p>
                           @endif
                            </div>

                    </div>



                    <div class="tab-pane fade" id="service-four">

                        <section class="auction-product-info-deta">
                            <p>This is dummy copy. It is not meant to be read. It has been placed here solely to demonstrate the look and feel of finished, typeset text. Only for show. He who searches for meaning here. It has been placed here solely to demonstrate the look.  That being the case, there is really no point in your continuing to read them. After all, you have many other things you should be doing. To demonstrate the look and feel of finished, typeset text. Only for show. He who searches for meaning here will be sorely disappointed. These words are here to protvide the reader with a basic impression of how actual text will appear in its final presentation. Think of them merely as actors on a paper stage, in a performance devoid of content yet rich in form. That being the case, there is really no point in your continuing to read them. After all, you have many other things you should be doing. To demonstrate the look and feel of finished, typeset text. Only for show. He who searches for meaning here will be sorely disappointed. These words are here to protvide the reader with a basic impression of how actual text will appear in its final presentation.</p>
                        </section>
                        <div class="featureds-auction-details">
                            <h2>Features:</h2>
                            <div class="auction-products-det-auc">
                                <div class="row no-gutters detial-timei">
                                    <div class="col-md-6">
                                        <div class="auction-products-det-auc-inner">
                                            <h3>Auction Starts:</h3>
                                            <p>{{  $auctions->start_date}}</p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="auction-products-det-auc-inner">
                                            <h3>Auction Ends:</h3>
                                            <p>{{  $auctions->end_date}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row no-gutters detial-timei">
                                    <div class="col-md-6">
                                        <div class="auction-products-det-auc-inner">
                                            <h3>Count:</h3>
                                            <p>{{  $acutioncount->count() }}</p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="auction-products-det-auc-inner">
                                            <h3>Auction ID:</h3>
                                            <p>{{  $auctions->id }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>







        </div>
    </div>
    <!--product description-->

    <!-- Auction Products Details Tabs Section end here-->



    <!-- Search Off Acution Start here -->
    <div class="auction-fillter-wrapper" id="auction-details-biding">
        <div class="container">




            <div class="row auction-page-products">
                <div class="best_selling_row">
                    <div class="tr_p_head">
                        <h3>Related Auctions</h3>
                    </div>
                    @if(($categorydata->isNotEmpty()))
                    <div class="container">
                        <div class="row">
                            <div class="best_selling_left">



                                    <div class="best_selling_slider owl-carousel owl-theme">

                                    <?php $product_count = count($categorydata);   $loop_num =$product_count = round(count($categorydata)); $no=4;  ?>
                                     @foreach($categorydata as $categorydatas)

                                        <?php
                                        if($no==4){
                                            $no=0;
                                        echo "<div class='item'>";
                                    }
                                          ?>
                                     <div class="best_selling_single">
                                            <div class="best_selling_slider_img">
                                                <img src="{{isset($categorydatas->product_images->image) ? asset('/storage/'.$categorydatas->product_images->image): asset('/storage/default.jpg')}}">
                                            </div>
                                            <div class="auction_slider_text best_selling_slider_text">
                                                <div class="auction-page-cat">
                                                    <p>Auction Category</p>
                                                </div>
                                                <div class="auction_text">
                                                    <h2>{{ $categorydatas->name }}</h2>
                                                    <p>{{ $categorydatas->description }}</p>
                                                    <div class="current-biding-acution">
                                                        <h4>Current Bid: <span class="current-biding-acution-price">${{ $categorydatas->price }}</span></h4>
                                                    </div>
                                                    <?php
                                                //     $formatted_dt1= \Carbon\Carbon::parse($categorydatas->start_date." ".$categorydatas->start_time);
                                                //   $formatted_dt2= \Carbon\Carbon::parse($categorydatas->end_date." ".$categorydatas->end_time);
                                                  $formatted_dt1=Carbon::now();
                                                  $formatted_dt2=Carbon::parse($categorydatas->end_date." ".$categorydatas->end_time);


                                                  $date_diff=$formatted_dt1->diffInDays($formatted_dt2);

                                                  $week_diff=$formatted_dt1->diffInWeeks($formatted_dt2);


                                                  $hours_diff = $formatted_dt1->diffInHours($formatted_dt2);
                                                //  echo $hours_diff.' Hours '; //48 Hours


                                                  $Minutesdiff = $formatted_dt1->diffInMinutes($formatted_dt2);
                                                 // echo $Minutesdiff.' Minutes '; //2880 Minutes



                                                  $seconddiff = $formatted_dt1->DiffInSeconds($formatted_dt2);
                                                 // echo $seconddiff.' Seconds '; //172800  Seconds
                                                    // $period =  \Carbon\CarbonPeriod::create($start,$end);

                                                    // dump($period);
                                                    //     // Iterate over the period
                                                    //     foreach ($period as $date) {
                                                    //         echo $date->format('Y-m-d');
                                                    //     }

                                                    //     // Convert the period to an array of dates
                                                    //     $dates = $period->toArray();
                                                        ?>
                                                    <div class="auction-products-page-days">
                                                        <div class="c-same weeks-auction">
                                                            <div class="weeks-txt">
                                                                <p><span class="ccounter">{{ $week_diff }}</span></p>
                                                                <h6>Weeks</h6>
                                                            </div>
                                                        </div>
                                                        <div class="c-same weeks-auction">
                                                            <div class="weeks-txt">
                                                                <p><span class="ccounter">{{ $date_diff }}</span></p>
                                                                <h6>Days</h6>
                                                            </div>
                                                        </div>
                                                        <div class="c-same weeks-auction">
                                                            <div class="weeks-txt">
                                                                <p><span class="ccounter">{{ $hours_diff }}</span></p>
                                                                <h6>Hours</h6>
                                                            </div>
                                                        </div>
                                                        <div class="c-same weeks-auction">
                                                            <div class="weeks-txt">
                                                                <p><span class="ccounter">{{ $Minutesdiff }}</span></p>
                                                                <h6>Minutes</h6>
                                                            </div>
                                                        </div>
                                                        <div class="c-same weeks-auction">
                                                            <div class="weeks-txt">
                                                                <p><span class="ccounter">{{ $seconddiff }}</span></p>
                                                                <h6>Seconds</h6>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tr_single_overlay best_selling_overlay">
                                                <div class="auction_wishlist tr_s_wishlist best_selling_wishlist">
                                                    <a href="#"><i class="far fa-heart"></i></a>
                                                </div>
                                                <div class="main_slider_btns tr_single_btns best_selling_btns">
                                                    <a href="{{ route('auction-single', ['id' => $categorydatas->id]) }}">Bid Now</a>
                                                    <!--  -->
                                                </div>
                                            </div>
                                        </div>
                                        <?php if($no==3){ echo '</div>'; $no++; } else { $no++; }?>

                                            @endforeach
                                            </div>



                                </div>
                            </div>

                        </div>
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Search Of Auction end here -->



    <!-- Auction You May Also Like Start here-->
    <div class="auction-fillter-wrapper" id="auction-you-like">
        <div class="container">




            <div class="row auction-page-products">
                <div class="best_selling_row">
                    <div class="tr_p_head">
                        <h3>Auction you may also like</h3>
                    </div>
                    @if(($categorydata->isNotEmpty()))
                    <div class="container">
                        <div class="row">
                            <div class="best_selling_left">

                                <div class="best_selling_slider owl-carousel owl-theme">

                                    <?php $product_count = count($categorydata);   $loop_num =$product_count = round(count($categorydata)); $no=2;  ?>
                                     @foreach($categorydata as $categorydatas)

                                        <?php
                                        if($no==2){
                                            $no=0;
                                        echo "<div class='item'>";
                                    }
                                          ?>

                                    <div class="best_selling_single">
                                        <div class="best_selling_slider_img">
                                            <img src="{{isset($categorydatas->product_images->image) ? asset('/storage/'.$categorydatas->product_images->image): asset('/storage/default.jpg')}}">
                                        </div>
                                        <div class="auction_slider_text best_selling_slider_text">
                                            <div class="auction-page-cat">
                                                <p>Auction Category</p>
                                            </div>
                                            <div class="auction_text">
                                                <h2>{{ $categorydatas->name }}</h2>
                                                <p>{{ $categorydatas->description }}</p>
                                                <div class="current-biding-acution">
                                                    <h4>Current Bid: <span class="current-biding-acution-price">${{ $categorydatas->price }}</span></h4>
                                                </div>
                                                <?php
                                            //     $formatted_dt1= \Carbon\Carbon::parse($categorydatas->start_date." ".$categorydatas->start_time);
                                            //   $formatted_dt2= \Carbon\Carbon::parse($categorydatas->end_date." ".$categorydatas->end_time);
                                            $formatted_dt1=Carbon::now();
                                            $formatted_dt2=Carbon::parse($categorydatas->end_date." ".$categorydatas->end_time);


                                            $date_diff=$formatted_dt1->diffInDays($formatted_dt2);

                                            $week_diff=$formatted_dt1->diffInWeeks($formatted_dt2);


                                            $hours_diff = $formatted_dt1->diffInHours($formatted_dt2);
                                            //  echo $hours_diff.' Hours '; //48 Hours


                                            $Minutesdiff = $formatted_dt1->diffInMinutes($formatted_dt2);
                                            // echo $Minutesdiff.' Minutes '; //2880 Minutes



                                            $seconddiff = $formatted_dt1->DiffInSeconds($formatted_dt2);

                                                    ?>
                                                <div class="auction-products-page-days">
                                                    <div class="c-same weeks-auction">
                                                        <div class="weeks-txt">
                                                            <p><span class="ccounter">{{ $week_diff }}</span></p>
                                                            <h6>Weeks</h6>
                                                        </div>
                                                    </div>
                                                    <div class="c-same weeks-auction">
                                                        <div class="weeks-txt">
                                                            <p><span class="ccounter">{{ $date_diff }}</span></p>
                                                            <h6>Days</h6>
                                                        </div>
                                                    </div>
                                                    <div class="c-same weeks-auction">
                                                        <div class="weeks-txt">
                                                            <p><span class="ccounter">{{ $hours_diff }}</span></p>
                                                            <h6>Hours</h6>
                                                        </div>
                                                    </div>
                                                    <div class="c-same weeks-auction">
                                                        <div class="weeks-txt">
                                                            <p><span class="ccounter">{{ $Minutesdiff }}</span></p>
                                                            <h6>Minutes</h6>
                                                        </div>
                                                    </div>
                                                    <div class="c-same weeks-auction">
                                                        <div class="weeks-txt">
                                                            <p><span class="ccounter">{{ $seconddiff }}</span></p>
                                                            <h6>Seconds</h6>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="tr_single_overlay best_selling_overlay">
                                            <div class="auction_wishlist tr_s_wishlist best_selling_wishlist">
                                                <a href="#"><i class="far fa-heart"></i></a>
                                            </div>
                                            <div class="main_slider_btns tr_single_btns best_selling_btns">
                                                <a href="{{ route('auction-single', ['id' => $categorydatas->id]) }}">Bid Now</a>
                                                <!--  -->
                                            </div>
                                        </div>
                                    </div>
                                                                            <?php if($no==1){ echo '</div>'; $no++; } else { $no++; }?>

                                        @endforeach

                                    </div>

                                </div>
                            </div>

                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset('js/share.js') }}"></script>
    <script src="{{ asset('auction_timper/TimeCircles.js') }}"></script>
    <script>




        $( "#add_bid" ).on( "click", function( event ) {
        var price=$('#demoInput').val();
        $.ajax({
                    url: "{{url('buyer/add_bid')}}",
                    type: "GET",
                    data: {price:price,product_id:{{ $auctions->id }} },
                    success: function (response) {

//  console.log(response);

                        if(response.type=="error"){
                            swal({
                            title: "error!",
                            icon: "warning",
                            text: response.message,
                            type: "error",
                          //  timer: 1000
                        });
                        }
                        else if(response.type == "success"){
                            swal({
                            title: "Success!",
                            icon: "success",
                            text: response.message,
                            type: "success",
                          //  timer: 1000
                        });
                            $('.bidding_price').html(price);

                        }
                        console.log(response);
                        // alert(response);

                    }
                });

        })
        function increment() {
            document.getElementById('demoInput').stepUp();
        }
        function decrement() {
            document.getElementById('demoInput').stepDown();
        }
    </script>
    <script>
        $("#DateCountdown").TimeCircles();
        $("#CountDownTimer").TimeCircles({
            time: {
                Days: {
                    show: true,
                    color:"#2393d1",

                },
                Hours: {
                    show: true,
                    color:"#2393d1",
                },
                Minutes: {
                    show: true,
                    color:"#2393d1",
                },
                Seconds: {
                    show: true,
                    color:"#2393d1",
                }


            }
        });
        $("#PageOpenTimer").TimeCircles();
        $("#CountDownTimer").TimeCircles({count_past_zero: false});
        // $("#CountDownTimer").TimeCircles({count_past_zero: false}).addListener(countdownComplete);

        // function countdownComplete(){
        //     alert($('#CountDownTimer').data('timer'));
        //         // if($('#CountDownTimer').data('timer') <= 0){
        //         //     $(this).fadeOut('slow').replaceWith("<h2>Time's Up!</h2>");
        //         // }

        // }



        jQuery("#auction-you-like .best_selling_slider").owlCarousel({
    loop:false,
    items: 1,
    // margin:10,
    dots: false,
    nav: true,
    navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
    autoplay: false,
    autoplayTimeout: 3000,
    responsiveClass:true,
    autoplaySpeed: 2000,
  });




        jQuery("#auction-details-biding .best_selling_slider").owlCarousel({
    loop:false,
    items: 1,
    // margin:10,
    dots: false,
    nav: true,
    navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
    autoplay: false,
    autoplayTimeout: 3000,
    responsiveClass:true,
    autoplaySpeed: 2000,
  });
    </script>
@stop




