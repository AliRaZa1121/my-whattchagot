@extends('layouts.app2')

@section('content')

    <!--Profile Page Badge-->
    <div class="profile-page-badge">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="profile-badge-txt">
                        <ul>
                            <li>Home</li>
                            <li>Profile</li>
                            <li>{{  $user->name }}</li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <!--trade Products Row Start here-->
    <div class="trending_products profile-page-trade" id="my-profile">
        <div class="container">
            <div class="row">
                <div class="profile-side-br">
                    <div class="profile-side-inner-wrapper">
                        <div class="profile-imge-side">
                            <img src="<?php   echo isset(user_image($user->id)->image) ? asset('/storage/'.user_image($user->id)->image) : url('/images/auction-detail-seller-img.png') ?>">

                        </div>
                        <div class="profile-content-wrapper">
                            <div class="auction_wishlist tr_s_wishlist">
{{--                                <a href="#"><i class="fa fa-heart" aria-hidden="true"></i></a>--}}
                            </div>
                            <div class="profile-title">
                                <h2>{{  $user->name }}</h2>
                            </div>
                        <?php

//                                                            print_r(reviewbycount($key->id));
                                                            $review_val = reviewbycount_user($user->id);
//                                                            print_r($review_val);
                                                            if(!empty($review_val)){
                                                             $review_data = $review_val['rate']/$review_val['rate_count'];
//                                                             echo $review_data;
                                                            }else{
                                                                $review_data=0;
                                                            }
                                                            ?>
                            <div class="auction_star_rating slider_star_rating" >
                                <div class="rate">
                                    @for($i=1;$i<=5;$i++)
                                        <input type="radio" id="star{{ $i }}" name="rate" value="{{ $i }}"
                                               @if( $review_data>=$i) checked @endif>
                                        <label for="star{{ $i }}" class="@if( $review_data>=$i) start_select  @endif"
                                               title="text">
                                            {{ $i }} stars</label>
                                    @endfor
                                </div>

                                <div class="auction_total_reviews slider_total_reviews">
                                    &nbsp;&nbsp;{{$review_val['rate_count'] ?? '0'}} Review(s)
                                </div>

                            </div>

                            <div class="profile-content-p">
                                <p></p>
                            </div>
                        </div>
                    </div>

                    <div class="profile-sdie-add">
                        <img src="{{ url('/images/profile-side-add.jpg') }}">
                    </div>


                </div>
                <div class="trending_products_wrapper">
                    <div class="tr_p_head">
                        <h3>Trade PRODUCTS</h3>
                    </div>
                    @if($products_trade->count()!=0)
                    <div class="trending_products_slider owl-carousel owl-theme">

                        <?php $product_count = count($products_trade);   $loop_num =$product_count = round(count($products_trade)); $no=6;  ?>
                        @forelse ($products_trade as $sellersdata)

                            <?php
                            if($no==6){
                                $no=0;
                                echo "<div class='item'>";
                            }
                            ?>
                            <div class="trending_single_wrapper">
                                <div class="trending_single_product">
                                    <div class="ts_label">New</div>
                                    <img src="{{isset($sellersdata->product_images->image) ? asset('/storage/'.$sellersdata->product_images->image): asset('/storage/default.jpg')}}">
                                    <h3>{{ $sellersdata->name }}</h3>
                                    {{-- <span class="ts_price">$45.00</span> --}}
                                </div>
                                <div class="tr_single_overlay">
                                    <div class="auction_wishlist tr_s_wishlist">
                                        <a href="javascript:;" class="wishlist_ajax"
                                           data-id="{{$sellersdata->id}}"><i
                                                    class="far fa-heart"></i></a>
                                    </div>
                                    <div class="main_slider_btns tr_single_btns best_selling_btns">


                                            <a href="{{$sellersdata->flags == 'trade' ? route('trading-single',$sellersdata->id) : url('product-details').'/'.$sellersdata->id }}">view Product</a>

                                    </div>
                                </div>
                            </div>
                            <?php if($no==5){ echo '</div>'; $no++; } else { $no++; }?>
                            @empty
                                <p style="text-align: center">No Products Available</p>
                            @endforelse




                        </div>

                        @else
                        <p>No Products</p>

                    @endif

                    </div>
                </div>

                <!--top products Start here-->
                <div class="trending_products_wrapper top-products-profile">
                    <div class="tr_p_head">
                        <h3>Top PRODUCTS</h3>
                    </div>
                    <div class="trending_products_slider owl-carousel owl-theme">
                        @if($sellers->count() != 0)
                        <?php $product_count = count($products_trade);   $loop_num =$product_count = round(count($sellers)); $no=3;  ?>
                        @forelse ($sellers as $sellersdata)

                            <?php
                            if($no==3){
                                $no=0;
                                echo "<div class='item'>";
                            }
                            ?>
                                <div class="trending_single_wrapper">
                                    <div class="trending_single_product">

                                        <img src="{{isset($sellersdata->product_images->image) ? asset('/storage/'.$sellersdata->product_images->image): asset('/storage/default.jpg')}}">
                                        <h3>{{ $sellersdata->name }}</h3>
                                        {{-- <span class="ts_price">$45.00</span> --}}
                                    </div>
                                    <div class="tr_single_overlay">
                                        <div class="auction_wishlist tr_s_wishlist">
                                            <a href="javascript:;" class="wishlist_ajax"
                                               data-id="{{$sellersdata->id}}"><i
                                                        class="far fa-heart"></i></a>
                                        </div>
                                        <div class="main_slider_btns tr_single_btns best_selling_btns">


                                                <a href="{{$sellersdata->flags == 'trade' ? route('trading-single',$sellersdata->id) : url('product-details').'/'.$sellersdata->id }}">view Product</a>

                                        </div>
                                    </div>
                                </div>
                                <?php if($no==2){ echo '</div>'; $no++; } else { $no++; }?>
                            @empty
                                <p style="text-align: center">No Products Available</p>
                                @endforelse
                            @else
                            <p style="text-align: center">No Products Available</p>

                        @endif
                    </div>
                </div>

                <!--Top Products End here-->

            </div>
        </div>
    </div>
@stop




