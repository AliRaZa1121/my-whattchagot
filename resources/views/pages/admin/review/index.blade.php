@extends('layout.default')
@php
    if (Auth::guard('admin')->check()) {
        $role='';
        } else if (Auth::guard('seller')->check()) {
                $role='seller_';

        } else if (Auth::guard('buyer')->check()) {
            $role='buyer_';

        }
        $page_breadcrumbs=[[
        'page'=>route($role.'dashboard'),
        'title'=>'Dashboard'
        ],
        [
        'page'=>route($role.'review.index'),
        'title'=>'Review'
        ]
        ];
@endphp
@section('styles')
    <link rel="stylesheet" href="{{asset('plugins/custom/datatables/datatables.bundle.css?v=7.0.4')}}">
@endsection
@section('content')
    @include('pages.message')

    <style>
        /*.rate:not(:checked) > label {*/
        /*    float: right;*/
        /*    width: 1em;*/
        /*    overflow: hidden;*/
        /*    white-space: nowrap;*/
        /*    cursor: pointer;*/
        /*    font-size: 20px;*/
        /*    color: #ccc;*/
        /*    line-height: 1;*/
        /*}*/
        /*.rate:not(:checked) > label:before {*/
        /*    content: '★ ';*/
        /*}*/

        .acidjs-rating-stars,
        .acidjs-rating-stars label::before {
            display: inline-block;
        }

        .acidjs-rating-stars label:hover,
        .acidjs-rating-stars label:hover ~ label {
            color: #f5b301;
        }

        .acidjs-rating-stars * {
            margin: 0;
            padding: 0;
        }

        .acidjs-rating-stars input {
            display: none;
        }

        .acidjs-rating-stars {
            unicode-bidi: bidi-override;
            direction: rtl;
        }

        .acidjs-rating-stars label {
            color: #ccc;
        }

        .acidjs-rating-stars label::before {
            content: "\2605";
            width: 18px;
            line-height: 18px;
            text-align: center;
            font-size: 18px;
            cursor: pointer;
        }

        .acidjs-rating-stars input:checked ~ label {
            color: #f5b301;
        }

        .acidjs-rating-disabled {
            opacity: .50;

            -webkit-pointer-events: none;
            -moz-pointer-events: none;
            pointer-events: none;
        }
    </style>
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Reviews List
                </h3>
            </div>
            <div class="card-toolbar">

            </div>
        </div>
        <div class="card-body">
            <!--begin: Search Form-->
            <!--begin::Search Form-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Product name</th>
                    <th>Price</th>
                    <th>Qty</th>
                    <th>Total Price</th>
                    <th>Customer Name</th>
                    @if(Auth()->guard('admin')->check())

                        <th>Seller Name</th>
                    @endif
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($order as $p)
                    <tr>
                        <td>{{$p->id}}</td>
                        <td>{{$p->products->name}}</td>
                        <td>${{$p->price}}</td>
                        <td>{{$p->qty}}</td>
                        <td>${{$p->price*$p->qty}}</td>
                        <td>{{$p->coustomer_name->name}}</td>
                        @if(Auth()->guard('admin')->check())
                            <td>{{$p->seller_name->name}}</td>
                        @endif
                        <td>{{$p->status}}</td>
                        <td>
                        <span style="overflow: visible; position: relative; width: 125px;">
                            <div class="dropdown dropdown-inline "> <a href="javascript:;"
                                                                       class="btn btn-sm btn-light btn-text-primary btn-icon mr-2"
                                                                       data-toggle="dropdown">
                                    <span class="svg-icon svg-icon-md"> <svg xmlns="http://www.w3.org/2000/svg"
                                                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                             width="24px" height="24px"
                                                                             viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                <path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z"
                                                      fill="#000000"></path>
                                            </g>
                                        </svg> </span> </a>
                                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                    <ul class="navi flex-column navi-hover py-2">
                                        <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2">
                                            Choose an action: </li>


                                        @if($p->comment==null)
                                            <li class="navi-item">
                                                <a data-toggle="modal"
                                                   onclick='model_data({{$p->id}},"{{$p->products->name}}",{{$p->product_id}},{{$p->user_id}},{{$p->seller_id}})'
                                                   data-target=".bd-example-modal-sm"
                                                   class="navi-link">
                                                    <span class="navi-icon">
                                                        <i class="fa fa-check-circle"></i>
                                                    </span>
                                                    <span class="navi-text">Reviews</span>
                                                </a>
                                             </li>
                                        @endif

                                        @if($p->comment!=null)
                                            <li class="navi-item">
                                                <a data-toggle="modal"
                                                   onclick='model_data1("{{$p->comment}}","{{$p->rate}}","{{$p->products->name}}")'
                                                   data-target=".bd-example-modal-sm-view"
                                                   class="navi-link">
                                                    <span class="navi-icon">
                                                        <i class="la la-eye"></i>
                                                    </span>
                                                    <span class="navi-text">view Reviews</span>
                                                </a>
                                             </li>
                                        @endif




                                    </ul>
                                </div>
                            </div>
                        </span>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="product_name">New message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{route('review.store')}}">
                    <div class="modal-body">

                        @csrf
                        <input type="hidden" name="user_id" id="user_id">
                        <input type="hidden" name="seller_id" id="seller_id">
                        <input type="hidden" name="product_id" id="product_id">
                        <input type="hidden" name="order_id" id="order_id">

                        <p>Star</p>
                        <div class="acidjs-rating-stars">
                            <input type="radio" name="rate" id="group-2-0" value="5"/><label
                                    for="group-2-0"></label><!--
        --><input type="radio" name="rate" id="group-2-1" value="4"/><label for="group-2-1"></label><!--
        --><input type="radio" name="rate" id="group-2-2" value="3"/><label
                                    for="group-2-2"></label><!--
        --><input type="radio" name="rate" id="group-2-3" value="2"/><label for="group-2-3"></label><!--
        --><input type="radio" name="rate" id="group-2-4" value="1"/><label for="group-2-4"></label>

                        </div>

                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Comment:</label>
                            <textarea class="form-control" name="comment" id="message-text"></textarea>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-sm-view" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="product_name2">New message</h5>
                    <button type="button" class="close" data-dismiss="modal_view" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">


                    <p>Star</p>
                    <div class="acidjs-rating-stars">
                        <input type="radio" name="rate" disabled id="rate1" id="group-2-0" value="5"/><label
                                for="group-2-0"></label><!--
        --><input type="radio" id="rate2" name="rate" disabled id="group-2-1" value="4"/><label for="group-2-1"></label><!--
        --><input type="radio" id="rate3" name="rate" disabled id="group-2-2" value="3"/><label
                                for="group-2-2"></label><!--
        --><input type="radio" name="rate" id="rate4" disabled id="group-2-3" value="2"/><label for="group-2-3"></label><!--
        --><input type="radio" name="rate" id="rate5" disabled id="group-2-4" value="1"/><label for="group-2-4"></label>

                    </div>

                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Comment:</label>
                        <textarea class="form-control" disabled name="comment" id="comment"></textarea>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
{{--                    <input type="submit" class="btn btn-primary">--}}
                </div>
                </form>
            </div>
        </div>
    </div>


@endsection

{{-- Styles Section --}}
@section('styles')

@endsection

@section('scripts')
    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js?v=7.0.4') }}" type="text/javascript"></script>
    <script>
        function model_data1(comment, rate, product_name) {
            $('#product_name2').html(product_name);
            $('#comment').val(comment);
            $('#rate' + rate).prop('checked', true);


        }

        function model_data(id, product_name, product_id, user_id, seller_id) {
            // alert(id);
            // $('.input[name=group-2]').prop('radio',false);

            $('#order_id').val(id);
            $('#product_name').html(product_name);
            $('#product_id').val(product_id);
            $('#user_id').val(user_id);
            $('#seller_id').val(seller_id);

        }
        <?php if(Auth()->guard('admin')->check()) {?>
        var target_coloum = 7;
        <?php }else { ?>
        var target_coloum = 6;

        <?php } ?>
        var table = $('#kt_datatable');

        // begin first table
        table.DataTable({
            // DOM Layout settings
            dom: "<'row py-3'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>" +
                "<'row py-3'<'col-sm-12'tr>>" +
                "<'row py-3'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>", // read more: https://datatables.net/examples/basic_init/dom.html

            columnDefs: [{
                targets: -1,
                title: 'Actions',
                orderable: false,

            },
                {
                    width: '75px',
                    targets: target_coloum,
                    render: function (data, type, full, meta) {
                        var status = {
                            'Pending': {
                                'title': 'Pending',
                                'class': 'label-light-primary'
                            },

                            'Shipping': {
                                'title': 'Shipping',
                                'class': ' label-light-info'
                            },
                            'Warehouse': {
                                'title': 'Warehouse',
                                'class': ' label-light-info'
                            },
                            'Delivered': {
                                'title': 'Delivered',
                                'class': ' label-light-success'
                            },
                            'Canceled': {
                                'title': 'Canceled',
                                'class': ' label-light-danger'
                            },

                        };
                        if (typeof status[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="label label-lg font-weight-bold ' + status[data].class + ' label-inline">' + status[data].title + '</span>';
                    },
                },
            ],
        });
    </script>
@endsection