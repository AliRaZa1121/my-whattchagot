@extends('layout.default')
@php
    if (Auth::guard('admin')->check()) {
        $role='';
        } else if (Auth::guard('seller')->check()) {
                $role='seller_';

        } else if (Auth::guard('seller')->check()) {
            $role='buyer_';

        }
        $page_breadcrumbs=[
        [
        'page'=>route($role.'dashboard'),
        'title'=>'dashboard'
        ],
        [
        'page'=>route('subscription.index'),
        'title'=>'Subscription Create'
        ]
        ];
@endphp
@section('styles')
    <link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <style>
        .label-centered-field {
            display: flex;
            align-items: center;
            margin-top: 8px;
        }

        .time-off-action-row label {
            visibility: hidden;
        }

        .time-off-action-row a {
            display: table-cell;
            padding: 5px;
        }

        .day-activities {
            display: flex;
            align-items: center
        }

        .switch-right-side {
            margin-left: 5px;
        }

        #map {
            height: 400px;
            /* The height is 400 pixels */
            width: 100%;
            /* The width is the width of the web page */
        }

        #reset_location {
            margin-top: 5px;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-custom flex-wrap border-0 pt-6 pb-0">
                <form class="form" id="form" method="POST" action="{{route('subscription.store')}}">
                    @csrf
                    <div class="card card-custom card-sticky" id="kt_page_sticky_card">
                        <div class="card-header" style="">
                            <div class="card-title col-md-9">
                                <h3 class="card-label">New Subscription

                                </h3>
                            </div>
                            <div class="card-toolbar">
                                <a href="{{route('subscription.index')}}"
                                   class="btn btn-secondary font-weight-bold mr-2">
                                    <i class="ki ki-long-arrow-back icon-xs"></i>Cancel
                                </a>
                                <button type="submit" class="btn btn-primary font-weight-bolder">Save Subscription
                                </button>
                            </div>


                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="productName">Subscription Amount</label>
                                        <input type="number" required id="amount" class="form-control" name="amount"
                                               placeholder='1'>
                                    </div>


                                </div>
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="productName">Subscription Amount</label>
                                        <select class="form-control" required name="type"
                                                placeholder='1'>
                                            <option value="">Select type</option>
                                            <option value="yearly">Yearly</option>
                                            <option value="monthly">Monthly</option>
                                        </select>
                                    </div>


                                </div>
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label for="productName">Subscription Amount</label>
                                        <textarea class="form-control" name="description"
                                                  placeholder='1'></textarea>
                                    </div>


                                </div>


                            </div>
                        </div>

                </form>
            </div>

        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('js/jquery-ui.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

    <script>


        const bv = FormValidation.formValidation(
            document.getElementById('form'), {
                fields: {


                    amount: {
                        validators: {
                            notEmpty: {
                                message: 'Subscription Amount is required'
                            }
                        }
                    },
                    type: {
                        validators: {
                            notEmpty: {
                                message: 'Subscription type is required'
                            }
                        }
                    },


                }, plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    submitButton: new FormValidation.plugins.SubmitButton(),
                }
            }
        ).on('core.form.valid', function () {
            // Send the form data to back-end
            // You need to grab the form data and create an Ajax request to send them
            // Get the form instance
            var $form = $('#form');
            // Get the BootstrapValidator instance
            let method = $form.attr('method');
            let url = $form.attr('action');
            let data = new FormData(document.getElementById('form'));
            submitFormAjax(method, url, data, function (res) {

                successAlert(res.msg);
                $('#form').trigger('reset');
                $('.dz-complete').remove();
                $('.needsclick').show();


                window.location.href = "{{route('subscription.index')}}";

            }, function (jqXHR) {
                let errors = jqXHR.responseJSON.errors;
                let erMsgs = ``;
                $.each(Object.keys(errors), function (i, v) {
                    erMsgs += `<p>${i + 1}-${errors[v][0]}</p>`;
                })
                errorAlert(erMsgs);
            }, bv);
        });


        function readURL(input, id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $(id)
                        .attr('src', e.target.result);
                    $('.profile-icon').hide();
                    $(id).show();
                };
                reader.readAsDataURL(input.files[0]);
            }
        }


        $('#features').summernote({
            height: 150,
            popover: {
                image: [

                    // This is a Custom Button in a new Toolbar Area
                    ['custom', ['examplePlugin']],
                    ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                    ['float', ['floatLeft', 'floatRight', 'floatNone']],
                    ['remove', ['removeMedia']]
                ]
            }
        })
            .on('summernote.change', function (customEvent, contents, $editable) {
                // Revalidate the content when its value is changed by Summernote
                bv.revalidateField('features');
            });
        ;
        $('#visit_area').summernote({
            height: 150,
            popover: {
                image: [

                    // This is a Custom Button in a new Toolbar Area
                    ['custom', ['examplePlugin']],
                    ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                    ['float', ['floatLeft', 'floatRight', 'floatNone']],
                    ['remove', ['removeMedia']]
                ]
            }
        }).on('summernote.change', function (customEvent, contents, $editable) {
            // Revalidate the content when its value is changed by Summernote
            bv.revalidateField('visit_area');
        });
        $('#product_balance_notes').summernote({
            height: 150,
            popover: {
                image: [

                    // This is a Custom Button in a new Toolbar Area
                    ['custom', ['examplePlugin']],
                    ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                    ['float', ['floatLeft', 'floatRight', 'floatNone']],
                    ['remove', ['removeMedia']]
                ]
            }
        }).on('summernote.change', function (customEvent, contents, $editable) {
            // Revalidate the content when its value is changed by Summernote
            bv.revalidateField('product_balance_notes');
        });
        $('#producting_contact').summernote({
            height: 150,
            popover: {
                image: [

                    // This is a Custom Button in a new Toolbar Area
                    ['custom', ['examplePlugin']],
                    ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                    ['float', ['floatLeft', 'floatRight', 'floatNone']],
                    ['remove', ['removeMedia']]
                ]
            }
        }).on('summernote.change', function (customEvent, contents, $editable) {
            // Revalidate the content when its value is changed by Summernote
            bv.revalidateField('producting_contact');
        });
        $('#special_notes').summernote({
            height: 150,
            popover: {
                image: [

                    // This is a Custom Button in a new Toolbar Area
                    ['custom', ['examplePlugin']],
                    ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                    ['float', ['floatLeft', 'floatRight', 'floatNone']],
                    ['remove', ['removeMedia']]
                ]
            }
        }).on('summernote.change', function (customEvent, contents, $editable) {
            // Revalidate the content when its value is changed by Summernote
            bv.revalidateField('special_notes');
        });


        $('#about_event').summernote({
            height: 150,
            popover: {
                image: [

                    // This is a Custom Button in a new Toolbar Area
                    ['custom', ['examplePlugin']],
                    ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                    ['float', ['floatLeft', 'floatRight', 'floatNone']],
                    ['remove', ['removeMedia']]
                ]
            }
        })
            .on('summernote.change', function (customEvent, contents, $editable) {
                // Revalidate the content when its value is changed by Summernote
                bv.revalidateField('about_event');
            });


    </script>
@endsection
