@extends('layout.default')
@php
if (Auth::guard('admin')->check()) {
$role='';
} else if (Auth::guard('seller')->check()) {
$role='seller_';

} else if (Auth::guard('seller')->check()) {
$role='buyer_';

}


$page_breadcrumbs=[[
'page'=>route($role.'dashboard'),
'title'=>'dashboard'
],
[
'page'=>route($role.'seller.index'),
'title'=>'Make A Trade'
]
];




@endphp
@section('styles')
<link rel="stylesheet" href="{{asset('plugins/custom/datatables/datatables.bundle.css?v=7.0.4')}}">
@endsection
@section('content')
@include('pages.message')
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Trade List
            </h3>
        </div>

    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Product Name</th>
                    <th>Product Image</th>
                    <th>Buyer Name</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php $count=1;?>
                @foreach($make_trade as $p)

                <tr>
                    <td>{{$count++}}</td>
                    <td><a href="{{ route('trading-single',$p->request_product_id) }}"
                            target="_blank">{{isset($p->trade_with_products->product_images) ?
                            $p->trade_with_products->name: ""}}</a></td>
                    <td>
                        <img src="{{isset($p->trade_with_products->product_images->image) ? asset('/storage/'.$p->trade_with_products->product_images->image): asset('/storage/default.jpg')}}"
                            style="width:100px;max-height:100px;">
                    </td>
                    <td>{{ $p->products->users->name ?? 'N/A' }}</td>
                    <td>{{$p->status}}</td>
                    <td>
                        <span style="overflow: visible; position: relative; width: 125px;">
                            <div class="dropdown dropdown-inline "> <a href="javascript:;"
                                    class="btn btn-sm btn-light btn-text-primary btn-icon mr-2" data-toggle="dropdown">
                                    <span class="svg-icon svg-icon-md"> <svg xmlns="http://www.w3.org/2000/svg"
                                            xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                            viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                <path
                                                    d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z"
                                                    fill="#000000"></path>
                                            </g>
                                        </svg> </span> </a>
                                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                    <ul class="navi flex-column navi-hover py-2">
                                        <li
                                            class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2">
                                            Choose an action: </li>
                                        <li class="navi-item">
                                            <a href="{{ route('maketrade.detail.admin',[$p->request_product_id, $p->user_id , $p->status]) }}" class="navi-link">
                                                <span class="navi-icon">
                                                    <i class="fa fa-check-circle"></i>
                                                </span>
                                                <span class="navi-text">View details</span>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </span>
                    </td>

                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>


@endsection

{{-- Styles Section --}}
@section('styles')

@endsection

@section('scripts')
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js?v=7.0.4') }}" type="text/javascript"></script>
<script>
    <?php if(Auth()->guard('seller')->check()){ ?>
        var title_name ="Actions"

        <?php }else { ?>
            var title_name ="status"
        <?php } ?>
        var table = $('#kt_datatable');

        // begin first table
        table.DataTable({
            // DOM Layout settings
            dom: "<'row py-3'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>" +
                "<'row py-3'<'col-sm-12'tr>>" +
                "<'row py-3'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>", // read more: https://datatables.net/examples/basic_init/dom.html

            columnDefs: [{
                targets: -1,
                title: title_name,
                orderable: false,

            },
                {
                    width: '75px',
                    targets: 8,
                    render: function (data, type, full, meta) {
                        var status = {
                            'Pending': {
                                'title': 'Pending',
                                'class': 'label-light-primary'
                            },

                            'Accept': {
                                'title': 'Accept',
                                'class': ' label-light-success'
                            },
                            'Decline': {
                                'title': 'Decline',
                                'class': ' label-light-danger'
                            },
                            'Cancel': {
                                'title': 'Cancel',
                                'class': 'label-light-primary'
                            },


                        };
                        if (typeof status[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="label label-lg font-weight-bold ' + status[data].class + ' label-inline">' + status[data].title + '</span>';
                    },
                },
            ],
        });
</script>

@endsection
