@extends('layout.default')
@php
if (Auth::guard('admin')->check()) {
$role='';
} else if (Auth::guard('seller')->check()) {
$role='seller_';

} else if (Auth::guard('seller')->check()) {
$role='buyer_';

}


$page_breadcrumbs=[[
'page'=>route($role.'dashboard'),
'title'=>'dashboard'
],
[
'page'=>route($role.'seller.index'),
'title'=>'Make A Trade'
]
];




@endphp
@section('styles')
<link rel="stylesheet" href="{{asset('plugins/custom/datatables/datatables.bundle.css?v=7.0.4')}}">
@endsection
@section('content')
@include('pages.message')
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Trade List
            </h3>
        </div>

    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="mb-17">
            <!--begin::Content-->
            <div class="d-flex flex-stack mb-5">
                <!--begin::Title-->
                <h3 class="text-dark">All Trade Offers</h3>
                <!--end::Title-->
            </div>
            <!--end::Content-->
            <!--begin::Separator-->
            <div class="separator separator-dashed mb-9"></div>
            <!--end::Separator-->
            
            @forelse($make_trade as $key => $p)
            @php
            $details = maketradeDetail($p->request_product_id, $p->user_id, $p->status);
            @endphp
            <!--begin::Row-->
            <div class="row g-10">
                <!--begin::Col-->
                <div class="col-md-10">
                    <!--begin::Hot sales post-->
                    <div class="card-xl-stretch me-md-6">
                        <!--begin::Overlay-->
                        <a class="d-block overlay" data-fslightbox="lightbox-hot-sales" href="{{ route('trading-single',$p->request_product_id) }}">
                            <!--begin::Image-->
                            <div class="overlay-wrapper bgi-no-repeat bgi-position-center bgi-size-cover card-rounded min-h-175px" style="background-image:url('{{isset($p->trade_with_products->product_images->image) ? asset('/storage/'.$p->trade_with_products->product_images->image): asset('/storage/default.jpg')}}')"></div>
                            <!--end::Image-->
                            <!--begin::Action-->
                            <div class="overlay-layer card-rounded bg-dark bg-opacity-25">
                                <i class="bi bi-eye-fill fs-2x text-white"></i>
                            </div>
                            <!--end::Action-->
                        </a>
                        <!--end::Overlay-->
                        <!--begin::Body-->
                        <div class="mt-8">
                            <!--begin::Title-->
                            <a href="{{ route('trading-single',$p->request_product_id) }}" class="fs-4 text-dark fw-bolder text-hover-primary text-dark lh-base">{{ $p->products->users->name ?? 'N/A' }}</a>
                            <!--end::Title-->
                            <!--begin::Text-->
                            <div class="fw-bold fs-5 text-gray-600 text-dark mt-3">We’ve been focused on making a the from also not been eye</div>
                            <!--end::Text-->
                            <!--begin::Text-->
                            <div class="fs-6 fw-bolder mt-5 d-flex flex-stack">
                                <!--begin::Label-->
                                <span class="badge border border-dashed fs-2 fw-bolder text-dark p-2">
                                    <span class="fs-6 fw-bold text-gray-900">{{$p->status}}</span>
                                <!--end::Label-->
                                
                                
                            </div>
                            <!--end::Text-->
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Hot sales post-->
                </div>
                <!--end::Col-->
            </div>
            <!--end::Row-->
            @forelse($details as $key => $value)
            @php
            $seller_product_price =  intval($value->trade_with_products->price);
            $buyer_product_price  = intval($value->products->price) + intval($buyer_product_price);
            $price = abs($seller_product_price - $buyer_product_price);
            @endphp
            <!--begin::Row-->
            <div class="row g-10">
                <!--begin::Col-->
                <div class="col-md-4">
                    <!--begin::Hot sales post-->
                    <div class="card-xl-stretch me-md-6">
                        <!--begin::Overlay-->
                        <a class="d-block overlay" data-fslightbox="lightbox-hot-sales" href="/metronic8/demo1/assets/media/stock/600x400/img-23.jpg">
                            <!--begin::Image-->
                            <div class="overlay-wrapper bgi-no-repeat bgi-position-center bgi-size-cover card-rounded min-h-175px" style="background-image:url('/metronic8/demo1/assets/media/stock/600x400/img-23.jpg')"></div>
                            <!--end::Image-->
                            <!--begin::Action-->
                            <div class="overlay-layer card-rounded bg-dark bg-opacity-25">
                                <i class="bi bi-eye-fill fs-2x text-white"></i>
                            </div>
                            <!--end::Action-->
                        </a>
                        <!--end::Overlay-->
                        <!--begin::Body-->
                        <div class="mt-5">
                            <!--begin::Title-->
                            <a href="#" class="fs-4 text-dark fw-bolder text-hover-primary text-dark lh-base">25 Products Mega Bundle with 50% off discount amazing</a>
                            <!--end::Title-->
                            <!--begin::Text-->
                            <div class="fw-bold fs-5 text-gray-600 text-dark mt-3">We’ve been focused on making a the from also not been eye</div>
                            <!--end::Text-->
                            <!--begin::Text-->
                            <div class="fs-6 fw-bolder mt-5 d-flex flex-stack">
                                <!--begin::Label-->
                                <span class="badge border border-dashed fs-2 fw-bolder text-dark p-2">
                                    <span class="fs-6 fw-bold text-gray-400">$</span>28</span>
                                <!--end::Label-->
                                
                            </div>
                            <!--end::Text-->
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Hot sales post-->
                </div>
                <!--end::Col-->
                
            </div>
            <!--end::Row-->                
            @empty
                
            @endforelse

            @empty
            <div>
                <img class="center" src="{{ asset('images/no-data-found.png') }}" style="margin-left: 200px;" />
            </div>
            @endforelse
            

            <!--begin::Separator-->
            <div class="separator separator-dashed mb-9"></div>
            <!--end::Separator-->
            
        </div>
    </div>
</div>


@endsection

