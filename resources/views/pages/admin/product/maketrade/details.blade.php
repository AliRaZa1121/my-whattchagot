@php
    $buyer_product_price  =  0;
    $price = 0;
@endphp
@extends('layout.default')
@section('styles')
<link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<style>
    .label-centered-field {
        display: flex;
        align-items: center;
        margin-top: 8px;
    }

    .time-off-action-row label {
        visibility: hidden;
    }

    .time-off-action-row a {
        display: table-cell;
        padding: 5px;
    }

    .day-activities {
        display: flex;
        align-items: center
    }

    .switch-right-side {
        margin-left: 5px;
    }

    #map {
        height: 400px;
        /* The height is 400 pixels */
        width: 100%;
        /* The width is the width of the web page */
    }

    #reset_location {
        margin-top: 5px;
    }
</style>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card mb-5 mb-xl-10" id="kt_profile_details_view">
            <!--begin::Card header-->
            <div class="card-header" style="">
                <div class="card-title col-md-9">
                    <h3 class="card-label">New Trading Product
                        <!-- <i class="mr-2"></i> -->
                        <!-- <small class="">try to scroll the page</small> -->
                    </h3>
                </div>
                <div class="card-toolbar">
                    <a href="{{ Auth::guard('admin')->check() ? route('maketrade.admin') : route('maketrade')  }}" class="btn btn-secondary font-weight-bold mr-2">
                        <i class="ki ki-long-arrow-back icon-xs"></i>Cancel
                    </a>
                </div>


            </div>
            <!--begin::Card header-->
            @foreach ($make_trade as $p)
            @php

               $seller_product_price =  intval($p->trade_with_products->price);

               $buyer_product_price  = intval($p->products->price) + intval($buyer_product_price);
               $price = abs($seller_product_price - $buyer_product_price);

            @endphp
            <!--begin::Card body-->
            <div class="card-body p-9">
                <!--begin::Row-->
                <div class="row mb-7">
                    <!--begin::Label-->
                    <label class="col-lg-4 fw-bold text-muted">Product Name</label>
                    <!--end::Label-->
                    <!--begin::Col-->
                    <div class="col-lg-8">
                        <span class="fw-bolder fs-6 text-gray-800">{{ $p->products->name ?? ''}}</span>
                    </div>
                    <!--end::Col-->
                </div>
                <!--end::Row-->
                <!--begin::Input group-->
                <div class="row mb-7">
                    <!--begin::Label-->
                    <label class="col-lg-4 fw-bold text-muted">Brand</label>
                    <!--end::Label-->
                    <!--begin::Col-->
                    <div class="col-lg-8 fv-row">
                        <span class="fw-bold text-gray-800 fs-6">{{ $p->products->brand->name ?? '' }}</span>
                    </div>
                    <!--end::Col-->
                </div>
                <!--end::Input group-->
                <!--begin::Input group-->
                <div class="row mb-7">
                    <!--begin::Label-->
                    <label class="col-lg-4 fw-bold text-muted">Category</label>
                    <!--end::Label-->
                    <!--begin::Col-->
                    <div class="col-lg-8 d-flex align-items-center">
                        <span class="fw-bolder fs-6 text-gray-800 me-2">{{ $p->products->categories->name ?? '' }}</span>
                    </div>
                    <!--end::Col-->
                </div>
                <!--end::Input group-->
                <!--begin::Input group-->
                <div class="row mb-7">
                    <!--begin::Label-->
                    <label class="col-lg-4 fw-bold text-muted">Sub Category</label>
                    <!--end::Label-->
                    <!--begin::Col-->
                    <div class="col-lg-8">
                        <span class="fw-bolder fs-6 text-gray-800 me-2">{{ $p->products->sub_categories->name ?? '' }}</span>
                    </div>
                    <!--end::Col-->
                </div>
                <!--end::Input group-->

                     <!--begin::Input group-->
                     <div class="row mb-7">
                        <!--begin::Label-->
                        <label class="col-lg-4 fw-bold text-muted">Price</label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-8">
                            <span class="fw-bolder fs-6 text-gray-800 me-2">{{ $p->products->price ?? '' }}</span>
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                <!--begin::Input group-->
                <div class="row mb-7">
                    <!--begin::Label-->
                    <label class="col-lg-4 fw-bold text-muted">Height</label>
                    <!--end::Label-->
                    <!--begin::Col-->
                    <div class="col-lg-8">
                        <span class="fw-bolder fs-6 text-gray-800">{{ $p->products->height ?? '' }}</span>
                    </div>
                    <!--end::Col-->
                </div>
                <!--end::Input group-->
                <!--begin::Input group-->
                <div class="row mb-7">
                    <!--begin::Label-->
                    <label class="col-lg-4 fw-bold text-muted">Width</label>
                    <!--end::Label-->
                    <!--begin::Col-->
                    <div class="col-lg-8">
                        <span class="fw-bolder fs-6 text-gray-800">{{ $p->products->width ?? '' }}</span>
                    </div>
                    <!--end::Col-->
                </div>
                <!--end::Input group-->
                <!--begin::Input group-->
                <div class="row mb-10">
                    <!--begin::Label-->
                    <label class="col-lg-4 fw-bold text-muted">Length</label>
                    <!--begin::Label-->
                    <!--begin::Label-->
                    <div class="col-lg-8">
                        <span class="fw-bold fs-6 text-gray-800">{{ $p->products->length ?? '' }}</span>
                    </div>
                    <!--begin::Label-->
                </div>
                <!--end::Input group-->

                <div class="form-group">
                    <label for="Description">Description</label>
                    <textarea name="description" required="false" class="form-control"  disabled>{{ $p->products->description ?? '' }}</textarea>
                </div>
                <div class="form-group">
                    <label for="Description">Short Details</label>
                    <textarea name="short_details" disabled class="form-control">{{ $p->description ?? '' }}</textarea>
                </div>
                <div class="form-group">
                    <label for="tradingName">Trading Policy</label>
                    </label>
                    <textarea required="False" disabled class="form-control" name="policy" >{{ $p->products->policy ?? '' }}</textarea>
                </div>

                {{-- @if($p->products->trade_with_amount === 1)
                <div class="form-group">
                    <label for="tradingName">Trade with amount</label></label>
                    <textarea required="False" disabled class="form-control" >User requesting to trade with amount in case of remaining amount between products</textarea>
                </div>
                @endif --}}

                <div class="form-group">
                    @if(isset($p->products->product_images_many) && !empty($p->products->product_images_many))
                                @foreach($p->products->product_images_many as $images)
                                <div class="col-6 " Styles="margin-left=5px">
                                    <img src="{{asset('/storage/'.$images->image)}}"
                                         width="20%" >   </div>
                                @endforeach
                                @endif
                                @if(isset($p->products->banner_image))
                                    <img src="{{asset('/storage/'.$trip->banner_image)}}"
                                         width="20%">
                                         @endif

                    </div>


            </div>
            <!--end::Card body-->
            <hr>
            @endforeach

              @if($make_trade[0]->trade_with_products->trade_with_amount === 1)
                <div class="form-group">
                    <p class="text-center alert-warning" style="font-weight: 900">Trade with amount</p>
                    <p class="text-center alert-warning" style="font-weight: 900">
                        {{ ($seller_product_price > $buyer_product_price) ? 'Buyer Will Pay you additional amount ' .  $price  : 'You Will Pay to additional amount on checkout '.   $price   }}
                    </p>


                </div>
                @endif

        </div>


    </div>
</div>
@endsection
@section('scripts')
<script src="{{asset('js/jquery-ui.js')}}"></script>
@endsection
