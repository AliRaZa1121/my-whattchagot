@extends('layout.default')
@php
if (Auth::guard('admin')->check()) {
$role='';
} else if (Auth::guard('seller')->check()) {
$role='seller_';

} else if (Auth::guard('seller')->check()) {
$role='buyer_';

}
$buyer_product_price  =  0;
$price = 0;

$page_breadcrumbs=[[
'page'=>route($role.'dashboard'),
'title'=>'dashboard'
],
[
'page'=>route($role.'seller.index'),
'title'=>'Make A Trade'
]
];


@endphp
@section('styles')
<link rel="stylesheet" href="{{asset('plugins/custom/datatables/datatables.bundle.css?v=7.0.4')}}">

@endsection
@section('content')
@include('pages.message')
<style>
    span.heading-custom {
        font-size: 18px; text-transform: uppercase;
        }
        .dropdown.dropdown-inline {
    display: flex;
    justify-content: end;
}
.btn.btn-light .svg-icon svg g [fill] {
    fill: #000000 !important;
}
.alert-warning {
    padding: 16px;
    margin-top: 20px;
}
.btn.btn-light.custm-btn  {
    width: 180px !important;
    padding-top: 10px;
    height: 49px !important;
    margin-top: -44px;
}
.flex.justify-between.flex-1.sm\:hidden {
    display: flex;
    justify-content: center;
}
.hidden.sm\:flex-1.sm\:flex.sm\:items-center.sm\:justify-between {
    display: flex;
    justify-content: center;
}
/* svg.w-5.h-5 {
    display: none;
} */
svg.w-5.h-5 {
    width: 20px;
}
div#pagination {
    background: #00000014 !important;
    padding-top: 10px;
}
div#pagination nav.flex.items-center.justify-between > div:first-of-type {
    margin-bottom: -28px;
    display: flex;
    justify-content: space-between;
    top: -2px;
    position: relative;
    width: 97%;
    margin-left: auto;
    margin-right: auto;
}

div#pagination nav.flex.items-center.justify-between > div:first-of-type * {
    position: relative;
    z-index: 9;
}
div#pagination nav.flex.items-center.justify-between > div:last-child div {
    margin-right: 20px;
    position: relative;
    z-index: 2;
}
</style>
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Trade List
            </h3>
        </div>

    </div>

    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="mb-17">
            <!--begin::Content-->
            <div class="d-flex flex-stack mb-5">
                <!--begin::Title-->
                <h3 class="text-dark">All Trade Offers</h3>
                <!--end::Title-->
            </div>
            <!--end::Content-->
            <!--begin::Separator-->
            <div class="separator separator-dashed mb-9"></div>
            <!--end::Separator-->

            @forelse($make_trade as $key => $p)
            @php
            $details = maketradeDetail($p->request_product_id, $p->user_id, $p->status);
            @endphp
            <!--begin::Row-->
            <div class="row g-10">
                <!--begin::Col-->
                <div class="col-md-10">
                    <!--begin::Hot sales post-->
                    <div class="card-xl-stretch me-md-6 pb-6">
                        <!--begin::Overlay-->
                        <a class="d-block overlay col-md-6" data-fslightbox="lightbox-hot-sales" href="{{isset($p->trade_with_products->product_images->image) ? asset('/storage/'.$p->trade_with_products->product_images->image): asset('/storage/default.jpg')}}">
                            <!--begin::Image-->
                            <div class="overlay-wrapper bgi-no-repeat bgi-position-center bgi-size-cover card-rounded min-h-250px w-80" style="background-image:url('{{isset($p->trade_with_products->product_images->image) ? asset('/storage/'.$p->trade_with_products->product_images->image): asset('/storage/default.jpg')}}')"></div>
                            <!--end::Image-->
                            <!--begin::Action-->
                            <div class="overlay-layer card-rounded bg-dark bg-opacity-25 min-h-250px w-80">
                                <i class="bi bi-eye-fill fs-2x text-white"></i>
                            </div>
                            <!--end::Action-->
                        </a>
                        <!--end::Overlay-->
                        <!--begin::Body-->
                        <div class="mt-8">
                            <!--begin::Title-->
                            <a href="{{ route('trading-single',$p->request_product_id) }}" class="fs-4 text-dark fw-bolder text-hover-primary text-dark lh-base">{{ $p->trade_with_products->name ?? 'N/A' }}</a>
                            <!--end::Title-->
                            <!--begin::Text-->
                            <div class="fw-bold fs-5 text-gray-600 text-dark mt-3">{{ $p->products->description ?? '' }}</div>
                            <!--end::Text-->
                            <!--begin::Text-->
                            <div class="fs-6 fw-bolder mt-5 d-flex flex-stack">
                                <!--begin::Label-->

                                <span class="badge border border-dashed fs-2 fw-bolder text-dark p-2">
                                    <span class="fs-6 fw-bold text-gray-900 heading-custom">{{$p->status}}</span>
                                <!--end::Label-->
                            </div>

                            <span style="overflow: visible; position: relative; width: 125px;">
                                <div class="dropdown dropdown-inline "> <a href="javascript:;"
                                        class="custm-btn btn btn-sm btn-light btn-text-primary btn-icon mr-2" data-toggle="dropdown">
                                      <p class="btn-action">CHOOSE AN ACTION:</p></span> </a>
                                    <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                        <ul class="navi flex-column navi-hover py-2">
                                            <li
                                                class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2">
                                                Choose an action: </li>
                                            @if($p->status=='Pending')
                                            <li class="navi-item">
                                                <a href="{{route('maketrade.update.status',['request_product_id' => $p->request_product_id, 'user_id' => $p->user_id,'status'=>'Accept'])}}"
                                                    class="navi-link">
                                                    <span class="navi-icon">
                                                        <i class="fa fa-check-circle"></i>
                                                    </span>
                                                    <span class="navi-text">Accept</span>
                                                </a>
                                            </li>
                                            <li class="navi-item">
                                                <a href="{{route('maketrade.update.status',['request_product_id' => $p->request_product_id, 'user_id' => $p->user_id, 'status'=>'Decline'])}}"
                                                    class="navi-link">
                                                    <span class="navi-icon">
                                                        <i class="fa fa-check-circle"></i>
                                                    </span>
                                                    <span class="navi-text">Decline</span>
                                                </a>
                                            </li>
                                            <li class="navi-item">
                                                <a href="{{route('maketrade.update.status',['request_product_id' => $p->request_product_id, 'user_id' => $p->user_id,'status'=>'Cancel'])}}"
                                                    class="navi-link">
                                                    <span class="navi-icon">
                                                        <i class="fa fa-check-circle"></i>
                                                    </span>
                                                    <span class="navi-text">Cancel</span>
                                                </a>
                                            </li>
                                            @endif
                                            <li class="navi-item">
                                                <a href="{{ route('maketrade.detail',[$p->request_product_id, $p->user_id, $p->status]) }}" class="navi-link">
                                                    <span class="navi-icon">
                                                        <i class="fa fa-check-circle"></i>
                                                    </span>
                                                    <span class="navi-text">View details</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </span>
                            <!--end::Text-->
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Hot sales post-->
                </div>
                <!--end::Col-->
            </div>
            <!--end::Row-->
             <!--begin::Row-->
             <div class="row g-10">

            @forelse($details as $key => $value)
            @php
            $seller_product_price =  intval($value->trade_with_products->price);
            $buyer_product_price  = intval($value->products->price) + intval($buyer_product_price);
            $price = abs($seller_product_price - $buyer_product_price);
            @endphp
                <!--begin::Col-->
                <div class="col-md-4">
                    <!--begin::Hot sales post-->
                    <div class="card-xl-stretch me-md-6">
                        <!--begin::Overlay-->
                        <a class="d-block overlay" data-fslightbox="lightbox-hot-sales" href="{{isset($value->products->product_images->image) ? asset('/storage/'.$value->products->product_images->image): asset('/storage/default.jpg')}}">
                            <!--begin::Image-->
                            <div class="overlay-wrapper bgi-no-repeat bgi-position-center bgi-size-cover card-rounded min-h-175px" style="background-image:url('{{isset($value->products->product_images->image) ? asset('/storage/'.$value->products->product_images->image): asset('/storage/default.jpg')}}"></div>
                            <!--end::Image-->
                            <!--begin::Action-->
                            <div class="overlay-layer card-rounded bg-dark bg-opacity-25 cutom-style">
                                <i class="bi bi-eye-fill fs-2x text-white"></i>
                            </div>
                            <!--end::Action-->
                        </a>
                        <!--end::Overlay-->
                        <!--begin::Body-->
                        <div class="mt-5">
                            <!--begin::Title-->
                            <a href="#" class="fs-4 text-dark fw-bolder text-hover-primary text-dark lh-base">{{ $value->products->name ?? ''}}</a>
                            <!--end::Title-->
                            <!--begin::Text-->
                            <div class="fw-bold fs-5 text-gray-600 text-dark mt-3">{{ $value->description ?? ''}}</div>
                            <!--end::Text-->
                            <!--begin::Text-->
                            <div class="fs-6 fw-bolder mt-5 d-flex flex-stack">
                                <!--begin::Label-->
                                <span class="badge border border-dashed fs-2 fw-bolder text-dark p-2">
                                    <span class="fs-6 fw-bold text-gray-400">$</span>{{$value->products->price ?? ''}}</span>
                                <!--end::Label-->

                            </div>
                            <!--end::Text-->
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Hot sales post-->
                </div>
                <!--end::Col-->


            @empty

            @endforelse
            </div>
            <!--end::Row-->
            @if($p->trade_with_products->trade_with_amount === 1)
            <div class="form-group">
                <p class="text-center alert-warning" style="font-weight: 900">Trade with amount</p>
                <p class="text-center alert-warning" style="font-weight: 900">
                    {{ ($seller_product_price > $buyer_product_price) ? 'Buyer Will Pay you additional amount ' .  $price  : 'You Will Pay to additional amount on checkout '.   $price   }}
                </p>
            </div>
            @endif

            @empty
            <div>
                <img class="center" src="{{ asset('images/no-data-found.png') }}" style="margin-left: 200px;" />
            </div>
            @endforelse
           <div class="separator separator-dashed mb-9"></div>
           <div  id="pagination">{{ $make_trade->links() }}</div>
            <!--begin::Separator-->
            <div class="separator separator-dashed mb-9"></div>
            <!--end::Separator-->

        </div>
    </div>
</div>


@endsection

