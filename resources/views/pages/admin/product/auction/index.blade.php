@extends('layout.default')
@php
    if (Auth::guard('admin')->check()) {
        $role='';
        } else if (Auth::guard('seller')->check()) {
                $role='seller_';

        } else if (Auth::guard('seller')->check()) {
            $role='buyer_';

        }
        $page_breadcrumbs=[[
        'page'=>route($role.'dashboard'),
        'title'=>'dashboard'
        ],
        [
        'page'=>route($role.'auction.index'),
        'title'=>'Auction'
        ]
        ];
@endphp
@section('styles')
    <link rel="stylesheet" href="{{asset('plugins/custom/datatables/datatables.bundle.css?v=7.0.4')}}">
@endsection
@section('content')
    @include('pages.message')
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Product Auction List
                </h3>
            </div>

            @if($role=='seller_')
            <div class="card-toolbar">
                <a href="{{route($role.'auction.create')}}"
                   class="btn btn-primary font-weight-bolder font-size-sm mr-3 ">Add New</a>
            </div>
                @endif
        </div>
        <div class="card-body">
            <!--begin: Search Form-->
            <!--begin::Search Form-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
                <thead>
                <tr>
                    <th> ID</th>
                    <th>Image</th>
                    <th>name</th>
                    <th>Price</th>
                    <th>Category</th>
                    <th>Sub Category</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php $no=1 ?>
                @foreach($auctions as $p)
                    <tr>
                        <td>{{$no++}}</td>
                        <td>
                            <img src="{{isset($p->product_images->image) ? asset('/storage/'.$p->product_images->image): asset('/storage/default.jpg')}}"
                                 style="width:150px;max-height:150px;"></td>
                        <td>{{$p->name}}</td>
                        <td>${{$p->price}}{{$p->currency_type}}</td>
                        <td>{{$p->categories->name ?? 'N/A'}}</td>
                        <td>{{$p->sub_categories->name ?? 'N/A'}}</td>
                        <td>{{$p->status}}</td>
                        <td>
                        <span style="overflow: visible; position: relative; width: 125px;">
                            <div class="dropdown dropdown-inline "> <a href="javascript:;"
                                                                       class="btn btn-sm btn-light btn-text-primary btn-icon mr-2"
                                                                       data-toggle="dropdown">
                                    <span class="svg-icon svg-icon-md"> <svg xmlns="http://www.w3.org/2000/svg"
                                                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                             width="24px" height="24px"
                                                                             viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                <path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z"
                                                      fill="#000000"></path>
                                            </g>
                                        </svg> </span> </a>
                                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                    <ul class="navi flex-column navi-hover py-2">
                                        <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2">
                                            Choose an action: </li>
                                        <li class="navi-item">
                                            <a href="{{route($role.'auction.show',$p->id)}}" class="navi-link">
                                                <span class="navi-icon">
                                                    <i class="la la-eye"></i>
                                                </span>
                                                <span class="navi-text">View Details</span>
                                            </a>
                                        </li>
                                             @if($role=='seller_')

                                              <li class="navi-item">
                                            <a href="{{route($role.'auction.edit',$p->id)}}" class="navi-link">
                                                <span class="navi-icon">
                                                    <i class="la la-eye"></i>
                                                </span>
                                                <span class="navi-text">Edit</span>
                                            </a>
                                        </li>
                                        @endif

                                      @if(($p->status=='Pending' || $p->status=='Canceled' || $p->status=='Deleted' ) && $role == '')
                                            <li class="navi-item">
                                            <a href="{{route('products.update.status',['product'=>$p->id,'status'=>'Approved'])}}"
                                               class="navi-link">
                                                <span class="navi-icon">
                                                    <i class="fa fa-check-circle"></i>
                                                </span>
                                                <span class="navi-text">Approve</span>
                                            </a>
                                        </li>

                                        @endif
                                        @if(($p->status!='Canceled' || $p->status=='Deleted')  && $role == '')

                                        <li class="navi-item">
                                            <a href="{{route('products.update.status',['product'=>$p->id,'status'=>'Canceled'])}}"
                                               class="navi-link">
                                                <span class="navi-icon">
                                                    <i class="fa fa-times-circle"></i>
                                                </span>
                                                <span class="navi-text">Cancel</span>
                                            </a>
                                        </li>
                                        @endif
                                               <li class="navi-item">
                                            <a href="{{route($role.'products.update.status',['product'=>$p->id,'status'=>'Deleted'])}}"
                                               class="navi-link">
                                                <span class="navi-icon">
                                                    <i class="fa fa-times-circle"></i>
                                                </span>
                                                <span class="navi-text">Delete</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </span>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


@endsection

{{-- Styles Section --}}
@section('styles')

@endsection

@section('scripts')
    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js?v=7.0.4') }}" type="text/javascript"></script>
    <script>
        var table = $('#kt_datatable');

        // begin first table
        table.DataTable({
            // DOM Layout settings
            dom: "<'row py-3'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>" +
                "<'row py-3'<'col-sm-12'tr>>" +
                "<'row py-3'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>", // read more: https://datatables.net/examples/basic_init/dom.html

            columnDefs: [{
                targets: -1,
                title: 'Actions',
                orderable: false,

            },
                {
                    width: '75px',
                    targets: 6,
                    render: function (data, type, full, meta) {
                        var status = {
                            'Pending': {
                                'title': 'Pending',
                                'class': 'label-light-primary'
                            },

                            'Approved': {
                                'title': 'Approved',
                                'class': ' label-light-success'
                            },
                            'Canceled': {
                                'title': 'Canceled',
                                'class': ' label-light-danger'
                            },

                        };
                        if (typeof status[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="label label-lg font-weight-bold ' + status[data].class + ' label-inline">' + status[data].title + '</span>';
                    },
                },
            ],
        });
    </script>
@endsection
