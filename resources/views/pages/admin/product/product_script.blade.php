<script>

    $('#category_select').change(function() {
        var cat_id = $(this).val();
        var path = "{{ url('/') }}"
        $.ajax({
            type: 'GET',    
            url: `${path}/get/sub-categories/${cat_id}`,
            success: function(data) {
                $("#sub_category_select").children().remove();
                $("#sub_category_select").append(new Option("None", ''));
                for (i = 0; i < data.length; i++) {
                    $("#sub_category_select").append(`<option value=${data[i].id}>${data[i].name}</option>`);
                }
            }
            , error: function() {
                console.log(data);
            }
        });
    })

</script>
