@extends('layout.default')
@php
    if (Auth::guard('admin')->check()) {
        $role='';
        } else if (Auth::guard('seller')->check()) {
                $role='seller_';

        }
        $page_breadcrumbs=[[
            'page'=>route($role.'dashboard'),
        'title'=>'Dashboard'
        ],
        [
        'page'=>route('trading.index'),
        'title'=>'Trading'
        ]
        ];
@endphp
@section('styles')
    <link rel="stylesheet" href="{{asset('plugins/custom/datatables/datatables.bundle.css?v=7.0.4')}}">
@endsection
@section('content')
    @include('pages.message')
    @if($errors->any())
        <div class="alert alert-success" role="alert">
            {{$errors->first()}}
        </div>
    @endif
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Product Trading List
                </h3>
            </div>
            <div class="card-toolbar">
                @if($role=='')

{{--                    <a href="{{ route('trade-price') }}"--}}
{{--                       class="btn btn-primary font-weight-bolder font-size-sm mr-3 ">Trade Pay</a>--}}
                @endif
                @if($role=='seller_')
                    <a href="{{route($role.'trading.create')}}"
                       class="btn btn-primary font-weight-bolder font-size-sm mr-3 ">Add New</a>
                @endif
            </div>
        </div>
        <div class="card-body">

            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
                <thead>
                <tr>
                    <th> ID</th>
                    <th>Image</th>
                    <th>name</th>
                                        <th>Price Value</th>
                    <th>Category</th>
                    <th>Sub Category</th>
                    <th>Status</th>
{{--                    <th>payment</th>--}}
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php $no = 1 ?>
{{-- {{ dd($tradings) }} --}}
                @foreach($tradings as $p)
                    <tr>
                        <td>{{$no++}}</td>
                        <td>
                            <img src="{{isset($p->product_images->image) ? asset('/storage/'.$p->product_images->image): asset('/storage/default.jpg')}}"
                                 style="width:150px;max-height:150px;"></td>
                        <td>{{$p->name}}</td>
                         <td>${{$p->price}}</td>
                        <td>{{$p->categories->name ?? 'N/A'}}</td>
                        <td>{{$p->sub_categories->name ?? 'N/A'}}</td>
                        <td>{{$p->status}}</td>
                        {{--                        <td>{{$p->payment}}</td>--}}
                        <td>
                        <span style="overflow: visible; position: relative; width: 125px;">
                            <div class="dropdown dropdown-inline "> <a href="javascript:;"
                                                                       class="btn btn-sm btn-light btn-text-primary btn-icon mr-2"
                                                                       data-toggle="dropdown">
                                    <span class="svg-icon svg-icon-md"> <svg xmlns="http://www.w3.org/2000/svg"
                                                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                             width="24px" height="24px"
                                                                             viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                <path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z"
                                                      fill="#000000"></path>
                                            </g>
                                        </svg> </span> </a>
                                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                    <ul class="navi flex-column navi-hover py-2">
                                        <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2">
                                            Choose an action: </li>
                                        <li class="navi-item">
                                            <a href="{{route($role.'trading.show',$p->id)}}" class="navi-link">
                                                <span class="navi-icon">
                                                    <i class="la la-eye"></i>
                                                </span>
                                                <span class="navi-text">View Details</span>
                                            </a>
                                        </li>
                                             @if($role=='seller_')

                                            <li class="navi-item">
                                                <a href="{{route($role.'trading.edit',$p->id)}}" class="navi-link">
                                                    <span class="navi-icon">
                                                        <i class="la la-edit"></i>
                                                </span>
                                                    <span class="navi-text">Edit</span>
                                            </a>
                                        </li>
                                            @if($p->payment=='Pending')
                                                {{--                                                <li class="navi-item">--}}
                                                {{--                                            <a data-toggle="modal"--}}
                                                {{--                                               onclick='model_data({{$p->id}})'--}}
                                                {{--                                               data-target=".bd-example-modal-sm" class="navi-link">--}}
                                                {{--                                                <span class="navi-icon">--}}
                                                {{--                                                    <i class="la la-usd"></i>--}}
                                                {{--                                                </span>--}}
                                                {{--                                                <span class="navi-text">payment</span>--}}
                                                {{--                                            </a>--}}
                                                {{--                                        </li>--}}
                                            @endif
                                        @endif


                                        @if(($p->status=='Pending' || $p->status=='Canceled' || $p->status=='Deleted' ) && $role == '')
{{--                                            @if($p->payment == 'Completed')--}}
                                                <li class="navi-item">
                                                    <a href="{{route($role.'products.update.status',['product'=>$p->id,'status'=>'Approved'])}}"
                                                       class="navi-link">
                                                        <span class="navi-icon">
                                                            <i class="fa fa-check-circle"></i>
                                                        </span>
                                                        <span class="navi-text">Approve</span>
                                                    </a>
                                                </li>
{{--                                            @endif--}}

                                        @endif
                                        @if(($p->status!='Canceled' || $p->status=='Deleted')  && $role == '')

                                            <li class="navi-item">
                                            <a href="{{route($role.'products.update.status',['product'=>$p->id,'status'=>'Canceled'])}}"
                                               class="navi-link">
                                                <span class="navi-icon">
                                                    <i class="fa fa-times-circle"></i>
                                                </span>
                                                <span class="navi-text">Cancel</span>
                                            </a>
                                        </li>
                                        @endif
                                               <li class="navi-item">
                                            <a href="{{route($role.'products.update.status',['product'=>$p->id,'status'=>'Deleted'])}}"
                                               class="navi-link">
                                                <span class="navi-icon">
                                                    <i class="fa fa-times-circle"></i>
                                                </span>
                                                <span class="navi-text">Delete</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </span>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="product_name">Payment </h5>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                <form role="form" action="{{ route('stripe_trade.post') }}" method="post" class="require-validation"
                      data-cc-on-file="false"
                      data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"
                      id="payment-form">
                    @csrf
                    <div class="modal-body">

                        @csrf
                        <div class='form-row row'>
                            <div class='col-xs-12 col-md-12  form-group required'>
                                <label class='control-label'>Price will charge for per trade </label> <input disabled
                                                                                                             class='form-control'
                                                                                                             value="{{$trade_price->price}}">
                            </div>
                        </div>
                        <div class='form-row row'>
                            <div class='col-xs-12 col-md-12  form-group required'>
                                <label class='control-label'>Name on Card</label> <input
                                        class='form-control' size='4' type='text'>
                            </div>
                        </div>

                        <div class='form-row row'>
                            <div class='col-xs-12 col-md-12 form-group card required'>
                                <label class='control-label'>Card Number</label> <input
                                        autocomplete='off' class='form-control card-number number_only' maxlength="16"
                                        size='16'
                                        type='text'>
                            </div>
                        </div>

                        <div class='form-row row'>
                            <div class='col-xs-12 col-md-4 form-group cvc required'>
                                <label class='control-label'>CVC</label> <input autocomplete='off'
                                                                                class='form-control card-cvc number_only'
                                                                                placeholder='ex. 311' size='4'
                                                                                type='text' maxlength="3"
                                                                                pattern="[0-9]+">
                            </div>
                            <div class='col-xs-12 col-md-4 form-group expiration required'>
                                <label class='control-label'>Expiration Month</label> <input
                                        class='form-control card-expiry-month number_only' placeholder='MM' size='2'
                                        type='text' maxlength="2" min="1" max="12">
                            </div>
                            <div class='col-xs-12 col-md-4 form-group expiration required'>
                                <label class='control-label'>Expiration Year</label> <input
                                        class='form-control card-expiry-year number_only' placeholder='YYYY' size='4'
                                        type='text' maxlength="4" minlength="4" min="2021">
                            </div>
                        </div>

                        <div class='form-row row'>
                            <div class='col-md-12 error form-group hide'>
                                <div class='alert-danger alert' id="error_stripe" style="display: none">Please correct
                                    the errors and try
                                    again.
                                </div>
                            </div>
                        </div>
                        <input name="amount" type="hidden" value="{{base64_encode($trade_price->price)}}">
                        <input name="product_id" id="payment_id" type="hidden">


                    </div>

                    <div class="modal-footer">

                        <button class="btn btn-primary btn-lg btn-block" type="submit">Pay Now</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection

{{-- Styles Section --}}
@section('styles')

@endsection

@section('scripts')
    <script>
        $('.number_only').bind('keyup paste', function () {
            this.value = this.value.replace(/[^0-9]/g, '');
        });
    </script>

    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>

    <script type="text/javascript">

        $(Document).ready(function () {
            if ($('#shipping-address').is(':checked')) {
                $('#shippingAddress input').prop('disabled', false);
                $('#shippingAddress select').prop('disabled', false);
                $("#shippingAddress").show();
            } else {
                $('#shippingAddress input').prop('disabled', true);
                $('#shippingAddress select').prop('disabled', true);

                $("#shippingAddress").hide();
            }

        });
        $('#shipping-address').click(function () {
            if ($('#shipping-address').is(':checked')) {
                $('#shippingAddress input').prop('disabled', false);
                $('#shippingAddress select').prop('disabled', false);
                $("#shippingAddress").show();
            } else {
                $('#shippingAddress input').prop('disabled', true);
                $('#shippingAddress select').prop('disabled', true);
                $("#shippingAddress").hide();
            }
        });
        $(function () {
            var $form = $(".require-validation");
            $('form.require-validation').bind('submit', function (e) {
                var $form = $(".require-validation"),
                    inputSelector = ['input[type=email]', 'input[type=password]',
                        'input[type=text]', 'input[type=file]',
                        'textarea'].join(', '),
                    $inputs = $form.find('.required').find(inputSelector),
                    $errorMessage = $form.find('div.error'),
                    valid = true;
                $errorMessage.addClass('hide');

                $('.has-error').removeClass('has-error');
                $inputs.each(function (i, el) {
                    var $input = $(el);
                    if ($input.val() === '') {
                        $input.parent().addClass('has-error');
                        $errorMessage.removeClass('hide');
                        e.preventDefault();
                    }
                });

                if (!$form.data('cc-on-file')) {
                    e.preventDefault();
                    Stripe.setPublishableKey($form.data('stripe-publishable-key'));
                    Stripe.createToken({
                        number: $('.card-number').val(),
                        cvc: $('.card-cvc').val(),
                        exp_month: $('.card-expiry-month').val(),
                        exp_year: $('.card-expiry-year').val()
                    }, stripeResponseHandler);
                }

            });

            function stripeResponseHandler(status, response) {
                if (response.error) {
                    $('#error_stripe').show();
                    $('.error')
                        .removeClass('hide')
                        .find('.alert')
                        .text(response.error.message);
                } else {
                    // token contains id, last4, and card type
                    var token = response['id'];
                    // insert the token into the form so it gets submitted to the server
                    $form.find('input[type=text]').empty();
                    $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
                    $form.get(0).submit();
                }
            }

        });
    </script>
    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js?v=7.0.4') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function () {

        })

        function model_data(id) {
            $('#payment_id').val(id);
        }

        var table = $('#kt_datatable');

        // begin first table
        table.DataTable({
            // DOM Layout settings
            dom: "<'row py-3'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>" +
                "<'row py-3'<'col-sm-12'tr>>" +
                "<'row py-3'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>", // read more: https://datatables.net/examples/basic_init/dom.html

            columnDefs: [{
                targets: -1,
                title: 'Actions',
                orderable: false,

            },
                {
                    width: '75px',
                    targets: 5,
                    render: function (data, type, full, meta) {
                        var status = {
                            'Pending': {
                                'title': 'Pending',
                                'class': 'label-light-primary'
                            },

                            'Approved': {
                                'title': 'Approved',
                                'class': ' label-light-success'
                            },
                            'Canceled': {
                                'title': 'Canceled',
                                'class': ' label-light-danger'
                            },
                            

                        };
                        if (typeof status[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="label label-lg font-weight-bold ' + status[data].class + ' label-inline">' + status[data].title + '</span>';
                    },
                }, {
                    width: '75px',
                    targets: 6,
                    render: function (data, type, full, meta) {
                        var status = {
                            'Pending': {
                                'title': 'Pending',
                                'class': 'label-light-primary'
                            },
                            'Approved': {
                                'title': 'Approved',
                                'class': ' label-light-success'
                            },

                            'Completed': {
                                'title': 'Completed',
                                'class': ' label-light-success'
                            },
                            'Canceled': {
                                'title': 'Canceled',
                                'class': ' label-light-danger'
                            },
                            'Solded': {
                               'title': 'Solded',
                               'class': 'label-light-warning'
                           },
                            'Trade-Confirmed': {
                               'title': 'Trade-Confirmed',
                               'class': 'label-light-warning'
                           },

                        };
                        if (typeof status[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="label label-lg font-weight-bold ' + status[data].class + ' label-inline">' + status[data].title + '</span>';
                    },
                },
            ],
        });
    </script>

    <script>
        $('.number_only').bind('keyup paste', function () {
            this.value = this.value.replace(/[^0-9]/g, '');
        });
    </script>


@endsection
