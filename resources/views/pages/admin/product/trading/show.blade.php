@extends('layout.default')
@php
if (Auth::guard('admin')->check()) {
$role='';
} else if (Auth::guard('seller')->check()) {
$role='seller_';

} else if (Auth::guard('seller')->check()) {
$role='buyer_';

}
$page_breadcrumbs=[
[
'page'=>route($role.'dashboard'),
'title'=>'dashboard'
],
[
'page'=>route('trading.index'),
'title'=>'Trading Create'
]
];
@endphp
@section('styles')
<link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<style>
    .label-centered-field {
        display: flex;
        align-items: center;
        margin-top: 8px;
    }

    .time-off-action-row label {
        visibility: hidden;
    }

    .time-off-action-row a {
        display: table-cell;
        padding: 5px;
    }

    .day-activities {
        display: flex;
        align-items: center
    }

    .switch-right-side {
        margin-left: 5px;
    }

    #map {
        height: 400px;
        /* The height is 400 pixels */
        width: 100%;
        /* The width is the width of the web page */
    }

    #reset_location {
        margin-top: 5px;
    }
</style>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-custom flex-wrap border-0 pt-6 pb-0">
            <form class="form" id="form" method="post" action="{{route('trading.update',$product->id)}}">
                @csrf
                @method('put')

                <div class="card card-custom card-sticky" id="kt_page_sticky_card">
                    <div class="card-header" style="">
                        <div class="card-title col-md-9">
                            <h3 class="card-label">New Trading Product
                                <!-- <i class="mr-2"></i> -->
                                <!-- <small class="">try to scroll the page</small> -->
                            </h3>
                        </div>
                        <div class="card-toolbar">
                            <a href="{{route('trading.index')}}" class="btn btn-secondary font-weight-bold mr-2">
                                <i class="ki ki-long-arrow-back icon-xs"></i>Cancel
                            </a>

                            <a href="{{route($role.'trading.edit',$product->id)}}"
                                class="btn btn-primary font-weight-bolder">Edit Trading</a>
                        </div>


                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">

                                <div class="form-group">
                                    <label for="tradingName">Product Name</label>
                                    <input type="text" required id="name" class="form-control" name="name"
                                        placeholder='e.g.,"Early Brid" or "VIP"' value="{{$product->name}}">
                                </div>
                                <div class="form-group">
                                    <label for="sellerName">brand</label>
                                    <div class="input-group ">
                                        <select class="form-control" name="brand" required>
                                            <option value="">None</option>
                                            @foreach($brand as $brands)
                                            <option value="{{$brands->id}}" {{$product->brand_id == $brands->id ?
                                                'selected': '' }}>{{$brands->name}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>

                                <div class="form-group">

                                    {{-- <div class="form-group">

                                        <label class="" for="">
                                            Quantity
                                        </label>
                                        <div class="form-check">

                                            <input class="form-check-input" type="checkbox" value="0"
                                                id="checkbox_check" name="price_check" {{$product->quantity ==0 ?
                                            'checked' : ''}}
                                            >
                                            <label class="form-check-label" for="checkbox_check">
                                                Limit Quantity
                                            </label>

                                        </div>
                                        <input class="form-control" placeholder="Quantity" type="text" name="quantity"
                                            value="{{$product->quantity }}" disabled id="quantity">
                                    </div> --}}


                                    <div class="form-group">
                                        <div class="form-check">

                                            <input class="form-check-input" type="checkbox" disabled
                                                id="trade_with_amount" name="trade_with_amount" value="1" {{
                                                $product->trade_with_amount == 1 ? 'checked' : '' }} >
                                            <label class="form-check-label" for="checkbox_check">
                                                Trade with amount in case of amount is not equal between products
                                            </label>

                                        </div>
                                    </div>

                                    <div class="form-group" id="trading_group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="tradingName">Trading Price</label>
                                                <input type="number" required id="price" class="form-control"
                                                    name="price" placeholder='0' value="{{$product->price }}">
                                            </div>
                                            {{-- <div class="col-md-6">
                                                <label for="tradingName">Curracny Type</label>
                                                <Select required class="form-control" required id="currency_type"
                                                    name="currency_type">
                                                    <option value="">none</option>
                                                    <option value="US Dollor- USD($)" {{$product->currency_type =='US
                                                        Dollor- USD($)' ? 'selected': '' }}>US Dollor- USD($)</option>
                                                </Select>
                                            </div> --}}
                                        </div>


                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label class="col-form-label">Category</label>
                                            <div class="input-group ">
                                                <select class="form-control" name="category_id" required>
                                                    <option value="">None</option>
                                                    @foreach($category as $cat)
                                                    <option value="{{$cat->id}}" {{$product->category_id == $cat->id ?
                                                        'selected': '' }}>{{$cat->name}}</option>
                                                    @endforeach
                                                </select>

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="col-form-label"> Sub Category</label>
                                            <div class="input-group ">
                                                <select class="form-control" name="sub_category_id" required>
                                                    <option value="">None</option>
                                                    @foreach($sub_category as $sub_cat)
                                                    <option value="{{$sub_cat->id}}" {{$product->sub_category_id ==
                                                        $sub_cat->id ? 'selected': '' }}>{{$sub_cat->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                    </div>

                                    {{-- <div class="form-group" id="">

                                        <div id="date_time_section">
                                            <div class="form-group row">
                                                <div class="col-md-7">
                                                    <label class="col-form-label"> Start Date</label>
                                                    <div class="input-group date">
                                                        <input type="text" class="form-control" name="start_date"
                                                            value="{{isset($product->start_date) ?  \Carbon\Carbon::parse($product->start_date)->format('m/d/Y'): ''}}">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="la la-calendar">

                                                                </i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <label class="col-form-label"> Start Time</label>
                                                    <div class="input-group date">

                                                        <input type="text" class="form-control" name="start_time"
                                                            id="start_time" REQUIRED
                                                            value="{{isset($product->start_time) ? $product->start_time: ''}}">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="la la-calendar"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-7">
                                                    <label class="col-form-label">End Date</label>
                                                    <div class="input-group date">
                                                        <input type="text" class="form-control" name="end_date"
                                                            value="{{isset($product->end_date) ? \Carbon\Carbon::parse($product->end_date)->format('m/d/Y') : ''}}">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="la la-calendar"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <label class="col-form-label"> End Time</label>
                                                    <div class="input-group date">
                                                        <input type="text" class="form-control" name="end_time"
                                                            id="end_time" REQUIRED
                                                            value="{{isset($product->end_time) ? $product->end_time: ''}}">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="la la-calendar"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                    </div> --}}
                                    <div class="form-group" id="">
                                        <label for="">Dimensions</label>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="tradingName">Height</label>
                                                <input type="number" required id="height_size" class="form-control"
                                                    value="{{$product->height}}" name="height_size" placeholder='0'>
                                            </div>
                                            <div class="col-md-4">
                                                <label for="tradingName">width</label>
                                                <input type="number" required id="width_size" class="form-control"
                                                    name="width_size" placeholder='0' value="{{$product->width}}">
                                            </div>
                                            <div class="col-md-4">
                                                <label for="tradingName">length</label>
                                                <input type="number" required id="length_size" class="form-control"
                                                    value="{{$product->length}}" name="length_size" placeholder='0'>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="form-group">
                                        <label for="Description">Description(Optional)</label>
                                        <textarea name="description" required="false" class="form-control"
                                            placeholder="Add a few words about product, e.g., An unforgettable show!">{{$product->policy}}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="Description">Short Details</label>
                                        <textarea name="short_details" required="true" disabled class="form-control"
                                            placeholder="Add a few words about product, e.g., An unforgettable show!">{{$product->short_details}}</textarea>
                                    </div>
                                    <div class="form-group">
                                        {{-- <label for="tradingName">Trading Policy</label> --}}
                                        <label for="tradingName">Trading Policy Text (Optional)
                                        </label>
                                        <textarea required="False" class="form-control" name="policy"
                                            placeholder='This is your event trading. Trading holders must present their tradings on entry. You can either print your trading or present this digital version. You can find all the details about this event on our website. If you have any questions, issues or wish to get a refund, contact the event host. If you can’t attend the event, please get in touch. Looking forward to seeing you there!'>{{$product->description}}</textarea>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="flex-grow-1" style="position: relative;">
                                    <div class="form-group">
                                        @if(isset($product->product_images_many) &&
                                        !empty($product->product_images_many))
                                        @foreach($product->product_images_many as $images)
                                        <div class="col-6 " Styles="margin-left=5px">

                                            <img src="{{asset('/storage/'.$images->image)}}" width="100%">
                                        </div>
                                        @endforeach
                                        @endif

                                        <!-- <h1 class="image-upload-msg">Banner Image</h1> -->
                                        <div class="" id="banner_image">


                                            <!-- <div class="dropzone-msg dz-message needsclick">

                                                        <h3 class="dropzone-msg-title">Drop files here or click to
                                                            upload.</h3>
                                                        <span class="dropzone-msg-desc">Only images are allowed for upload</span>

                                                </div> -->
                                        </div>
                                    </div>
                                </div>
                                <div id="multi_image">
                                    @if(isset($product->product_images_many) && !empty($product->product_images_many))
                                    @foreach($product->product_images_many as $images)
                                    <input type="hidden" value="{{$images->image}}" name="banner_image[]" multiple>
                                    @endforeach
                                    @endif

                                </div>
                            </div>


                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="flex-grow-1" style="position: relative;">
                            <div class="form-group">
                                <label class="col-form-label"> Expectation Trading items return (Optional) </label>


                                <div class="row">
                                    @if(isset($product->trading_return_expectations) &&
                                    !empty($product->trading_return_expectations))
                                    @foreach($product->trading_return_expectations as $images)
                                    <div class="col-2" Styles="margin-left=5px">

                                        <img src="{{asset('/storage/'.$images->image)}}" width="100%" height="100px"
                                            id="image{{$images->id}}">


                                    </div>
                                    @endforeach
                                    @endif
                                </div>

                            </div>
                        </div>
                    </div>

            </form>
        </div>

    </div>
</div>
@endsection
@section('scripts')
<script src="{{asset('js/jquery-ui.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

<script>
    $(document).ready(function(){
        $("#form :input").prop("disabled", true);
        $("#quantity").prop("disabled", true);
    });

        const paymentMethodValidator = {
            validators: {
                callback: {
                    message: 'Please choose at least one method',
                    callback: function (input) {
                        if ($(input.elements[0]).is(':checked')) {
                            $("[name=payment_method]").val(1);
                        } else if ($('.payment-methods input:checked').length == 0) {
                            $("[name=payment_method]").val(null);
                        }
                        bv.revalidateField('payment_method')
                        return true;
                    }
                }
            }
        };
        const locationValidator = {
            validators: {
                notEmpty: {
                    message: 'Amount is required'
                }
            }
        };
        const daysActivityValidator = {
            validators: {
                notEmpty: {
                    message: 'Activties text is required'
                }
            }
        };
        const pickUpTimeValidator = {
            validators: {
                notEmpty: {
                    message: 'Pick Up Time is required'
                }
            }
        };
        const startDateValidator = {
            validators: {
                notEmpty: {
                    message: 'Start Date is required'
                },
                date: {
                    format: 'MM/DD/YYYY',
                    message: 'The value is not a valid date'
                }
            }
        };
        const endDateValidator = {
            validators: {
                notEmpty: {
                    message: 'End Date is required'
                },
                date: {
                    format: 'MM/DD/YYYY',
                    message: 'The value is not a valid date'
                }
            }
        };
        const startTimeValidator = {
            validators: {
                notEmpty: {
                    message: 'Start Time is required'
                }
            }
        };
        const endTimeValidator = {
            validators: {
                notEmpty: {
                    message: 'End Time is required'
                }
            },
        };
        const bv = FormValidation.formValidation(
            document.getElementById('form'), {
                fields: {
                    name: {
                        validators: {
                            notEmpty: {
                                message: 'Event Name is required'
                            }
                        }
                    },
                    brand: {
                        validators: {
                            notEmpty: {
                                message: 'brand is required'
                            }
                        }
                    },
                    price: {
                        validators: {
                            notEmpty: {
                                message: 'price is required'
                            }
                        }
                    },
                    currency_type: {
                        validators: {
                            notEmpty: {
                                message: 'Currency Type is required'
                            }
                        }
                    },
                    category_id: {
                        validators: {
                            notEmpty: {
                                message: 'Category is required'
                            }
                        }
                    },
                    sub_category_id: {
                        validators: {
                            notEmpty: {
                                message: 'Sub Category is required'
                            }
                        }
                    },

                    height_size: {
                        validators: {
                            notEmpty: {
                                message: 'Height is required'
                            }
                        }
                    },
                    width_size: {
                        validators: {
                            notEmpty: {
                                message: 'Width is required'
                            }
                        }
                    },
                    length_size: {
                        validators: {
                            notEmpty: {
                                message: 'Length is required'
                            }
                        }
                    },


                },

                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    submitButton: new FormValidation.plugins.SubmitButton(),
                }
            }
        ).on('core.form.valid', function () {
            // Send the form data to back-end
            // You need to grab the form data and create an Ajax request to send them
            // Get the form instance
            var $form = $('#form');
            // Get the BootstrapValidator instance
            let method = $form.attr('method');
            let url = $form.attr('action');
            let data = new FormData(document.getElementById('form'));
            submitFormAjax(method, url, data, function (res) {


                successAlert(res.msg);
                // $('#form').trigger('reset');
                $('.dz-complete').remove();
                $('.needsclick').show();


                window.location.href = "{{route('trading.index')}}";

            }, function (jqXHR) {
                alert('dada');
                let errors = jqXHR.responseJSON.errors;
                let erMsgs = ``;
                $.each(Object.keys(errors), function (i, v) {
                    erMsgs += `<p>${i + 1}-${errors[v][0]}</p>`;
                })
                errorAlert(erMsgs);
            }, bv);
        });


        $('#checkbox_check').click(function () {

            if ($('#checkbox_check').is(':checked')) {
                $('#quantity').prop("disabled", true);

            } else {
                $('#quantity').prop("disabled", false);
            }
        });
        $(document).ready(function (){
            if ($('#checkbox_check').is(':checked')) {
                $('#quantity').prop("disabled", true);

            } else {
                $('#quantity').prop("disabled", false);
            }
        });


        $(`[name=start_date]`).datepicker({
            dateFormat: 'mm/dd/yy',
            showOn: 'focus',
            showButtonPanel: true,
            closeText: 'Clear', // Text to show for "close" button
            showAnim: "slideDown",
            orientation: "bottom right",
            minDate: 0,
            onClose: function () {
                var event = arguments.callee.caller.caller.arguments[0];
                // If "Clear" gets clicked, then really clear it
                if ($(event.delegateTarget).hasClass('ui-datepicker-close')) {
                    $(this).val('');
                    bv.revalidateField('start_date');
                }
            },
            onSelect: function () {
                bv.revalidateField('start_date');
            }
        });
        $(`[name=end_date]`).datepicker({
            dateFormat: 'mm/dd/yy',
            showOn: 'focus',
            showButtonPanel: true,
            closeText: 'Clear', // Text to show for "close" button
            showAnim: "slideDown",
            orientation: "bottom right",
            minDate: 0,
            onClose: function () {
                var event = arguments.callee.caller.caller.arguments[0];
                // If "Clear" gets clicked, then really clear it
                if ($(event.delegateTarget).hasClass('ui-datepicker-close')) {
                    $(this).val('');
                    bv.revalidateField('end_date');
                }
            },
            onSelect: function () {
                bv.revalidateField('end_date');
            }
        });


        $('#banner_image').dropzone({
            url: "{{route($role.'product.attachment.upload','banner')}}", // Set the url for your upload script location
            paramName: "banner", // The name that will be used to transfer the file
            maxFiles: 10,
            maxFilesize: 10, // MB
            acceptedFiles: "image/*",
            maxfilesreached:1,
            queueLimit:1,

            addRemoveLinks: true,
            removedfile: function(file) {
                file.previewElement.remove();
                console.log(file);
                let file_input = file.name;
                var myobj = document.getElementById(file_input);

                myobj.remove();

            },
            sending: function (file, xhr, formData) {
                formData.append("_token", "{{ csrf_token() }}");
            },

            success: function (http,file) {
                let data = jQuery.parseJSON(http.xhr.response);
                $('#multi_image').last().append('<input type="hidden" value="'+data.file_name+'" name="banner_image[]" id="'+data.file2+'" multiple>');
                $( ".dz-remove" ).addClass('btn btn-danger');
                $( ".dz-remove" ).last().attr('data-info', data.file_name);

            },



        });
        $('#about_event').summernote({
            height: 150,
            popover: {
                image: [

                    // This is a Custom Button in a new Toolbar Area
                    ['custom', ['examplePlugin']],
                    ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                    ['float', ['floatLeft', 'floatRight', 'floatNone']],
                    ['remove', ['removeMedia']]
                ]
            }
        })
            .on('summernote.change', function (customEvent, contents, $editable) {
                // Revalidate the content when its value is changed by Summernote
                bv.revalidateField('about_event');
            });


        bv.addField('start_date', startDateValidator);
        bv.addField('end_date', endDateValidator);
        bv.addField('start_time', startTimeValidator);
        bv.addField('end_time', endTimeValidator);







        $('#start_time').timepicker({
            defaultTime: new Date(),
            minuteStep: 1,
            showSeconds: true,
            showMeridian: true
        })
        $('#end_time').timepicker({
            defaultTime: null,
            minuteStep: 1,
            showSeconds: true,
            showMeridian: true
        })



        $(`[name=start_date]`).datepicker({
            dateFormat: 'mm/dd/yy',
            showOn: 'focus',
            showButtonPanel: true,
            closeText: 'Clear', // Text to show for "close" button
            showAnim: "slideDown",
            orientation: "bottom right",
            minDate: 0,
            onClose: function () {
                var event = arguments.callee.caller.caller.arguments[0];
                // If "Clear" gets clicked, then really clear it
                if ($(event.delegateTarget).hasClass('ui-datepicker-close')) {
                    $(this).val('');
                    bv.revalidateField('start_date');
                }
            },
            onSelect: function () {
                bv.revalidateField('start_date');
            }
        });
        $(`[name=end_date]`).datepicker({
            dateFormat: 'mm/dd/yy',
            showOn: 'focus',
            showButtonPanel: true,
            closeText: 'Clear', // Text to show for "close" button
            showAnim: "slideDown",
            orientation: "bottom right",
            minDate: 0,
            onClose: function () {
                var event = arguments.callee.caller.caller.arguments[0];
                // If "Clear" gets clicked, then really clear it
                if ($(event.delegateTarget).hasClass('ui-datepicker-close')) {
                    $(this).val('');
                    bv.revalidateField('end_date');
                }
            },
            onSelect: function () {
                bv.revalidateField('end_date');
            }
        });

</script>
@endsection