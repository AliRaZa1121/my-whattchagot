@extends('layout.default')
@php
$page_breadcrumbs=[
[
'page'=>route('dashboard'),
'title'=>'dashboard'
],
[
'page'=>route('products.index'),
'title'=>'Product Create'
]
];
@endphp
@section('styles')
<link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">

<style>
    .dz-remove {
        font-size: 10px !important;
    }

    .label-centered-field {
        display: flex;
        align-items: center;
        margin-top: 8px;
    }

    .time-off-action-row label {
        visibility: hidden;
    }

    .time-off-action-row a {
        display: table-cell;
        padding: 5px;
    }

    .day-activities {
        display: flex;
        align-items: center
    }

    .switch-right-side {
        margin-left: 5px;
    }

    #map {
        height: 400px;
        /* The height is 400 pixels */
        width: 100%;
        /* The width is the width of the web page */
    }

    #reset_location {
        margin-top: 5px;
    }
</style>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-custom flex-wrap border-0 pt-6 pb-0">
            <form class="form" id="form" method="post" action="{{route('products.store')}}">
                @csrf

                <div class="card card-custom card-sticky" id="kt_page_sticky_card">
                    <div class="card-header" style="">
                        <div class="card-title col-md-9">
                            <h3 class="card-label">New Product
                            </h3>
                        </div>
                        <div class="card-toolbar">
                            <a href="{{route('products.index')}}" class="btn btn-secondary font-weight-bold mr-2">
                                <i class="ki ki-long-arrow-back icon-xs"></i>Cancel
                            </a>
                            <button type="submit" class="btn btn-primary font-weight-bolder">Save Product</button>
                        </div>


                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">

                                <div class="form-group">
                                    <label for="sellerName">Product Name</label>
                                    <input type="text" required id="name" class="form-control" name="name"
                                        placeholder='e.g.,"Early Brid" or "VIP"'>
                                </div>
                                <div class="form-group">
                                    <label for="sellerName">brand</label>
                                    <div class="input-group ">
                                        <select class="form-control" name="brand" required>
                                            @foreach($brand as $brands)
                                            <option value="{{$brands->id}}">{{$brands->name}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="sellerName">Product Flag</label>
                                    <div class="input-group ">
                                        <select class="form-control" id="flags" name="flags">
                                            <option value="selling">Selling</option>
                                            <option value="trade">Trade</option>
                                            <option value="auction">Auction</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="" for="">
                                                    Color
                                                </label>
                                                <select class="form-control" name="color" placeholder="Select color">
                                                    <option value="Black">Black</option>
                                                    <option value="White">White</option>
                                                    <option value="Red">Red</option>
                                                    <option value="Orange">Orange</option>
                                                    <option value="Green">Green</option>
                                                    <option value="Yellow">Yellow</option>
                                                    <option value="Purpel">Purpel</option>
                                                    <option value="Pink">Pink</option>
                                                </select>
                                            </div>

                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="" for="">
                                                    Size (Optional)
                                                </label>
                                                <select class="form-control" name="size_value" placeholder="Select Size">
                                                    <option>None</option>
                                                    <option value="S">S</option>
                                                    <option value="M">M</option>
                                                    <option value="L">L</option>
                                                    <option value="XL">XL</option>
                                                    <option value="XXL">XXL</option>
                                                </select>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="form-group" id="seller_group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="sellerName">Price</label>
                                                <input type="number" required id="price" class="form-control"
                                                    name="price" placeholder='0'>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label class="col-form-label">Category</label>
                                            <div class="input-group ">
                                                <select class="form-control" id="category_select" name="category_id"
                                                    required>
                                                    <option value="">None</option>
                                                    @foreach($category as $cat)
                                                    <option value="{{$cat->id}}">{{$cat->name}}</option>
                                                    @endforeach
                                                </select>

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="col-form-label">Subcategory</label>
                                            <div class="input-group ">
                                                <select class="form-control" id="sub_category_select"
                                                    name="sub_category_id" required>
                                                    <option value="">None</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group" id="datetime_div">

                                        <div id="date_time_section">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <label class="col-form-label">Start Date (Optional)</label>
                                                    <div class="input-group date">
                                                        <input type="datetime" class="form-control" name="start_date" readonly>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="la la-calendar">
                                                                </i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <label class="col-form-label">End Date (Optional)</label>
                                                    <div class="input-group date">
                                                        <input type="datetime" class="form-control" name="end_date" readonly>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="la la-calendar"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-group" id="datetime_div_auction" style="display: none">

                                        <div id="date_time_section">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <label class="col-form-label">Start Date</label>
                                                    <div class="input-group date">
                                                        <input type="datetime" class="form-control" name="start_date" readonly>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="la la-calendar">
                                                                </i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <label class="col-form-label">Auction End</label>
                                                    <div class="input-group">
                                                        <select class="form-control" name="auction_end">
                                                            <option>Please select auctions days</option>
                                                            <option value="1">1 Day</option>
                                                            <option value="3">3 Day</option>
                                                            <option value="5">5 Day</option>
                                                            <option value="7">7 Day</option>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>


                                    <div class="form-group" id="">
                                        <label for="">Dimensions</label>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="sellerName">Height (Inches)</label>
                                                <input type="number" required id="height_size" class="form-control"
                                                    name="height_size" placeholder='0'>
                                            </div>
                                            <div class="col-md-4">
                                                <label for="sellerName">width (Inches)</label>
                                                <input type="number" required id="width_size" class="form-control"
                                                    name="width_size" placeholder='0'>
                                            </div>
                                            <div class="col-md-4">
                                                <label for="sellerName">length (Inches)</label>
                                                <input type="number" required id="length_size" class="form-control"
                                                    name="length_size" placeholder='0'>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="form-group">
                                        <label for="Description">Description(Optional)</label>
                                        <textarea name="description" required="false" class="form-control"
                                            placeholder="Add a few words about product, e.g., An unforgettable show!"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="Description">Short Details</label>
                                        <textarea name="short_details" required="true" class="form-control"
                                            placeholder="Add a few words about product, e.g., An unforgettable show!"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="sellerName">Seller Policy Text (Optional)
                                        </label>
                                        <textarea required="False" class="form-control" name="policy"
                                            placeholder='This is your event seller. Seller holders must present their sellers on entry. You can either print your seller or present this digital version. You can find all the details about this event on our website. If you have any questions, issues or wish to get a refund, contact the event host. If you can’t attend the event, please get in touch. Looking forward to seeing you there!'></textarea>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="flex-grow-1" style="position: relative;">
                                    <div class="form-group">
                                        <!-- <h1 class="image-upload-msg">Banner Image</h1> -->
                                        <div class="dropzone dropzone-default dropzone-success" id="banner_image">
                                            <div class="dropzone-msg dz-message needsclick">
                                                @if(isset($trip->banner_image))
                                                <img src="{{asset('/storage/'.$trip->banner_image)}}" width="100%">
                                                @else
                                                <h3 class="dropzone-msg-title">Drop files here or click to
                                                    upload.</h3>
                                                <span class="dropzone-msg-desc">Only images are allowed for
                                                    upload</span>
                                                @endif
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="multi_image">
                            </div>



                        </div>
                    </div>

            </form>
        </div>

    </div>
</div>
@endsection
@section('scripts')
<script src="{{asset('js/jquery-ui.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

<script>
    $('#choices-multiple-remove-button').change(function (){
            $('#color_val').val($('#choices-multiple-remove-button').val());
        });
        $('#choices-multiple-remove-button2').change(function (){
            $('#size_value_val').val($('#choices-multiple-remove-button2').val());
        });
        $(document).ready(function () {

            var multipleCancelButton = new Choices('#choices-multiple-remove-button', {
                removeItemButton: true,
                maxItemCount: 50,
                searchResultLimit: 50,
                renderChoiceLimit: 50
            });


        });

        $(document).ready(function () {

            var multipleCancelButton2 = new Choices('#choices-multiple-remove-button2', {
                removeItemButton: true,
                maxItemCount: 50,
                searchResultLimit: 50,
                renderChoiceLimit: 50
            });


        });


        const bv = FormValidation.formValidation(
            document.getElementById('form'), {
                fields: {
                    name: {
                        validators: {
                            notEmpty: {
                                message: 'Name is required'
                            }
                        }
                    },
                    brand: {
                        validators: {
                            notEmpty: {
                                message: 'Brand is required'
                            }
                        }
                    },
                    price: {
                        validators: {
                            notEmpty: {
                                message: 'price is required'
                                }

                        }
                    },
                    category_id: {
                        validators: {
                            notEmpty: {
                                message: 'Category is required'
                            }
                        }
                    },
                    sub_category_id: {
                        validators: {
                            notEmpty: {
                                message: 'Subcategory is required'
                            }
                        }
                    },

                    height_size: {
                        validators: {
                            notEmpty: {
                                message: 'Height is required'
                            }
                        }
                    },
                    width_size: {
                        validators: {
                            notEmpty: {
                                message: 'Width is required'
                            }
                        }
                    },
                    length_size: {
                        validators: {
                            notEmpty: {
                                message: 'Length is required'
                            }
                        }
                    },


                },

                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    submitButton: new FormValidation.plugins.SubmitButton(),
                }
            }
        ).on('core.form.valid', function () {
            // Send the form data to back-end
            // You need to grab the form data and create an Ajax request to send them
            // Get the form instance
            var $form = $('#form');
            // Get the BootstrapValidator instance
            let method = $form.attr('method');
            let url = $form.attr('action');
            let data = new FormData(document.getElementById('form'));
            submitFormAjax(method, url, data, function (res) {

                successAlert(res.msg);
                // $('#form').trigger('reset');
                $('.dz-complete').remove();
                $('.needsclick').show();


                window.location.href = "{{route('products.index')}}";

            }, function (jqXHR) {
                let errors = jqXHR.responseJSON.errors;
                let erMsgs = ``;
                $.each(Object.keys(errors), function (i, v) {
                    erMsgs += `<p>${i + 1}-${errors[v][0]}</p>`;
                })
                errorAlert(erMsgs);
            }, bv);
        });


        // $('#checkbox_check').click(function () {

        //     if ($('#checkbox_check').is(':checked')) {
        //         $('#quantity').prop("disabled", true);

        //     } else {
        //         $('#quantity').prop("disabled", false);
        //     }
        // });


        $(`[name=start_date]`).datetimepicker({
            // dateFormat: 'mm/dd/yy',
            showOn: 'focus',
            showButtonPanel: true,
            closeText: 'Clear', // Text to show for "close" button
            showAnim: "slideDown",
            orientation: "bottom right",
            minDate: 0,
            onClose: function () {
                var event = arguments.callee.caller.caller.arguments[0];
                // If "Clear" gets clicked, then really clear it
                if ($(event.delegateTarget).hasClass('ui-datepicker-close')) {
                    $(this).val('');
                    bv.revalidateField('start_date');
                }
            },
            onSelect: function () {
                bv.revalidateField('start_date');
            }
        });
        $(`[name=end_date]`).datetimepicker({
            // dateFormat: 'mm/dd/yy',
            showOn: 'focus',
            showButtonPanel: true,
            closeText: 'Clear', // Text to show for "close" button
            showAnim: "slideDown",
            orientation: "bottom right",
            minDate: 0,
            onClose: function () {
                var event = arguments.callee.caller.caller.arguments[0];
                // If "Clear" gets clicked, then really clear it
                if ($(event.delegateTarget).hasClass('ui-datepicker-close')) {
                    $(this).val('');
                    bv.revalidateField('end_date');
                }
            },
            onSelect: function () {
                bv.revalidateField('end_date');
            }
        });


        $('#banner_image').dropzone({
            url: "{{route('products.attachment.upload','banner')}}", // Set the url for your upload script location
            paramName: "banner", // The name that will be used to transfer the file
            maxFiles: 10,
            maxFilesize: 10, // MB
            acceptedFiles: "image/*",
            maxfilesreached:1,
            queueLimit:1,

            addRemoveLinks: true,
            removedfile: function(file) {
                file.previewElement.remove();
                let file_input = file.name;
                var myobj = document.getElementById(file_input);

                myobj.remove();

            },
            sending: function (file, xhr, formData) {
                formData.append("_token", "{{ csrf_token() }}");
            },

            success: function (http,file) {
                let data = jQuery.parseJSON(http.xhr.response)
                        console.log(file);
                        $(".dz-remove").addClass('btn btn-danger');
                        $(".dz-remove").last().attr('data-info', data.file_name);
                        $('.dz-preview').last().append('<input type="hidden" value="'+data.file_name+'" name="banner_image[]" id="'+data.file_name+'" multiple>');
                        $('#banner_image').previewElement.appendChild(removeButton);
            },

        });

        $('#flags').change(function(){
            if ($(this).val() == "auction") {
                $("#datetime_div_auction").show();
                $("#datetime_div").hide();
            }
            else{
                $("#datetime_div_auction").hide();
                $("#datetime_div").show();

            }
        });



</script>
@include('pages.admin.product.product_script')
@endsection
