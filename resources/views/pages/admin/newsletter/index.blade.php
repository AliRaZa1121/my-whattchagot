@extends('layout.default')

@section('styles')
    <link rel="stylesheet" href="{{asset('plugins/custom/datatables/datatables.bundle.css?v=7.0.4')}}">
@endsection
@section('content')
    @include('pages.message')
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Newsletter List
                </h3>
            </div>

        </div>
        <div class="card-body">
            <!--begin: Search Form-->
            <!--begin::Search Form-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Email</th>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody>
                @foreach($newsletter as $c)
                    <tr>
                        <td>{{$c->id}}</td>
                        <td>{{$c->email}}</td>
                        <td>{{$c->created_at->format('m-d-Y')}}
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


@endsection

{{-- Styles Section --}}
@section('styles')

@endsection

@section('scripts')
    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js?v=7.0.4') }}" type="text/javascript"></script>
    <script>
        var table = $('#kt_datatable');

        // begin first table
        table.DataTable({
                dom: 'Bfrtip',
            buttons: [
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ]
        });
    </script>
@endsection
