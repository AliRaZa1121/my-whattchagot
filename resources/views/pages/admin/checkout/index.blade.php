@extends('layout.default')
@php
    if (Auth::guard('admin')->check()) {
        $role='';
        } else if (Auth::guard('seller')->check()) {
                $role='seller_';

        } else if (Auth::guard('buyer')->check()) {
            $role='buyer_';

        }
        $cage_breadcrumbs=[[
        'page'=>route($role.'dashboard'),
        'title'=>'Dashboard'
        ],
        [
        'page'=>route('checkout.list'),
        'title'=>'Checkout'
        ]
        ];
@endphp
@section('styles')
    <link rel="stylesheet" href="{{asset('plugins/custom/datatables/datatables.bundle.css?v=7.0.4')}}">
@endsection
@section('content')
    @include('pages.message')

    <style>
        .acidjs-rating-stars,
        .acidjs-rating-stars label::before {
            display: inline-block;
        }

        .acidjs-rating-stars label:hover,
        .acidjs-rating-stars label:hover ~ label {
            color: #f5b301;
        }

        .acidjs-rating-stars * {
            margin: 0;
            padding: 0;
        }

        .acidjs-rating-stars input {
            display: none;
        }

        .acidjs-rating-stars {
            unicode-bidi: bidi-override;
            direction: rtl;
        }

        .acidjs-rating-stars label {
            color: #ccc;
        }

        .acidjs-rating-stars label::before {
            content: "\2605";
            width: 18px;
            line-height: 18px;
            text-align: center;
            font-size: 18px;
            cursor: pointer;
        }

        .acidjs-rating-stars input:checked ~ label {
            color: #f5b301;
        }

        .acidjs-rating-disabled {
            opacity: .50;

            -webkit-pointer-events: none;
            -moz-pointer-events: none;
            pointer-events: none;
        }
    </style>
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Checkout List
                </h3>
            </div>
            <div class="card-toolbar">

            </div>
        </div>
        <div class="card-body">
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Product name</th>
                    <th>Type</th>
                    <th>Price</th>
                    <th>Qty</th>
                    <th>Total Price</th>
                    <th>Seller Name</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($checkouts as $c)
                    <tr>
                        <td>{{$c->id}}</td>
                        <td>{{$c->products->name}}</td>
                        <td>{{$c->type}}</td>
                        <td>${{$c->price}}</td>
                        <td>{{$c->qty}}</td>
                        <td>${{$c->price*$c->qty}}</td>
                        <td>{{isset($c->products->users) ? $c->products->users->name : 'User Not Found'}}</td>
                        <td>
                        <span style="overflow: visible; position: relative; width: 125px;">
                            <div class="dropdown dropdown-inline "> <a href="javascript:;"
                                                                       class="btn btn-sm btn-light btn-text-primary btn-icon mr-2"
                                                                       data-toggle="dropdown">
                                    <span class="svg-icon svg-icon-md"> <svg xmlns="http://www.w3.org/2000/svg"
                                                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                             width="24px" height="24px"
                                                                             viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                <path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z"
                                                      fill="#000000"></path>
                                            </g>
                                        </svg> </span> </a>
                                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                    <ul class="navi flex-column navi-hover py-2">
                                        <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2">
                                            Choose an action: </li>

                                            <li class="navi-item">
                                                <a href="{{ route('checkout.single',$c->id) }}"
                                                   class="navi-link">
                                                    <span class="navi-icon">
                                                        <i class="fa fa-check-circle"></i>
                                                    </span>
                                                    <span class="navi-text">Checkout</span>
                                                </a>
                                             </li>



                                    </ul>
                                </div>
                            </div>
                        </span>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    



@endsection

{{-- Styles Section --}}
@section('styles')

@endsection

@section('scripts')
    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js?v=7.0.4') }}" type="text/javascript"></script>
    <script>
        
        var table = $('#kt_datatable');

        // begin first table
        table.DataTable({
            // DOM Layout settings
            dom: "<'row py-3'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>" +
                "<'row py-3'<'col-sm-12'tr>>" +
                "<'row py-3'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>", // read more: https://datatables.net/examples/basic_init/dom.html

            columnDefs: [{
                targets: -1,
                title: 'Actions',
                orderable: false,

            },
            ],
        });
    </script>
@endsection