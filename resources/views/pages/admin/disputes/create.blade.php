@extends('layout.default')
@section('styles')
<link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<style>
    .label-centered-field {
        display: flex;
        align-items: center;
        margin-top: 8px;
    }

    .time-off-action-row label {
        visibility: hidden;
    }

    .time-off-action-row a {
        display: table-cell;
        padding: 5px;
    }

    .day-activities {
        display: flex;
        align-items: center
    }

    .switch-right-side {
        margin-left: 5px;
    }

    #map {
        height: 400px;
        /* The height is 400 pixels */
        width: 100%;
        /* The width is the width of the web page */
    }

    #reset_location {
        margin-top: 5px;
    }
</style>
@endsection
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card card-custom flex-wrap border-0 pt-6 pb-0">
            <form class="form" id="form" method="POST" action="{{ route('disputes.store.buyer')}}">
                @csrf
                <div class="card card-custom card-sticky" id="kt_page_sticky_card">
                    <div class="card-header" style="">
                        <div class="card-title col-md-8">
                            <h3 class="card-label">Dispute Form
                                <!-- <i class="mr-2"></i> -->
                                <!-- <small class="">try to scroll the page</small> -->
                            </h3>
                        </div>
                        <div class="card-toolbar">
                            <a href="{{route('sub-category.index')}}" class="btn btn-secondary font-weight-bold mr-2">
                                <i class="ki ki-long-arrow-back icon-xs"></i>
                                Cancel
                            </a>
                            <button type="submit" class="btn btn-primary font-weight-bolder">Save Dispute</button>
                        </div>


                    </div>
                    <div class="card-body">
                        <div class="row">

                            @if($dispute != null)
                            <div class="col-md-8">
                               <div class="form-group">
                                   <label for="status">Status</label>
                                   <input type="text" required id="status" value="{{  $dispute->status  }}"
                                       class="form-control" name="status" readonly>
                               </div>
                           </div>
                            @endif

                            <div class="col-md-8">

                                <div class="form-group">
                                    <label for="name">Your Name</label>
                                    <input type="text" value="{{ auth()->user()->name  }}" required id="name"
                                        class="form-control" name="name" readonly>
                                </div>

                            </div>

                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="email">Your Email</label>
                                    <input type="email" required id="email" value="{{ auth()->user()->email  }}"
                                        class="form-control" name="email" readonly>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="order_id" value="{{ $order_id }}">
                        <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                        

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message">Message</label>
                                    <textarea required id="message" class="form-control"
                                        name="message">{{ ($dispute != null) ? $dispute->message : '' }}</textarea>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>

            </form>
        </div>

    </div>
</div>
@endsection
@section('scripts')
<script src="{{asset('js/jquery-ui.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

<script>
    const message = {
            validators: {
                notEmpty: {
                    message: 'Enter is name'
                }
            }
        }

        const bv = FormValidation.formValidation(

            document.getElementById('form'), {
                fields: {
                    message: {
                        validators: {
                            notEmpty: {
                                message: 'Message Feild is Required'
                            }
                        }
                    },

                }, plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    submitButton: new FormValidation.plugins.SubmitButton(),
                }
            }
        ).on('core.form.valid', function () {
            // Send the form data to back-end
            // You need to grab the form data and create an Ajax request to send them
            // Get the form instance
            var $form = $('#form');
            // Get the BootstrapValidator instance
            let method = $form.attr('method');
            let url = $form.attr('action');
            let data = new FormData(document.getElementById('form'));
            submitFormAjax(method, url, data, function (res) {
                if (res.code == 200) {
                successAlert(res.msg);
                $('.dz-complete').remove();
                $('.needsclick').show();
                window.location.href = "{{route('disputes.create.buyer',$order_id)}}";
                }
                else{
                    errorAlert('Something went wrong');
                }


            }, function (jqXHR) {
                let errors = jqXHR.responseJSON.errors;
                let erMsgs = ``;
                $.each(Object.keys(errors), function (i, v) {
                    erMsgs += `<p>${i + 1}-${errors[v][0]}</p>`;
                })
                errorAlert(erMsgs);
            }, bv);
        });


        function readURL(input, id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $(id)
                        .attr('src', e.target.result);
                    $('.profile-icon').hide();
                    $(id).show();
                };
                reader.readAsDataURL(input.files[0]);
            }
        }



</script>
@endsection
