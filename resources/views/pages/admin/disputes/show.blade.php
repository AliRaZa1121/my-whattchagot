@extends('layout.default')
@section('styles')
<link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<style>
    .label-centered-field {
        display: flex;
        align-items: center;
        margin-top: 8px;
    }

    .time-off-action-row label {
        visibility: hidden;
    }

    .time-off-action-row a {
        display: table-cell;
        padding: 5px;
    }

    .day-activities {
        display: flex;
        align-items: center
    }

    .switch-right-side {
        margin-left: 5px;
    }

    #map {
        height: 400px;
        /* The height is 400 pixels */
        width: 100%;
        /* The width is the width of the web page */
    }

    #reset_location {
        margin-top: 5px;
    }
</style>
@endsection
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card card-custom flex-wrap border-0 pt-6 pb-0">
            <form class="form" id="form" method="POST" action="{{ route('disputes.store.buyer')}}">
                @csrf
                <div class="card card-custom card-sticky" id="kt_page_sticky_card">
                    <div class="card-header" style="">
                        <div class="card-title col-md-8">
                            <h3 class="card-label">Dispute Form
                                <!-- <i class="mr-2"></i> -->
                                <!-- <small class="">try to scroll the page</small> -->
                            </h3>
                        </div>
                        <div class="card-toolbar">
                          @if(Auth::guard('admin')->check())
                            <a href="{{route('dispute.list.admin')}}" class="btn btn-secondary font-weight-bold mr-2">
                                <i class="ki ki-long-arrow-back icon-xs"></i>
                               Go Back
                            </a>
                              @elseif(Auth::guard('seller')->check())
                                 <a href="{{route('dispute.list.seller')}}" class="btn btn-secondary font-weight-bold mr-2">
                                <i class="ki ki-long-arrow-back icon-xs"></i>
                               Go Back
                            </a>
                            @endif
                            {{-- <button type="submit" class="btn btn-primary font-weight-bolder">Save Dispute</button> --}}
                        </div>


                    </div>
                    <div class="card-body">
                        <div class="row">

                            @if($dispute != null)
                            <div class="col-md-8">
                               <div class="form-group">
                                   <label for="status">Status</label>
                                   <input type="text" required id="status" value="{{  $dispute->status  }}"
                                       class="form-control" name="status" readonly>
                               </div>
                           </div>
                            @endif

                            <div class="col-md-8">

                                <div class="form-group">
                                    <label for="name">Your Name</label>
                                    <input type="text" value="{{ auth()->user()->name  }}" required id="name"
                                        class="form-control" name="name" readonly>
                                </div>

                            </div>

                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="email">Your Email</label>
                                    <input type="email" required id="email" value="{{ auth()->user()->email  }}"
                                        class="form-control" name="email" readonly>
                                </div>
                            </div>
                        </div>
                    
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message">Message</label>
                                    <textarea required id="message" class="form-control" readonly
                                        name="message">{{ ($dispute != null) ? $dispute->message : '' }}</textarea>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>

            </form>
        </div>

    </div>
</div>
@endsection
@section('scripts')
<script src="{{asset('js/jquery-ui.js')}}"></script>


@endsection
