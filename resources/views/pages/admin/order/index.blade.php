@extends('layout.default')
@php
$page_breadcrumbs=[[
'page'=>route('dashboard'),
'title'=>'dashboard'
],
[
'page'=>route('orders.index'),
'title'=>'Order'
]
];
@endphp
@section('styles')
<link rel="stylesheet" href="{{asset('plugins/custom/datatables/datatables.bundle.css?v=7.0.4')}}">
@endsection
@section('content')
@include('pages.message')
@if($errors->any())
<div class="alert alert-success" role="alert">
    {{$errors->first()}}
</div>
@endif
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Order List
            </h3>
        </div>
        <div class="card-toolbar">

        </div>
    </div>
    <div class="card-body">

        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Product name</th>
                    <th>Price</th>
                    <th>Qty</th>
                    <th>Color</th>
                    <th>Size</th>
                    <th>Shipping Price</th>
                    <th>Total Price</th>
                    <th>Customer Name</th>
                    @if(auth()->user()->user_type === "admin")
                    <th>Seller Name</th>
                    @endif
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($order as $key => $p)

                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$p->products->name ?? 'N/A'}}</td>
                    <td>${{$p->price}}</td>
                    <td>{{$p->qty}}</td>
                    <td>{{$p->color ?? 'None'}}</td>
                    <td>{{$p->size ?? 'None'}}</td>
                    <td>${{$p->shipping_price}}</td>
                    <td>${{($p->price*$p->qty)+$p->shipping_price}}</td>
                    <td>{{$p->coustomer_name->name}}</td>
                    @if(auth()->user()->user_type === "admin")
                    <td>{{$p->seller_name->name}}</td>
                    @endif
                    <td>{{$p->status}}</td>
                    <td>
                        <span style="overflow: visible; position: relative; width: 125px;">
                            <div class="dropdown dropdown-inline "> <a href="javascript:;"
                                    class="btn btn-sm btn-light btn-text-primary btn-icon mr-2" data-toggle="dropdown">
                                    <span class="svg-icon svg-icon-md"> <svg xmlns="http://www.w3.org/2000/svg"
                                            xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                            viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                <path
                                                    d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z"
                                                    fill="#000000"></path>
                                            </g>
                                        </svg> </span> </a>
                                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                    <ul class="navi flex-column navi-hover py-2">
                                        <li
                                            class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2">
                                            Choose an action: </li>
                                            @if($p->status !== "Canceled")
                                                <li class="navi-item">
                                                    <a
                                                    href="{{ $p->status === "Pending" ? route('order_items.update.status',['order_item'=>$p->id,'status'=>'In Process']) : '#' }} "
                                                        class="navi-link">
                                                        <span class="navi-icon">
                                                            <i class="{{ $p->status === "Pending" ? 'fas fa-exclamation-circle' : 'fa fa-check-circle' }}"></i>
                                                        </span>
                                                        <span class="navi-text">In Process</span>
                                                    </a>
                                                </li>

                                                <li class="navi-item">
                                                    <a href="{{ $p->status === "In Process" ? route('order_items.update.status',['order_item'=>$p->id,'status'=>'Warehouse']) : '#' }}"
                                                        class="navi-link">
                                                        <span class="navi-icon">
                                                            <i class="{{ $p->status === "In Process" ? 'fas fa-exclamation-circle' : 'fa fa-check-circle' }}"></i>
                                                        </span>
                                                        <span class="navi-text">Warehouse</span>
                                                    </a>
                                                </li>

                                                <li class="navi-item">
                                                    <a href="{{ $p->status === "In Process" ? route('order_items.update.status',['order_item'=>$p->id,'status'=>'Shipping']) : '#' }}"
                                                        class="navi-link">
                                                        <span class="navi-icon">
                                                            <i class="{{ $p->status === "In Process" ? 'fas fa-exclamation-circle' : 'fa fa-check-circle' }}"></i>
                                                        </span>
                                                        <span class="navi-text">Shipping</span>
                                                    </a>
                                                </li>
                                                @endif

                                                <li class="navi-item">
                                                    <span
                                                        onclick="cancel_order('{{route('order_items.update.status',['order_item'=>$p->id,'status'=>'Canceled'])}}')"
                                                        class="navi-link">
                                                        <span class="navi-icon">
                                                            <i class="fa fa-check-circle"></i>
                                                        </span>
                                                        <span class="navi-text">Cancel</span>
                                                    </span>
                                                </li>

                                                <li class="navi-item">
                                                    <a data-toggle="modal"
                                                        onclick='model_data({{$p->id}},{{$p->order_id}},"{{$p->products->name}}","{{$p->products->price}}",{{$p->qty}},{{$p->shipping_price}},{{($p->products->price*$p->qty)+$p->shipping_price}},{{$p->product_id}},{{$p->user_id}},{{$p->seller_id}},"{{$p->created_at}}")'
                                                        data-target=".bd-example-modal-sm" class="navi-link">
                                                        <span class="navi-icon">
                                                            <i class="fa fa-eye"></i>
                                                        </span>
                                                        <span class="navi-text">View And Print</span>
                                                    </a>
                                                </li>

                                    </ul>
                                </div>
                            </div>
                        </span>
                    </td>
                </tr>


                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="printarea" style="min-width: 100%">
                <div class="modal-header">
                    <h5 class="modal-title" id=""> Invoice<h3 class="pull-right" id="order_id">Order # 12345</h3>
                    </h5>

                </div>
                <hr>
                <div class="modal-body" style="min-width: 100%">
                    <!--checkout Page Start here-->
                    <div class="container">
                        <div class="row" style="width: 100%;">
                            <div style="width: 100%;">



                                <div class="row">
                                    <div class="col-md-6" style="min-width: 50%; float: left">
                                        <address>
                                            <strong>Billed To:</strong><br>
                                            <span id="billing_name"></span><br>
                                            <span id="billing_email"></span><br>
                                            <span id="billing_phone"></span><br>
                                            <span id="billing_country"></span><br>
                                            <span id="billing_state"></span><br>
                                            <span id="billing_zip"></span><br>
                                            <span id="billing_address"></span><br>

                                        </address>
                                    </div>
                                    <div class="col-md-6 text-right"
                                        style="min-width: 50%;float: right;      text-align: right;">
                                        <address>
                                            <strong>Shipped To:</strong><br>
                                            <span id="shipping_name"></span><br>
                                            <span id="shipping_email"></span><br>
                                            <span id="shipping_phone"></span><br>
                                            <span id="shipping_country"></span><br>
                                            <span id="shipping_state"></span><br>
                                            <span id="shipping_zip"></span><br>
                                            <span id="shipping_address"></span><br>
                                        </address>

                                        <address>
                                            <strong>Order Date:</strong><br>
                                            <span id="order_date"></span><br><br>
                                        </address>

                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">

                                    <div class="panel-body">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><strong>Order summary</strong></h3>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-condensed" style="min-width: 100%;">
                                                <thead>
                                                    <tr style="border-bottom: 1px solid black;">
                                                        <td><strong>Product Name</strong></td>
                                                        <td class="text-center"><strong>Price</strong></td>
                                                        <td class="text-center"><strong>Quantity</strong></td>
                                                        <td class="text-center"><strong>Shipping Price</strong></td>
                                                        <td class="text-right"><strong>Totals</strong></td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                                    <tr>
                                                        <td id="product_name_order">BS-200</td>
                                                        <td id="price_order" class="text-center">$10.99</td>
                                                        <td id="quantity_order" class="text-center">1</td>
                                                        <td id="shpping_price" class="text-center">1</td>
                                                        <td id="total_order" class="text-right">$10.99</td>
                                                    </tr>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-primary hidden-print print" onclick='printDiv();'><i class="fa fa-print"
                        aria-hidden="true"></i>
                    Print
                </button>
            </div>
        </div>
    </div>
</div>


@endsection

{{-- Styles Section --}}
@section('styles')

@endsection

@section('scripts')
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js?v=7.0.4') }}" type="text/javascript"></script>
<script>



    function cancel_order($url){

        Swal.fire({
            title: 'the amount will refund if you cancel the order, Do you want to cancel the order?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: `Yes`,
            denyButtonText: `No`,
            customClass: {
                cancelButton: 'order-1 right-gap',
                confirmButton: 'order-2',
                denyButton: 'order-3',
            }
        }).then((result) => {
            if (result.isConfirmed) {
                Swal.fire('Saved!', '', 'success')
                window.location = $url;
            } else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
}



       function model_data(id,order_id, product_name,product_price,qty,shpping_price,product_total, product_id, user_id, seller_id,created_at) {

           $('#product_name_order').html(product_name);
           $('#price_order').html('$'+product_price);
           $('#quantity_order').html(qty);
           $('#total_order').html('$'+product_total);
           $('#shpping_price').html('$'+shpping_price);
           $('#order_id').html('Order # '+id);
           $('#order_date').html(created_at);

           $.ajax({
               url: "{{url('/ajax_get_billing_address')}}",
               type: "GET",
               data: {
                   "_token": "{{ csrf_token() }}",
                   id: order_id,


               },
               success: function (response) {

                   let obj = JSON.parse(response);
                   $('#billing_name').html(obj['firstname']+' '+obj['lastname']);
                   $('#billing_email').html(obj['email']);
                   $('#billing_phone').html(obj['phone']);
                   $('#billing_country').html(obj['country']);
                   $('#billing_state').html(obj['state']);
                   $('#billing_zip').html(obj['zip']);
                   $('#billing_address').html(obj['address']);
               }
           });
           $.ajax({
               url: "{{url('/ajax_get_shipping')}}",
               type: "GET",
               data: {
                   "_token": "{{ csrf_token() }}",
                   id: order_id,


               },
               success: function (response) {

                   let obj = JSON.parse(response);
                   $('#shipping_name').html(obj['firstname']+' '+obj['lastname']);
                   $('#shipping_email').html(obj['email']);
                   $('#shipping_phone').html(obj['phone']);
                   $('#shipping_country').html(obj['country']);
                   $('#shipping_state').html(obj['state']);
                   $('#shipping_zip').html(obj['zip']);
                   $('#shipping_address').html(obj['address']);

               }
           });

       }
       @if (auth()->user()->user_type === "admin")
       var target_coloum = 8;
       @else
       var target_coloum = 9;
       @endif
       var table = $('#kt_datatable');

       // begin first table
       table.DataTable({
           // DOM Layout settings
           dom: "<'row py-3'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
               "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>" +
               "<'row py-3'<'col-sm-12'tr>>" +
               "<'row py-3'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
               "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>", // read more: https://datatables.net/examples/basic_init/dom.html

           columnDefs: [{
               targets: -1,
               title: 'Actions',
               orderable: false,
           },
               {
                   width: '75px',
                   targets: target_coloum,
                   render: function (data, type, full, meta) {
                       var status = {
                           'Pending': {
                               'title': 'Pending',
                               'class': 'label-light-primary'
                           },

                           'Shipping': {
                               'title': 'Shipping',
                               'class': ' label-light-info'
                           },
                           'Warehouse': {
                               'title': 'Warehouse',
                               'class': ' label-light-info'
                           },
                           'Delivered': {
                               'title': 'Delivered',
                               'class': ' label-light-success'
                           },
                           'Canceled': {
                               'title': 'Canceled',
                               'class': ' label-light-danger'
                           },
                           'In Process': {
                               'title': 'In Process',
                               'class': 'label-light-primary'
                           },
                           'dispute': {
                               'title': 'Refunded',
                               'class': 'label-light-danger'
                           },



                       };
                       if (typeof status[data] === 'undefined') {
                           return data;
                       }
                       return '<span class="label label-lg font-weight-bold ' + status[data].class + ' label-inline">' + status[data].title + '</span>';
                   },
               },
           ],
       });

       function printDiv() {

           var divToPrint = document.getElementById('printarea');

           var newWin = window.open('', 'Print-Window');

           newWin.document.open();

           newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');

           newWin.document.close();

           setTimeout(function () {
               newWin.close();
           }, 10);

       }


       $("#btn-print").click(function () {
           $("#printarea").print();
       });
       $('.print').on('click', function () { // select print button with class "print," then on click run callback function
           $.print(".content"); // inside callback function the section with class "content" will be printed
       });

       // plugin creator and full list of options: https://github.com/DoersGuild/jQuery.print
</script>
@endsection
