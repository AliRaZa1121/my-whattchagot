@extends('layout.default')
@php
    $page_breadcrumbs=[
    [
    'page'=>route('dashboard'),
    'title'=>'dashboard'
    ],
    [
    'page'=>route('category.index'),
    'title'=>'Category Create'
    ]
    ];
@endphp
@section('styles')
    <link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
    <style>
        .label-centered-field {
            display: flex;
            align-items: center;
            margin-top: 8px;
        }

        .time-off-action-row label {
            visibility: hidden;
        }

        .time-off-action-row a {
            display: table-cell;
            padding: 5px;
        }

        .day-activities {
            display: flex;
            align-items: center
        }

        .switch-right-side {
            margin-left: 5px;
        }

        #map {
            height: 400px;
            /* The height is 400 pixels */
            width: 100%;
            /* The width is the width of the web page */
        }

        #reset_location {
            margin-top: 5px;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-custom flex-wrap border-0 pt-6 pb-0">
                <form class="form" id="form" method="POST" action="{{route('category.store')}}">
                    @csrf
                  
                    <div class="card card-custom card-sticky" id="kt_page_sticky_card">
                        <div class="card-header" style="">
                            <div class="card-title col-md-9">
                                <h3 class="card-label">New Category
                                    <!-- <i class="mr-2"></i> -->
                                    <!-- <small class="">try to scroll the page</small> -->
                                </h3>
                            </div>
                            <div class="card-toolbar">
                                <a href="{{route('category.index')}}" class="btn btn-secondary font-weight-bold mr-2">
                                    <i class="ki ki-long-arrow-back icon-xs"></i>Cancel
                                </a>
                                <button type="submit" class="btn btn-primary font-weight-bolder">Save Category</button>
                            </div>


                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8">

                                    <div class="form-group">
                                        <label for="productName">Category Name</label>
                                        <input type="text" required id="name" class="form-control" name="name"
                                               placeholder='e.g.,"Early Brid" or "VIP"'>
                                    </div>

                                </div>


                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('js/jquery-ui.js')}}"></script>

    <script>
        const bv = FormValidation.formValidation(

            document.getElementById('form'), {
                fields: {
                    name: {
                        validators: {
                            notEmpty: {
                                message: 'Category Name is required'
                            }
                        }
                    },
                }, plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    submitButton: new FormValidation.plugins.SubmitButton(),
                }
            }
        ).on('core.form.valid', function () {
            // Send the form data to back-end
            // You need to grab the form data and create an Ajax request to send them
            // Get the form instance
            var $form = $('#form');
            // Get the BootstrapValidator instance
            let method = $form.attr('method');
            let url = $form.attr('action');
            let data = new FormData(document.getElementById('form'));
            submitFormAjax(method, url, data, function (res) {

                successAlert(res.msg);
                $('#form').trigger('reset');
                $('.dz-complete').remove();
                $('.needsclick').show();


                window.location.href = "{{route('category.index')}}";

            }, function (jqXHR) {
                let errors = jqXHR.responseJSON.errors;
                let erMsgs = ``;
                $.each(Object.keys(errors), function (i, v) {
                    erMsgs += `<p>${i + 1}-${errors[v][0]}</p>`;
                })
                errorAlert(erMsgs);
            }, bv);
        });
    </script>
@endsection