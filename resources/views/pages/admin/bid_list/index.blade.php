@extends('layout.default')
@php
    if (Auth::guard('admin')->check()) {
        $role='';
        } else if (Auth::guard('seller')->check()) {
                $role='seller_';

        } else if (Auth::guard('buyer')->check()) {
            $role='buyer_';

        }
        $page_breadcrumbs=[[
        'page'=>route($role.'dashboard'),
        'title'=>'dashboard'
        ],
         [
        'page'=>url('seller/bid_list_seller'),
        'title'=>'Product Bidding'
        ]

        ];
@endphp
@section('styles')
    <link rel="stylesheet" href="{{asset('plugins/custom/datatables/datatables.bundle.css?v=7.0.4')}}">
@endsection
@section('content')
    @include('pages.message')

    <style>
        /*.rate:not(:checked) > label {*/
        /*    float: right;*/
        /*    width: 1em;*/
        /*    overflow: hidden;*/
        /*    white-space: nowrap;*/
        /*    cursor: pointer;*/
        /*    font-size: 20px;*/
        /*    color: #ccc;*/
        /*    line-height: 1;*/
        /*}*/
        /*.rate:not(:checked) > label:before {*/
        /*    content: '★ ';*/
        /*}*/

        .acidjs-rating-stars,
        .acidjs-rating-stars label::before {
            display: inline-block;
        }

        .acidjs-rating-stars label:hover,
        .acidjs-rating-stars label:hover ~ label {
            color: #f5b301;
        }

        .acidjs-rating-stars * {
            margin: 0;
            padding: 0;
        }

        .acidjs-rating-stars input {
            display: none;
        }

        .acidjs-rating-stars {
            unicode-bidi: bidi-override;
            direction: rtl;
        }

        .acidjs-rating-stars label {
            color: #ccc;
        }

        .acidjs-rating-stars label::before {
            content: "\2605";
            width: 18px;
            line-height: 18px;
            text-align: center;
            font-size: 18px;
            cursor: pointer;
        }

        .acidjs-rating-stars input:checked ~ label {
            color: #f5b301;
        }

        .acidjs-rating-disabled {
            opacity: .50;

            -webkit-pointer-events: none;
            -moz-pointer-events: none;
            pointer-events: none;
        }
    </style>
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Bid List
                </h3>
            </div>
            <div class="card-toolbar">

            </div>
        </div>
        <div class="card-body">
            <!--begin: Search Form-->
            <!--begin::Search Form-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Product name</th>
                    <th>Base Price</th>
                    <th>Bid Price</th>
                    @if(Auth::guard('buyer')->check())
                    <th>Seller Name</th>
                    @endif
                    @if(Auth()->guard('seller')->check())
                    <th>buyer Name</th>
                    @endif
                    @if(Auth::guard('admin')->check())
                    <th>Seller Name</th>
                    <th>buyer Name</th>
                    @endif
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                    <?php $no = 1;?>

                @foreach($bitlist as $p)
                    <tr>
                        <td>{{$no++}}</td>
                        <td> <a href="{{ route('auction-single', ['id' => $p->product_id]) }}" target="target_">{{$p->products->name}}</a></td>
                        <td>${{$p->products->price ?? 'N/A'}}</td>
                        <td>${{$p->user_price}}</td>
                        @if(Auth::guard('buyer')->check())
                        <td>{{$p->seller_name->name ?? 'N/A'}}</td>
                        @endif
                        @if(Auth::guard('seller')->check())
                        <td>{{$p->buyer_name->name ?? 'N/A'}}</td>
                        @endif
                        @if(Auth::guard('admin')->check())
                        <td>{{$p->seller_name->name ?? 'N/A'}}</td>
                        <td>{{$p->buyer_name->name ?? 'N/A'}}</td>
                        @endif
                        @if($p->bidprice == null)
                        <td>Auction In Process</td>
                        @elseif($p->bidprice->end_auction == 0)
                        <td>Auction In Process</td>
                        @else
                        <td>@if($p->bidprice->end_auction == 1 && $p->user_price == $p->bidprice->bid_price_update) win @else Lose @endif</td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>





@endsection

{{-- Styles Section --}}
@section('styles')

@endsection

@section('scripts')
    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js?v=7.0.4') }}" type="text/javascript"></script>
    <script>
        function model_data1(comment, rate, product_name) {
            $('#product_name2').html(product_name);
            $('#comment').val(comment);
            $('#rate' + rate).prop('checked', true);


        }

        function model_data(id, product_name, product_id, user_id, seller_id) {
            // alert(id);
            // $('.input[name=group-2]').prop('radio',false);

            $('#order_id').val(id);
            $('#product_name').html(product_name);
            $('#product_id').val(product_id);
            $('#user_id').val(user_id);
            $('#seller_id').val(seller_id);

        }
        <?php if(Auth()->guard('admin')->check()) {?>
        var target_coloum = 1;
        <?php }else { ?>
        var target_coloum = 1;

        <?php } ?>
        var table = $('#kt_datatable');

        // begin first table
        table.DataTable({
            // DOM Layout settings
            dom: "<'row py-3'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>" +
                "<'row py-3'<'col-sm-12'tr>>" +
                "<'row py-3'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>", // read more: https://datatables.net/examples/basic_init/dom.html

            columnDefs: [{
                targets: -1,
                title: 'Actions',
                orderable: false,

            },
                {
                    width: '75px',
                    targets: target_coloum,
                    render: function (data, type, full, meta) {
                        var status = {
                            'Pending': {
                                'title': 'Pending',
                                'class': 'label-light-primary'
                            },

                            'Shipping': {
                                'title': 'Shipping',
                                'class': ' label-light-info'
                            },
                            'Warehouse': {
                                'title': 'Warehouse',
                                'class': ' label-light-info'
                            },
                            'Delivered': {
                                'title': 'Delivered',
                                'class': ' label-light-success'
                            },
                            'Canceled': {
                                'title': 'Canceled',
                                'class': ' label-light-danger'
                            },

                        };
                        if (typeof status[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="label label-lg font-weight-bold ' + status[data].class + ' label-inline">' + status[data].title + '</span>';
                    },
                },
            ],
        });
    </script>
@endsection
