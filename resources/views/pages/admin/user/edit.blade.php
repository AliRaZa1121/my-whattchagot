@extends('layout.default')
@php
        $page_breadcrumbs=[
        [
        'page'=>route('dashboard'),
        'title'=>'dashboard'
        ],
        [
        'page'=>route('seller.index'),
        'title'=>'Profile Update'
        ]
        ];
@endphp
@section('styles')
    <link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
    <script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">

    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <style>
        .dz-remove {
            font-size: 10px !important;
        }

        .label-centered-field {
            display: flex;
            align-items: center;
            margin-top: 8px;
        }

        .time-off-action-row label {
            visibility: hidden;
        }

        .time-off-action-row a {
            display: table-cell;
            padding: 5px;
        }

        .day-activities {
            display: flex;
            align-items: center
        }

        .switch-right-side {
            margin-left: 5px;
        }

        #map {
            height: 400px;
            /* The height is 400 pixels */
            width: 100%;
            /* The width is the width of the web page */
        }

        #reset_location {
            margin-top: 5px;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-custom flex-wrap border-0 pt-6 pb-0">
                <form class="form" id="form" method="post" action="{{route('user-update')}}">
                    @csrf

                    <div class="card card-custom card-sticky" id="kt_page_sticky_card">
                        <div class="card-header" style="">
                            <div class="card-title col-md-9">
                                <h3 class="card-label">Profile Update
                                </h3>
                            </div>
                            <div class="card-toolbar">
                                <a href="{{url()->previous()}}"
                                   class="btn btn-secondary font-weight-bold mr-2">
                                    <i class="ki ki-long-arrow-back icon-xs"></i>Cancel
                                </a>
                                <button type="submit" class="btn btn-primary font-weight-bolder">Update Profile</button>
                            </div>


                        </div>

                        <div class="card-body">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="flex-grow-1" style="position: relative;">
                                        <div class="form-group">
                                            <label class="image-upload-msg">Profile Image</label>
                                            <div class="dropzone dropzone-default dropzone-success" id="banner_image">
                                                <div class="dropzone-msg dz-message needsclick">
                                                    @if(!empty($user_image))
                                                        <img src="{{asset('/storage/'.$user_image->image)}}"
                                                             width="100px">
                                                    @else
                                                        <h3 class="dropzone-msg-title">Drop Profile Image here or click
                                                            to upload.</h3>
                                                        <span class="dropzone-msg-desc">Only images are allowed for upload</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="banner_image"
                                           value="{{isset($user_image->image) ? $user_image->image: ''}}">
                                </div>
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label for="sellerName">Name</label>
                                        <input type="text" required class="form-control" name="name"
                                               value="{{$user->name}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="sellerName">Email</label>
                                        <input type="email" required class="form-control" name="email"
                                               value="{{$user->email}}" disabled>
                                    </div>

                                    <input type="hidden" required class="form-control" name="confirm_email"
                                           value="{{$user->email}}">


                                    <div class="form-group">

                                        <label for="sellerName">New Password</label>
                                        <input type="password" class="form-control" name="password" value="" minlength="6">
                                        <label for="sellerName">If you don't want to change your password don't fill
                                            this input</label>


                                    </div>
                                </div>


                            </div>
                        </div>

                </form>
            </div>

        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('js/jquery-ui.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

    <script>


        const bv = FormValidation.formValidation(
            document.getElementById('form'), {
                fields: {
                    name: {
                        validators: {
                            notEmpty: {
                                message: 'User Name is required'
                            }
                        }
                    },


                },

                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    submitButton: new FormValidation.plugins.SubmitButton(),
                }
            }
        ).on('core.form.valid', function () {
            // Send the form data to back-end
            // You need to grab the form data and create an Ajax request to send them
            // Get the form instance
            var $form = $('#form');
            // Get the BootstrapValidator instance
            let method = $form.attr('method');
            let url = $form.attr('action');
            let data = new FormData(document.getElementById('form'));
            submitFormAjax(method, url, data, function (res) {


                successAlert(res.msg);
                // $('#form').trigger('reset');
                $('.dz-complete').remove();
                $('.needsclick').show();


                window.location.href = "{{url()->previous()}}";

            }, function (jqXHR) {
                let errors = jqXHR.responseJSON.errors;
                let erMsgs = ``;
                $.each(Object.keys(errors), function (i, v) {
                    erMsgs += `<p>${i + 1}-${errors[v][0]}</p>`;
                })
                errorAlert(erMsgs);
            }, bv);
        });


        $('#banner_image').dropzone({
            url: "{{route('user.attachment.upload','banner')}}", // Set the url for your upload script location
            paramName: "banner", // The name that will be used to transfer the file
            maxFiles: 1,
            maxFilesize: 10, // MB
            // addRemoveLinks: true,
            acceptedFiles: "image/*",

            sending: function (file, xhr, formData) {
                formData.append("_token", "{{ csrf_token() }}");
            },
            accept: function (file, done) {
                done();
            },
            success: function (http) {
                let data = jQuery.parseJSON(http.xhr.response)

                $('[name=banner_image]').val(data.file_name);

            }

        });


    </script>
@endsection