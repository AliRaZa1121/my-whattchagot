@extends('layout.default')
@php
        $page_breadcrumbs=[[
        'page'=>route('dashboard'),
        'title'=>'dashboard'
        ],
        [
        'page'=>route('chat'),
        'title'=>'Chat'
        ]
        ];

@endphp
@section('styles')
    <link rel="stylesheet" href="{{asset('plugins/custom/datatables/datatables.bundle.css?v=7.0.4')}}">
@endsection
@section('content')
    @include('pages.message')
    <style>
        .d-flex.flex-column.mb-5.align-items-start {
        }

        .d-flex.flex-column.mb-5.align-items-start .mt-2.rounded.p-5.bg-light-success.text-dark-50.font-weight-bold.font-size-lg.text-left.max-w-400px {
            font-weight: 500 !important;
            float: left;
            padding: 10px !important;
            border-color: #313478;
            background-color: #313478 !important;
            color: #fff !important;
            border-radius: 5px !important;
            width: 200px;
        }

        .card-body .d-flex.flex-column.mb-5.align-items-end .mt-2.rounded.p-5.bg-light-primary.text-dark-50.font-weight-bold.font-size-lg.text-right.max-w-400px {
            float: right;
            padding: 10px !important;
            border-color: #2393d1 !important;
            background-color: #2393d1 !important;
            color: #3c763d !important;
            border-radius: 5px !important;
            width: 200px !important;
            color: #fff !important;
            text-align: left !important;
        }

        .d-flex.flex-column.mb-5.align-items-end {
        }

        .d-flex.flex-column.mb-5.align-items-end .mt-2.rounded.p-5.bg-light-primary.text-dark-50.font-weight-bold.font-size-lg.text-right.max-w-400px {
            width: 100%;
            padding: 15px 20px !important;
            border-radius: 50px !important;
            color: black !important;
            font-weight: 500 !important;
        }

        div#kt_chat_content {
            height: 67vh !important;
            overflow-y: auto;
        }

        div#kt_chat_content::-webkit-scrollbar {
            width: 6px;
            background-color: #87ceeb00;
            border-radius: 25px;
        }

        div#kt_chat_content::-webkit-scrollbar-thumb {
            background-color: #87ceeb;
            border-radius: 25px !important;
        }

        div#kt_chat_content::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgb(0 0 0 / 30%);
            background-color: #e1f0ff8a;
        }

        div#show_form {
            position: fixed;
            bottom: 44px;
            width: 48.4%;
            background-color: white;
            border: none !important;
        }

        div#message {
            padding-bottom: 60px !important;
        }


        .card-header.align-items-center.px-4.py-3 > div > div {
            font-weight: 500;
            font-size: 1.275rem;
            color: #212121;
            text-align: left;
        }

        div#show_form form .d-flex.align-items-center.justify-content-between.mt-5 {
            margin: 0 !important;
        }

        div#show_form form .d-flex.align-items-center.justify-content-between.mt-5 textarea#text {
            border: 1px solid #ccc !important;
            padding: 0px 20px !important;
            height: 40px !important;
            line-height: 33px;
            overflow: hidden !important;
            resize: none;
        }

        div#show_form form .d-flex.align-items-center.justify-content-between.mt-5 button.btn.btn-primary.btn-md.text-uppercase.font-weight-bold.chat-send.py-2.px-6 {
            padding: 10px 20px !important;
        }

        .card-custom .card-body .mt-7.scroll.scroll-pull > div {
            background-color: #f1eaea;
            padding: 10px;
            margin: 10px 0;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 0px #cccccc30;
            min-height: 130px;
        }

        .card-custom .card-body .mt-7.scroll.scroll-pull > div.show {
            background-color: #2393d1;
        }

        .card-custom .card-body .mt-7.scroll.scroll-pull > div.show .text-muted {
            color: #fff !important;
        }

        .card-custom .card-body .mt-7.scroll.scroll-pull > div.show span.text-muted.font-weight-bold.font-size-sm {
            color: #fff !important;
        }

        .card-custom .card-body .mt-7.scroll.scroll-pull > div.show .d-flex.flex-column {
            color: #fff !important;
        }

        #kt_chat_aside .d-flex.flex-column {
            /*color: #fff !important;*/
            font-size: 18px;
            font-weight: bold;
        }

        #kt_chat_content .card.card-custom.aaa {
            min-height: 100%;
        }

        div#kt_chat_aside {
            /* height: 100% !important; */
        }

        div#kt_chat_aside .card-body {
            height: 340px;
            overflow-y: auto;
        }

        div#kt_chat_aside .card.card-custom {
            height: 100%;
        }

               /** New Css Start here ***/
               .offcanvas-content.pr-5.mr-n5.scroll.ps a.btn.btn-sm.btn-light-primary.font-weight-bolder.py-2.px-5 {
    padding-left: 1.25rem!important;
    color: #3699FF !important;
    padding-right: 1.25rem !important;
}
.offcanvas-content.pr-5.mr-n5.scroll.ps a.btn.btn-sm.btn-light-primary.font-weight-bolder.py-2.px-5:hover {
    color: #fff !important;
}

    </style>

    <head>
        <title>Pusher Test</title>
        <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css"
              rel="stylesheet">

        <script
                src="https://code.jquery.com/jquery-3.6.0.js"
                integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
                crossorigin="anonymous"></script>

        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet"
              id="bootstrap-css">


    </head>




    <body>
    <div class="content d-flex flex-column flex-column-fluid" id="" style="height: 200px;">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Chat-->
                <div class="d-flex flex-row">
                    <!--begin::Aside-->
                    <div class="flex-row-auto offcanvas-mobile w-350px w-xl-350px" id="kt_chat_aside">
                        <!--begin::Card-->
                        <div class="card card-custom">
                            <!--begin::Body-->
                            <div class="card-body">

                                <div class="mt-7 scroll scroll-pull">
                                    <!--begin:User-->

                                    @foreach($chat as $chatKey)
                                        @php
                                        $users_receiver= $chatKey->users_receiver->name;
                                        $users_sender= user_name($chatKey->sender_id)->name;
                                        @endphp
                                        <div id="show{{$chatKey->product_id}}"
                                             onclick="chat_box({{$chatKey->product_id}},{{$chatKey->sender_id}},{{$chatKey->receiver_id}},'{{$users_sender}}','{{$users_receiver}}')"
                                        >

                                            <div class="d-flex align-items-center justify-content-between mb-5">
                                                <div class="d-flex align-items-center">
                                                    <div class="symbol symbol-circle symbol-50 mr-3">
                                                        <img src="{{isset($key->users->image) ? asset('/storage/'.$key->users->image): url('/images/auction-detail-seller-img.png') }}">

                                                    </div>
                                                    <div class="d-flex flex-column">

                                                        {{$users_receiver}}
                                                        <span class="text-muted font-weight-bold font-size-sm">

                                                    {{$chatKey->message==$chatKey->product_id ? 'want to details of product:'.$chatKey->products->name  : substr($chatKey->message, 0, 7)}}




                                                </span>
                                                    </div>
                                                </div>
                                                <div class="d-flex flex-column align-items-end">
                                                    <div id="status{{$chatKey->product_id.$chatKey->receiver_id}}}">
                                                        @if($chatKey->status==0)
                                                            <span class="label label-sm label-success"></span>

                                                        @endif
                                                    </div>

                                                    <span class="text-muted font-weight-bold font-size-sm">{{$chatKey->created_at}}</span>
                                                </div>
                                            </div>
                                        </div>

                                @endforeach
                                <!--end:User-->

                                </div>
                                <!--end:Users-->
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Card-->
                    </div>

                    <!--end::Aside-->


                    <div class="flex-row-fluid ml-lg-6" id="kt_chat_content">
                        <!--begin::Card-->


                        <div class="card card-custom aaa">
                            <!--begin::Header-->

                            <div class="card-header align-items-center px-4 py-3">

                                <div class="text-center flex-grow-1">
                                    <div class="text-dark-75 font-weight-bold font-size-h5"
                                         id="name_title"></div>
                                    <div>
                                        {{--                                                <span class="label label-sm label-dot label-success"></span>--}}
                                        {{--                                                <span class="font-weight-bold text-muted font-size-sm">Active</span>--}}
                                    </div>
                                </div>

                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card-body">
                                <!--begin::Scroll-->
                                <div class="scroll scroll-pull" data-mobile-height="350">
                                    <!--begin::Messages-->
                                    <div class="messages" id="message">

                                    </div>
                                    <!--end::Messages-->
                                </div>
                                <!--end::Scroll-->
                            </div>
                            <!--end::Body-->
                            <!--begin::Footer-->
                            <div class="card-footer align-items-center" id="show_form" style="display: none">
                                <!--begin::Compose-->


                                <!--end::Footer-->
                            </div>
                            <!--end::Card-->
                        </div>
                        <!--end::Content-->
                    </div>

                    <!--end::Chat-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Entry-->
        </div>
    </div>


    @endsection

    {{-- Styles Section --}}
    @section('styles')

    @endsection

    @section('scripts')
        <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
        <script type="text/javascript">

            $('#product_change').hide();



            function chat_box($product_id, $sender_id, $receiver_id, $receiver_name,$users_sender) {


                $('#show_form').show();


                var sender_id = $sender_id;
                var users_sender = $receiver_name;
                var receiver_id = $receiver_id;
                var product_id = $product_id;
                var receiver_name = $users_sender;
                $('#name_title').html(receiver_name);
                var auth_sender_id = {{auth()->user()->id}};

                $.ajax({
                    url: "{{url('/ajax_get_message_admin')}}",
                    type: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        sender_id: sender_id,
                        receiver_id: receiver_id,
                        product_id: product_id,

                    },
                    success: function (response) {

                        let obj = JSON.parse(response);


                        var myvar = '';
                        var product_no = 0;
                        $.each(obj, function (i, item) {

                            if (parseInt(item.message) == parseInt(item.product_id)) {
                                var message = ' Name:' + item.name + ' Price: $' + item.price;
                            } else {
                                var message = item.message;

                            }



                            const date = new Date(item.created_at).toDateString();
                            if (sender_id != item.sender_id) {
                                myvar += '<div class="d-flex flex-column mb-5 align-items-start">' +
                                    '                                                    <div class="d-flex align-items-center">' +
                                    '' +
                                    '                                                        <div>' +
                                    '                                                            <a href="#"' +
                                    '                                                               class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">' + receiver_name + '</a>' +
                                    '                                                            <span class="text-muted font-size-sm">' + date + '</span>' +
                                    '                                                        </div>' +
                                    '                                                    </div>' +
                                    '                                                    <div class="mt-2 rounded p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px">' +
                                    '                                                     ' + message + '' +
                                    '                                                    </div>' +
                                    '                                                </div>';
                            } else {
                                myvar += '<div class="d-flex flex-column mb-5 align-items-end">' +
                                    '                                                    <div class="d-flex align-items-center">' +
                                    '                                                        <div>' +
                                    '                                                            <span class="text-muted font-size-sm">' + date + '</span>' +
                                    '                                                          '+users_sender+'' +
                                    '                                                        </div>' +
                                    '                                                        <div class="symbol symbol-circle symbol-40 ml-3">' +
                                    '                                                           ' +
                                    '                                                        </div>' +
                                    '                                                    </div>' +
                                    '                                                    <div class="mt-2 rounded p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-right max-w-400px">' +
                                    '                                                  ' + message + '' +
                                    '                                                    </div>' +
                                    '                                                </div>';
                            }


                        });


                        $('#message').html(myvar);

                    }
                });
            }

            $(document).ready(function () {
                $('#product_form').submit(function (event) {
                    event.preventDefault();
                    // alert('asd');
                    var product_user_id = $('#product_user_id_input').val();
                    var product_product_id = $('#product_product_id_input').val();
                    var product_price_change = $('#product_price_change').val();
                    $.ajax({
                        url: "{{url('/checkout_chat_ajax')}}",
                        type: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            user_id: product_user_id,
                            price: product_price_change,
                            product_id: product_product_id,

                        },
                        success: function (response) {
                            $('#text').val('Your Product Price in now change please check this <a style="color: green" href="http://localhost/watchagot/public/cart" > link</a> now you will pay only '+$('#product_name').text()+' in $'+product_price_change+" ")
                            $('#product_price_change').val('');

                        }
                    });
                })
            })


            //
            //
            // tinymce.init({
            //     selector: "#mytextarea",
            //     plugins: "emoticons",
            //     toolbar: "emoticons",
            //     toolbar_location: "bottom",
            //     menubar: false
            // });


        </script>


@endsection
