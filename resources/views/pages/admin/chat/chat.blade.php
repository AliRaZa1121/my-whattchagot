@extends('layout.default')
@php
        $page_breadcrumbs=[[
        'page'=>route('dashboard'),
        'title'=>'dashboard'
        ],
        [
        'page'=>route('users.chat'),
        'title'=>'Chat'
        ]
        ];
@endphp


@section('styles')
    <link rel="stylesheet" href="{{asset('plugins/custom/datatables/datatables.bundle.css?v=7.0.4')}}">

@endsection
@section('content')


    @include('pages.message')
    <style>
        .d-flex.flex-column.mb-5.align-items-start {
        }

        .d-flex.flex-column.mb-5.align-items-start .mt-2.rounded.p-5.bg-light-success.text-dark-50.font-weight-bold.font-size-lg.text-left.max-w-400px {
            font-weight: 500 !important;
            float: left;
            padding: 10px !important;
            border-color: #313478;
            background-color: #313478 !important;
            color: #fff !important;
            border-radius: 5px !important;
            width: 200px;
        }

        .card-body .d-flex.flex-column.mb-5.align-items-end .mt-2.rounded.p-5.bg-light-primary.text-dark-50.font-weight-bold.font-size-lg.text-right.max-w-400px {
            float: right;
            padding: 10px !important;
            border-color: #2393d1 !important;
            background-color: #2393d1 !important;
            color: #3c763d !important;
            border-radius: 5px !important;
            width: 200px !important;
            color: #fff !important;
            text-align: left !important;
        }

        .d-flex.flex-column.mb-5.align-items-end {
        }

        .d-flex.flex-column.mb-5.align-items-end .mt-2.rounded.p-5.bg-light-primary.text-dark-50.font-weight-bold.font-size-lg.text-right.max-w-400px {
            width: 100%;
            padding: 15px 20px !important;
            border-radius: 50px !important;
            color: black !important;
            font-weight: 500 !important;
        }

        div#kt_chat_content {
            height: 67vh !important;
            overflow-y: auto;
        }

        div#kt_chat_content::-webkit-scrollbar {
            width: 6px;
            background-color: #87ceeb00;
            border-radius: 25px;
        }

        div#kt_chat_content::-webkit-scrollbar-thumb {
            background-color: #87ceeb;
            border-radius: 25px !important;
        }

        div#kt_chat_content::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgb(0 0 0 / 30%);
            background-color: #e1f0ff8a;
        }

        div#show_form {
            position: fixed;
            bottom: 44px;
            width: 48.4%;
            background-color: white;
            border: none !important;
        }

        div#message {
            padding-bottom: 60px !important;
        }


        .card-header.align-items-center.px-4.py-3 > div > div {
            font-weight: 500;
            font-size: 1.275rem;
            color: #212121;
            text-align: left;
        }

        div#show_form form .d-flex.align-items-center.justify-content-between.mt-5 {
            margin: 0 !important;
        }

        div#show_form form .d-flex.align-items-center.justify-content-between.mt-5 textarea#text {
            border: 1px solid #ccc !important;
            padding: 0px 20px !important;
            height: 40px !important;
            line-height: 33px;
            overflow: hidden !important;
            resize: none;
        }

        div#show_form form .d-flex.align-items-center.justify-content-between.mt-5 button.btn.btn-primary.btn-md.text-uppercase.font-weight-bold.chat-send.py-2.px-6 {
            padding: 10px 20px !important;
        }

        .card-custom .card-body .mt-7.scroll.scroll-pull > div {
            background-color: #f1eaea;
            padding: 10px;
            margin: 10px 0;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 0px #cccccc30;
            min-height: 130px;
        }

        .card-custom .card-body .mt-7.scroll.scroll-pull > div.show {
            background-color: #2393d1;
        }

        .card-custom .card-body .mt-7.scroll.scroll-pull > div.show .text-muted {
            color: #fff !important;
        }

        .card-custom .card-body .mt-7.scroll.scroll-pull > div.show span.text-muted.font-weight-bold.font-size-sm {
            color: #fff !important;
        }

        .card-custom .card-body .mt-7.scroll.scroll-pull > div.show .d-flex.flex-column {
            color: #fff !important;
        }

        #kt_chat_aside .d-flex.flex-column {
            /*color: #fff !important;*/
            font-size: 18px;
            font-weight: bold;
        }

        #kt_chat_content .card.card-custom.aaa {
            min-height: 100%;
        }

        div#kt_chat_aside {
            /* height: 100% !important; */
        }

        div#kt_chat_aside .card-body {
            height: 340px;
            overflow-y: auto;
        }

        div#kt_chat_aside .card.card-custom {
            height: 100%;
        }


        form#form {
            display: grid;
            grid-template-columns: 80% auto auto;
            width: 100%;
        }

        .emoji-wysiwyg-editor {
            border: 1px solid #d0d0d0;
            overflow: auto;
            outline: none;
            width: 99% !important;
        }

        a.emoji-button:before {
            content: '\f118';
            position: absolute;
            top: 5px;
            left: 11px;
            width: 20px;
            height: 20px;
            color: #2393d1;
            font-size: 20px;
            font-family: 'Font Awesome 5 Free';
        }

        a.emoji-button:hover:before {
            color: #313478;
        }

        a.emoji-button {
            position: relative;
            font-size: 0;
            width: 40px;
            height: 40px;
        }

        .emoji-menu {
            position: absolute;
            z-index: 999;
            width: 180px;
            margin-left: -100px;
            padding: 0;
            overflow: hidden;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            top: unset !important;
            bottom: 110px !important;
        }

                /** New Css Start here ***/
                .offcanvas-content.pr-5.mr-n5.scroll.ps a.btn.btn-sm.btn-light-primary.font-weight-bolder.py-2.px-5 {
    padding-left: 1.25rem!important;
    color: #3699FF !important;
    padding-right: 1.25rem !important;
}
.offcanvas-content.pr-5.mr-n5.scroll.ps a.btn.btn-sm.btn-light-primary.font-weight-bolder.py-2.px-5:hover {
    color: #fff !important;
}
.d-flex.align-items-center.mt-5 {
    margin-top: 10px !important;
    width: 100% !important;
}

        /** New Css End here **/

        @media only screen and (min-width: 1600px) {
            div#show_form {
                position: fixed;
                bottom: 44px;
                width: 39.4%;
                background-color: white;
                border: none !important;
            }
        }
    </style>

    <head>
        <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css"
              rel="stylesheet">

        <script
                src="https://code.jquery.com/jquery-3.6.0.js"
                integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
                crossorigin="anonymous"></script>

        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet"
              id="bootstrap-css">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="{{asset('jquery.emojiarea.css')}}">
        <link rel="stylesheet" href="stylesheet.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script src="{{asset('jquery.emojiarea.js')}}"></script>
        <script src="{{asset('packs/basic/emojis.js')}}"></script>
        <link rel="stylesheet" href="{{asset('fontello/css/fontello.css')}}">
        {{--        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>--}}
        <script>

            // Enable pusher logging - don't include this in production
            Pusher.logToConsole = true;

            var pusher = new Pusher('962913f7141a1e72feb7', {
                cluster: 'ap1'
            });

            var channel = pusher.subscribe('watchagot');
            channel.bind('form-submitted', function (data) {
                console.log((data));

                var product_product_id = $('#product_id').val();


                var receiver_id = data.receiver_id;
                var auth_sender_id = {{auth()->user()->id}};
                if (data.sender_id == auth_sender_id) {
                    $.ajax({
                        url: "{{url('/ajax_save_message')}}",
                        type: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            sender_id: data.sender_id,
                            receiver_id: data.receiver_id,
                            product_id: data.product_id,
                            message: data.text,

                        },
                        success: function (response) {


                            if (product_product_id == data.product_id) {
                                $.ajax({
                                    url: "{{url('/ajax_show_message')}}",
                                    type: "POST",
                                    data: {
                                        "_token": "{{ csrf_token() }}",
                                        sender_id: auth_sender_id,
                                        receiver_id: data.receiver_id,
                                        product_id: data.product_id,
                                        message: data.text,

                                    },
                                    success: function (response) {

                                        let obj = JSON.parse(response);


                                        var myvar = '';
                                        $.each(obj, function (i, item) {

                                            if (parseInt(item.message) == parseInt(item.product_id)) {
                                                if (item.price != 0) {
                                                    var message = ' Name:' + item.name + ' Price:' + item.price;

                                                } else {
                                                    var message = ' Name:' + item.name;

                                                }
                                            } else {
                                                var message = item.message;

                                            }
                                            const date = new Date(item.created_at).toDateString();
                                            if (auth_sender_id != data.sender_id) {
                                                myvar += '<div class="d-flex flex-column mb-5 align-items-start">' +
                                                    '                                                    <div class="d-flex align-items-center">' +
                                                    '' +
                                                    '                                                        <div>' +
                                                    '                                                            <a href="#"' +
                                                    '                                                               class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">' + data.sender_name + '</a>' +
                                                    // '                                                            <span class="text-muted font-size-sm">' + date + '</span>' +
                                                    '                                                        </div>' +
                                                    '                                                    </div>' +
                                                    '                                                    <div class="mt-2 rounded p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px">' +
                                                    '                                                     ' + message + '' +
                                                    '                                                    </div>' +
                                                    '                                                </div>';
                                            } else {
                                                myvar += '<div class="d-flex flex-column mb-5 align-items-end">' +
                                                    '                                                    <div class="d-flex align-items-center">' +
                                                    '                                                        <div>' +
                                                    // '                                                            <span class="text-muted font-size-sm">' + date + '</span>' +
                                                    '                                                          You' +
                                                    '                                                        </div>' +
                                                    '                                                        <div class="symbol symbol-circle symbol-40 ml-3">' +
                                                    '                                                           ' +
                                                    '                                                        </div>' +
                                                    '                                                    </div>' +
                                                    '                                                    <div class="mt-2 rounded p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-right max-w-400px">' +
                                                    '                                                  ' + message + '' +
                                                    '                                                    </div>' +
                                                    '                                                </div>';
                                                $('#status' + item.product_id + item.receiver_id).append(' <span class="label label-sm label-success"></span>');

                                            }


                                        });
                                        $('#text').val('');


                                        $('#message').append(myvar);

                                    }
                                });
                            }

                        }


                    });


                } else {

                    if (product_product_id == data.product_id) {

                        setTimeout(function(){
                        $.ajax({
                            url: "{{url('/ajax_show_message')}}",
                            type: "POST",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                sender_id: auth_sender_id,
                                receiver_id: data.receiver_id,
                                product_id: data.product_id,
                                message: data.text,

                            },
                            success: function (response) {

                                let obj = JSON.parse(response);

                                var myvar = '';
                                $.each(obj, function (i, item) {

                                    if (parseInt(item.message) == parseInt(item.product_id)) {
                                        if (item.price != 0) {
                                            var message = ' Name:' + item.name + ' Price:' + item.price;

                                        } else {
                                            var message = ' Name:' + item.name;

                                        }
                                    } else {
                                        var message = item.message;

                                    }
                                    const date = new Date(item.created_at).toDateString();
                                    if (auth_sender_id != data.sender_id) {
                                        myvar += '<div class="d-flex flex-column mb-5 align-items-start">' +
                                            '                                                    <div class="d-flex align-items-center">' +
                                            '' +
                                            '                                                        <div>' +
                                            '                                                            <a href="#"' +
                                            '                                                               class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">' + data.sender_name + '</a>' +
                                            // '                                                            <span class="text-muted font-size-sm">' + date + '</span>' +
                                            '                                                        </div>' +
                                            '                                                    </div>' +
                                            '                                                    <div class="mt-2 rounded p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px">' +
                                            '                                                     ' + message + '' +
                                            '                                                    </div>' +
                                            '                                                </div>';
                                    } else {
                                        myvar += '<div class="d-flex flex-column mb-5 align-items-end">' +
                                            '                                                    <div class="d-flex align-items-center">' +
                                            '                                                        <div>' +
                                            // '                                                            <span class="text-muted font-size-sm">' + date + '</span>' +
                                            '                                                          You' +
                                            '                                                        </div>' +
                                            '                                                        <div class="symbol symbol-circle symbol-40 ml-3">' +
                                            '                                                           ' +
                                            '                                                        </div>' +
                                            '                                                    </div>' +
                                            '                                                    <div class="mt-2 rounded p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-right max-w-400px">' +
                                            '                                                  ' + message + '' +
                                            '                                                    </div>' +
                                            '                                                </div>';
                                        $('#status' + item.product_id + item.receiver_id).append(' <span class="label label-sm label-success"></span>');

                                    }


                                });
                                $('#text').val('');


                                $('#message').append(myvar);

                            }
                        }, );
                        }, 1000)
                    }
                }


            });


        </script>
        {{--        <script src="{{asset('assets/js/pages/custom/chat/chat.js?v=7.0.4')}}"></script>--}}
    </head>




    <body>
    <div class="content d-flex flex-column flex-column-fluid" id="" style="height: 200px;">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container-fluid">
                <!--begin::Chat-->
                <div class="d-flex flex-row">
                    <!--begin::Aside-->
                    <div class="flex-row-auto offcanvas-mobile w-350px w-xl-350px" id="kt_chat_aside">
                        <!--begin::Card-->
                        <div class="card card-custom">
                            <!--begin::Body-->
                            <div class="card-body">

                                <div class="mt-7 scroll scroll-pull">
                                    <!--begin:User-->

                                    @foreach($chat as $chatKey)
                                        @php
                                            if($chatKey->users_receiver->id == auth()->user()->id){
                                                   $user_name= $chatKey->users_sender->name;
                                            }else{
                                                   $user_name = $chatKey->users_receiver->name;
                                            }

                                               if($chatKey->users_receiver->id==auth()->user()->id){
                                                   $user_name_show= $chatKey->users_receiver->name;

                                            }else{
                                               $user_name_show= $chatKey->users_sender->name;
                                            }

                                         if($chatKey->receiver_id == auth()->user()->id){
                                               $receiver_id_box= $chatKey->sender_id;
                                            }else{
                                               $receiver_id_box= $chatKey->receiver_id;

                                            }
                                        @endphp

                                        <div id="show{{$chatKey->product_id}}"
                                             onclick="chat_box({{$chatKey->product_id}},{{$chatKey->sender_id}},{{$receiver_id_box}},'{{$user_name}}','{{$user_name_show}}')"
                                        >

                                            <div class="d-flex align-items-center justify-content-between mb-5">
                                                <div class="d-flex align-items-center">
                                                    <div class="symbol symbol-circle symbol-50 mr-3">
                                                        <img src="{{isset($key->users->image) ? asset('/storage/'.$key->users->image): url('/images/auction-detail-seller-img.png') }}">
                                                        {{--                                                <img alt="Pic" src="assets/media/users/300_12.jpg" />--}}
                                                    </div>
                                                    <div class="d-flex flex-column">
                                                        {{$user_name}}
                                                        <span class="text-muted font-weight-bold font-size-sm">

                                                    {{$chatKey->message==$chatKey->product_id ? 'want to details of product:'.$chatKey->products->name  : substr($chatKey->message, 0, 7)}}




                                                </span>
                                                    </div>
                                                </div>
                                                <div class="d-flex flex-column align-items-end">
                                                    <div id="status{{$chatKey->product_id.$chatKey->receiver_id}}}">
                                                        @if($chatKey->status==0)
                                                            <span class="label label-sm label-success"></span>

                                                        @endif
                                                    </div>

                                                    <span class="text-muted font-weight-bold font-size-sm">{{$chatKey->created_at}}</span>
                                                </div>
                                            </div>
                                        </div>

                                @endforeach
                                <!--end:User-->

                                </div>
                                <!--end:Users-->
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Card-->
                    </div>

                    <!--end::Aside-->


                    <div class="flex-row-fluid ml-lg-6" id="kt_chat_content">
                        <!--begin::Card-->

                            <div id="product_change" style="display: none">

                                <form id="product_form">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <td>ID</td>
                                            <td>NAME</td>
                                            <td>LISTED PRICE</td>
                                            <td>PRICE CHANGE</td>
                                            <td>ACTION</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td id="product_product_id"></td>
                                            <td id="product_name"></td>
                                            <td id="product_price"></td>

                                            <td><input type="number" name="price" id="product_price_change"></td>
                                            <td>
                                                <input type="hidden" name="user_id" id="product_user_id_input"
                                                       value="">
                                                <input type="hidden" name="qty" value="1">
                                                <input type="hidden" name="product_id" id="product_product_id_input"
                                                       value="">
                                                <span id="e_1" class="emoji"
                                                      data-src="https://www.webfx.com/assets/emoji-cheat-sheet/img/graphics/emojis/bowtie.png"></span>
                                                <input type="submit" class="btn btn-primary">
                                            </td>
                                        </tr>
                                        </tbody>

                                    </table>
                                </form>
                            </div>

                        <div class="card card-custom aaa">
                            <!--begin::Header-->

                            <div class="card-header align-items-center px-4 py-3">

                                <div class="text-center flex-grow-1">
                                    <div class="text-dark-75 font-weight-bold font-size-h5"
                                         id="name_title"></div>
                                    <div>
                                    </div>
                                </div>

                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card-body">
                                <!--begin::Scroll-->
                                <div class="scroll scroll-pull" data-mobile-height="350">
                                    <!--begin::Messages-->
                                    <div class="messages" id="message">

                                    </div>
                                    <!--end::Messages-->
                                </div>
                                <!--end::Scroll-->
                            </div>
                            <!--end::Body-->
                            <!--begin::Footer-->
                            <div class="card-footer align-items-center" id="show_form" style="display: none">
                                <!--begin::Compose-->
                                <form method="POST" id="form" action="{{url('/sender')}}">
                                    @csrf
                                    <input type="hidden" id='sender_id' style=""
                                           value="{{auth()->user()->id}}"/>
                                    <input type="hidden" id='receiver_id'/>
                                    <input type="hidden" id='product_id'/>
                                    <input type="hidden" id='sender_name'/>
                                    <input rows="5" class="emojis-wysiwyg" id='text' required>
                                    <div class="d-flex align-items-center justify-content-between mt-5">
                                      
                                        <div>
                                            <button type="submit"
                                                    class="btn btn-primary btn-md text-uppercase font-weight-bold chat-send py-2 px-6">
                                                <i class="fa fa-paper-plane-o"
                                                   aria-hidden="true"></i></button>
                                        </div>
                                    </div>

                                </form>

                                <!--end::Footer-->
                            </div>
                            <!--end::Card-->
                        </div>
                        <!--end::Content-->
                    </div>

                    <!--end::Chat-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Entry-->
        </div>
    </div>

    <script>
        $('.emojis-plain').emojiarea({wysiwyg: false});

        var $wysiwyg = $('.emojis-wysiwyg').emojiarea({wysiwyg: true});
        var $wysiwyg_value = $('#emojis-wysiwyg-value');

        $wysiwyg.on('change', function () {
            $wysiwyg_value.text($(this).val());
        });
        $wysiwyg.trigger('change');
    </script>
    @endsection

    {{-- Styles Section --}}
    @section('styles')

    @endsection

    @section('scripts')
        <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
        <script type="text/javascript">
            $('#text').change(function () {
                var str = $(this).val();
                // alert(str);
                const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                const rephone = /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;
                var valuetext = '';
                // var patt = new RegExp(re);
                var strarray = str.split(" ");
                for (var i = 0; i < strarray.length; i++) {
                    var res = re.test(strarray[i]);
                    var resphone = rephone.test(strarray[i]);
                    if (res == true) {
                        valuetext += "emailnotallow ";
                    } else if (resphone == true) {
                        valuetext += "phonenumbernotallow ";

                    } else {
                        valuetext += strarray[i] + " ";
                    }
                }
                $(this).val(valuetext)


            })

            $('#product_change').hide();

            $('#form').on('submit', function (event) {
                event.preventDefault();

                var text = $('#text').val();
                var sender_id = $('#sender_id').val();
                var receiver_id = $('#receiver_id').val();
                var product_id = $('#product_id').val();
                var sender_name = $('#sender_name').val();

                $.ajax({
                    url: "{{url('/sender')}}",
                    type: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        text: text,
                        sender_id: sender_id,
                        receiver_id: receiver_id,
                        product_id: product_id,
                        sender_name: sender_name,

                    },
                    success: function (response) {
                        console.log(response);
                        $('#text').val('')
                        $('.emoji-wysiwyg-editor').html('')
                    },
                });
            });


            function chat_box($product_id, $sender_id, $receiver_id, $send_name,$user_name_show) {


                $('#show_form').show();
                var auth_sender_id = {{auth()->user()->id}};


                var sender_id = $sender_id;
                var receiver_id = $receiver_id;
                var product_id = $product_id;
                var sender_name = $send_name;
                $('.show').removeClass('show notshow');
                $('#show' + product_id).addClass('show')
                $('#name_title').html(sender_name);

                $('#receiver_id').val(receiver_id);
                $('#product_id').val(product_id);
                $('#sender_name').val($user_name_show);


                var auth_sender_id = {{auth()->user()->id}};

                $.ajax({
                    url: "{{url('/ajax_get_message')}}",
                    type: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        sender_id: auth_sender_id,
                        receiver_id: receiver_id,
                        product_id: product_id,

                    },
                    success: function (response) {

                        let obj = JSON.parse(response);


                        var myvar = '';
                        var product_no = 0;
                        $.each(obj, function (i, item) {

                            if (parseInt(item.message) == parseInt(item.product_id)) {
                                if (item.price != 0) {
                                    var message = ' Name:' + item.name + ' Price:' + item.price;

                                } else {
                                    var message = ' Name:' + item.name;

                                }
                            } else {
                                var message = item.message;

                            }
                            if (product_no == 0) {


                                $('#product_product_id').html(item.product_id)
                                $('#product_name').html(item.name)
                                $('#product_price').html(item.price)
                                $('#product_user_id_input').val(receiver_id)
                                $('#product_product_id_input').val(item.product_id)
                                if (item.flags != "trade") {

                                    $('#product_change').show()
                                } else {
                                    $('#product_change').hide()
                                }

                                // $('#product_change').show()


                                product_no++;
                            }


                            const date = new Date(item.created_at).toDateString();
                            if (auth_sender_id != item.sender_id) {
                                myvar += '<div class="d-flex flex-column mb-5 align-items-start">' +
                                    '                                                    <div class="d-flex align-items-center">' +
                                    '' +
                                    '                                                        <div>' +
                                    '                                                            <a href="#"' +
                                    '                                                               class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">' + sender_name + '</a>' +
                                    // '                                                            <span class="text-muted font-size-sm">' + date + '</span>' +
                                    '                                                        </div>' +
                                    '                                                    </div>' +
                                    '                                                    <div class="mt-2 rounded p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px emojis-wysiwyg">' +
                                    '                                                     ' + message + '' +
                                    '                                                    </div>' +
                                    '                                                </div>';
                            } else {
                                myvar += '<div class="d-flex flex-column mb-5 align-items-end">' +
                                    '                                                    <div class="d-flex align-items-center">' +
                                    '                                                        <div>' +
                                    // '                                                            <span class="text-muted font-size-sm">' + date + '</span>' +
                                    '                                                          You' +
                                    '                                                        </div>' +
                                    '                                                        <div class="symbol symbol-circle symbol-40 ml-3">' +
                                    '                                                           ' +
                                    '                                                        </div>' +
                                    '                                                    </div>' +
                                    '                                                    <div class="mt-2 rounded p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-right max-w-400px emojis-wysiwyg">' +
                                    '                                                  ' + message + '' +
                                    '                                                    </div>' +
                                    '                                                </div>';
                            }


                        });


                        $('#message').html(myvar);

                    }
                });


            }

            $(document).ready(function () {
                $('#product_form').submit(function (event) {
                    event.preventDefault();
                    // alert('asd');
                    var product_user_id = $('#product_user_id_input').val();
                    var product_product_id = $('#product_product_id_input').val();
                    var product_price_change = $('#product_price_change').val();
                    $.ajax({
                        url: "{{url('/checkout_chat_ajax')}}",
                        type: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            user_id: product_user_id,
                            price: product_price_change,
                            product_id: product_product_id,

                        },
                        success: function (response) {
                            $('#text').val('Your Product Price in now change please check this <a style="color: green" href="{{url("/cart")}}" > link</a> now you will pay only ' + $('#product_name').text() + ' in $' + product_price_change + " ")
                            $('.emoji-wysiwyg-editor').html('Your Product Price in now change please check this <a style="color: green" href="{{url("/cart")}}" > link</a> now you will pay only ' + $('#product_name').text() + ' in $' + product_price_change + " ")
                            $('#product_price_change').val('');

                        }
                    });
                })
            })


        </script>




@endsection
