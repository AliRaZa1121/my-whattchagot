@extends('layout.default')
@php
    if (Auth::guard('admin')->check()) {
    $role='';
    } else if (Auth::guard('seller')->check()) {
            $role='seller_';

    } else if (Auth::guard('seller')->check()) {
        $role='buyer_';

    }


        $page_breadcrumbs=[[
        'page'=>route($role.'dashboard'),
        'title'=>'dashboard'
        ],
        [
        'page'=>route($role.'seller.index'),
        'title'=>'Financial Statement'
        ]
        ];




@endphp
@section('styles')
    <link rel="stylesheet" href="{{asset('plugins/custom/datatables/datatables.bundle.css?v=7.0.4')}}">
@endsection
@section('content')
    @include('pages.message')
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Financial Statement
                </h3>
            </div>
            @if($role=='seller_')
            <div class="card-toolbar">
                            <a href="{{route($role.'seller.create')}}" class="btn btn-primary font-weight-bolder font-size-sm mr-3 ">Add New</a>
            </div>
               @endif
        </div>
        <div class="card-body">


            <!--begin: Search Form-->
            <!--begin::Search Form-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
                <thead>

                <tr>
                    <th> ID</th>
                    <th>Seller Name</th>
                    <th> Delivered amount</th>
                    <th> Delivered Shipment Price</th>
                    <th> Whatchagot charges</th>
                    <th>Total Delivered amount</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php $no=1 ?>

                @foreach($sellers as $p)
                    <tr>
                        <td>{{$no++}}</td>

                        <td>{{$p->name}}</td>
                        <td>${{Order_finanicial_amount_price($p->id,Request()->input('product_flag'))}}</td>
                        <td>${{Order_finanicial_amount_shipping($p->id,Request()->input('product_flag'))}}</td>
                        <td>-${{Order_finanicial_amount_whatchagot_charges($p->id,Request()->input('product_flag'))}}</td>
                        <td>${{Order_finanicial_amount($p->id,Request()->input('product_flag'))}}</td>
                        <td>
                            <a href="{{route($role.'financial_statement.show',$p->id).'?product_flag='.Request()->input('product_flag')}}" >
                                <span class="navi-item btn btn-primary">
                                        More Details
                                </span>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


@endsection

{{-- Styles Section --}}
@section('styles')

@endsection

@section('scripts')
    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js?v=7.0.4') }}" type="text/javascript"></script>
    <script>
        var table = $('#kt_datatable');

        // begin first table
        table.DataTable({
            // DOM Layout settings
            dom: "<'row py-3'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>" +
                "<'row py-3'<'col-sm-12'tr>>" +
                "<'row py-3'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>", // read more: https://datatables.net/examples/basic_init/dom.html

            columnDefs: [{
                targets: -1,
                title: 'Actions',
                orderable: false,

            },
                {
                    width: '75px',
                    targets: 6,
                    render: function (data, type, full, meta) {
                        var status = {
                            'Pending': {
                                'title': 'Pending',
                                'class': 'label-light-primary'
                            },

                            'Approved': {
                                'title': 'Approved',
                                'class': ' label-light-success'
                            },

                            'Canceled': {
                                'title': 'Canceled',
                                'class': ' label-light-danger'
                            },

                        };
                        if (typeof status[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="label label-lg font-weight-bold ' + status[data].class + ' label-inline">' + status[data].title + '</span>';
                    },
                },
            ],
        });
    </script>
@endsection
