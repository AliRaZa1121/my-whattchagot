@extends('layout.default')
@php
    if (Auth::guard('admin')->check()) {
    $role='';
    } else if (Auth::guard('seller')->check()) {
            $role='seller_';

    } else if (Auth::guard('seller')->check()) {
        $role='buyer_';

    }


        $page_breadcrumbs=[[
        'page'=>route($role.'dashboard'),
        'title'=>'dashboard'
        ],
        [
        'page'=>route($role.'seller.index'),
        'title'=>'Financial Statement'
        ]
        ];




@endphp
@section('styles')
    <link rel="stylesheet" href="{{asset('plugins/custom/datatables/datatables.bundle.css?v=7.0.4')}}">
@endsection
@section('content')
    @include('pages.message')
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Financial Statement
                </h3>
            </div>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                Filter
            </button>

            @if($role=='seller_')
                <div class="card-toolbar">
                    <a href="{{route($role.'seller.create')}}"
                       class="btn btn-primary font-weight-bolder font-size-sm mr-3 ">Add New</a>
                </div>
            @endif
        </div>
        <div class="card-body">

            <!--begin: Search Form-->
            <!--begin::Search Form-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
                <thead>
                <tr>
                    <th> ID</th>

                    <th> amount</th>
                    <th> Shipment Price</th>
                    <th> Whatchagot charges</th>

                    <th>Total amount</th>
                    <th>Status</th>
                    <th>Created At</th>
                    <th>Delivered date</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php $no = 1 ?>

                @foreach($order_details as $p)
                    <tr>
                        <td>{{$no++}}</td>


                        <td>${{$p->price}}</td>
                        <td>${{$p->shipping_price}}</td>
                        <td>-${{$p->seller_charge_price}}</td>
                        <td>${{$p->price}}</td>
                        <td>{{$p->status}}</td>
                        <td>{{$p->created_at}}</td>
                        <td>{{$p->delivered_date}}</td>
                        <td>
                            @if($p->status=='Delivered')
                                @if($p->stripe_transfer_id==1)

                                    paid

                                @elseif($p->seller_name->stripe_flag==1)
                                    <a href="{{route($role.'financial_statement.transfer',$p->id)}}">
                                <span class="navi-item btn btn-primary" data-toggle="tooltip" data-placement="left"
                                      title="if you click this amount will transfer to the seller">
                                        Transfer Amount
                                </span>
                                    </a>
                                @else
                                    unregister
                                @endif
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Filter</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route($role.'financial_statement.show',$id)}}" method="get">
                    <input name="product_flag" value="{{Request()->input('product_flag')}}" hidden>
                    <div class="modal-body">
                        <select class="form-control" name="status">
                            <option>all</option>
                            <option>Delivered</option>
                            <option>Pending</option>
                            <option>Shipping</option>
                            <option>Warehouse</option>
                            <option>Canceled</option>
                            <option>In Process</option>
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection

{{-- Styles Section --}}
@section('styles')

@endsection

@section('scripts')
    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js?v=7.0.4') }}" type="text/javascript"></script>
    <script>
        var table = $('#kt_datatable');

        // begin first table
        table.DataTable({
            // DOM Layout settings
            dom: "<'row py-3'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>" +
                "<'row py-3'<'col-sm-12'tr>>" +
                "<'row py-3'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>", // read more: https://datatables.net/examples/basic_init/dom.html

            columnDefs: [{
                targets: -1,
                title: 'Actions',
                orderable: false,

            },
                {
                    width: '200px',
                    targets: 8,
                    render: function (data, type, full, meta) {
                        var status = {
                            'unregister': {
                                'title': 'not bank account yet',
                                'class': 'label-light-danger'
                            },
                            'paid': {
                                'title': 'Amount Paid',
                                'class': 'label-light-success'
                            },


                        };
                        if (typeof status[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="label label-lg font-weight-bold ' + status[data].class + ' label-inline">' + status[data].title + '</span>';
                    },
                },
            ],
        });
    </script>
@endsection
