@extends('layout.default')
@php
    if (Auth::guard('admin')->check()) {
        $role='';
        } else if (Auth::guard('seller')->check()) {
                $role='seller_';

        }
        $page_breadcrumbs=[[
            'page'=>route($role.'dashboard'),
        'title'=>'dashboard'
        ],
        [
        'page'=>route('trading.index'),
        'title'=>'Buy Banner Package'
        ]
        ];
@endphp
@section('styles')
    <link rel="stylesheet" href="{{asset('plugins/custom/datatables/datatables.bundle.css?v=7.0.4')}}">
@endsection
@section('content')
    @include('pages.message')
    {!! session()->get('error') !!}
    @if($errors->any())
        <div class="alert alert-success" role="alert">
            {{$errors->first()}}
        </div>
    @endif
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Banner Packages
                    <div class="text-muted pt-2 font-size-sm">All  Packages List</div>
                </h3>
            </div>

        </div>
        <div class="card-body">
            <!--begin: Search Form-->
            <!--begin::Search Form-->

            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
                <thead>
                <tr>
                    <th title="Field #1">#</th>
                    <th title="Field #2">Package Name</th>
                    <th title="Field #3">Price</th>
                    <th title="Field #4">Description</th>
                    <th title="Field #4">Type</th>
                    <th title="Field #4">Products count</th>
                    <th title="Field #4">Days</th>
                    <th title="Field #7">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($adPackages as $key => $adPackage)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$adPackage->name}}</td>
                        <td>{{$adPackage->price}}</td>
                        <td>{{ $adPackage->description ? \Illuminate\Support\Str::limit($adPackage->description, 30, $end='...') : 'N/A' }}</td>
                        <td>{{ $adPackage->type ?? 'N/A' }}</td>
                        <td>{{ $adPackage->product_count }}</td>
                        <td>{{ $adPackage->days }}</td>

                        <td>
                            <a data-toggle="modal"
                            onclick='model_data({{$adPackage->id}})'
                            data-target=".bd-example-modal-sm" class="navi-link">
                            <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                             <span class="navi-text">Buy</span>
                         </a>
                            {{-- <a href="{{route('saller-buy-packages', $adPackage->id)}}"
                               class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon">
                               <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                            </a> --}}
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <!--end: Datatable-->

    <div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="product_name">Payment </h5>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                <form role="form" action="{{ route('saller-buy-packages') }}" method="post" class="require-validation"
                      data-cc-on-file="false"
                      data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"
                      id="payment-form">
                    @csrf
                    <div class="modal-body">

                        @csrf
                        <div class='form-row row'>
                            <div class='col-xs-12 col-md-12  form-group required'>
                                <label class='control-label'>Name on Card</label> <input
                                        class='form-control' size='4' type='text'>
                            </div>
                        </div>

                        <div class='form-row row'>
                            <div class='col-xs-12 col-md-12 form-group card required'>
                                <label class='control-label'>Card Number</label><input
                                        autocomplete='off' class='form-control card-number number_only'  maxlength="16" size='16'
                                        type='text'>
                            </div>
                        </div>

                        <div class='form-row row'>
                            <div class='col-xs-12 col-md-4 form-group cvc required'>
                                <label class='control-label'>CVC</label> <input autocomplete='off'
                                                                                class='form-control card-cvc number_only' placeholder='ex. 311' size='3'
                                                                                type='text' maxlength="3"  pattern="[0-9]+">
                            </div>
                            <div class='col-xs-12 col-md-4 form-group expiration required'>
                                <label class='control-label'>Expiration Month</label><input
                                        class='form-control card-expiry-month number_only' placeholder='MM' size='2'
                                        type='text' maxlength="2" min="1" max="12">
                            </div>
                            <div class='col-xs-12 col-md-4 form-group expiration required'>
                                <label class='control-label'>Expiration Year</label> <input
                                        class='form-control card-expiry-year number_only' placeholder='YYYY' size='4'
                                        type='text'  maxlength="4" minlength="4" min="2021">
                            </div>
                        </div>
                        </div>

                        <div class='form-row row'>
                            <div class='col-md-12 error form-group hide'>
                                <div class='alert-danger alert' id="error_stripe" style="display: none">Please correct
                                    the errors and try
                                    again.
                                </div>
                            </div>
                        </div>
                        <input name="amount" type="hidden" value="{{base64_encode(25)}}">
                        <input name="package_id" id="payment_id" type="hidden">



                    <div class="modal-footer">

                        <button class="btn btn-primary btn-lg btn-block" type="submit">Pay Now</button>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>




@endsection

{{-- Styles Section --}}
@section('styles')

@endsection

@section('scripts')

<script>
    $('.number_only').bind('keyup paste', function(){
        this.value = this.value.replace(/[^0-9]/g, '');
    });
</script>

    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>

    <script type="text/javascript">

        $(Document).ready(function () {
            if ($('#shipping-address').is(':checked')) {
                $('#shippingAddress input').prop('disabled', false);
                $('#shippingAddress select').prop('disabled', false);
                $("#shippingAddress").show();
            } else {
                $('#shippingAddress input').prop('disabled', true);
                $('#shippingAddress select').prop('disabled', true);

                $("#shippingAddress").hide();
            }

        });
        $('#shipping-address').click(function () {
            if ($('#shipping-address').is(':checked')) {
                $('#shippingAddress input').prop('disabled', false);
                $('#shippingAddress select').prop('disabled', false);
                $("#shippingAddress").show();
            } else {
                $('#shippingAddress input').prop('disabled', true);
                $('#shippingAddress select').prop('disabled', true);
                $("#shippingAddress").hide();
            }
        });
        $(function () {
            var $form = $(".require-validation");
            $('form.require-validation').bind('submit', function (e) {
                var $form = $(".require-validation"),
                    inputSelector = ['input[type=email]', 'input[type=password]',
                        'input[type=text]', 'input[type=file]',
                        'textarea'].join(', '),
                    $inputs = $form.find('.required').find(inputSelector),
                    $errorMessage = $form.find('div.error'),
                    valid = true;
                $errorMessage.addClass('hide');

                $('.has-error').removeClass('has-error');
                $inputs.each(function (i, el) {
                    var $input = $(el);
                    if ($input.val() === '') {
                        $input.parent().addClass('has-error');
                        $errorMessage.removeClass('hide');
                        e.preventDefault();
                    }
                });

                if (!$form.data('cc-on-file')) {
                    e.preventDefault();
                    Stripe.setPublishableKey($form.data('stripe-publishable-key'));
                    Stripe.createToken({
                        number: $('.card-number').val(),
                        cvc: $('.card-cvc').val(),
                        exp_month: $('.card-expiry-month').val(),
                        exp_year: $('.card-expiry-year').val()
                    }, stripeResponseHandler);
                }

            });

            function stripeResponseHandler(status, response) {
                if (response.error) {
                    $('#error_stripe').show();
                    $('.error')
                        .removeClass('hide')
                        .find('.alert')
                        .text(response.error.message);
                } else {
                    // token contains id, last4, and card type
                    var token = response['id'];
                    // insert the token into the form so it gets submitted to the server
                    $form.find('input[type=text]').empty();
                    $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
                    $form.get(0).submit();
                }
            }

        });
    </script>
    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js?v=7.0.4') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function () {

        })

        function model_data(id) {
            $('#payment_id').val(id);
        }

        var table = $('#kt_datatable');

        // begin first table
        table.DataTable({
            // DOM Layout settings
            dom: "<'row py-3'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>" +
                "<'row py-3'<'col-sm-12'tr>>" +
                "<'row py-3'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>", // read more: https://datatables.net/examples/basic_init/dom.html

            columnDefs: [{
                targets: -1,
                title: 'Actions',
                orderable: false,

            },
                {
                    width: '75px',
                    targets: 5,
                    render: function (data, type, full, meta) {
                        var status = {
                            'Pending': {
                                'title': 'Pending',
                                'class': 'label-light-primary'
                            },

                            'Approved': {
                                'title': 'Approved',
                                'class': ' label-light-success'
                            },
                            'Canceled': {
                                'title': 'Canceled',
                                'class': ' label-light-danger'
                            },

                        };
                        if (typeof status[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="label label-lg font-weight-bold ' + status[data].class + ' label-inline">' + status[data].title + '</span>';
                    },
                }, {
                    width: '75px',
                    targets: 6,
                    render: function (data, type, full, meta) {
                        var status = {
                            'Pending': {
                                'title': 'Pending',
                                'class': 'label-light-primary'
                            },

                            'Completed': {
                                'title': 'Completed',
                                'class': ' label-light-success'
                            },
                            'Canceled': {
                                'title': 'Canceled',
                                'class': ' label-light-danger'
                            },

                        };
                        if (typeof status[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="label label-lg font-weight-bold ' + status[data].class + ' label-inline">' + status[data].title + '</span>';
                    },
                },
            ],
        });
    </script>


@endsection
