@extends('layout.default')
@php
$page_breadcrumbs=[
[
'page'=>route('dashboard'),
'title'=>'dashboard'
],
[
'page'=>route('advertise.banner.create'),
'title'=>'Advertise Banner Create'
]
];
@endphp
@section('styles')
<link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
<script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">
<style>
    .label-centered-field {
        display: flex;
        align-items: center;
        margin-top: 8px;
    }

    .time-off-action-row label {
        visibility: hidden;
    }

    .time-off-action-row a {
        display: table-cell;
        padding: 5px;
    }

    .day-activities {
        display: flex;
        align-items: center
    }

    .switch-right-side {
        margin-left: 5px;
    }

    #map {
        height: 400px;
        /* The height is 400 pixels */
        width: 100%;
        /* The width is the width of the web page */
    }

    #reset_location {
        margin-top: 5px;
    }
</style>
@endsection
@section('content')
<div class="row">


    <div class="col-md-12">
        <form class="form" id="form" method="post" action="{{route('advertise.banner.store')}}">
            <input type="hidden" value="{{ auth()->user()->id }}" name="user_id">
            <div class="card card-custom flex-wrap border-0 pt-6 pb-0">
                @csrf

                <div class="card card-custom card-sticky" id="kt_page_sticky_card">
                    <div class="card-header" style="">
                        <div class="card-title col-md-9">
                            <h3 class="card-label">Create banner
                            </h3>
                        </div>
                        <div class="card-toolbar">
                            <a href="{{route('advertise.banner.index')}}"
                                class="btn btn-secondary font-weight-bold mr-2">
                                <i class="ki ki-long-arrow-back icon-xs"></i>Cancel
                            </a>
                            <button type="submit" class="btn btn-primary font-weight-bolder">Save Banner
                            </button>
                        </div>


                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">

                                <div class="form-group">
                                    <label for="sellerName">Select Products</label>
                                    <div class="input-group ">
                                        <select class="form-control" name="product_id">
                                            <option value="">Select Product</option>
                                            @foreach($products as $p)
                                            <option value="{{$p->id}}">{{$p->name}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="sellerName">Select Banner Package</label>
                                    <div class="input-group ">
                                        <select class="form-control" name="package_id">
                                            <option value="">Select Package</option>
                                            @foreach($packages as $p)
                                            <option value="{{$p->id}}">{{"{$p->name} Package for {$p->price}$"  }}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>

                                <div class="form-group" id="">

                                    <div class="col-md-12">
                                        <label class="col-form-label">Start Date</label>
                                        <div class="input-group date">
                                            <input type="datetime" class="form-control" name="start_date" readonly>
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="la la-calendar">
                                                    </i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>


                                </div>



                            </div>
                            <div class="col-md-4">
                                <div class="flex-grow-1" style="position: relative;">
                                    <div class="form-group">
                                        <!-- <h1 class="image-upload-msg">Banner Image</h1> -->
                                        <div class="dropzone dropzone-default dropzone-success" id="banner_image">
                                            <div class="dropzone-msg dz-message needsclick">
                                                @if(isset($trip->banner_image))
                                                <img src="{{asset('/storage/'.$trip->banner_image)}}" width="100%">
                                                @else
                                                <h3 class="dropzone-msg-title">Drop files here or click
                                                    to
                                                    upload.</h3>
                                                <span class="dropzone-msg-desc">Only images are allowed for
                                                    upload</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="multi_image">
                            </div>


                            <div id="multi_image_expectation">
                            </div>


                        </div>


                    </div>


                </div>


            </div>
        </form>
        
    </div>
    @endsection
    @section('scripts')
    @section('scripts')
    <script src="{{asset('js/jquery-ui.js')}}"></script>

    <script>
        $('#choices-multiple-remove-button').change(function () {
                    $('#color_val').val($('#choices-multiple-remove-button').val());
                });
                $('#choices-multiple-remove-button2').change(function () {
                    $('#size_value_val').val($('#choices-multiple-remove-button2').val());
                });
                $(document).ready(function () {

                    var multipleCancelButton = new Choices('#choices-multiple-remove-button', {
                        removeItemButton: true,
                        maxItemCount: 50,
                        searchResultLimit: 50,
                        renderChoiceLimit: 50
                    });

                });

                $(document).ready(function () {

                    var multipleCancelButton2 = new Choices('#choices-multiple-remove-button2', {
                        removeItemButton: true,
                        maxItemCount: 50,
                        searchResultLimit: 50,
                        renderChoiceLimit: 50
                    });
                });


                const bv = FormValidation.formValidation(
                    document.getElementById('form'), {
                        fields: {

                            start_date: {
                                validators: {
                                    notEmpty: {
                                        message: 'Start Date is required'
                                    }
                                }
                            },
                            package_id: {
                                validators: {
                                    notEmpty: {
                                        message: 'Banner Package selection is required'
                                    }
                                }
                            },                           
                             product_id: {
                                validators: {
                                    notEmpty: {
                                        message: 'Product selection is required'
                                    }
                                }
                            },
                            

                        },

                        plugins: {
                            trigger: new FormValidation.plugins.Trigger(),
                            // Bootstrap Framework Integration
                            bootstrap: new FormValidation.plugins.Bootstrap(),
                            // Validate fields when clicking the Submit button
                            submitButton: new FormValidation.plugins.SubmitButton(),
                        }
                    }
                ).on('core.form.valid', function () {
                    // Send the form data to back-end
                    // You need to grab the form data and create an Ajax request to send them
                    // Get the form instance
                    var $form = $('#form');
                    // Get the BootstrapValidator instance
                    let method = $form.attr('method');
                    let url = $form.attr('action');
                    let data = new FormData(document.getElementById('form'));
                    submitFormAjax(method, url, data, function (res) {

                       successAlert(res.msg);
                        $('#form').trigger('reset');
                        window.location.href = "{{route('advertise.banner.index')}}";
                    }, function (jqXHR) {
                        let errors = jqXHR.responseJSON.errors;
                        let erMsgs = ``;
                        $.each(Object.keys(errors), function (i, v) {
                            erMsgs += `<p>${i + 1}-${errors[v][0]}</p>`;
                        })
                        errorAlert(erMsgs);
                    }, bv);
                });

                $('#banner_image').dropzone({
                    url: "{{route('products.attachment.upload','banner')}}", // Set the url for your upload script location
                    paramName: "banner", // The name that will be used to transfer the file
                    maxFiles: 1,
                    maxFilesize: 10, // MB
                    acceptedFiles: "image/*",
                    maxfilesreached: 1,
                    queueLimit: 1,

                    addRemoveLinks: true,
                    removedfile: function (file) {
                        file.previewElement.remove();
                        console.log(file);
                        let file_input = file.name;
                        var myobj = document.getElementById(file_input);

                        myobj.remove();

                    },
                    sending: function (file, xhr, formData) {
                        formData.append("_token", "{{ csrf_token() }}");
                    },

                    success: function (http, file) {
                        let data = jQuery.parseJSON(http.xhr.response)



                        console.log(file);

                        // var removeButton = Dropzone.createElement("<button>Remove file</button>");

                        // $('#multi_image').last().append('<input type="hidden" value="' + data.file_name + '" name="banner_image" id="' + data.file2 + '" multiple>');

                        // $( ".dz-remove" ).last().css({ fontsize: '15px'});
                        $(".dz-remove").addClass('btn btn-danger');
                        // $( ".dz-remove" ).html('<i class="fas fa-trash-alt"></i>');
                        $(".dz-remove").last().attr('data-info', data.file_name);
                        // Capture the Dropzone instance as closure.


                        $('.dz-preview').last().append('<input type="hidden" value="'+data.file_name+'" name="banner_image" id="'+data.file_name+'" multiple>');
                        $('#banner_image').previewElement.appendChild(removeButton);


                    },


                });
             

                $('.dz-remove').click(function () {
                    // alert('ali')
                    var dataid = $(this).data('data-info');

                    // alert(dataid);
                    $('#'.dataid).remove();
                    // $(this).find('.dz-preview').remove();

                });


            $(`[name=start_date]`).datetimepicker({
            showOn: 'focus',
            showButtonPanel: true,
            closeText: 'Clear', // Text to show for "close" button
            orientation: "top left",
            startDate: new Date(),
            todayBtn: true,
            minDate: 0,
            onClose: function () {
                var event = arguments.callee.caller.caller.arguments[0];
                // If "Clear" gets clicked, then really clear it
                if ($(event.delegateTarget).hasClass('ui-datepicker-close')) {
                    $(this).val('');
                    bv.revalidateField('start_date');
                }
            },
            onSelect: function () {
                bv.revalidateField('start_date');
            }
        });
              
    </script>
    @endsection