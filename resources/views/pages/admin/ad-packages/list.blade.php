@php
    use App\Functions\Helper;
@endphp
@extends('layout.default')
@section('content')

    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Banner Packages
                    <div class="text-muted pt-2 font-size-sm">All Packages List</div>
                </h3>
            </div>
            <div class="card-toolbar">

                <!--begin::Button-->
                <a href="{{route('ad-packages.create')}}" class="btn btn-primary font-weight-bolder">
                    <span class="svg-icon svg-icon-md">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <circle fill="#000000" cx="9" cy="15" r="6"/>
                                <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"
                                      fill="#000000" opacity="0.3"/>
                            </g>
                        </svg>
                        <!--end::Svg Icon-->
                    </span>New Record</a>
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
                <thead>
                <tr>
                    <th title="Field #1">#</th>
                    <th title="Field #2">Package Name</th>
                    <th title="Field #3">Price</th>
                    <th title="Field #4">Description</th>
                    <th title="Field #4">Type</th>
                    <th title="Field #4">Days</th>
                    <th title="Field #6">Created At</th>
                    <th title="Field #7">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($adPackages as $key => $adPackage)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$adPackage->name}}</td>
                        <td>{{$adPackage->price}}</td>
                        <td>{{ $adPackage->description ? \Illuminate\Support\Str::limit($adPackage->description, 30, $end='...') : 'N/A' }}</td>
                        <td>{{ $adPackage->type ?? 'N/A' }}</td>
                        <td>{{ $adPackage->days }}</td>
                        <td>{{ ($adPackage->created_at != ''|| $adPackage->created_at != null)?$adPackage->created_at->format('Y-m-d'): '' }}</td>

                        <td>
                             <span style="overflow: visible; position: relative; width: 125px;">
                            <div class="dropdown dropdown-inline "> <a href="javascript:;"
                                                                       class="btn btn-sm btn-light btn-text-primary btn-icon mr-2"
                                                                       data-toggle="dropdown">
                                    <span class="svg-icon svg-icon-md"> <svg xmlns="http://www.w3.org/2000/svg"
                                                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                             width="24px" height="24px"
                                                                             viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                <path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z"
                                                      fill="#000000"></path>
                                            </g>
                                        </svg> </span> </a>
                                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                    <ul class="navi flex-column navi-hover py-2">
                                        <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2">
                                            Choose an action: </li>
                                        <li class="navi-item">
                            <a href="{{route('ad-packages.edit', $adPackage->id)}}" class="navi-link">
                                                <span class="navi-icon">
                                                    <i class="la la-edit"></i>
                                                </span>
                                                <span class="navi-text">Edit</span>
                                            </a>
                            </li>
                            <li class="navi-item">
                                <a href="javascript:void(0)"
                                   onclick="deleteTableRecord('{{route('ad-packages.destroy',$adPackage->id)}}',$(this).closest('tr'))"
                                   class="navi-link">
                                                   <span class="navi-icon">
                                                    <i class="la la-trash"></i>
                                                </span>
                                    <span class="navi-text">Delete</span>
                                    </i>

                                </a>
                            </li>

                            </ul>
        </div>
    </div>
    </span>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>

    {{-- @include('admin.packages.modal') --}}

@endsection
