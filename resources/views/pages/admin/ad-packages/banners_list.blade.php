@extends('layout.default')
@php
        $page_breadcrumbs=[[
        'page'=>route('dashboard'),
        'title'=>'dashboard'
        ],
        [
        'page'=>route('advertise.banner.index'),
        'title'=>'Advertise Banners'
        ]
        ];
@endphp
@section('styles')
    <link rel="stylesheet" href="{{asset('plugins/custom/datatables/datatables.bundle.css?v=7.0.4')}}">
@endsection
@section('content')
    @include('pages.message')
    {!! session()->get('error') !!}
    @if($errors->any())
        <div class="alert alert-success" role="alert">
            {{$errors->first()}}
        </div>
    @endif
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Advertise Banners
                    <div class="text-muted pt-2 font-size-sm">Advertise Banners List</div>
                </h3>
            </div>
            <div class="card-toolbar">
                <a href="{{route('advertise.banner.create')}}" class="btn btn-primary font-weight-bolder font-size-sm mr-3 ">Add Banner</a>
            </div>

        </div>
        <div class="card-body">
            <!--begin: Search Form-->
            <!--begin::Search Form-->

            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
                <thead>
                <tr>
                    <th title="Field #1">#</th>
                    <th title="Field #2">Image</th>
                    <th title="Field #3">Product Name</th>
                    @if(auth()->user()->user_type === "admin") 
                    <th title="Field #4">Seller Name</th>
                    @endif
                    <th title="Field #4">Type</th>
                    <th title="Field #4">Days</th>
                    <th title="Field #4">Start On</th>
                    <th title="Field #4">End On</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($banners as $key => $banner)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td><img src="{{ asset('/storage/'.$banner->banner_image)}}" style="width:150px;max-height:150px;">
                        <td>{{ $banner->product->name ?? 'N/A' }}</td>
                        @if(auth()->user()->user_type === "admin") 
                        <td>{{ $banner->user->name ?? 'N/A' }}</td>
                        @endif
                        <td>{{ $banner->package->type ?? 'N/A' }}</td>
                        <td>{{ $banner->package->days }}</td>
                        <td>{{ Carbon\Carbon::parse($banner->start_date)->format('m-d-Y H:i A') }}</td>
                        <td>{{ Carbon\Carbon::parse($banner->end_date)->format('m-d-Y H:i A')}}</td>                           
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
  

@endsection

{{-- Styles Section --}}
@section('styles')

@endsection

@section('scripts')
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js?v=7.0.4') }}" type="text/javascript"></script>
<script>
    var table = $('#kt_datatable');
        // begin first table
        table.DataTable();
</script>
@endsection