@php
    use App\Functions\Helper;
@endphp
@extends('layout.default')
@section('content')

    <!--begin::Card-->
    <div class="card card-custom">
        <!--begin::Card header-->
        <div class="card-header card-header-tabs-line nav-tabs-line-3x">
            <!--begin::Toolbar-->
            <div class="card-toolbar">
                <ul class="nav nav-tabs nav-bold nav-tabs-line nav-tabs-line-3x">
                    <!--begin::Item-->
                    <li class="nav-item mr-3">
                        <a class="nav-link active" data-toggle="tab" href="#kt_user_edit_tab_1">
                        <span class="nav-icon">

                            <i class="flaticon-user-ok"></i>
                        </span>
                            <span class="nav-text font-size-lg">Banner Package</span>
                        </a>
                    </li>
                    <!--end::Item-->

                </ul>
            </div>
            <!--end::Toolbar-->
        </div>
        <!--end::Card header-->
        <!--begin::Card body-->
        <div class="card-body px-0">
            <form class="form admin-pack" id="kt_form" method="post"
                  action="{{($adPackage->id === null)?route('ad-packages.store'):route('ad-packages.update', $adPackage->id)}}"
                  autocomplete="off" enctype="multipart/form-data">
                @csrf
                <div class="tab-content">
                    <!--begin::Tab-->
                    <div class="tab-pane show active px-7" id="kt_user_edit_tab_1" role="tabpanel">
                        <!--begin::Row-->
                        <div class="row">
                            <div class="col-xl-2"></div>
                            <div class="col-xl-7 my-2">
                                <!--begin::Row-->
                                <div class="row">
                                    <label class="col-3"></label>
                                    <div class="col-9">
                                        <h6 class="text-dark font-weight-bold mb-10">package Info:</h6>
                                    </div>
                                </div>
                                <!--end::Row-->
                                <!--begin::Group-->
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left">Name<span class="text-danger">*</span></label>
                                    <div class="col-9">
                                        <input class="form-control form-control-lg form-control-solid" type="text"
                                               name="name"
                                               value="{{($adPackage->id === null)?old('name'):$adPackage->name}}"
                                               placeholder="Name" required/>
                                    </div>
                                </div>
                                <!--end::Group-->
                                <!--begin::Group-->
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left">Price<span class="text-danger">*</span></label>
                                    <div class="col-9">
                                        <div class="input-group input-group-lg input-group-solid">
                                            <input type="number" class="form-control form-control-lg form-control-solid"
                                                   min="1" max="999" name="price"
                                                   step=".01" pattern="^[0-9]+\.[0-9][0-9]$"
                                                   value="{{($adPackage->id === null)?old('price'):$adPackage->price}}"
                                                   placeholder="Price (10.99)" required/>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Group-->
                                <!--begin::Group-->
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left" for="type">asype<span class="text-danger">*</span></label>
                                    <div class="col-9">
                                        <div class="input-group input-group-lg input-group-solid">
                                            <select name="type" id="type"
                                                    class="form-control form-control-lg form-control-solid" required>
                                                <option value="">Select Package Type</option>
                                                <option value="Basic" {{($adPackage->type == 'Basic')?'selected':''}}>
                                                  Basic
                                                </option>
                                                <option value="Silver" {{($adPackage->type == 'Silver')?'selected':''}}>
                                                    Silver
                                                </option>
                                                <option value="Gold" {{($adPackage->type == 'Gold')?'selected':''}}>Gold
                                                </option>
                                                <option value="Platinum" {{($adPackage->type == 'Platinum')?'selected':''}}>
                                                    Platinum
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Group-->
                                <!--begin::Group-->
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left">Products Count<span class="text-danger">*</span></label>
                                    <div class="col-9">
                                        <div class="input-group input-group-lg input-group-solid">
                                            <input type="number" class="form-control form-control-lg form-control-solid"
                                                   min="0" max="5" name="product_count"
                                                   value="{{($adPackage->id === null)?old('product_count'):$adPackage->product_count}}"
                                                   placeholder="1" step="any" required />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left">Days<span class="text-danger">*</span></label>
                                    <div class="col-9">
                                        <div class="input-group input-group-lg input-group-solid">
                                            <input type="number" class="form-control form-control-lg form-control-solid"
                                                   min="0" max="" name="days"
                                                   value="{{($adPackage->id === null)?old('product_count'):$adPackage->days}}"
                                                   placeholder="1" step="any" required />
                                        </div>
                                    </div>
                                </div>
                                <!--end::Group-->

                                <!--begin::Group-->
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left">Description<span class="text-danger">*</span></label>
                                    <div class="col-9">
                                        <div class="input-group input-group-lg input-group-solid">
                                            <textarea name="description" id="" class="form-control" cols="30" rows="10"
                                                      required>{{($adPackage->id === null)?old('description'):$adPackage->description}}</textarea>
                                        </div>
                                        {{-- <span class="form-text text-muted">We'll never share your email with anyone else.</span> --}}
                                    </div>
                                </div>
                                <!--end::Group-->
                            </div>
                        </div>
                        <!--end::Row-->
                        <div class="separator separator-dashed my-10"></div>
                        <!--begin::Footer-->
                        <div class="card-footer pb-0">
                            <div class="row">
                                <div class="col-xl-2"></div>
                                <div class="col-xl-7">
                                    <div class="row">
                                        <div class="col-3"></div>
                                        <div class="col-9">
                                            <input type="submit" class="btn btn-light-primary font-weight-bold"
                                                   value="Save changes">
                                            <a href="{{route('ad-packages')}}" class="btn btn-clean font-weight-bold">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Footer-->
                    </div>
                    <!--end::Tab-->
                </div>
            </form>
        </div>
        <!--begin::Card body-->
    </div>
    <!--end::Card-->
@endsection
@section('scripts')
    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js?v=7.0.4') }}" type="text/javascript"></script>
    <script>
        function model_data1(comment, rate, product_name) {
            $('#product_name2').html(product_name);
            $('#comment').val(comment);
            $('#rate' + rate).prop('checked', true);


        }

        function model_data(id, product_name, product_id, user_id, seller_id) {
            // alert(id);
            // $('.input[name=group-2]').prop('radio',false);

            $('#order_id').val(id);
            $('#product_name').html(product_name);
            $('#product_id').val(product_id);
            $('#user_id').val(user_id);
            $('#seller_id').val(seller_id);

        }
        <?php if(Auth()->guard('admin')->check()) {?>
        var target_coloum = 1;
        <?php }else { ?>
        var target_coloum = 1;

        <?php } ?>
        var table = $('#kt_datatable');

        // begin first table
        table.DataTable({
            // DOM Layout settings
            dom: "<'row py-3'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>" +
                "<'row py-3'<'col-sm-12'tr>>" +
                "<'row py-3'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>", // read more: https://datatables.net/examples/basic_init/dom.html

            columnDefs: [{
                targets: -1,
                title: 'Actions',
                orderable: false,

            },
                {
                    width: '75px',
                    targets: target_coloum,
                    render: function (data, type, full, meta) {
                        var status = {
                            'Pending': {
                                'title': 'Pending',
                                'class': 'label-light-primary'
                            },

                            'Shipping': {
                                'title': 'Shipping',
                                'class': ' label-light-info'
                            },
                            'Warehouse': {
                                'title': 'Warehouse',
                                'class': ' label-light-info'
                            },
                            'Delivered': {
                                'title': 'Delivered',
                                'class': ' label-light-success'
                            },
                            'Canceled': {
                                'title': 'Canceled',
                                'class': ' label-light-danger'
                            },

                        };
                        if (typeof status[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="label label-lg font-weight-bold ' + status[data].class + ' label-inline">' + status[data].title + '</span>';
                    },
                },
            ],
        });
    </script>
@endsection


