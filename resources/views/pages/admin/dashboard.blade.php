@extends('layout.default')
@php
    $page_breadcrumbs=[[
    'page'=>route('dashboard'),
    'title'=>'Dashboard'
    ]
    ];
@endphp
@section('styles')
    <style>
        .counter-card {
       display: flex;
    justify-content: center;
    align-items: center;
        }

        .counter-card i {
            margin-right: 20px;
        }

        .counter-card-count-section {
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
        }

        .counter-card-count {
            font-size: 24px;
            font-weight: bold;
        }
    </style>
@endsection
@section('content')
@include('pages.message')
    <br>
    <div class="row ">
        <div class="col-md-3 bg-white px-6 py-8 rounded-xl mr-7 mb-7 counter-card">
            <i class="fas fa-calendar-check" style="font-size:60px;color:#10bbf3;"></i>
            <div class="counter-card-count-section">
                <a href="" class="counter-card-count">{{$sellerCount}}</a>
                <a href="#" class="text-primary font-weight-bold font-size-h6 mt-2">
                    Selling Product Order
                </a>
            </div>
        </div>
        <div class="col-md-3 bg-white px-6 py-8 rounded-xl  mr-7 mb-7 counter-card">
            <i class="fas  fa-file-invoice-dollar" style="font-size:60px;color:#10bbf3;"></i>
            <div class="counter-card-count-section">
                <a href="" class="counter-card-count">{{$auctionCount}}</a>
                <a href="#" class="text-primary font-weight-bold font-size-h6 mt-2">
                    Auction Orders
                </a>
            </div>

        </div>
        <div class="col-md-3 bg-white px-6 py-8 rounded-xl  mr-7 mb-7 counter-card">
            <i class="fas  fa-users" style="font-size:60px;color:#10bbf3;"></i>
            <div class="counter-card-count-section">
                <a href="" class="counter-card-count">{{$tradingCount}}</a>
                <a href="#" class="text-primary font-weight-bold font-size-h6 mt-2">
                    Trading Orders
                </a>
            </div>
        </div>
        @if (auth()->user()->user_type === "user")
        <div class="col-md-3 bg-white px-6 py-8 rounded-xl  mr-7 mb-7 counter-card">
            <i class="fas  fa-heart" style="font-size:60px;color:#10bbf3;"></i>
            <div class="counter-card-count-section">
                <a href="" class="counter-card-count">{{$wishlistCount}}</a>
                <a href="{{url('wishlist')}}" class="text-primary font-weight-bold font-size-h6 mt-2">
                    Wishlist
                </a>
            </div>
        </div>
    @endif
    </div>
    @if (auth()->user()->user_type === "admin")
    <div class="row ">
        <div class="col-md-3 bg-white px-6 py-8 rounded-xl mr-7 mb-7 counter-card">
            <i class="fas fa-exclamation text-danger" style="font-size:60px;"></i>
            <div class="counter-card-count-section">
                <a href="" class="counter-card-count text-danger">{{$count_pending['pending_sellerCount'] ?? 0}}</a>
                <a href="#" class="text-danger font-weight-bold font-size-h6 mt-2">
                    Pending Selling Order
                </a>
            </div>
        </div>
        <div class="col-md-3 bg-white px-6 py-8 rounded-xl  mr-7 mb-7 counter-card">
            <i class="fas fa-exclamation text-danger" style="font-size:60px;"></i>
            <div class="counter-card-count-section">
                <a href="" class="counter-card-count text-danger">{{$count_pending['pending_auctionCount'] ?? 0}}</a>
                <a href="#" class="text-danger font-weight-bold font-size-h6 mt-2">
                    Pending Auction Orders
                </a>
            </div>

        </div>
        <div class="col-md-3 bg-white px-6 py-8 rounded-xl  mr-7 mb-7 counter-card">
            <i class="fas fa-exclamation text-danger" style="font-size:60px;"></i>
            <div class="counter-card-count-section">
                <a href="" class="counter-card-count text-danger">{{$count_pending['pending_tradingCount'] ?? 0}}</a>
                <a href="#" class="text-danger font-weight-bold font-size-h6 mt-2">
                    Pending Trading Orders
                </a>
            </div>

        </div>
    </div>

    @endif


    @if(auth()->user()->user_type === "user")
    <div class="row ">

        <div class="col-md-6 bg-white px-6 py-8 rounded-xl  mr-7 mb-7 counter-card">
            <i class="fas fa-calendar" style="font-size:60px;color:#10bbf3;"></i>
            <div class="counter-card-count-section">
                <a href="" class="counter-card-count">{{(auth()->user()->package_valid_till != null) ? Carbon\Carbon::parse(auth()->user()->package_valid_till)->format('M-d-Y') : '0' }}</a>
                <a href="#" class="text-primary font-weight-bold font-size-h6 mt-2">
                    Package Valid Till
                </a>
            </div>
        </div>
    </div>
    @endif
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
@endsection
