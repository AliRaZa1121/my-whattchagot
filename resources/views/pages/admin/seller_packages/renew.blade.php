@extends('layout.default')
@php
if (Auth::guard('admin')->check()) {
    $role = '';
} elseif (Auth::guard('seller')->check()) {
    $role = 'seller_';
} elseif (Auth::guard('seller')->check()) {
    $role = 'buyer_';
}
$page_breadcrumbs = [
    [
        'page' => route($role . 'dashboard'),
        'title' => 'Package Renewel',
    ],
];
@endphp
@section('styles')
    <script src="https://js.stripe.com/v3/"></script>

    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <style>
        .label-centered-field {
            display: flex;
            align-items: center;
            margin-top: 8px;
        }

        .time-off-action-row label {
            visibility: hidden;
        }

        .time-off-action-row a {
            display: table-cell;
            padding: 5px;
        }

        .day-activities {
            display: flex;
            align-items: center
        }

        .switch-right-side {
            margin-left: 5px;
        }

        #map {
            height: 400px;
            /* The height is 400 pixels */
            width: 100%;
            /* The width is the width of the web page */
        }

        #reset_location {
            margin-top: 5px;
        }

    </style>

<style>
    /**
        * The CSS shown here will not be introduced in the Quickstart guide, but shows
        * how you can use CSS to style your Element's container.
        */
    .StripeElement {
        box-sizing: border-box;

        height: 40px;
        width: 100%;
        padding: 10px 12px;

        border: 1px solid transparent;
        border-radius: 4px;
        background-color: #e6ebf1;

        box-shadow: 0 1px 3px 0 #e6ebf1;
        -webkit-transition: box-shadow 150ms ease;
        transition: box-shadow 150ms ease;
    }

    .StripeElement--focus {
        box-shadow: 0 1px 3px 0 #cfd7df;
    }

    .StripeElement--invalid {
        border-color: #fa755a;
    }

    .StripeElement--webkit-autofill {
        background-color: #fefde5 !important;
    }

    #card-errors {
        color: orangered
    }

</style>
@endsection
@section('content')
@include('pages.message')

    <div class="row">
        <div class="col-md-12">
            <div class="card card-custom flex-wrap border-0 pt-6 pb-0">
                <form class="form" id="form-renew" method="POST" action="{{ route('package-renew.update') }}">
                    @csrf

                    <div class="card card-custom card-sticky" id="kt_page_sticky_card">
                        <div class="card-header" style="">
                            <div class="card-title col-md-9">
                                <h3 class="card-label">Seller Packages Renewel

                                </h3>
                            </div>
                            <div class="card-toolbar">
                                <a href="{{ route('seller.packages') }}" class="btn btn-secondary font-weight-bold mr-2">
                                    <i class="ki ki-long-arrow-back icon-xs"></i>Cancel
                                </a>
                                <button type="submit" class="btn btn-primary font-weight-bolder">Update
                                    Package</button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">

                                <div class="col-md-8">

                                    <div class="form-group">
                                        <label for="productName">Name</label>
                                        <select name="seller_package_id" id="seller_package_id" class="form-control"
                                            required>
                                            <option value="">Select Seller Package</option>
                                            @foreach ($packages as $pkg)
                                                <option value="{{ $pkg->id }}"
                                                    data-trade="{{ $pkg->trade_product_quantity }}"
                                                    data-seller="{{ $pkg->seller_product_quantity }}"
                                                    data-auction="{{ $pkg->auction_product_quantity }}">
                                                    {{ '$' . $pkg->price . ' ' . 'for' . ' ' . $pkg->duration . ' Months' }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                                <input type="hidden" name="nonce" id="stripe_token" value="">

                                <div class="col-md-8">

                                    <div class="form-group">
                                        <label for="productName">Seller Product Quantity</label>
                                        <input type="text" id="seller" class="form-control" disabled value="">
                                    </div>

                                </div>

                                <div class="col-md-8">

                                    <div class="form-group">
                                        <label for="productName">Auction Product Quantity</label>
                                        <input type="text" id="auction" class="form-control" disabled value="">
                                    </div>

                                </div>


                                <div class="col-md-8">

                                    <div class="form-group">
                                        <label for="productName">Trade Product Quantity</label>
                                        <input type="text" id="tarde" class="form-control" disabled value="">
                                    </div>

                                </div>

                            </div>
                        </div>

                </form>
            </div>

        </div>
    </div>

    <div class="modal fade" id="stripe" tabindex="-1" role="dialog" aria-labelledby="stripeLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3>Pay with Stripe</h3>
                    <hr>
                    <form class="container" id="stripe-payment-form">
                        <div class="row">
                            <div class="col-sm-12">
                                <label for="card-element">
                                    Credit or debit card
                                </label>
                                <div id="card-element">
                                    <!-- A Stripe Element will be inserted here. -->
                                </div>

                                <!-- Used to display Element errors. -->
                                <div id="card-errors" role="alert"></div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12">
                                <button class="btn btn-primary pay-btn">Pay</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/jquery-ui.js') }}"></script>

    <script>
        $('select').on('change', '', function(e) {
            var trade = $('option:selected', this).data("trade");
            var seller = $('option:selected', this).data("seller");
            var auction = $('option:selected', this).data("auction");

            $("#tarde").val(trade);
            $("#seller").val(seller);
            $("#auction").val(auction);

        });

        $('.close').click(function() {
            $('#stripe').hide();
            $('#stripe').removeClass('show');
        })

        $('#form-renew').on('submit', function(e) {
            e.preventDefault();
            e.stopImmediatePropagation();
            $('#stripe').show();
            $('#stripe').addClass('show');

        });

    </script>

<script> // Create a Stripe client.
    // Issue on route cache
    var stripe = Stripe("{{ env('STRIPE_KEY') }}");


    // Create an instance of Elements.
    var elements = stripe.elements();

    // Custom styling can be passed to options when creating an Element.
    // (Note that this demo uses a wider set of styles than the guide below.)
    var style = {
        base: {
            color: '#32325d',
            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
            fontSmoothing: 'antialiased',
            fontSize: '16px',
            '::placeholder': {
                color: '#aab7c4'
            }
        },
        invalid: {
            color: '#fa755a',
            iconColor: '#fa755a'
        }
    };

    // Create an instance of the card Element.
    var card = elements.create('card', {
        style: style
    });

    // Add an instance of the card Element into the `card-element` <div>.
    card.mount('#card-element');
    // Handle real-time validation errors from the card Element.
    card.on('change', function(event) {
        var displayError = document.getElementById('card-errors');
        if (event.error) {
            displayError.textContent = event.error.message;
        } else {
            displayError.textContent = '';
        }
    });

    // Handle form submission.
    var form = document.getElementById('stripe-payment-form');
    form.addEventListener('submit', function(event) {
        event.preventDefault();
        $('.pay-btn').addClass('spinner spinner-darker-success spinner-left mr-3');
        $('.pay-btn').attr("disabled", "disabled");
        stripe.createToken(card).then(function(result) {
            if (result.error) {
                // Inform the user if there was an error.
                var errorElement = document.getElementById('card-errors');
                errorElement.textContent = result.error.message;
            } else {
                // Send the token to your server.
                stripeTokenHandler(result.token);
            }
        });
    });

    // Submit the form with the token ID.
    function stripeTokenHandler(token) {
        // Insert the token ID into the form so it gets submitted to the server
        var form = document.getElementById('form-renew');
        document.getElementById('stripe_token').value = token.id
        document.getElementById('form-renew').submit();
        //   alert(token);

        // Submit the form
        form.submit();
    }
</script>

@endsection
