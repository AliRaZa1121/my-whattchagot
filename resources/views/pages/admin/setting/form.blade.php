@extends('layout.default')
@php
$page_breadcrumbs=[
[
'page'=>route('dashboard'),
'title'=>'dashboard'
],
[
'page'=>route('settings'),
'title'=>'Whattchagot Settings'
]
];
@endphp
@section('styles')
<link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
<script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">
<style>
    .label-centered-field {
        display: flex;
        align-items: center;
        margin-top: 8px;
    }

    .time-off-action-row label {
        visibility: hidden;
    }

    .time-off-action-row a {
        display: table-cell;
        padding: 5px;
    }

    .day-activities {
        display: flex;
        align-items: center
    }

    .switch-right-side {
        margin-left: 5px;
    }

    #map {
        height: 400px;
        /* The height is 400 pixels */
        width: 100%;
        /* The width is the width of the web page */
    }

    #reset_location {
        margin-top: 5px;
    }
</style>
@endsection
@section('content')
<div class="row">


    <div class="col-md-12">
        <form class="form" id="form" method="post" action="{{route('settings.update',$setting->id)}}">

            <div class="card card-custom flex-wrap border-0 pt-6 pb-0">

                @csrf

                <div class="card card-custom card-sticky" id="kt_page_sticky_card">
                    <div class="card-header" style="">
                        <div class="card-title col-md-9">
                            <h3 class="card-label">Setting
                            </h3>
                        </div>
                        <div class="card-toolbar">

                            <button type="submit" class="btn btn-primary font-weight-bolder">Submit
                            </button>
                        </div>


                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="Email" required id="email" class="form-control" name="email"
                                        placeholder='example@example.com'
                                        value="{{isset($setting->email) ? $setting->email : '' }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="phone">Phone</label>
                                    <input type="text" required id="phone" class="form-control" name="phone"
                                        placeholder='(111) 111-1111' maxlength="14"
                                        value="{{isset($setting->phone) ? $setting->phone : '' }}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="country">Facebook link</label>
                                    <input class="form-control form-control-lg form-control-solid" type="url"
                                    name="facebook_link"
                                    value="{{ $setting->id === null ? old('facebook_link') : $setting->facebook_link }}"
                                    placeholder="facebook link " />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="state">Twitter link</label>
                                    <input class="form-control form-control-lg form-control-solid" type="url"
                                    name="twitter_link"
                                    value="{{ $setting->id === null ? old('twitter_link') : $setting->twitter_link }}"
                                    placeholder="Twitter Link" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="zip">Instagram link</label>

                                    <input class="form-control form-control-lg form-control-solid" type="url"
                                    name="instagram_link"
                                    value="{{ $setting->id === null ? old('instagram_link') : $setting->instagram_link }}"
                                    placeholder="instagram link" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="zip">Tiktok link</label>

                                    <input class="form-control form-control-lg form-control-solid" type="url"
                                    name="tiktok_link"
                                    value="{{ $setting->id === null ? old('tiktok_link') : $setting->tiktok_link }}"
                                    placeholder="tiktok_link link" />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <textarea required id="address" class="form-control" name="address"
                                        placeholder="complete address">{{isset($setting->address) ? $setting->address : '' }}</textarea>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="address">Shipping Details</label>
                                    <textarea required id="shipping_deatils" class="form-control" name="shipping_deatils"
                                        placeholder="complete shipping deatils">{{isset($setting->shipping_deatils) ? $setting->shipping_deatils : '' }}</textarea>

                                </div>
                            </div>


                        </div>


                    </div>


                </div>


            </div>
        </form>
    </div>
    @endsection

    @section('scripts')
    <script src="{{asset('js/jquery-ui.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

    <script>
        const bv = FormValidation.formValidation(
                    document.getElementById('form'), {
                        fields: {
                            email: {
                                validators: {
                                    notEmpty: {
                                        message: 'Emai is required'
                                    }
                                }
                            },
                            phone: {
                                validators: {
                                    notEmpty: {
                                        message: 'Phone is required'
                                    }
                                }
                            },

                            address: {
                                validators: {
                                    notEmpty: {
                                        message: 'Address is required'
                                    }
                                }
                            },
                            address: {
                                validators: {
                                    notEmpty: {
                                        message: 'Address is required'
                                    }
                                }
                            },
                        },


                        plugins: {
                            trigger: new FormValidation.plugins.Trigger(),
                            // Bootstrap Framework Integration
                            bootstrap: new FormValidation.plugins.Bootstrap(),
                            // Validate fields when clicking the Submit button
                            submitButton: new FormValidation.plugins.SubmitButton(),
                        }
                    }
                ).on('core.form.valid', function () {
                    // Send the form data to back-end
                    // You need to grab the form data and create an Ajax request to send them
                    // Get the form instance
                    var $form = $('#form');
                    // Get the BootstrapValidator instance
                    let method = $form.attr('method');
                    let url = $form.attr('action');
                    let data = new FormData(document.getElementById('form'));
                    submitFormAjax(method, url, data, function (res) {


                        successAlert(res.msg);
                        // $('#form').trigger('reset');
                        $('.dz-complete').remove();
                        $('.needsclick').show();


                        window.location.href = "{{route('settings')}}";

                    }, function (jqXHR) {
                        let errors = jqXHR.responseJSON.errors;
                        let erMsgs = ``;
                        $.each(Object.keys(errors), function (i, v) {
                            erMsgs += `<p>${i + 1}-${errors[v][0]}</p>`;
                        })
                        errorAlert(erMsgs);
                    }, bv);
                });


                $(document).ready(function () {
                    $('#phone').usPhoneFormat({
                        format: '(xxx) xxx-xxxx',
                    });

                });


                (function ($) {
                    $.fn.usPhoneFormat = function (options) {
                        var params = $.extend({
                            format: 'xxx-xxx-xxxx',
                            international: false,

                        }, options);

                        if (params.format === 'xxx-xxx-xxxx') {
                            $(this).bind('paste', function (e) {

                                e.preventDefault();
                                var inputValue = e.originalEvent && e.originalEvent.clipboardData.getData('Text');
                                inputValue = inputValue.replace(/\D/g, '');
                                if (!$.isNumeric(inputValue)) {
                                    return false;
                                } else {
                                    if (inputValue.length > 9) {
                                        inputValue = String(inputValue.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"));
                                    } else {
                                        inputValue = String(inputValue.replace(/(\d{3})(?=\d)/g, '$1-'));
                                    }
                                    $(this).val(inputValue);
                                    $(this).val('');
                                    inputValue = inputValue.substring(0, 12);
                                    $(this).val(inputValue);
                                }
                            });
                            $(this).on('keydown touchend', function (e) {

                                e = e || window.event;
                                var key = e.which || e.keyCode; // keyCode detection
                                var ctrl = e.ctrlKey || e.metaKey || key === 17; // ctrl detection
                                if (key == 86 && ctrl) { // Ctrl + V Pressed !

                                } else if (key == 67 && ctrl) { // Ctrl + C Pressed !

                                } else if (key == 88 && ctrl) { // Ctrl + x Pressed !

                                } else if (key == 65 && ctrl) { // Ctrl + a Pressed !
                                    $(this).trigger("paste");
                                } else if (key != 9 && e.which != 8 && e.which != 0 && !(e.keyCode >= 96 && e.keyCode <= 105) && !(e.keyCode >= 48 && e.keyCode <= 57)) {
                                    return false;
                                }
                                var curchr = this.value.length;
                                var curval = $(this).val();
                                if (curchr == 3 && e.which != 8 && e.which != 0) {
                                    $(this).val(curval + "-");
                                } else if (curchr == 7 && e.which != 8 && e.which != 0) {
                                    $(this).val(curval + "-");
                                }
                                $(this).attr('maxlength', '12');
                            });

                        } else if (params.format === '(xxx) xxx-xxxx') {
                            $(this).on('keydown touchend', function (e) {

                                e = e || window.event;
                                var key = e.which || e.keyCode; // keyCode detection
                                var ctrl = e.ctrlKey || e.metaKey || key === 17; // ctrl detection
                                if (key == 86 && ctrl) { // Ctrl + V Pressed !

                                } else if (key == 67 && ctrl) { // Ctrl + C Pressed !

                                } else if (key == 88 && ctrl) { //Ctrl + x Pressed

                                } else if (key == 65 && ctrl) { //Ctrl + a Pressed !
                                    $(this).trigger("paste");
                                } else if (key != 9 && e.which != 8 && e.which != 0 && !(e.keyCode >= 96 && e.keyCode <= 105) && !(e.keyCode >= 48 && e.keyCode <= 57)) {
                                    return false;
                                }
                                var curchr = this.value.length;
                                var curval = $(this).val();
                                if (curchr == 3 && e.which != 8 && e.which != 0) {
                                    $(this).val('(' + curval + ')' + " ");
                                } else if (curchr == 9 && e.which != 8 && e.which != 0) {
                                    $(this).val(curval + "-");
                                }
                                $(this).attr('maxlength', '14');

                            });
                            $(this).bind('paste', function (e) {

                                e.preventDefault();
                                var inputValue = e.originalEvent && e.originalEvent.clipboardData.getData('Text');
                                inputValue = inputValue.replace(/\D/g, '');

                                if (!$.isNumeric(inputValue)) {
                                    return false;
                                } else {

                                    if (inputValue.length > 9) {
                                        inputValue = String(inputValue.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3"));
                                    } else if (inputValue.length > 6) {
                                        inputValue = String(inputValue.replace(/(\d{3})(\d{3})(?=\d)/g, '($1) $2-'));
                                    } else if (inputValue.length > 3) {
                                        inputValue = String(inputValue.replace(/(\d{3})(?=\d)/g, '($1) '));
                                    }

                                    $(this).val(inputValue);
                                    $(this).val('');
                                    inputValue = inputValue.substring(0, 14);
                                    $(this).val(inputValue);
                                }
                            });

                        }

                    }
                }(jQuery));


    </script>
    @endsection
