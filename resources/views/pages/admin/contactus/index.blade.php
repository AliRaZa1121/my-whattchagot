@extends('layout.default')

@section('styles')
    <link rel="stylesheet" href="{{asset('plugins/custom/datatables/datatables.bundle.css?v=7.0.4')}}">
@endsection
@section('content')
    @include('pages.message')
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Contact Us Listing</h3>
            </div>
           
        </div>

        <div class="card-body">
            <!--begin: Search Form-->
            <!--begin::Search Form-->
            @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Date</th>
                    <th>Message</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>

                @foreach($contactus as $c)
                    <tr>
                        
                        <td>{{$c->name}}</td>
                        <td>{{$c->email}}</td>
                        <td>{{$c->phone}}</td>
                        <td>{{$c->created_at ? $c->created_at->format('m-d-Y') : '--'}}
                        <td>{{$c->message}}</td>
                        <td><a href="{{route('contact-delete',$c->id)}}" class="btn btn-sm btn-danger">Delete</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection

{{-- Styles Section --}}
@section('styles')

@endsection

@section('scripts')
    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js?v=7.0.4') }}" type="text/javascript"></script>
    <script>
        var table = $('#kt_datatable');

        // begin first table
        table.DataTable({
            // DOM Layout settings
            dom: "<'row py-3'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>" +
                "<'row py-3'<'col-sm-12'tr>>" +
                "<'row py-3'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>", // read more: https://datatables.net/examples/basic_init/dom.html

            columnDefs: [{
                targets: -1,
                title: 'Actions',
                orderable: false,

            },
            ],
        });
    </script>
@endsection
