@php
    use App\Functions\Helper;
@endphp
@extends('layout.default')
@section('content')

    <!--begin::Card-->
    <div class="card card-custom">
        <!--begin::Card header-->
        <div class="card-header card-header-tabs-line nav-tabs-line-3x">
            <!--begin::Toolbar-->
            <div class="card-toolbar">
                <ul class="nav nav-tabs nav-bold nav-tabs-line nav-tabs-line-3x">
                    <!--begin::Item-->
                    <li class="nav-item mr-3">
                        <a class="nav-link active" data-toggle="tab" href="#kt_user_edit_tab_1">
                        <span class="nav-icon">

                            <i class="flaticon-user-ok"></i>
                        </span>
                            <span class="nav-text font-size-lg">Package</span>
                        </a>
                    </li>
                    <!--end::Item-->

                </ul>
            </div>
            <!--end::Toolbar-->
        </div>
        <!--end::Card header-->
        <!--begin::Card body-->
        <div class="card-body px-0">
            <form class="form admin-pack" id="package_form" method="post"
                  action="{{($package->id === null)?route('packages.store'):route('packages.update', $package->id)}}"
                  autocomplete="off" enctype="multipart/form-data">
                @csrf
                <div class="tab-content">
                    <!--begin::Tab-->
                    <div class="tab-pane show active px-7" id="kt_user_edit_tab_1" role="tabpanel">
                        <!--begin::Row-->
                        <div class="row">
                            <div class="col-xl-2"></div>
                            <div class="col-xl-7 my-2">
                                <!--begin::Row-->
                                <div class="row">
                                    <label class="col-3"></label>
                                    <div class="col-9">
                                        <h6 class="text-dark font-weight-bold mb-10">Package Info:</h6>
                                    </div>
                                </div>
                                <!--end::Row-->
                                <!--begin::Group-->
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left">
                                      Name<span class="text-danger">*</span>
                                    </label>
                                    <div class="col-9">
                                        <input class="form-control form-control-lg form-control-solid" type="text"
                                               name="name"
                                               value="{{($package->id === null)?old('name'):$package->name}}"
                                               placeholder="Name" required/>
                                    </div>
                                </div>
                                <!--end::Group-->
                                <!--begin::Group-->
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left">
                                      Monthly Price
                                      <span class="text-danger">*</span>
                                    </label>
                                    <div class="col-9">
                                        <div class="input-group input-group-lg input-group-solid">
                                            <input type="number" class="form-control form-control-lg form-control-solid"
                                                   min="1" max="9999" name="monthly_price"
                                                   value="{{($package->id === null)?old('monthly_price'):$package->monthly_price}}"
                                                   placeholder="Price (10.99)" step=".01" pattern="^[0-9]+\.[0-9][0-9]$" required/>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Group-->
{{--                                <!--begin::Group-->--}}
{{--                                <div class="form-group row">--}}
{{--                                    <label class="col-form-label col-3 text-lg-right text-left">Stripe Monthly Plan Price</label>--}}
{{--                                    <div class="col-9">--}}
{{--                                        <div class="input-group input-group-lg input-group-solid">--}}
{{--                                            <input type="text" class="form-control form-control-lg form-control-solid"--}}
{{--                                                   name="monthly_plan_price"--}}
{{--                                                   value="{{($package->id === null)?old('monthly_plan_price'):$package->monthly_plan_price}}"--}}
{{--                                                   placeholder="Monthly Price from stripe product price" required/>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <!--end::Group-->--}}
{{--                                <!--begin::Group-->--}}
{{--                                <div class="form-group row">--}}
{{--                                    <label class="col-form-label col-3 text-lg-right text-left">Stripe Yearly Plan Price</label>--}}
{{--                                    <div class="col-9">--}}
{{--                                        <div class="input-group input-group-lg input-group-solid">--}}
{{--                                            <input type="text" class="form-control form-control-lg form-control-solid"--}}
{{--                                                   name="yearly_plan_price"--}}
{{--                                                   value="{{($package->id === null)?old('yearly_plan_price'):$package->yearly_plan_price}}"--}}
{{--                                                   placeholder="Yearly Price from stripe product price" required/>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <!--end::Group-->--}}
                                <!--begin::Group-->
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left">
                                      Yearly Price
                                      <span class="text-danger">*</span>
                                    </label>
                                    <div class="col-9">
                                        <div class="input-group input-group-lg input-group-solid">
                                            <input type="number" class="form-control form-control-lg form-control-solid"
                                                   min="1" max="9999" name="yearly_price"
                                                   value="{{($package->id === null)?old('yearly_price'):$package->yearly_price}}"
                                                   placeholder="Price (10.99)" step=".01" pattern="^[0-9]+\.[0-9][0-9]$" required/>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Group-->
                                <!--begin::Group-->
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left" for="type">Package
                                        Type
                                        <span class="text-danger">*</span>
                                    </label>
                                    <div class="col-9">
                                        <div class="input-group input-group-lg input-group-solid">
                                            <select name="type" id="type"
                                                    class="form-control form-control-lg form-control-solid" required>
                                                <option value="">Select Package Type</option>
                                                <option value="silver" {{($package->type == 'silver')?'selected':''}}>
                                                    Silver
                                                </option>
                                                <option value="gold" {{($package->type == 'gold')?'selected':''}}>Gold
                                                </option>
                                                <option value="platinum" {{($package->type == 'platinum')?'selected':''}}>
                                                    Platinum
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Group-->
                              <!--begin::Group-->
                              <div class="form-group row">
                                <label class="col-form-label col-3 text-lg-right text-left" for="type">
                                  No. of Employees (Optional)
                                </label>
                                <div class="col-9">
                                  <div class="input-group input-group-lg input-group-solid">
                                    <select name="no_of_employees" id="no_of_employees"
                                            class="form-control form-control-lg form-control-solid">
                                      <option value="">Select No of Employees</option>
                                      <option value="opc_emp" {{($package->no_of_employees == 'opc_emp')?'selected':''}}>One Person Business (OPC)</option>
                                      <option value="1_to_5_emp" {{($package->no_of_employees == '1_to_5_emp')?'selected':''}}>1 to 5 employees</option>
                                      <option value="5_to_30_emp" {{($package->no_of_employees == '5_to_30_emp')?'selected':''}}>5 to 30 employees</option>
                                      <option value="30_to_100_emp" {{($package->no_of_employees == '30_to_100_emp')?'selected':''}}>30 to 100+ employees</option>
                                    </select>
                                  </div>
                                </div>
                              </div>
                              <!--end::Group-->
                                <!--begin::Group-->
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left" for="address_phone_fax">Address,
                                        Phone &
                                        Fax</label>
                                    <div class="col-3">
                                        <div class="input-group">
                                            <input  class="admin-pack-check " type="checkbox" name="address_phone_fax"
                                                   {{($package->address_phone_fax == 1)?'checked':''}}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <!--end::Group-->
                                <!--begin::Group-->
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left"
                                           for="maps_and_directions">Maps & Direction</label>
                                    <div class="col-3">
                                        <div class="input-group">
                                            <input class="admin-pack-check" type="checkbox" name="maps_and_directions"
                                                   {{($package->maps_and_directions == 1)?'checked':''}}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <!--end::Group-->
                                <!--begin::Group-->
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left"
                                           for="hours_of_operation">Hours of Operation</label>
                                    <div class="col-3">
                                        <div class="input-group">
                                            <input class="admin-pack-check" type="checkbox" name="hours_of_operation"

                                                   {{($package->hours_of_operation == 1)?'checked':''}}
                                                   />
                                        </div>
                                    </div>
                                </div>
                                <!--end::Group-->
                                <!--begin::Group-->
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left" for="area_served">Area
                                        Served</label>
                                    <div class="col-3">
                                        <div class="input-group">
                                            <input class="admin-pack-check" type="checkbox" name="area_served"

                                                   {{($package->area_served == 1)?'checked':''}}
                                                   />
                                        </div>
                                    </div>
                                </div>
                                <!--end::Group-->
                                <!--begin::Group-->
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left" for="contact_us_form">Contact
                                        us Form</label>
                                    <div class="col-3">
                                        <div class="input-group">
                                            <input class="admin-pack-check" type="checkbox" name="contact_us_form"

                                                   {{($package->contact_us_form == 1)?'checked':''}}
                                                   />
                                        </div>
                                    </div>
                                </div>
                                <!--end::Group-->
                                <!--begin::Group-->
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left" for="website_link">Website
                                        Link</label>
                                    <div class="col-3">
                                        <div class="input-group">
                                            <input class="admin-pack-check" type="checkbox" name="website_link"

                                                   {{($package->website_link == 1)?'checked':''}}
                                                   />
                                        </div>
                                    </div>
                                </div>
                                <!--end::Group-->
                                <!--begin::Group-->
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left" for="about_us_desc">
                                      About us Description</label>
                                    <div class="col-3">
                                        <div class="input-group">
                                            <input class="admin-pack-check" type="checkbox" name="about_us_desc"

                                                   {{($package->about_us_desc == 1)?'checked':''}}
                                                   />
                                        </div>
                                    </div>
                                </div>
                                <!--end::Group-->
                                <!--begin::Group-->
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left" for="social_media_link">Social
                                        Media Links</label>
                                    <div class="col-3">
                                        <div class="input-group">
                                            <input class="admin-pack-check" type="checkbox" name="social_media_link"

                                                   {{($package->social_media_link == 1)?'checked':''}}
                                                   />
                                        </div>
                                    </div>
                                </div>
                                <!--end::Group-->
                                <!--begin::Group-->
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left" for="manage_reviews">Manage
                                        Reviews</label>
                                    <div class="col-3">
                                        <div class="input-group">
                                            <input class="admin-pack-check" type="checkbox" name="manage_reviews"

                                                   {{($package->manage_reviews == 1)?'checked':''}}
                                                   />
                                        </div>
                                    </div>
                                </div>
                                <!--end::Group-->
                                <!--begin::Group-->
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left" for="home_page_exposure">
                                        Home Page Exposure</label>
                                    <div class="col-3">
                                        <div class="input-group">
                                            <input class="admin-pack-check " type="checkbox" name="homepage_exposure"
                                                    {{($package->homepage_exposure == 1)? 'checked' : ''}}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <!--end::Group-->
                                <!--begin::Group-->
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left"
                                           for="menu_and_attachment">
                                      Menu & Attachment
                                      <span class="text-danger">*</span>
                                    </label>
                                    <div class="col-9">
                                        <div class="input-group input-group-lg input-group-solid">
                                            <input type="number" class="form-control form-control-lg form-control-solid"
                                                   min="1" max="20" name="menu_and_attachment"
                                                   value="{{($package->id === null)?old('menu_and_attachment'):$package->menu_and_attachment}}"
                                                   placeholder="1" step="1" />
                                        </div>
                                    </div>
                                </div>
                                <!--end::Group-->
                                <!--begin::Group-->
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left" for="categories">
                                      Categories
                                      <span class="text-danger">*</span>
                                    </label>
                                    <div class="col-9">
                                        <div class="input-group input-group-lg input-group-solid">
                                            <input type="number" class="form-control form-control-lg form-control-solid"
                                                   min="1" max="20" name="categories"
                                                   value="{{($package->id === null)?old('categories'):$package->categories}}"
                                                   placeholder="1" step="1" />
                                        </div>
                                    </div>
                                </div>
                                <!--end::Group-->
                                <!--begin::Group-->
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left">
                                      Images Count
                                      <span class="text-danger">*</span>
                                    </label>
                                    <div class="col-9">
                                        <div class="input-group input-group-lg input-group-solid">
                                            {{-- <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="la la-at"></i>
                                                </span>
                                            </div> --}}
                                            <input type="number" class="form-control form-control-lg form-control-solid"
                                                   min="0" max="20" name="images"
                                                   value="{{($package->id === null)?old('images'):$package->images}}"
                                                   placeholder="1" step="any" required />
                                        </div>
                                    </div>
                                </div>
                                <!--end::Group-->
                                <!--begin::Group-->
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left">
                                      Videos Count
                                      <span class="text-danger">*</span>
                                    </label>
                                    <div class="col-9">
                                        <div class="input-group input-group-lg input-group-solid">
                                            <input type="number" class="form-control form-control-lg form-control-solid"
                                                   min="0" max="20" name="video"
                                                   value="{{($package->id === null)?old('video'):$package->video}}"
                                                   placeholder="1" step="any" required />
                                        </div>
                                    </div>
                                </div>
                                <!--end::Group-->
                                <!--begin::Group-->
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left" for="coupons">
                                      Coupons Count
                                      <span class="text-danger">*</span>
                                    </label>
                                    <div class="col-9">
                                        <div class="input-group input-group-lg input-group-solid">
                                            <input type="number" class="form-control form-control-lg form-control-solid"
                                                   min="1" max="20" name="coupons"
                                                   value="{{($package->id === null)?old('coupons'):$package->coupons}}"
                                                   placeholder="1" step="any" required/>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Group-->
                                <!--begin::Group-->
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left" for="allow_featured">Allow
                                        Featured</label>
                                    <div class="col-3">
                                        <div class="input-group">
                                            <input class="admin-pack-check" type="checkbox" id="allow_featured"
                                                   class="form-control"
                                                    {{($package->featured !== null)?'checked':''}}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <!--end::Group-->
                                <!--begin::Group-->
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left" for="allow_priority_listing">Allow
                                        Priority listing</label>
                                    <div class="col-3">
                                        <div class="input-group">
                                            <input class="admin-pack-check" type="checkbox" id="allow_priority_listing"
                                                   class="form-control"
                                                    {{($package->priority_listing !== null)?'checked':''}}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <!--end::Group-->
                                <!--begin::Group-->
                                <div class="form-group row" id="featured_div">
                                    <label class="col-form-label col-3 text-lg-right text-left" for="featured">Featured
                                        Price</label>
                                    <div class="col-9">
                                        <div class="input-group input-group-lg input-group-solid">
                                            <input type="number" class="form-control form-control-lg form-control-solid"
                                                   min="1" max="9999" name="featured"
                                                   id="featured"
                                                   value="{{($package->id === null)?old('featured'):$package->featured}}"
                                                   placeholder="1" step=".01" pattern="^[0-9]+\.[0-9][0-9]$" />
                                        </div>
                                    </div>
                                </div>
                                <!--end::Group-->
                                <!--begin::Group-->
                                <div class="form-group row" id="priority_listing_div">
                                    <label class="col-form-label col-3 text-lg-right text-left" for="priority_listing">Priority
                                        Listing</label>
                                    <div class="col-9">
                                        <div class="input-group input-group-lg input-group-solid">
                                            <input type="number" class="form-control form-control-lg form-control-solid"
                                                   min="1" max="20" name="priority_listing"
                                                   id="priority_listing"
                                                   value="{{($package->id === null)?old('priority_listing'):$package->priority_listing}}"
                                                   placeholder="1" step="any"/>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Group-->
                                <!--begin::Group-->
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left" for="events">
                                      Events per year
                                      <span class="text-danger">*</span>
                                    </label>
                                    <div class="col-9">
                                        <div class="input-group input-group-lg input-group-solid">
                                            <input type="number" class="form-control form-control-lg form-control-solid"
                                                   min="0" max="20" name="events"
                                                   value="{{($package->id === null)?old('events'):$package->events}}"
                                                   placeholder="1" step="any" required/>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Group-->
                                <!--begin::Group-->
                                <div class="form-group row">
                                    <label class="col-form-label col-3 text-lg-right text-left">
                                      Description
                                      <span class="text-danger">*</span>
                                    </label>
                                    <div class="col-9">
                                        <div class="input-group input-group-lg input-group-solid">
                                            {{-- <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="la la-phone"></i>
                                                </span>
                                            </div> --}}
                                            <textarea name="description" id="" class="form-control" cols="30" rows="10"
                                                      required>{{($package->id === null)?old('description'):$package->description}}</textarea>
                                        </div>
                                        {{-- <span class="form-text text-muted">We'll never share your email with anyone else.</span> --}}
                                    </div>
                                </div>
                                <!--end::Group-->
                            </div>
                        </div>
                        <!--end::Row-->
                        <div class="separator separator-dashed my-10"></div>
                        <!--begin::Footer-->
                        <div class="card-footer pb-0">
                            <div class="row">
                                <div class="col-xl-2"></div>
                                <div class="col-xl-7">
                                    <div class="row">
                                        <div class="col-3"></div>
                                        <div class="col-9">
                                            <input type="submit" class="btn btn-light-primary font-weight-bold"
                                                   value="Save changes">
                                            <a href="{{route('packages')}}" class="btn btn-clean font-weight-bold">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Footer-->
                    </div>
                    <!--end::Tab-->
                </div>
            </form>
        </div>
        <!--begin::Card body-->
    </div>
    <!--end::Card-->

    <script src="{{ asset('assets/js/jquery-2.1.3.min.js') }}"></script>
    <style>
        .admin-pack .input-group{
            display: flex;
            align-items: center;
            /*justify-content: center;*/
            width: 100%;
            height: 100%;
        }

        .admin-pack .admin-pack-check{
            display: flex;
            align-items: center;
            justify-content: center;
            width: 20px;
            height: 20px;
        }
    </style>
    <script>
        var featuredPackgeVal = "{{ $package->featured }}"
        var priorityListingPackgeVal = "{{ $package->priority_listing }}"
        var currentRouteName = "{{ Route::currentRouteName() }}"

        if (featuredPackgeVal == null || currentRouteName == 'packages.create') {
            $('#featured_div').hide();
        }

        if (priorityListingPackgeVal == null || currentRouteName == 'packages.create') {
            $('#priority_listing_div').hide();
        }

        $('#allow_featured').change(function () {
            // featured_div
            var checkBoxVal = $('#allow_featured').prop('checked');

            if (checkBoxVal) {
                $('#featured_div').show();
                $('#featured').prop('required', true);
            } else {
                $('#featured_div').hide();
                $('#featured').prop('required', false);
            }
        })

        $('#allow_priority_listing').change(function () {
            // featured_div
            var checkBoxVal = $('#allow_priority_listing').prop('checked');

            if (checkBoxVal) {
                $('#priority_listing_div').show();
                $('#priority_listing').prop('required', true);
            } else {
                $('#priority_listing_div').hide();
                $('#priority_listing').prop('required', false);
            }
        })

        var $packageForm = $('#package_form');

        $packageForm.on('submit', function(e) {
           e.preventDefault();

           $.ajax({
               url: $packageForm.attr('action'),
               type: 'POST',
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
               data: $packageForm.serialize(),
               success: function(response) {
                   toastr.success(response.message);

                   setTimeout(function () {
                       window.location.href = response.redirectUrl;
                   }, 2000);
               },
               error: function (xhr, status, error) {
                   var jsonResponse = JSON.parse(xhr.responseText);
                   var errorStatusCode = xhr.status;

                   if(errorStatusCode === 422) {
                       for(const property in jsonResponse.errors) {
                           toastr.error(jsonResponse.errors[property]);
                       }
                   } else {
                       toastr.error(jsonResponse.message);

                       setTimeout(function () {
                           window.location.href = jsonResponse.redirectUrl;
                       }, 2000);
                   }


               }

           })
        });
    </script>

@endsection
