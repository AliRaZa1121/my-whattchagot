@php
    use App\Functions\Helper;
@endphp
@extends('layout.default')
@section('content')

    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Packages
                    <div class="text-muted pt-2 font-size-sm">All Packages List</div>
                </h3>
            </div>
            <div class="card-toolbar">

                <!--begin::Button-->
                <a href="{{route('packages.create')}}" class="btn btn-primary font-weight-bolder">
                    <span class="svg-icon svg-icon-md">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <circle fill="#000000" cx="9" cy="15" r="6"/>
                                <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"
                                      fill="#000000" opacity="0.3"/>
                            </g>
                        </svg>
                        <!--end::Svg Icon-->
                    </span>New Record</a>
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="" id="myTable" style="width: 100%; display: block; overflow-x: auto; white-space: nowrap" border="0">
                <thead>
                <tr>
                    <th title="Field #1">#</th>
                    <th title="Field #2">Package</th>
                    <th title="Field #3">Monthly Price($)</th>
                    <th title="Field #3">Yearly Price($)</th>
                    <th title="Field #4">Description</th>
                    <th title="Field #4">Address, Phone & Fax</th>
                    <th title="Field #4">Maps & Directions</th>
                    <th title="Field #4">Hours of Operation</th>
                    <th title="Field #4">Area Served</th>
                    <th title="Field #4">Contact us Form</th>
                    <th title="Field #4">Web Link</th>
                    <th title="Field #4">About us</th>
                    <th title="Field #4">Social Links</th>
                    <th title="Field #4">Manage Reviews</th>
                    <th title="Field #4">Homepage Exposure</th>
                    <th title="Field #4">Categories</th>
                    <th title="Field #4">Images</th>
                    <th title="Field #4">Featured($)</th>
                    <th title="Field #4">Menu & Attachment</th>
                    <th title="Field #4">Video</th>
                    <th title="Field #4">Coupons</th>
                    <th title="Field #4">Events</th>
                    <th title="Field #4">Listing Priority </th>
                    <th title="Field #6">Created At</th>
                    <th title="Field #7">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($packages as $key => $package)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$package->name}}</td>
                        <td>{{$package->monthly_price}}</td>
                        <td>{{$package->yearly_price}}</td>
                        <td>{{$package->description}}</td>
                        <td align="left">
                            <span style="width: 157px;">
                                @if($package->address_phone_fax == 1)
                                  <i class="flaticon2-check-mark text-success"></i>
                                @elseif($package->address_phone_fax == 0)
                                  <i class="flaticon2-cancel text-danger"></i>
                                @endif
                            </span>
                        </td>
                        <td align="left">
                            <span style="width: 157px;">
                              @if($package->maps_and_directions == 1)
                                <i class="flaticon2-check-mark text-success"></i>
                              @elseif($package->maps_and_directions == 0)
                                <i class="flaticon2-cancel text-danger"></i>
                              @endif
                            </span>
                        </td>
                        <td align="left">
                            <span style="width: 157px;">
                              @if($package->hours_of_operation == 1)
                                <i class="flaticon2-check-mark text-success"></i>
                              @elseif($package->hours_of_operation == 0)
                                <i class="flaticon2-cancel text-danger"></i>
                              @endif
                            </span>
                        </td>
                        <td align="left">
                            <span style="width: 157px;">
                              @if($package->area_served == 1)
                                <i class="flaticon2-check-mark text-success"></i>
                              @elseif($package->area_served == 0)
                                <i class="flaticon2-cancel text-danger"></i>
                              @endif
                            </span>
                        </td>
                        <td align="left">
                            <span style="width: 157px;">
                              @if($package->contact_us_form == 1)
                                <i class="flaticon2-check-mark text-success"></i>
                              @elseif($package->contact_us_form == 0)
                                <i class="flaticon2-cancel text-danger"></i>
                              @endif
                            </span>
                        </td>
                        <td align="left">
                            <span style="width: 157px;">
                              @if($package->website_link == 1)
                                <i class="flaticon2-check-mark text-success"></i>
                              @elseif($package->website_link == 0)
                                <i class="flaticon2-cancel text-danger"></i>
                              @endif
                            </span>
                        </td>
                        <td align="left">
                            <span style="width: 157px;">
                                @if($package->about_us_desc == 1)
                                <i class="flaticon2-check-mark text-success"></i>
                                @elseif($package->about_us_desc == 0)
                                <i class="flaticon2-cancel text-danger"></i>
                                @endif
                            </span>
                        </td>
                        <td align="left">
                            <span style="width: 157px;">
                                @if($package->social_media_link == 1)
                                <i class="flaticon2-check-mark text-success"></i>
                                @elseif($package->social_media_link == 0)
                                <i class="flaticon2-cancel text-danger"></i>
                                @endif
                            </span>
                        </td>
                        <td align="left">
                              <span style="width: 157px;">
                                 @if($package->manage_reviews == 1)
                                  <i class="flaticon2-check-mark text-success"></i>
                                @elseif($package->manage_reviews == 0)
                                  <i class="flaticon2-cancel text-danger"></i>
                                @endif
                              </span>
                        </td>
                        <td align="left">
                            <span style="width: 157px;">
                              @if($package->homepage_exposure == 1)
                                <i class="flaticon2-check-mark text-success"></i>
                              @elseif($package->homepage_exposure == 0)
                                <i class="flaticon2-cancel text-danger"></i>
                              @endif
                            </span>
                        </td>
                        <td align="left">
                              <span style="width: 157px;">
                                <span class="label label-lg font-weight-bold  label-light-info label-inline">
                                    {{$package->categories}}
                                </span>
                              </span>
                        </td>
                        <td align="left">
                                <span style="width: 157px;">
                                    <span class="label label-lg font-weight-bold  label-light-info label-inline">
                                        {{$package->images}}
                                    </span>
                                </span>
                        </td>
                        <td align="left">
                            @php
                                if($package->featured > 0.00) {
                                    $status = 'label label-lg font-weight-bold label-light-warning label-inline';
                                    $text = $package->featured;
                                }
                                elseif($package->featured === 0.00) {
                                    $status = 'label label-lg font-weight-bold label-light-success label-inline';
                                    $text = 'Free';
                                }
                                elseif($package->featured === null) {
                                    $status = null;
                                    $text = '<i class="flaticon2-cancel text-danger"></i>';
                                }
                            @endphp
                            <span style="width: 157px;">
                                <span class="{{$status ?? ''}}">
                                    {!! $text !!}
                                </span>
                            </span>
                        </td>
                        <td align="left">
                            <span style="width: 157px;">
                              <span class="label label-lg font-weight-bold  label-light-info label-inline">
                                  {{$package->menu_and_attachment}}
                              </span>
                            </span>
                        </td>
                        <td align="left">
                                <span style="width: 157px;">
                                    <span class="label label-lg font-weight-bold  label-light-info label-inline">
                                        {{$package->video}}
                                    </span>
                                </span>
                        </td>
                        <td align="left">
                                <span style="width: 157px;">
                                    <span class="label label-lg font-weight-bold  label-light-info label-inline">
                                        {{$package->coupons}}
                                    </span>
                                </span>
                        </td>
                        <td align="left">
                            @php
                                if($package->events == 0) {
                                    $status = null;
                                    $text = '<i class="flaticon2-cancel text-danger"></i>';
                                }
                                else {
                                    $status = 'label label-lg font-weight-bold label-light-info label-inline';
                                    $text = $package->events;
                                }
                            @endphp
                            <span style="width: 157px;">
                                    <span class="{{ $status ?? '' }}">
                                        {!! $text !!}
                                    </span>
                                </span>
                        </td>
                        <td align="left">
                            @php
                                if($package->priority_listing == 0) {
                                    $status = null;
                                    $text = '<i class="flaticon2-cancel text-danger"></i>';
                                }
                                else {
                                    $status = 'label label-lg font-weight-bold label-light-info label-inline';
                                    $text = $package->priority_listing;
                                }
                            @endphp
                            <span style="width: 157px;">
                                    <span class="{{$status ?? ''}}">
                                        {!! $text !!}
                                    </span>
                                </span>
                        </td>
                        <td>{{ ($package->created_at != ''|| $package->created_at != null)?$package->created_at->format('Y-m-d'): '' }}</td>

                        <td>
                            {{-- <a href="javascript:;" data-href="{{route('packages.detail', $package->id)}}" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon">
                                <i class="flaticon-eye"></i>
                            </a> --}}
                            <a href="{{route('packages.edit', $package->id)}}"
                               class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon">
                                <i class="flaticon-edit"></i>
                            </a>
                            <a href="javascript:;" data-url="{{route('packages.delete', $package->id)}}"
                               class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon delete">
                                <i class="flaticon2-rubbish-bin-delete-button"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>

    {{-- @include('admin.packages.modal') --}}

@endsection

