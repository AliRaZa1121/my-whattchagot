@extends('layouts.app2')

@section('content')
    <style>
        td.product-remove a:after {
            content: '\f00d';
            font-family: 'Font Awesome 5 Free' !important;
            font-weight: bold;
            color: #2393d1;
        }
        td.product-remove a:hover:after {
            content: '\f00d';
            font-family: 'Font Awesome 5 Free' !important;
            font-weight: bold;
            color: #3e4296;
        }
        .cart-item img {
            height: 100px;
            width: 100px;
            object-fit: cover;
            object-position: center;
        }
        .product-add-cart a.add-to-cart.btn.btn-primary.btn-square.btn-lg.icon-left-effect {
            border-radius: 5px !important;
            background-color: #2393d1;
            color: #fff;
            font-size: 14px;
            font-weight: 600;
            padding: 10px 30px !important;
            display: inline-block;
            margin-top: 0;
            width: max-content;
            width: fit-content;
            outline: none !important;
            padding: 6px 22px !important;
        }
        .product-add-cart a.add-to-cart.btn.btn-primary.btn-square.btn-lg.icon-left-effect:hover{
            background-color: #3e4296;
            color: #fff;
        }
        .product-name a {
            color: #3e4296 !important;
            text-decoration: none !important;
        }
        .product-name a:hover{
            color: #2393d1 !important;
            text-decoration: none !important;
        }
        table.table.table-cart tr.cart-item td {
            display: table-cell;
            vertical-align: middle;
        }
        .section.wishlist {
            margin: 50px 0 0 0;
        }
    </style>
    <!-- Full With banner -->
    @include('layout.nav_header.nav_slider_bar.nav_slider_bar')
    <!-- Full With banner -->
    <div class="full-width-banner about-us-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Wishlist</h2>
                </div>

            </div>
        </div>

    </div>
    <!-- WishList Section Start here-->
    <div class="section wishlist">
        <div class="container p-b-60">
            <div class="row">
                <div class="col-sm-12">
                    @if($errors->any())
                        <div class="alert alert-success">{{$errors->first()}}</div>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-cart">
                            <tbody>

                            @foreach($product as $key)

                            <tr class="cart-item">
                                <td class="product-remove">
                                    <a href="{{route('wishlish_delete',$key->wishlistID)}}" data-toggle="tooltip" data-placement="top" data-html="true" title="" data-original-title="<p>Remove</p>">
                                        <i class="nc-icon-outline ui-1_circle-remove"></i>
                                    </a>
                                </td>
                                <td class="product-thumbnail">
                                    <a href="shop-single">
                                        <img src="{{isset($key->product_images->image) ? asset('/storage/'.$key->product_images->image): asset('/storage/default.jpg')}}" width="100px">
                                    </a>
                                </td>
                                <td class="product-name">
                                   {{$key->name}}
                                </td>
                                <td class="product-price">$<span>{{$key->price}}</span></td>
                                @if($key->flags != 'selling')
                                <td>
                                    @php
                                    switch ($key->flags) {
                                        case('trade'):
                                         $route = route('trading-single', $key->id);
                                        break;
                                          case("auction"):
                                           $route = route('auction-single', $key->id);
                                            break;
                                                    default:
                                                        $route = '#';
                                                        break;
                                    }
                                @endphp
                                <a href="{{ $route }}" class="add-to-cart btn btn-primary btn-square btn-lg icon-left-effect"><i class="nc-icon-outline shopping_cart"></i><span>
                                    View</span></a>
                                </td>
                                @else
                                <td class="product-add-cart">
                                    <form class="ajax_add_cart">
                                        @csrf
                                        <input type="hidden"
                                               name="product_id"
                                               class="product_id"
                                               value="{{base64_encode($key->id)}}">
                                        <input type="hidden" name="price" class="price"
                                               value="{{base64_encode($key->price)}}">

                                        <button type="submit">Add To Cart</button>
                                    </form>
                                </td>
                                @endif
                            </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Wishlist Section End here-->
@stop




