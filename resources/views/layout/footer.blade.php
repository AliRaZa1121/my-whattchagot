<div class="footer">
    <div class="container">
        <div class="row">
            <div class="footer1 same_footer_box">
                <h3>BUY</h3>
                <ul>
                    <li><a href="{{url('buyer_login')}}">Registration</a></li>
                    <li><a href="{{ url('money-back-guarantee') }}">Money Back Guarantee</a></li>
                    @if(auth()->guard('seller')->check())
                    <li><a href="{{ route('saller-packages-list') }}">Advertise With Us</a></li>
                    @endif

                    {{-- <li><a href="{{ url('whattchagot-local') }}">Whattchagot Local</a></li> --}}
                </ul>
            </div>
            <div class="footer2 same_footer_box">
                <h3>SELL</h3>
                <ul>

                    <li><a href="{{url('login')}}">Start Selling</a></li>
                    <li><a href="{{ url('learn-to-sell') }}">Learn To Sell</a></li>
                    <li><a href="{{ url('/auction') }}">Auction</a></li>
                    <li><a href="{{ url('business-sellers') }}">Business Sellers</a></li>
                    <li><a href="{{ url('affiliates') }}">Affiliates</a></li>
                    {{-- <li><a href="{{ url('sitemap') }}">Site Map</a></li> --}}
                </ul>
            </div>
            <div class="footer2 same_footer_box">
                <h3>TRADING</h3>
                <ul>

                    <li><a href="{{url('products/?type=list')}}">Top Items</a></li>
                    <li><a href="{{ url('top-trades') }}">Top Trades</a></li>
                    <li><a href="{{ url('top-traders') }}">Top Traders</a></li>
                    <li><a href="{{ url('learn-to-sell') }}">Learn to trade</a></li>
                </ul>
            </div>
            <div class="footer3 same_footer_box">
                <h3>ABOUT</h3>
                <ul>
                    <li><a href="{{url('about-us')}}">Company Info</a></li>
                    <li><a href="{{url('how-to-buy')}}">How To Buy</a></li>
                    <li><a href="{{ url('faqs') }}">FAQs</a></li>
                    {{-- <li><a href="{{ url('careers') }}">Careers</a></li>
                      <li><a href="{{ url('stores') }}">Stores</a></li> --}}

                </ul>
            </div>
            <div class="footer4 same_footer_box">
                <h3>HELP & CONTACT</h3>
                <ul>
                    <li><a href="{{ url('help-center') }}">Help Center</a></li>
                    <li><a href="{{ url('bidding-buying-help') }}">Bidding & Buying Help</a></li>
                    <li><a href="{{url('auction')}}">Auction Help</a></li>
                    <li><a href="{{ url('seller-information-center') }}">Seller Information Center</a></li>
                    <li><a href="{{ route('privacy-policy') }}">Privacy Policy</a></li>
                    <li><a href="{{ url('terms-and-condition') }}">Terms & Conditions</a></li>
                    <li><a href="{{ url('contact-us') }}">Contact Us</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<style>
    .goog-te-banner-frame.skiptranslate {display: none !important;}
    body { top: 0px !important; }
</style>


<script>

    function googleTranslateElementInit() {
        new google.translate.TranslateElement({pageLanguage: 'ko', layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT}, 'google_translate_element');
    }
    $(document).ready(function (){

        // jQuery('.lang-select').click(function() {

        //     var theLang = jQuery(this).attr('data-lang');


        //     jQuery('.goog-te-combo').val(theLang);
        //     window.location = jQuery(this).attr('href');
        //     localStorage.setItem('active-container', theLang);
        //     localStorage.myLag =theLang;
        //     // var cyx = localStorage.getItem("myLag");
        //     // alert(cyx);
        //     window.location.reload();

        // });

        $('.goog-te-banner').hide()
    })
    $(document).ready(function () {
        setTimeout(() => {
            var cyx = localStorage.getItem("myLag");
            $('.lang-'+cyx).addClass('Active-lang');
            // alert(cyx);
            var activelang = jQuery(".Active-lang").text();
            jQuery(".dropdown-toggle").text(activelang);

        },1000)


    });


    $(window).load(function(){
        $(".goog-logo-link").empty();
        $('.goog-te-gadget').html($('.goog-te-gadget').children());
    })









</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>


