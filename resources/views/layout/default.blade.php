<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" {{ Metronic::printAttrs('html') }} {{ Metronic::printClasses('html') }}>

<head>
    <meta charset="utf-8" />

    {{-- Title Section --}}
{{--    <title>{{ config('app.name') }} | @yield('title', $page_title ?? '')</title>--}}
    <title style="text-transform: capitalize">{{ !empty(ucwords(str_replace('-',' ',request()->segment(1)))) ? ucwords(str_replace('-',' ',request()->segment(1))) : 'Home' }} | {{ config('app.name') }}</title>
    {{-- Meta Data --}}
    <meta name="description" content="@yield('page_description', $page_description ?? '')" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- Favicon --}}
    <link rel="shortcut icon" href="{{ asset('media\logos\Fav icon 32 x32-01.png') }}" />

    {{-- Fonts --}}
    {{ Metronic::getGoogleFontsInclude() }}

    {{-- Global Theme Styles (used by all pages) --}}
    @foreach(config('layout.resources.css') as $style)
        <link href="{{ config('layout.self.rtl') ? asset(Metronic::rtlCssPath($style)) : asset($style) }}" rel="stylesheet" type="text/css" />
    @endforeach

    {{-- Layout Themes (used by all pages) --}}
    @foreach (Metronic::initThemes() as $theme)
        <link href="{{ config('layout.self.rtl') ? asset(Metronic::rtlCssPath($theme)) : asset($theme) }}" rel="stylesheet" type="text/css" />
    @endforeach
    <link rel="stylesheet" href="{{asset('sweetalert2/dist/sweetalert2.min.css')}}">
    {{-- Includable CSS --}}
    @yield('styles')
</head>

<body {{ Metronic::printAttrs('body') }} {{ Metronic::printClasses('body') }}>

@if (config('layout.page-loader.type') != '')
    @include('layout.partials._page-loader')
@endif

@include('layout.base._layout')


{{-- Global Config (global config for global JS scripts) --}}
<script>
    var KTAppSettings = {
    !!json_encode(config('layout.js'), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES) !!
    };
</script>

{{-- Global Theme JS Bundle (used by all pages)  --}}
@foreach(config('layout.resources.js') as $script)
    <script src="{{ asset($script) }}" type="text/javascript"></script>
@endforeach

<script>
    $('body').on('focus', '.phone-mask', function() {
        if ($(this).val().length == 0) {
            $(this).attr("placeholder", "(XXX) XXX-XXXX");
        }
    });

    $('body').on('input', '.phone-mask', function(e) {
        var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
        e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] :
            '');
    });

    function submitFormAjax(method, url, data, successCallback = null, errorCallback = null, bv = null) {
        showLoading();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        let requestOptions = {
            url: url,
            method: method,
            data: data
        };
        if (method.toLowerCase() != 'put') {
            requestOptions['contentType'] = false;
            requestOptions['processData'] = false;
        }
        var request = jQuery.ajax(requestOptions);
        request.done(function(res) {
            hideLoading();
            if (method != 'put' && $('[name=_method]').val() != 'put') {
                bv.resetForm(true);
            }
            if (successCallback) {
                successCallback(res);
            } else {
                successAlert(res.msg);
            }
        });

        request.fail(function(jqXHR, textStatus) {
            hideLoading();
            if (jqXHR.status == 422 || jqXHR.status == 423) {
                errorCallback(jqXHR);
                console.log(jqXHR)
            } else if (jqXHR.status == 500) {
                errorAlert('something went wrong on the server.Please contact Support...');
                console.log(jqXHR);
            } else {
                console.log(jqXHR)
                errorAlert("Request failed: " + textStatus);
            }

        });
    }

    function submitAjax(method, url, data = {}, successCallback = null, errorCallback = null) {
        showLoading();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        let requestOptions = {
            url: url,
            method: method,
            data: data
        };
        if (method.toLowerCase() != 'put') {
            requestOptions['contentType'] = false;
            requestOptions['processData'] = false;
        }
        var request = jQuery.ajax(requestOptions);
        request.done(function(res) {
            hideLoading();
            if (successCallback) {
                successCallback(res);
            } else {
                successAlert(res.msg);
            }
        });

        request.fail(function(jqXHR, textStatus) {
            hideLoading();
            if (jqXHR.status == 422 || jqXHR.status == 423) {
                errorCallback(jqXHR);
                console.log(jqXHR)
            } else if (jqXHR.status == 500) {
                errorAlert('something went wrong on the server.Please contact Support...');
                console.log(jqXHR);
            } else {
                console.log(jqXHR)
                errorAlert("Request failed: " + textStatus);
            }

        });
    }

    function successAlert(msg) {
        Swal.fire({
            icon: 'success',
            title: 'success',
            html: msg,
        })
    }

    function errorAlert(msg) {
        Swal.fire({
            icon: 'error',
            title: 'error',
            html: msg,
        });
    }

    function showLoading() {
        Swal.fire({
            title: "processing...",
            text: "Please wait",
            imageUrl: "{{asset('media/loader.gif')}}",
            showConfirmButton: false,
            allowOutsideClick: false
        })
    }

    function hideLoading() {
        Swal.close()
    }

    function deleteTableRecord(url, tr = null) {
        if (confirm("Are you sure deleting this information")) {
            showLoading();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let requestOptions = {
                url: url,
                method: 'delete',
            };
            var request = jQuery.ajax(requestOptions);
            request.done(function(res) {
                if (tr) {
                    tr.remove();
                } else {
                    fillTableData();
                }

                successAlert(res.msg);

            });

            request.fail(function(jqXHR, textStatus) {
                hideLoading();
                errorAlert("Request failed: " + textStatus);
            });
        }
    }
    let url = "{{url()->current()}}";
    $('.menu-link').each((i, v) => {
        if ($(v).attr('href') == url) {
            $(v).parent('li').addClass('menu-item-active');
            $(v).parent('li').parent('.menu-subnav').parent('.menu-submenu').parent('.menu-item-submenu').addClass('menu-item-open menu-item-here')
        }
    })
</script>

<script>
    $(document).ready(function (){
        var down = $('.subheader  .breadcrumb  .breadcrumb-item a').last().html();


        var res = down.charAt(0).toUpperCase() + down.slice(1);
        // var res =  res.charAt(0).toUpperCase() + res.toLocaleLowerCase().substring(1);
        const words = res.split(" ");
        var html = "";

        for(var i=0;i< words.length;i++){
            html += catalize(words[i]);
        }
        function catalize(string){
            return ' '+string.charAt(0).toUpperCase() + string.slice(1);
        }


        document.title =html + ' | Whattchagot';

    });


</script>

{{-- Includable JS --}}
@yield('scripts')

</body>

</html>
