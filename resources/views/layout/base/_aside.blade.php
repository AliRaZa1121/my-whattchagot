<?php
if (auth()->user()->user_type === "admin") {

    $menu = [

        'items' => [
            [
                'title' => 'Dashboard',
                'root' => true,
                'icon' => 'flaticon-layer', // or can be 'flaticon-home' or any flaticon-*
                'page' => route('dashboard'),
                'new-tab' => false,
            ],
            [
                'title' => 'Users',
                'root' => true,
                'icon' => 'flaticon-users', // or can be 'flaticon-home' or any flaticon-*
                'page' => route('users'),
                'new-tab' => false,
            ],
            [
                'title' => 'E-Commerce',
                'root' => true,
                'icon' => 'fas fa-store', // or can be 'flaticon-home' or any flaticon-*

                'new-tab' => false,
                'submenu' => [
                    [
                        'title' => 'Products',
                        'bullet' => 'dot',
                        'icon' => 'fa fa-dot-circle-o', // or can be 'flaticon-home' or any flaticon-*
                        'page' => route('products.index'),

                    ],
                    [
                        'title' => 'Orders',
                        'bullet' => 'dot',
                        'icon' => 'fa fa-dot-circle-o', // or can be 'flaticon-home' or any flaticon-*
                        'page' => route('orders.index'),

                    ],
                    [
                        'title' => 'Make A Trade',
                        'bullet' => 'dot',
                        'icon' => 'fa fa-dot-circle-o', // or can be 'flaticon-home' or any flaticon-*
                        'page' => route('seller_trading.index'),

                    ],
                    [
                        'title' => 'Dispute',
                        'bullet' => 'dot',
                        'icon' => 'fa fa-dot-circle-o', // or can be 'flaticon-home' or any flaticon-*
                        'page' => route('seller_trading.index'),

                    ],
                    [
                        'title' => 'Product Bidding',
                        'bullet' => 'dot',
                        'icon' => 'fa fa-dot-circle-o', // or can be 'flaticon-home' or any flaticon-*
                        'page' => route('bid.list.seller'),

                    ],
                   
                    [
                        'title' => 'Advertise Banners',
                        'bullet' => 'dot',
                        'icon' => 'fa fa-dot-circle-o', // or can be 'flaticon-home' or any flaticon-*
                        'page' => route('advertise.banner.index'),

                    ],


                ]

            ],
            [
                'title' => 'Category',
                'root' => true,
                'icon' => 'fas fa-list-alt', // or can be 'flaticon-home' or any flaticon-*
                'page' => route('category.index'),
                'new-tab' => false,

            ],
            [
                'title' => 'Sub Category',
                'root' => true,
                'icon' => 'fas fa-list-alt', // or can be 'flaticon-home' or any flaticon-*
                'page' => route('sub-category.index'),
                'new-tab' => false,
            ],
            [
                'title' => 'Brand',
                'root' => true,
                'icon' => 'fab fa-bandcamp', // or can be 'flaticon-home' or any flaticon-*
                'page' => route('brand.index'),
                'new-tab' => false,
            ],
            [
                'title' => 'Packages',
                'root' => true,
                'icon' => 'fas fa-store', // or can be 'flaticon-home' or any flaticon-*

                'new-tab' => false,
                'submenu' => [
                    [
                        'title' => 'User Package',
                        'bullet' => 'dot',
                        'icon' => 'fa fa-dot-circle-o', // or can be 'flaticon-home' or any flaticon-*
                        'page' => route('seller.packages'),

                    ],
                    [
                        'title' => 'Banner Package',
                        'bullet' => 'dot',
                        'icon' => 'fa fa-dot-circle-o', // or can be 'flaticon-home' or any flaticon-*
                        'page' => route('ad-packages'),

                    ],
                ]

            ],
            [
                'title' => 'Inbox',
                'root' => true,
                'icon' => 'fas fa-sms', // or can be 'flaticon-home' or any flaticon-*
                'page' => route('chat'),
                'new-tab' => false,
            ],
            [
                'title' => 'Newsletter',
                'root' => true,
                'icon' => 'fas fa-inbox', // or can be 'flaticon-home' or any flaticon-*
                'page' => route('newsletters.index'),
                'new-tab' => false,
            ],

            [
                'title' => 'Whattchagot Warehouse',
                'root' => true,
                'icon' => 'fas fa-home', // or can be 'flaticon-home' or any flaticon-*
                'page' => route('warehouse.index'),
                'new-tab' => false,
            ],

               [
                'title' => 'Seller Warehouse',
                'root' => true,
                'icon' => 'fas fa-home', // or can be 'flaticon-home' or any flaticon-*
                'page' => route('warehouse.index.seller'),
                'new-tab' => false,
            ],

            [
                'title' => 'Website Banners',
                'root' => true,
                'icon' => 'fas fa-images', // or can be 'flaticon-home' or any flaticon-*
                'page' => route('website-banners.index'),
                'new-tab' => false,
            ],
            [
            'title' => 'Contacts Listing',
            'root' => true,
            'icon' => 'fas fa-phone', // or can be 'flaticon-home' or any flaticon-*
            'page' => route('getcontactus'),
            ],

            [
                'title' => 'Testimonials',
                'root' => true,
                'icon' => 'flaticon2-graph-1', // or can be 'flaticon-home' or any flaticon-*
                'page' => route('testimonials.index'),
                'new-tab' => false,
            ],
            [
                'title' => 'Setting',
                'root' => true,
                'icon' => 'flaticon-settings', // or can be 'flaticon-home' or any flaticon-*
                'page' => route('settings'),
                'new-tab' => false,
            ],

        ]

    ];
}

else if (auth()->user()->user_type === "user") {
    $menu = [

        'items' => [
            [
                'title' => 'Dashboard',
                'root' => true,
                'icon' => 'flaticon-layer', // or can be 'flaticon-home' or any flaticon-*
                'page' => route('dashboard'),
                'new-tab' => false,
            ],
            [
                'title' => 'Start Selling',
                'root' => true,
                'icon' => 'fas fa-store', // or can be 'flaticon-home' or any flaticon-*

                'new-tab' => false,
                'submenu' => [
                    [
                        'title' => 'Products',
                        'bullet' => 'dot',
                        'icon' => 'fa fa-dot-circle-o', // or can be 'flaticon-home' or any flaticon-*
                        'page' => route('products.index'),

                    ],
                    [
                        'title' => 'Orders',
                        'bullet' => 'dot',
                        'icon' => 'fa fa-dot-circle-o', // or can be 'flaticon-home' or any flaticon-*
                        'page' => route('orders.index'),

                    ],
                    [
                        'title' => 'Make A Trade',
                        'bullet' => 'dot',
                        'icon' => 'fa fa-dot-circle-o', // or can be 'flaticon-home' or any flaticon-*
                        'page' => route('seller_trading.index'),

                    ],
                    [
                        'title' => 'Dispute',
                        'bullet' => 'dot',
                        'icon' => 'fa fa-dot-circle-o', // or can be 'flaticon-home' or any flaticon-*
                        'page' => route('seller_trading.index'),

                    ],
                    [
                        'title' => 'Product Bidding',
                        'bullet' => 'dot',
                        'icon' => 'fa fa-dot-circle-o', // or can be 'flaticon-home' or any flaticon-*
                        'page' => route('bid.list.seller'),

                    ],
                    [
                        'title' => 'Advertise Banners',
                        'bullet' => 'dot',
                        'icon' => 'fa fa-dot-circle-o', // or can be 'flaticon-home' or any flaticon-*
                        'page' => route('advertise.banner.index'),

                    ],
                    [
                        'title' => 'Warehouse',
                        'bullet' => 'dot',
                        'icon' => 'fa fa-dot-circle-o', // or can be 'flaticon-home' or any flaticon-*
                        'page' => route('bid.list.seller'),

                    ],

                ]

            ],
            [
                'title' => 'My Orders',
                'root' => true,
                'icon' => 'flaticon-business', // or can be 'flaticon-home' or any flaticon-*

                'new-tab' => false,
                'submenu' => [
                    [
                        'title' => 'Selling',
                        'bullet' => 'dot',
                        'icon' => 'fa fa-dot-circle-o', // or can be 'flaticon-home' or any flaticon-*
                        'page' => route('seller_order.index','seller'),

                    ],
                    [
                        'title' => 'Auctions',
                        'bullet' => 'dot',
                        'icon' => 'fa fa-dot-circle-o', // or can be 'flaticon-home' or any flaticon-*
                        'page' => route('seller_order.index','auction'),

                    ],
                    [
                        'title' => 'Trading',
                        'bullet' => 'dot',
                        'icon' => 'fa fa-dot-circle-o', // or can be 'flaticon-home' or any flaticon-*
                        'page' => route('seller_order.index','trade'),

                    ],
                    [
                        'title' => 'Request Trading',
                        'bullet' => 'dot',
                        'icon' => 'fa fa-dot-circle-o', // or can be 'flaticon-home' or any flaticon-*
                        'page' => route('seller_order.index','request-trade'),

                    ],
                    [
                    'title' => 'Dispute',
                    'bullet' => 'dot',
                    'icon' => 'fa fa-dot-circle-o', // or can be 'flaticon-home' or any flaticon-*
                    'page' => route('dispute.list.seller'),

                    ]
                ]

            ],

            // [
            //     'title' => 'Trade Request',
            //     'root' => true,
            //     'icon' => 'fas fa-exchange-alt', // or can be 'flaticon-home' or any flaticon-*
            //     'page' => route('trading-request.admin'),
            //     'new-tab' => false,
            // ],
            
            [
                'title' => 'Inbox',
                'root' => true,
                'icon' => 'fas fa-sms', // or can be 'flaticon-home' or any flaticon-*
                'page' => route('users.chat'),
                'new-tab' => false,
            ],

            [
                'title' => 'Warehouse',
                'root' => true,
                'icon' => 'fas fa-home', // or can be 'flaticon-home' or any flaticon-*
                'page' => route('seller_warehouse.index'),
                'new-tab' => false,
            ],


        ]

    ];
}

// else if (Auth::guard('buyer')->check()) {
//     $menu = [

//         'items' => [
//             [
//                 'title' => 'Dashboard',
//                 'root' => true,
//                 'icon' => 'flaticon-layer', // or can be 'flaticon-home' or any flaticon-*
//                 'page' => route('buyer_dashboard'),
//                 'new-tab' => false,
//             ],



//             [
//             'title' => 'Orders',
//             'root' => true,
//             'icon' => 'flaticon-business', // or can be 'flaticon-home' or any flaticon-*

//             'new-tab' => false,
//             'submenu' => [
//                 [
//                     'title' => 'Selling',
//                     'bullet' => 'dot',
//                     'icon' => 'fa fa-dot-circle-o', // or can be 'flaticon-home' or any flaticon-*
//                     'page' => route('buyer_order.index','seller'),

//                 ],
//                 [
//                     'title' => 'Auctions',
//                     'bullet' => 'dot',
//                     'icon' => 'fa fa-dot-circle-o', // or can be 'flaticon-home' or any flaticon-*
//                     'page' => route('buyer_order.index','auction'),

//                 ],
//                 [
//                     'title' => 'Trading',
//                     'bullet' => 'dot',
//                     'icon' => 'fa fa-dot-circle-o', // or can be 'flaticon-home' or any flaticon-*
//                     'page' => route('buyer_order.index','trade'),

//                 ],
//                 [
//                         'title' => 'Request Trading',
//                         'bullet' => 'dot',
//                         'icon' => 'fa fa-dot-circle-o', // or can be 'flaticon-home' or any flaticon-*
//                         'page' => route('buyer_order.index','request-trade'),

//                 ]
//             ]

//         ],
//             [
//                 'title' => 'Feedback',
//                 'root' => true,
//                 'icon' => 'fas fa-comment-dots', // or can be 'flaticon-home' or any flaticon-*
//                 'page' => route('buyer_review.index'),
//                 'new-tab' => false,
//             ],
//             [
//                 'title' => 'Trade Request',
//                 'root' => true,
//                 'icon' => 'fas fa-exchange-alt', // or can be 'flaticon-home' or any flaticon-*
//                 'page' => route('request.trading'),
//                 'new-tab' => false,
//             ],
//             [
//                 'title' => 'Inbox',
//                 'root' => true,
//                 'icon' => 'fas fa-sms', // or can be 'flaticon-home' or any flaticon-*
//                 'page' => route('buyer_chat'),
//                 'new-tab' => false,
//             ],
//             [
//                 'title' => 'Product Bidding',
//                 'root' => true,
//                 'icon' => 'fas fa-gavel', // or can be 'flaticon-home' or any flaticon-*
//                 'page' => route('bid.list'),
//                 'new-tab' => false,
//             ],
//             [
//                 'title' => 'Checkout',
//                 'root' => true,
//                 'icon' => 'fas fa-shopping-cart', // or can be 'flaticon-home' or any flaticon-*
//                 'page' => route('checkout.list'),
//                 'new-tab' => false,
//             ],

//         ]

//     ];
// }






?>

<!-- @php
    $kt_logo_image = 'logo.png';
@endphp

@if (config('layout.brand.self.theme') === 'light')
    @php $kt_logo_image = 'logo.png' @endphp
@elseif (config('layout.brand.self.theme') === 'dark')
    @php $kt_logo_image = 'logo.png' @endphp
@endif -->
<style>

 div#kt_brand img {
    height: 100px !important;
    width: 100% !important;
    object-fit: contain !important;
    margin-top: 60px !important;
    z-index: 9999999 !important;
    position: relative !important;
    margin-left: 25px;
}
    div#kt_aside_menu_wrapper {
        margin-top: 40px;
    }


    /* Buy Link */
    a.navi-link {
        cursor: pointer !important;
    }
    .breadcrumb-item{
        text-transform:uppercase;
    }
</style>


<div class="aside aside-left {{ Metronic::printClasses('aside', false) }} d-flex flex-column flex-row-auto"
     id="kt_aside">

    {{-- Brand --}}
    <div class="brand flex-column-auto {{ Metronic::printClasses('brand', false) }}" id="kt_brand">
      <!--   <div class="brand-logo"> -->
            <a href="{{ url('/') }}">
{{--                <h1>Whattchagot</h1>--}}
                 <img alt="{{ config('app.name') }}" src="{{ asset('media/logos/'.$kt_logo_image) }}"/>
            </a>
        <!-- </div> -->

        @if (config('layout.aside.self.minimize.toggle'))
            <button class="brand-toggle btn btn-sm px-0" id="kt_aside_toggle">
                {{ Metronic::getSVG("media/svg/icons/Navigation/Angle-double-left.svg", "svg-icon-xl") }}
            </button>
        @endif

    </div>

    {{-- Aside menu --}}
    <div class="aside-menu-wrapper flex-column-fluid" id="kt_aside_menu_wrapper">

        @if (config('layout.aside.self.display') === false)
            <div class="header-logo">
                <a href="{{ url('/') }}">
                    <img alt="{{ config('app.name') }}" src="{{ asset('media/logos/'.$kt_logo_image) }}"/>
                </a>
            </div>
        @endif

        <div id="kt_aside_menu" class="aside-menu my-4 {{ Metronic::printClasses('aside_menu', false) }}"
             data-menu-vertical="1" {{ Metronic::printAttrs('aside_menu') }}>

            <ul class="menu-nav {{ Metronic::printClasses('aside_menu_nav', false) }}">
                {{ Menu::renderVerMenu($menu['items']) }}
            </ul>
        </div>
    </div>

</div>
