{{-- Footer --}}

<div class="footer bg-white py-4 d-flex flex-lg-column {{ Metronic::printClasses('footer', false) }}" id="kt_footer">
    {{-- Container --}}
    <div
        class="{{ Metronic::printClasses('footer-container', false) }} d-flex flex-column flex-md-row align-items-center justify-content-between">
        {{-- Copyright --}}
        <div class="text-dark order-2 order-md-1">
            <span> {{ date("Y") }} &copy; <span><a href="{{url('/')}}" target="_blank">Whattchagot</a></span> All Rights Reserved.</span>
            {{-- <span> Designed &amp; Developed by Dallas Web Design Company <a href="https://www.invictusstudio.com/" target="_blank">Invictus Studio</a>.</span> --}}
        </div>

        {{-- Nav --}}
        
    </div>
</div>