<header id="masthead" class="site-header">
    <div class="">
        <div class="row-one">
            <ul id="header-social-icons" class="cnss-social-icon " style="text-align:center;">
                <li class="cn-fa-linkedin cn-fa-icon"><a class="cnss-linkedin" target="_blank" href="https://www.linkedin.com/feed/?trk=guest_homepage-basic_nav-header-signin" title="LinkedIn"><i title="LinkedIn" class="fa fa-linkedin"></i></a></li>
                <li class="cn-fa-facebook cn-fa-icon"><a class="cnss-facebook" target="_blank" href="https://www.facebook.com/buswith.usco.7" title="Facebook"><i title="Facebook" class="fa fa-facebook"></i></a></li>
                <li class="cn-fa-pinterest cn-fa-icon"><a class="cnss-pinterest-p" target="_blank" href="https://www.pinterest.com/buswithus2505/" title="Pinterest"><i title="Pinterest" class="fa fa-pinterest-p"></i></a></li>
                <li class="cn-fa-instagram cn-fa-icon"><a class="cnss-instagram" target="_blank" href="https://www.instagram.com/buswithus.co/?igshid=1ckm8x1106lr7" title="Instagram"><i title="Instagram" class="fa fa-instagram"></i></a></li>
            </ul>
        </div>
        <div class="row-two">
            <a href="/" class="custom-logo-link" rel="home">
                <img width="185" height="29" src="{{asset('website/img/Bus_With_Us_Logo.png')}}" class="custom-logo" alt="Bus With Us"></a>
            <!-- .site-branding -->
        </div>
        <div class="row-three">
            <nav class="navbar navbar-expand-md">
                <a class="navbar-brand" href="/"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar1">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbar1">
                    <ul class="navbar-nav mr-auto">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ABOUT US <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="/about-us/">HOW WE WORK</a></li>
                                <li><a href="/our-team/">JOIN OUR TEAM</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">SERVICES <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="/our-services/">GROUP TRANSPORTATION</a></li>
                                <li><a href="/our-fleet/">OUR FLEET</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="/discovers/" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">DISCOVER <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="/discover/new-york-city-ny/">New York City, NY</a></li>
                                <li><a href="/discover/atlanta-ga/">Atlanta, GA</a></li>
                                <li><a href="/discover/washington-dc/">Washington, DC</a></li>
                                <li><a href="/discover/miami-fl/">Miami, FL</a></li>
                                <li><a href="/discover/myrtle-beach-sc/">Myrtle Beach, SC</a></li>
                                <li><a href="/discover/chicago-il/">Chicago, IL</a></li>
                                <li><a href="/discover/san-francisco-ca/">San Francisco, CA</a></li>
                                <li><a href="/discover/los-angeles-ca/">Los Angeles, CA</a></li>
                                <li><a href="/discover/las-vegas-nv/">Las Vegas, NV</a></li>
                                <li><a href="/discover/new-orleans-la/">New Orleans, LA</a></li>
                                <li><a href="/discover/toronto-canada/">Toronto, Canada</a></li>
                                <li><a href="/discover/orlando-fl/">Orlando, FL</a></li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/trip-with-us/">TRIPS WITH US</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/partner-with-us/">PARTNER WITH US</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('book-with-us/')}}">BOOK WITH US</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>