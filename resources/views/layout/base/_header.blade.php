{{-- Header --}}
<div id="kt_header" class="header {{ Metronic::printClasses('header', false) }}" {{ Metronic::printAttrs('header') }}>


    {{-- Container --}}
    <div class="container-fluid d-flex align-items-center justify-content-between">
        @if (config('layout.header.self.display'))

{{--            @php--}}
{{--                $kt_logo_image = 'logo-light.png';--}}
{{--            @endphp--}}

{{--            @if (config('layout.header.self.theme') === 'light')--}}
{{--                @php $kt_logo_image = 'logo-dark.png' @endphp--}}
{{--            @elseif (config('layout.header.self.theme') === 'dark')--}}
{{--                @php $kt_logo_image = 'logo-light.png' @endphp--}}
{{--            @endif--}}

            {{-- Header Menu --}}
            <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
                @if(config('layout.aside.self.display') == false)
                    <div class="header-logo">
                        <a href="{{ url('/') }}">
                            <img alt="Logo" src="{{ asset('media/logos/'.$kt_logo_image) }}"/>
                        </a>
                    </div>
                @endif

                <div id="kt_header_menu" class="header-menu header-menu-mobile {{ Metronic::printClasses('header_menu', false) }}" {{ Metronic::printAttrs('header_menu') }}>
                    <ul class="menu-nav {{ Metronic::printClasses('header_menu_nav', false) }}">

                        @if(auth()->user()->user_type === "Seller")
                               <a href="{{route('switch-user',['buyer',auth()->user()->id])}}"  class="btn btn-light-warning font-weight-bolder btn-sm mr-2">Switch to Buyer</a>
                        @elseif(auth()->user()->user_type === "Buyer")
                               <a href="{{route('switch-user', ['seller',auth()->user()->id])}}"  class="btn btn-light-warning font-weight-bolder btn-sm mr-2">Switch to Seller</a>
                        @endif
                        <a href="/" class="btn btn-light-primary font-weight-bolder btn-sm mr-2">Goto Website</a>

                    </ul>
                </div>
            </div>

        @else
            <div></div>
        @endif

        @include('layout.partials.extras._topbar')
            @if(auth()->guard('seller')->check())
                @if(auth()->guard('seller')->user()->stripe_flag != 1)

            <div class="alert alert-danger alert-dismissible fade show" role="alert" style=" max-width:310px; top:10px;   position: absolute;
                        float: right;
                        right: 1px;
                        opacity: 0.7;">
                <strong>
                    Please click this <a href="{{ env('STRIPE_CALL_BACK_LINK') }}" >link</a>  your strip connecting account that will help us to transfer your amount.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </strong>
            </div>
                    @endif
                @endif
    </div>
</div>
