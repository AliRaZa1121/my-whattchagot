<div class="newsletter_row">
    <div class="container">
        <div class="row">
            <div class="newsletter1">
                <i class="far fa-paper-plane"></i>
                <div class="newsletter_text">
                    <h3>SIGN UP FOR</h3>
                    <h2>SUBSCRIPTION</h2>
                </div>
            </div>
            <div class="newsletter2">
                <form id="news-letter">
                    <input type="email" id="newsletter_email" name="email" placeholder="Enter your email address" required><input type="submit" name="submit" value="Submit">
                </form>
            </div>
            <div class="newsletter3">
                <ul>
                    <li><a href="{{ $setting->facebook_link }}"><i class="fab fa-facebook-f"></i></a></li>
                    <li><a href="{{ $setting->twitter_link }}"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="{{ $setting->instagram_link }}"><i class="fab fa-instagram"></i></a></li>
                    {{-- <li><a href="https://www.pinterest.com/"><i class="fab fa-pinterest"></i></a></li> --}}
                    <li><a href="{{ $setting->tiktok_link }}"><i class="fab fa-tiktok fa-2x"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
