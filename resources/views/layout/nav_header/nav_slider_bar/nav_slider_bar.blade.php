<style type="text/css">
    .slider_nav_row.extra-class {
    margin: 0;
}
</style>
<div class="slider_nav_row extra-class">
    <div class="container">
        <div class="row">
            <div class="side_cat_navbar">
            <div class="accordion" id="faq">
              @if(!empty($categories))
                @foreach($categories as $category)
                    <div class="card">
                        <div class="card-header" id="{{ $category->id }}">
                            <a href="" class="btn btn-header-link collapsed" data-toggle="collapse"
                               data-target="#faq{{ $category->id }}"
                               aria-expanded="true" aria-controls="faq{{ $category->id }}">{{ $category->name }}</a>
                        </div>

                        <div id="faq{{ $category->id }}" class="collapse" aria-labelledby="{{ $category->id }}" data-parent="#faq">
                            <div class="card-body">
                                <ul>
                                @if(!empty($category->categories))
                                @foreach($category->categories as $subcat)
                                <li><a href="{{ url('products') }}?type=list&&sub_cat={{$subcat->id}}">{{ $subcat->name }}</a></li>
                                @endforeach
                                @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                   @endforeach
                   @endif

                </div>
            </div>
    
        </div>
    </div>
</div>
