
<div class="logo_bar">
    <div class="container search-section">
        <div class="row">
            <div class="col-md-2">
                <div class="logo_div">
                    <a href="{{ route('index') }}"><img src="{{asset('images/h_logo.png')}}"></a>
                </div>
            </div>
           @php $categories=getcategories(); @endphp
            <div class="col-md-8">
                <div class="header_search_form">
                    <form  action="{{ url('products') }}" method="get">
                        <input type="text" name="keyword" value="@if(isset($_GET['keyword'])){{ $_GET['keyword'] }} @endif" placeholder="Search product">
                        <select name="cat" class="select-cat categor">
                            <option value="">All Categories</option>
                            @if(!empty($categories))
                            @foreach($categories as $cat)
                            <option value="{{$cat->id}}" @if(isset($_GET['cat']) && $_GET['cat']==$cat->id) selected @endif>{{$cat->name}}</option>
                            @endforeach
                            @endif
                        </select>
                        <select name="sub_cat" class="select-cat subacte">
                            <option value="">All subcategory</option>
                        </select>
                        <select name="type">
                            <option value="list" @if(isset($_GET['type']) && $_GET['type']=='all') selected @endif>All</option>
                            <option value="seller" @if(isset($_GET['type']) && $_GET['type']=='seller') selected @endif>Selling</option>
                            <option value="trade" @if(isset($_GET['type']) && $_GET['type']=='trade') selected @endif>Trade</option>
                        </select>
                        <button type="submit"><i class='fas fa-search'></i></button>
                    </form>
                </div>
            </div>
            <div class="col-md-2">
{{--                <div class="chat_button">--}}
{{--                    <a href="#"><i class="fas fa-comments"></i> Chat With Us</a>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
    </div>
</div>
<script>
$('.categor').on('change', function() {
  var data_input=this.value;
  $('.subacte').html('<option value="">All subcategory</option>');
      $.ajax({
                url: "{{ route('get.sub') }}",
                type: "GET",
                data: {id:data_input},
                success: function (response) {
                    var a = JSON.parse(response)
                    $.each(a, function(index,value){
                        var optionvalue='<option value="'+value.id+'">'+value.name+'</option>'
                        $('.subacte').append(optionvalue)
                        console.log(value.id+'--'+value.name)
                    });

                }
            });
});
</script>
