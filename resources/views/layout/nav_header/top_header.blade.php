
<div class="top_header {{ (auth()->check() && auth()->user()->user_type === "user") ? 'buyer_login' : '' }}">
    <div class="container">
        <div class="row">
            <div class="top_h1">
                <a href="{{route('products.index')}}"><i class="fas fa-gift"></i> Sell With Us</a>
            </div>
            <div class="top_h2">
                <ul>
                    <li><a href="{{url('help-center')}}"><i class="fas fa-exclamation-circle"></i> Help</a></li>
                    @if(auth()->check() && auth()->user()->user_type === "user")
                    <li style="text-transform: capitalize!important;"><a href="{{route('wish-list')}}"><i class="fas fa-heart"></i> Wishlist</a></li>
                    @endif


                </ul>
            </div>
            <div class="top_h3">
             <ul>
                 @auth

                <li class="droo"><i class="fas fa-user-alt"></i>
                    @php
                        $name = explode(" ",auth()->user()->name);
                    @endphp
                    <button class="dropbtn">{{ $name[0] ?? '' }}
                        <i class="fa fa-caret-down"></i>
                    </button>
                    <div class="dropdown-content">
                        <a href="{{route('login')}}" class="btn btn-sm btn-light-primary font-weight-bolder  ">Dashboard</a>
                        <a href="{{route('checkout')}}" class="btn btn-sm btn-light-primary font-weight-bolder  ">Checkout</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                        <a href="#" class="btn btn-sm btn-light-primary font-weight-bolder " onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">Sign Out</a>              </div>
                </li>
                @if(auth()->user()->user_type === "user")
                <li><a href="{{route('cart')}}" class="header-cart"><i class="fas fa-cart-plus"></i><span id="cart_count">{{ get_cart_count() }}</span></a></li>
                @endif

                @endauth

            @guest
            <li><a href="{{route('login')}}"><i class="fas fa-sign-in-alt"></i> </a></li>
            @endguest

            </ul>

            </div>
            <div class="language_dropdown same_dropdown">

                <div class="col-md-1 text-center drop-en">
                    <ul class="list-unstyled list-inline ct-topbar__list">
                        <li class="ct-language dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" style="background: #131d3f; border-color: #131d3f">
                            </button>
                            <ul class="list-unstyled ct-language__dropdown dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                {{--<li><a href="#googtrans(en|en)" class="lang-en lang-select" data-lang="en" style="color: black">EN</a></li>
                                <li><a href="#googtrans(en|sq)" class="lang-sq lang-select" data-lang="sq" style="color: black">Albanian</a></li>
                                <li><a href="#googtrans(en|es)" class="lang-sq lang-select" data-lang="es" style="color: black">Spanish</a></li>--}}
                                <li><a href="" class="lang-en lang-select" style="color: black">EN</a></li>
                                <li><a href="" class="lang-sq lang-select" style="color: black">Albanian</a></li>
                                <li><a href="" class="lang-sq lang-select" style="color: black">Spanish</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</div>
{{--<script>--}}

{{--    var selectedCountry = $('#country_code').val();--}}

{{--   alert(selectedCountry);--}}
{{--</script>--}}
