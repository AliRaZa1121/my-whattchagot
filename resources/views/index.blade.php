@extends('layouts.app2')
<style>
    .best_selling_ad {
        background-image: url("{{asset(App\Helpers\Banner::get_banner_images('home', 'left-1'))}}") !important;
        width: 22.75%;
        background-size: cover;
        padding: 10px;
        padding-top: 40px;
        color: #fff;
    }
    .auction_sidebar {
    /* background-image: url('../images/upcoming_a.jpg'); */
    background-image: url("{{asset(App\Helpers\Banner::get_banner_images('home', 'left-2'))}}") !important;
    background-size: cover;
    background-repeat: no-repeat;
    padding: 25px;
}
</style>

@section('content')
    @if(!session()->has('loader'))
    <div id="preloader">
        <img src="{{ asset('images/loader.gif') }}" />
    </div>
    @endif
    @php
    use Carbon\Carbon;
    session()->put('loader', 'done');
    @endphp
    @include('layout.nav_header.nav_slider_bar.nav_slider_bar')

    <div class="slider_nav_row">
        <div class="row">
            <div class="container">
                <div class="home_main_slider owl-carousel owl-theme home-page-main-slider">
                    @if (!empty($bannerimages))
                        <?php $product_count = count($bannerimages);
                        $loop_num = $product_count = round(count($bannerimages));
                        $no = 1; ?>

                        @forelse ($bannerimages as $banner)
                        <?php
                            if ($no == 1) {
                                $no = 0;
                                echo "<div class='item'>";
                            }
                            ?>
                            <div class="main_slider_inner">
                                <div class="main_slider_left">
                                    <div class="auction_slider_text main_slider_text">
                                        <div class="auction_text Slider_text">
                                            <h2>{{ $banner->product->name ?? 'N/A' }}</h2>
                                            <p>{{ $banner->product_details->short_details ?? 'N/A' }}</p>
                                        </div>
                                    </div>
                                    <div class="main_slider_features">

                                    </div>
                                    <div class="main_slider_btns">
                                        @php
                                            switch (isset($banner->product) ? $banner->product->flags : 'N/A') {
                                                case('trade'):
                                                 $route = route('trading-single', $banner->product->id);
                                                break;
                                                  case("auction"):
                                                   $route = route('auction-single', $banner->product->id);
                                                    break;
                                                      case("selling"):
                                                       $route = route('product-details', $banner->product->id);
                                                        break;
                                                            default:
                                                                $route = '#';
                                                                break;
                                            }
                                        @endphp

                                        <a href="{{ $route }}">
                                            <i class="fas fa-info" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="main_slider_right">
                                    <img
                                        src="{{ isset($banner->banner_image) ? asset('/storage/' . $banner->banner_image) : asset('/storage/ecommerce-trends.png') }}">
                                </div>
                            </div>
                            <?php if ($no == 0) {
                                echo '</div>';
                                $no++;
                            } else {
                                $no++;
                            } ?>
                         @empty
                         <div class="main_slider_inner">
                            <div class="main_slider_left">
                                <div class="auction_slider_text main_slider_text">

                                    <div class="auction_star_rating slider_star_rating">
                                    </div>
                                    <div class="auction_text Slider_text">
                                        <h2>WhattchaGot</h2>
                                        <p>WhattchaGot is open for all types of goods, from electronic items to handicrafts and other
                                            vintage treasures. As a company, we struggle to lead with our guiding principles and help
                                            spread ideas of responsibility and sustainability so that the impact can reach far beyond
                                            our business.</p>

                                    </div>
                                </div>
                             <div class="main_slider_features">
                                </div>

                            </div>
                            <div class="main_slider_right">
                                <img
                                    src="{{ asset('images/ecommerce-trends.png') }}">
                            </div>
                        </div>
                        @endforelse
                    @endif

                </div>
            </div>
        </div>
    </div>
    <div class="selling_auction_row">
        <div class="container">
            <div class="row">
                <div class="best_selling same_pr_box">
                    <div class="pr_title">
                        <h2>Selling Products</h2>
                        <a href="{{ route('shop.products') }}">Click Here</a>
                    </div>
                    <div class="pr_image">
                        <img src="images/shoes.png">
                    </div>
                </div>
                <div class="upcoming_auction same_pr_box">
                    <div class="pr_title">
                        <h2>Auctions</h2>
                        <a href="{{ url('/auction') }}">Click Here</a>
                    </div>
                    <div class="pr_image">
                        <img src="images/headphones.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="trending_products">
        <div class="container">
            <div class="row">
                @if (count($live_auction) > 0)
                <div class="trending_sidebar">
                    <div class="tr_s_head">
                        <h3>LIVE AUCTIONS</h3>
                    </div>
                    <div class="live_trade_slider owl-carousel owl-theme">

                            <?php $product_count = count($live_auction);
                            $loop_num = $product_count = round(count($live_auction));
                            $no = 1; ?>
                            @foreach ($live_auction as $auction)

                                <?php
                                if ($no == 1) {
                                    $no = 0;
                                    echo "<div class='item'>";
                                }
                                ?>
                                <div class="lt_slider_wrapper">
                                    <img
                                        src="{{ isset($auction->product_images->image) ? asset('/storage/' . $auction->product_images->image) : asset('/storage/default.jpg') }}">
                                    <div class="live_trade_date">Uploaded On <span>{{ $auction->updated_at }}</span>
                                    </div>
                                    <h4>{{ $auction->name }}</h4>
                                    <div class="live_trade_price">
                                    </div>

                                    <a href="{{route('auction-single', $auction->id)}}">Bid Now</a>
                                </div>
                                <?php if ($no == 0) {
                                    echo '</div>';
                                    $no++;
                                } else {
                                    $no++;
                                } ?>

                            @endforeach



                    </div>
                </div>
                @endif
                <div class="trending_products_wrapper">
                    <div class="tr_p_head">
                        <h3>TRADING PRODUCTS</h3>
                    </div>
                    <div class="trending_products_slider owl-carousel owl-theme home-trending-products">
                        @if (!empty($products_trade))
                            <?php $product_count = count($products_trade);
                            $loop_num = $product_count = round(count($products_trade));
                            $no = 6; ?>
                            @foreach ($products_trade as $sellersdata)

                                <?php
                                if ($no == 6) {
                                    $no = 0;
                                    echo "<div class='item'>";
                                }
                                ?>


                                <div class="trending_single_wrapper">
                                    <div class="trending_single_product">
                                        <div class="ts_label">New</div>
                                        <img
                                            src="{{ isset($sellersdata->product_images->image) ? asset('/storage/' . $sellersdata->product_images->image) : asset('/storage/default.jpg') }}">
                                        <h3>{{ $sellersdata->name }}</h3>
                                    </div>
                                    <div class="tr_single_overlay">
                                        <div class="auction_wishlist tr_s_wishlist">
                                            <a href="javascript:;" class="wishlist_ajax"
                                                data-id="{{ $sellersdata->id }}"><i class="far fa-heart"></i></a>
                                        </div>
                                        <div class="main_slider_btns tr_single_btns best_selling_btns">
                                            <a href="{{ route('trading-single', 1); }}">
                                                <i class="fa fa-info" aria-hidden="true"></i>
                                             </a>

                                        </div>
                                    </div>
                                </div>
                                <?php if ($no == 5) {
                                    echo '</div>';
                                    $no++;
                                } else {
                                    $no++;
                                } ?>

                            @endforeach
                        @endif


                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="new_arrivals">
        <div class="container">
            <div class="row">
                <div class="new_arrival_img">
                    <img src="images/new-arrival-img.png">
                </div>
                <div class="new_arrival_text">
                    <h2>NEW ARRIVALS</h2>
                    <p>We offer this opportunity to buy, sell, trade, and auction your new arrivals online. We connect
                        buyers and sellers under one platform to help grow their business with little to no barrier of size,
                        background, or geographic location. You can find every item for trade, auction, and buying fast and
                        easy. Enjoy a highly personalized experience with an unparalleled selection at a great value.</p>
                    <a href="{{ url('/new-arrivals?type=list') }}">Shop Now</a>
                </div>
            </div>
        </div>
    </div>
    <div class="best_selling_row home-best-selling home-best-selling">
        <div class="container">

            <div class="row">
                <div class="best_selling_ad">

                </div>
                <div class="best_selling_left">
                    <div class="best_selling_head">
                        <h3>BEST SELLING PRODUCTS</h3>
                    </div>
                    <div class="best_selling_slider owl-carousel owl-theme">

                        @if (!empty($sellers))
                            <?php $product_count = count($sellers);
                            $loop_num = $product_count = round(count($sellers));
                            $no = 4; ?>
                            @foreach ($sellers as $sellersdata)

                                <?php
                                if ($no == 4) {
                                    $no = 0;
                                    echo "<div class='item'>";
                                }
                                ?>


                                <div class="best_selling_single">
                                    <div class="best_selling_slider_img">
                                        <img
                                            src="{{ isset($sellersdata->product_images->image) ? asset('/storage/' . $sellersdata->product_images->image) : asset('/storage/default.jpg') }}">

                                    </div>
                                    <div class="auction_slider_text best_selling_slider_text">
                                        <div class="auction_star_rating">
                                            <div class="rate">
                                                <input type="radio" id="star5" name="rate" value="5" checked />
                                                <label for="star5" title="text">5 stars</label>
                                                <input type="radio" id="star4" name="rate" value="4" />
                                                <label for="star4" title="text">4 stars</label>
                                                <input type="radio" id="star3" name="rate" value="3" />
                                                <label for="star3" title="text">3 stars</label>
                                                <input type="radio" id="star2" name="rate" value="2" />
                                                <label for="star2" title="text">2 stars</label>
                                                <input type="radio" id="star1" name="rate" value="1" />
                                                <label for="star1" title="text">1 star</label>
                                            </div>
                                            <div class="auction_total_reviews">
                                                <?php $countreview = getreviewbyproduct($sellersdata->id); ?>
                                                <a href="#">({{ $countreview->count() }} Reviews)</a>
                                            </div>


                                        </div>
                                        <div class="auction_text">
                                            <h2>{{ $sellersdata->name }}</h2>
                                            <p>{{ $sellersdata->decription }}</p>
                                            <div class="best_selling_price">
                                                ${{ $sellersdata->price }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tr_single_overlay best_selling_overlay">
                                        <div class="auction_wishlist tr_s_wishlist">
                                            <a href="javascript:;" class="wishlist_ajax"
                                                data-id="{{ $sellersdata->id }}"><i class="far fa-heart"></i></a>
                                        </div>
                                        <div class="main_slider_btns tr_single_btns best_selling_btns">
                                            @if (Auth()->guard('buyer')->check())

                                                <form class="ajax_add_cart">
                                                    @csrf
                                                    <input type="hidden" name="product_id" class="product_id"
                                                        value="{{ base64_encode($sellersdata->id) }}">
                                                    <input type="hidden" name="price" class="price"
                                                        value="{{ base64_encode($sellersdata->price) }}">

                                                    <button type="submit">Add To Cart</button>
                                                </form>


                                            @else
                                                <a
                                                    href="{{ route('add_check_out', ['seller' => Crypt::encrypt($sellersdata->user_id), 'product' => Crypt::encrypt($sellersdata->id), 'price' => Crypt::encrypt($sellersdata->price), 'last_segment' => Crypt::encrypt(request()->segment(count(request()->segments())))]) }}">Add
                                                    To Cart</a>

                                            @endif
                                            @if (Auth()->guard('buyer')->check())
                                                <form method="post" action="{{ route('chat_alreadylogin') }}">
                                                    @csrf
                                                    <input type="hidden" name="product_id" class="product_id"
                                                        value="{{ base64_encode($sellersdata->id) }}">
                                                    <input type="hidden" name="receiver_id" class="price"
                                                        value="{{ base64_encode($sellersdata->user_id) }}">

                                                    <button type="submit" class="submit-btn-after-login"><i
                                                            class="fas fa-comments"></i>
                                                    </button>
                                                </form>

                                            @else
                                                <a
                                                    href="{{ route('buyer_login_id', ['seller' => Crypt::encrypt($sellersdata->user_id), 'product' => Crypt::encrypt($sellersdata->id)]) }}"><i
                                                        class="fas fa-comments"></i></a>

                                            @endif

                                            <a
                                                href="{{ $sellersdata->flags == 'trade' ? route('trading-single', $key->id) : url('product-details') . '/' . $sellersdata->id }}"><i
                                                    class="fa fa-info" aria-hidden="true"></i></a>

                                        </div>
                                    </div>
                                </div>
                                <?php if ($no == 3) {
                                    echo '</div>';
                                    $no++;
                                } else {
                                    $no++;
                                } ?>

                            @endforeach
                        @endif
                    </div>
                </div>

            </div>

        </div>
    </div>
    </div>
    <div class="shop_with_row">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>WHY SHOP WITH WHATTCHAGOT?</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="same_four_box">
                        <div class="same_four_box_img">
                            <img src="images/icon1.png">
                        </div>
                        <h3>Fair Return Policy</h3>
                        <p>As a globalized marketplace of trade and auction, we are invested in making sure every purchase
                            is a great experience. Therefore, we follow Fair Return Policy to make sure none of our
                            customers are unsatisfied with an order or service. </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="same_four_box">
                        <div class="same_four_box_img">
                            <img src="images/icon2.png">
                        </div>
                        <h3>24/7 Support</h3>
                        <p>Although yes, we are 24/7 support. We need away for the consumer to understand they won’t receive an answer on the spot.</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="same_four_box">
                        <div class="same_four_box_img">
                            <img src="images/icon3.png">
                        </div>
                        <h3>Secure Payments</h3>
                        <p>We provide the technology behind our marketplace that keeps the connections safe, trading fun,
                            and payments secure.</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="same_four_box">
                        <div class="same_four_box_img">
                            <img src="images/icon4.png">
                        </div>
                        <h3>Sell Worldwide</h3>
                        <p>WhattchaGot is the global marketplace for the trading and auction of goods. We help our community
                            by selling, trading, and auction items worldwide, from electrical to vintage treasures. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if (count($auctions) > 0)
    <div class="upcoming_auction_row">
        <div class="container">
            <div class="row">
                <div class="auction_sidebar">
                    <div class="auction_sale_price">
                        {{-- <h3>BEST PRICE</h3>
                        <h2>UPTO 30% OFF</h2> --}}
                        {{-- <a href="{{ url('/products?type=list') }}">Shop Now</a> --}}
                        {{-- <a href="{{App\Helpers\Banner::get_banner_url('home', 'left-1') }}">SHOP NOW</a> --}}
                    </div>
                </div>

                <div class="auction_slider">
                    <div class="auction_top">
                        <h3>UPCOMING AUCTION</h3>
                    </div>

                    <div class="auction_slider_wrapper owl-carousel owl-theme">

                            <?php $product_count = count($auctions);
                            $loop_num = $product_count = round(count($auctions));
                            $no = 1; ?>
                            @foreach ($auctions as $sellersdata)

                                <?php
                                if ($no == 1) {
                                    $no = 0;
                                    echo "<div class='item'>";
                                }
                                ?>

                                <div class="auction_slider_inner">
                                    <div class="auction_slider_img">
                                        <a href="#">
                                            <img
                                                src="{{ isset($sellersdata->product_images->image) ? asset('/storage/' . $sellersdata->product_images->image) : asset('/storage/default.jpg') }}">
                                        </a>
                                    </div>
                                    <div class="auction_slider_text">

                                        <div class="auction_wishlist">
                                            <a href="javascript:;" class="wishlist_ajax"
                                                data-id="{{ $sellersdata->id }}"><i class="far fa-heart"></i></a>
                                        </div>
                                        <div class="auction_star_rating">
                                            <div class="rate">
                                                <input type="radio" id="star5" name="rate" value="5" />
                                                <label for="star5" title="text">5 stars</label>
                                                <input type="radio" id="star4" name="rate" value="4" />
                                                <label for="star4" title="text">4 stars</label>
                                                <input type="radio" id="star3" name="rate" value="3" />
                                                <label for="star3" title="text">3 stars</label>
                                                <input type="radio" id="star2" name="rate" value="2" />
                                                <label for="star2" title="text">2 stars</label>
                                                <input type="radio" id="star1" name="rate" value="1" />
                                                <label for="star1" title="text">1 star</label>
                                            </div>
                                            <div class="auction_total_reviews">
                                                <?php $countreview = getreviewbyproduct($sellersdata->id); ?>
                                                <a href="#">({{ $countreview->count() }} Reviews)</a>
                                            </div>
                                        </div>
                                        <div class="auction_text">
                                            <h2>{{ $sellersdata->name }}</h2>
                                            <p>{{ $sellersdata->short_details }}</p>
                                            <?php
                                            $formatted_dt1 = Carbon::now();
                                            $formatted_dt2 = Carbon::parse($sellersdata->start_date . ' ' . $sellersdata->start_time);

                                            $date_diff = $formatted_dt1->diffInDays($formatted_dt2);

                                            $week_diff = $formatted_dt1->diffInWeeks($formatted_dt2);

                                            $hours_diff = $formatted_dt1->diffInHours($formatted_dt2);
                                            //  echo $hours_diff.' Hours '; //48 Hours

                                            $Minutesdiff = $formatted_dt1->diffInMinutes($formatted_dt2);
                                            // echo $Minutesdiff.' Minutes '; //2880 Minutes
                                            // $seconddiff = $formatted_dt1->DiffInSeconds($formatted_dt2);

                                            ?>
                                            <div id="countdown">
                                                <ul>
                                                    <li><span id="days"></span>{{ $date_diff }} DAYS</li>
                                                    <li><span id="hours"></span> {{ $hours_diff - $date_diff * 24 }}
                                                        HOURS
                                                    </li>
                                                    <li><span id="minutes"></span> {{ $Minutesdiff - $hours_diff * 60 }}
                                                        MINS
                                                    </li>
                                                    {{-- <li><span
                                                            id="seconds"></span>{{ $seconddiff - $Minutesdiff * 60 }}SEC
                                                    </li> --}}
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php if ($no == 0) {
                                    echo '</div>';
                                    $no++;
                                } else {
                                    $no++;
                                } ?>

                            @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>

    @endif
    <script>
        jQuery(".live_trade_slider").owlCarousel({
            loop: true,
            items: 1,
            margin: 10,
            dots: false,
            nav: true,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            autoplay: true,
            autoplayTimeout: 3000,
            responsiveClass: true,
            autoplaySpeed: 2000,
        });



    </script>
@stop
