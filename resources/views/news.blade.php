@extends('layouts.app2')

@section('content')
    <!-- Full With banner -->

    @include('layout.nav_header.nav_slider_bar.nav_slider_bar')
    <div class="full-width-banner news-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>News</h2>
                </div>

            </div>
        </div>

    </div>


    <!-- Full With Banner End here -->

    <div class="best_selling_row news-main-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="best_selling_left">
                        <div class="best_selling_head">
                            <h3>Latest News</h3>
                        </div>
                        <div id="news-slider" class="best_selling_slider  owl-carousel owl-theme">
                            <div class="item">
                                <div class="best_selling_single">
                                    <div class="new-iner-col">
                                        <div class="news-col-img">
                                            <img src="./images/new-thumnail.jpg">
                                        </div>
                                        <div class="new-thumnail-txt">
                                            <h3>Dummy Title of Blogs and News title</h3>
                                            <p>This is dummy copy. It is not meant to be read. It has been placed here
                                                solely to demonstrate the look and feel.</p>
                                            <a href="{{route('news-single')}}">Read More</a>
                                        </div>

                                    </div>
                                </div> <!--asdsadasdasd-->

                                <div class="best_selling_single">
                                    <div class="new-iner-col">
                                        <div class="news-col-img">
                                            <img src="./images/new-thumnail.jpg">
                                        </div>
                                        <div class="new-thumnail-txt">
                                            <h3>Dummy Title of Blogs and News title</h3>
                                            <p>This is dummy copy. It is not meant to be read. It has been placed here
                                                solely to demonstrate the look and feel.</p>
                                            <a href="{{route('news-single')}}">Read More</a>
                                        </div>

                                    </div>
                                </div> <!--asdsadasdasd-->

                                <div class="best_selling_single">
                                    <div class="new-iner-col">
                                        <div class="news-col-img">
                                            <img src="./images/new-thumnail.jpg">
                                        </div>
                                        <div class="new-thumnail-txt">
                                            <h3>Dummy Title of Blogs and News title</h3>
                                            <p>This is dummy copy. It is not meant to be read. It has been placed here
                                                solely to demonstrate the look and feel.</p>
                                            <a href="{{route('news-single')}}">Read More</a>
                                        </div>

                                    </div>
                                </div> <!--asdsadasdasd-->

                            </div>
                            <div class="item">
                                <div class="best_selling_single">
                                    <div class="new-iner-col">
                                        <div class="news-col-img">
                                            <img src="./images/new-thumnail.jpg">
                                        </div>
                                        <div class="new-thumnail-txt">
                                            <h3>Dummy Title of Blogs and News title</h3>
                                            <p>This is dummy copy. It is not meant to be read. It has been placed here
                                                solely to demonstrate the look and feel.</p>
                                            <a href="{{route('news-single')}}">Read More</a>
                                        </div>

                                    </div>
                                </div> <!--asdsadasdasd-->

                                <div class="best_selling_single">
                                    <div class="new-iner-col">
                                        <div class="news-col-img">
                                            <img src="./images/new-thumnail.jpg">
                                        </div>
                                        <div class="new-thumnail-txt">
                                            <h3>Dummy Title of Blogs and News title</h3>
                                            <p>This is dummy copy. It is not meant to be read. It has been placed here
                                                solely to demonstrate the look and feel.</p>
                                            <a href="{{route('news-single')}}">Read More</a>
                                        </div>

                                    </div>
                                </div> <!--asdsadasdasd-->

                                <div class="best_selling_single">
                                    <div class="new-iner-col">
                                        <div class="news-col-img">
                                            <img src="./images/new-thumnail.jpg">
                                        </div>
                                        <div class="new-thumnail-txt">
                                            <h3>Dummy Title of Blogs and News title</h3>
                                            <p>This is dummy copy. It is not meant to be read. It has been placed here
                                                solely to demonstrate the look and feel.</p>
                                            <a href="{{route('news-single')}}">Read More</a>
                                        </div>

                                    </div>
                                </div> <!--asdsadasdasd-->

                            </div>

                            <div class="item">
                                <div class="best_selling_single">
                                    <div class="new-iner-col">
                                        <div class="news-col-img">
                                            <img src="./images/new-thumnail.jpg">
                                        </div>
                                        <div class="new-thumnail-txt">
                                            <h3>Dummy Title of Blogs and News title</h3>
                                            <p>This is dummy copy. It is not meant to be read. It has been placed here
                                                solely to demonstrate the look and feel.</p>
                                            <a href="{{route('news-single')}}">Read More</a>
                                        </div>

                                    </div>
                                </div> <!--asdsadasdasd-->

                                <div class="best_selling_single">
                                    <div class="new-iner-col">
                                        <div class="news-col-img">
                                            <img src="./images/new-thumnail.jpg">
                                        </div>
                                        <div class="new-thumnail-txt">
                                            <h3>Dummy Title of Blogs and News title</h3>
                                            <p>This is dummy copy. It is not meant to be read. It has been placed here
                                                solely to demonstrate the look and feel.</p>
                                            <a href="{{route('news-single')}}">Read More</a>
                                        </div>

                                    </div>
                                </div> <!--asdsadasdasd-->

                                <div class="best_selling_single">
                                    <div class="new-iner-col">
                                        <div class="news-col-img">
                                            <img src="./images/new-thumnail.jpg">
                                        </div>
                                        <div class="new-thumnail-txt">
                                            <h3>Dummy Title of Blogs and News title</h3>
                                            <p>This is dummy copy. It is not meant to be read. It has been placed here
                                                solely to demonstrate the look and feel.</p>
                                            <a href="{{route('news-single')}}">Read More</a>
                                        </div>

                                    </div>
                                </div> <!--asdsadasdasd-->

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="news-side-br">
                        <div class="news-side-br-inner">
                            <h2>Topics</h2>
                            <ul>
                                <li><a href="javascript:void(0)"><img src="./images/side-icon.png">All Topics</a></li>
                                <li><a href="javascript:void(0)"><img src="./images/side-icon.png">Announcements</a></li>
                                <li><a href="javascript:void(0)"><img src="./images/side-icon.png">Payments</a></li>
                                <li><a href="javascript:void(0)"><img src="./images/side-icon.png">Purpose</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="news-side-br">
                        <div class="news-side-br-inner recent-news-wrapper">
                            <h2>Recent News</h2>
                            <ul>
                                <li><a href="javascript:void(0)"><img src="./images/recent-icons.png">This is dummy copy. It is not meant to be read.
</a></li>
                                <li><a href="javascript:void(0)"><img src="./images/recent-icons.png">This is dummy copy. It is not meant to be read.
                                </a></li>
                                <li><a href="javascript:void(0)"><img src="./images/recent-icons.png">This is dummy copy. It is not meant to be read.
                                </a></li>
                                <li><a href="javascript:void(0)"><img src="./images/recent-icons.png">This is dummy copy. It is not meant to be read.
                                </a></li>
                                <li><a href="javascript:void(0)"><img src="./images/recent-icons.png">This is dummy copy. It is not meant to be read.
                                </a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="side-br-twitter">
                        <div class="twitter-img">
                            <img src="./images/white-logo-twitter.png">
                        </div>
                        <div class="twitter-txt">
                            <p>This is dummy copy. It is not meant to be read. It has been placed here solely to
                                demonstrate the look and feel of finished, typeset text. </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script>
      jQuery("#news-slider").owlCarousel({
    loop:true,
   items: 3,
    rows: 3,
    margin:10,
    dots: false,
    nav: true,
    navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
    autoplay: false,
    autoplayTimeout: 3000,
   
    // responsive: {
    //   0: {
    //     items: 1,
    //     rows: 2 //custom option not used by Owl Carousel, but used by the algorithm below
    //   },
    //   768: {
    //     items: 2,
    //     rows: 3 //custom option not used by Owl Carousel, but used by the algorithm below
    //   },
    //   991: {
    //     items: 2,
    //     rows: 3 //custom option not used by Owl Carousel, but used by the algorithm below
    //   }
    // }
    
  }); 


  

</script>
@stop




