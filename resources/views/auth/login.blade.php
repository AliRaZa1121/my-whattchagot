@extends('layout.auth')

@section('styles')
    <script src="https://js.stripe.com/v3/"></script>
    <link rel="stylesheet" href="{{ asset('css/pages/login/classic/login-1.css?v=7.0.4') }}">

    <style>
        /**
            * The CSS shown here will not be introduced in the Quickstart guide, but shows
            * how you can use CSS to style your Element's container.
            */
        .StripeElement {
            box-sizing: border-box;

            height: 40px;
            width: 100%;
            padding: 10px 12px;

            border: 1px solid transparent;
            border-radius: 4px;
            background-color: #e6ebf1;

            box-shadow: 0 1px 3px 0 #e6ebf1;
            -webkit-transition: box-shadow 150ms ease;
            transition: box-shadow 150ms ease;
        }

        .StripeElement--focus {
            box-shadow: 0 1px 3px 0 #cfd7df;
        }

        .StripeElement--invalid {
            border-color: #fa755a;
        }

        .StripeElement--webkit-autofill {
            background-color: #fefde5 !important;
        }

        #card-errors {
            color: orangered
        }

    </style>


@endsection

<?php
use Illuminate\Support\Facades\Crypt;

?>

@section('content')
    <div class="d-flex flex-column flex-root">
        <!--begin::Login-->
        <div class="login login-1 login-signin-on d-flex flex-column flex-lg-row flex-row-fluid bg-white" id="kt_login">
            <!--begin::Aside-->
            <div class="login-aside d-flex flex-row-auto bgi-size-cover bgi-no-repeat p-10 p-lg-10"
                style="background-color: #252525">
                <!--begin: Aside Container-->
                <div class="d-flex flex-row-fluid flex-column justify-content-between">
                    <!--begin: Aside header-->
                    <center>
                        <a href="#" class="flex-column-auto mt-5">
                            <img src="{{ asset('media/logos/logo.png') }}" class="max-h-100px" alt="" />
                        </a>
                    </center>

                    <!--end: Aside header-->
                    <!--begin: Aside content-->
                    <div class="flex-column-fluid d-flex flex-column justify-content-center">
                        <h3 class="font-size-h1 mb-5 text-white">Welcome to Whattchagot!</h3>
                        <p class="font-weight-lighter text-white opacity-80">WhattchaGot.com is an online marketplace that facilitates transactions between sellers and buyers securely and efficiently. We provide a platform to our customers to trade, auction, and buy products without a second thought. Our foremost goal is to establish a safe place where anyone can buy, sell, trade, and auction their goods.
WhattchaGot is open for all types of goods, from electronic items to handicrafts and other vintage treasures. As a company, we struggle to lead with our guiding principles and help spread ideas of responsibility and sustainability so that the impact can reach far beyond our business</p>
                    </div>
                    <!--end: Aside content-->
                    <!--begin: Aside footer for desktop-->
                    <div class="d-none flex-column-auto d-lg-flex justify-content-between mt-10">
                        <div class="opacity-70 font-weight-bold text-white">© <?php echo date('Y'); ?> <a
                                href="{{ route('index') }}">Whattchagot</a></div>
                        {{-- <div class="d-flex"> --}}
                        {{-- <a href="#" class="text-white">Privacy</a> --}}
                        {{-- <a href="#" class="text-white ml-10">Legal</a> --}}
                        {{-- <a href="#" class="text-white ml-10">Contact</a> --}}
                        {{-- </div> --}}
                    </div>
                    <!--end: Aside footer for desktop-->
                </div>
                <!--end: Aside Container-->
            </div>
            <!--begin::Aside-->
            <!--begin::Content-->
            <div class="flex-row-fluid d-flex flex-column position-relative p-7 overflow-hidden">
                <!--begin::Content header-->
                <div
                    class="position-absolute top-0 right-0 text-right mt-5 mb-15 mb-lg-0 flex-column-auto justify-content-center py-5 px-10 show_signup">
                    <span class="font-weight-bold text-dark-50">Dont have an account yet?</span>
                    <a href="javascript:;" class="font-weight-bold ml-2" id="kt_login_signup">Sign Up!</a>
                </div>

                <div class="position-absolute top-0 right-0 text-right mt-5 mb-15 mb-lg-0 flex-column-auto justify-content-center py-5 px-10 cancel_login"
                    style="display: none">
                    <span class="font-weight-bold text-dark-50">Already have an account?</span>
                    <a href="javascript:;" class="font-weight-bold ml-2 kt_login_signup_cancel"
                        id="kt_login_signup_cancel2">Sign In!</a>
                </div>
                <!--end::Content header-->
                <!--begin::Content body-->
                <div class="d-flex flex-column-fluid flex-center mt-30 mt-lg-0">
                    <!--begin::Signin-->
                    <div class="login-form login-signin">
                        <div class="text-center mb-10 mb-lg-20">
                            <h3 class="font-size-h1">Sign In</h3>
                            <p class="text-muted font-weight-bold">Enter your email and password</p>
                        </div>
                        @if (session('success'))
                            <div class="alert alert-primary">
                                {{ session('success') }}
                            </div>
                        @endif
                        <!--begin::Form-->
                        <form class="form" novalidate="novalidate" id="kt_login_signin_form" method="post"
                            action="{{ route('login.submit') }}">
                            @csrf
                            <div class="form-group">
                                <input class="form-control form-control-solid h-auto py-5 px-6" type="email"
                                    placeholder="Email" name="email" autocomplete="off" value="{{ old('email') }}" />
                            </div>
                            <div class="form-group">
                                <input class="form-control form-control-solid h-auto py-5 px-6" type="password"
                                    placeholder="Password" name="password" autocomplete="off" />
                            </div>
                            <div class="form-group">
                                <label class="checkbox mb-0">
                                    <input type="checkbox" name="remember"
                                        {{ old('remember') ? 'checked' : '' }} />Remember
                                    Me
                                    <span></span></label>
                            </div>

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if (session('failed'))
                                <div class="alert alert-danger">
                                    {{ session('failed') }}
                                </div>
                            @endif

                            <!--begin::Action-->
                            <div class="form-group d-flex flex-wrap justify-content-between align-items-center">
                                <a href="{{ route('reset-password') }}" class="text-dark-50 text-hover-primary my-3 mr-2"
                                    id="kt_login_forgot">Forgot Password ?</a>
                                <input type="submit" id="kt_login_signin_submit"
                                    class="btn btn-primary font-weight-bold px-9 py-4 my-3">
                            </div>
                            <!--end::Action-->
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Signin-->
                    <!--begin::Signup-->
                    <div class="login-form login-signup">
                        <div class="text-center mb-10 mb-lg-20">
                            <h3 class="font-size-h1">Sign Up</h3>
                            <p class="text-muted font-weight-bold">Enter your details to create your account</p>
                        </div>
                        <!--begin::Form-->
                        <form class="form" method="post" action="{{ route('register') }}" id="signup_form">
                            @csrf
                            <input name="user_type" value="{{ Crypt::encryptString('Seller') }}" type="hidden">
                            <div class="form-group">
                                <input class="form-control form-control-solid h-auto py-5 px-6" type="text"
                                    placeholder="Full Name" name="fullname" autocomplete="off" />
                            </div>
                            <div class="form-group">
                                <input class="form-control form-control-solid h-auto py-5 px-6" type="email"
                                    placeholder="Email" name="email" autocomplete="off" />
                            </div>

                            <div class="form-group">
                                <select name="seller_package_id" id="seller_package_id"
                                    class="form-control form-control-solid h-auto py-5 px-6" required>
                                    <option value="">Select Seller Package</option>
                                    @foreach ($packages as $pkg)
                                        <option value="{{ $pkg->id }}">
                                            {{ '$' . $pkg->price . ' ' . 'for' . ' ' . $pkg->duration . ' Months' }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <input type="hidden" name="nonce" id="stripe_token" value="">

                            <div class="form-group">
                                <input class="form-control form-control-solid h-auto py-5 px-6" type="password"
                                    placeholder="Password" name="password" autocomplete="off" />
                            </div>
                            <div class="form-group">
                                <input class="form-control form-control-solid h-auto py-5 px-6" type="password"
                                    placeholder="Confirm password" name="cpassword" autocomplete="off" />
                            </div>
                            <div class="form-group">
                                <label class="checkbox mb-0">
                                    <input type="checkbox" name="agree" required/>I Agree the
                                    <a href="#">terms and conditions</a>.
                                    <span></span></label>
                            </div>

                            <div class="form-group d-flex flex-wrap flex-center">
                                <input type="submit" id="kt_login_signup_submit"
                                    class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4">
                                <button type="button" id="kt_login_signup_cancel"
                                    class="btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-4 kt_login_signup_cancel">Cancel</button>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Signup-->
                    <!--begin::Forgot-->
                    <div class="login-form login-forgot">
                        <div class="text-center mb-10 mb-lg-20">
                            <h3 class="font-size-h1">Forgotten Password ?</h3>
                            <p class="text-muted font-weight-bold">Enter your email to reset your password</p>

                        </div>
                        <!--begin::Form-->
                        <form class="form" method="post" action="{{ route('reset.password') }}"
                            id="kt_login_forgot_form">
                            @csrf
                            <div class="form-group">
                                <input class="form-control form-control-solid h-auto py-5 px-6" type="email"
                                    placeholder="Email" name="email" autocomplete="off" />
                            </div>
                            <div class="form-group d-flex flex-wrap flex-center">
                                <button type="submit" id="kt_login_signup_submit"
                                    class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4">Submit</button>
                                <button type="button" id="kt_login_forgot_cancel"
                                    class="btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-4">Cancel</button>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Forgot-->
                </div>
                <!--end::Content body-->
                <!--begin::Content footer for mobile-->
                <div
                    class="d-flex d-lg-none flex-column-auto flex-column flex-sm-row justify-content-between align-items-center mt-5 p-5">
                    <div class="text-dark-50 font-weight-bold order-2 order-sm-1 my-2">© 2020 Metronic</div>
                    <div class="d-flex order-1 order-sm-2 my-2">
                        <a href="#" class="text-dark-75 text-hover-primary">Privacy</a>
                        <a href="#" class="text-dark-75 text-hover-primary ml-4">Legal</a>
                        <a href="#" class="text-dark-75 text-hover-primary ml-4">Contact</a>
                    </div>
                </div>
                <!--end::Content footer for mobile-->
            </div>
            <!--end::Content-->
        </div>
        <!--end::Login-->
    </div>

    <div class="modal fade" id="stripe" tabindex="-1" role="dialog" aria-labelledby="stripeLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3>Pay with Stripe</h3>
                    <hr>
                    <form class="container" id="stripe-payment-form">
                        <div class="row">
                            <div class="col-sm-12">
                                <label for="card-element">
                                    Credit or debit card
                                </label>
                                <div id="card-element">
                                    <!-- A Stripe Element will be inserted here. -->
                                </div>

                                <!-- Used to display Element errors. -->
                                <div id="card-errors" role="alert"></div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12">
                                <button class="btn btn-primary pay-btn">Pay</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')

    <script>
        $('.cancel_login').hide();

        $('#kt_login_signup').click(function() {
            $('.cancel_login').show();
            $('.show_signup').hide();

        });
        $('#kt_login_signup_cancel').click(function() {
            $('.cancel_login').hide();
            $('.show_signup').show();

        });
        $('#kt_login_signup_cancel2').click(function() {
            window.location.reload();
        });

        $('.close').click(function() {
            $('#stripe').hide();
            $('#stripe').removeClass('show');
        })

        $('#signup_form').on('submit', function(e) {
            e.preventDefault();
            e.stopImmediatePropagation();
            $('#stripe').show();
            $('#stripe').addClass('show');

        });


        // Create a Stripe client.
        // Issue on route cache
        var stripe = Stripe("{{ env('STRIPE_KEY') }}");


        // Create an instance of Elements.
        var elements = stripe.elements();

        // Custom styling can be passed to options when creating an Element.
        // (Note that this demo uses a wider set of styles than the guide below.)
        var style = {
            base: {
                color: '#32325d',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        };

        // Create an instance of the card Element.
        var card = elements.create('card', {
            style: style
        });

        // Add an instance of the card Element into the `card-element` <div>.
        card.mount('#card-element');
        // Handle real-time validation errors from the card Element.
        card.on('change', function(event) {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        // Handle form submission.
        var form = document.getElementById('stripe-payment-form');
        form.addEventListener('submit', function(event) {
            event.preventDefault();
            $('.pay-btn').addClass('spinner spinner-darker-success spinner-left mr-3');
            $('.pay-btn').attr("disabled", "disabled");
            stripe.createToken(card).then(function(result) {
                if (result.error) {
                    // Inform the user if there was an error.
                    var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;
                } else {
                    // Send the token to your server.
                    stripeTokenHandler(result.token);
                }
            });
        });

        // Submit the form with the token ID.
        function stripeTokenHandler(token) {
            // Insert the token ID into the form so it gets submitted to the server
            var form = document.getElementById('signup_form');
            document.getElementById('stripe_token').value = token.id
            document.getElementById('signup_form').submit();
            //   alert(token);

            // Submit the form
            form.submit();
        }
    </script>
    <script src="{{ asset('js/pages/custom/login/login-general.js?v=7.0.4') }}"></script>
@endsection
