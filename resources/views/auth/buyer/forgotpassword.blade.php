@extends('layouts.app2')

@section('content')
    <style>
        .form-group {
            margin-bottom: 1rem;
            position: relative;
        }
        .form-group i {
            position: absolute;
            right: 20px;
            top: 10px;
        }
        .face-icon {
            position: relative;
        }

        .face-icon:after {content: '\f082';position: absolute;top: 6px;right: 190px;font-family: 'Font Awesome 5 Brands';font-size: 16px;}
        .google-icon {
            position: relative;
        }

        .google-icon:after {content: '\f0d5';position: absolute;right: 184px;font-family: 'Font Awesome 5 Brands';font-size: 16px;}


    </style>



    <!-- Full With banner -->
    <div class="full-width-banner about-us-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Forgot Password </h2>
                </div>

            </div>
        </div>

    </div>


    <!-- Our Brands Section end here -->


    <!-- About Us Last Slider Start here -->
    <div class="about-last-slider-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12">

                </div>

                <div class="col-12">
                    <div class="login-form login-signup">
                        <div class="text-center mb-10 mb-lg-20">
                            <h3 class="font-size-h1">Forgot Password?</h3>
                            <p class="text-muted font-weight-bold">Enter your email to reset your password</p>
                        </div>
                        <!--begin::Form-->
                        <form class="form" method="post" action="{{route('reset.password')}}" id="kt_login_forgot_form">
                            @csrf
                            <div class="form-group">
                                <input class="form-control form-control-solid h-auto" type="email"
                                       placeholder="Email" name="email" autocomplete="off" />
                            </div>
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if (session('failed'))
                                <div class="alert alert-danger">
                                    {{ session('failed') }}
                                </div>
                            @endif
                            <div class="form-group d-flex flex-wrap flex-center">
                                <button type="submit" id="kt_login_signup_submit"
                                        class="btn btn-primary font-weight-bold px-9 ">Submit</button>

                                <a href="{{route('login')}}">
                                    <button type="button" id=""
                                            class="btn btn-light-primary font-weight-bold px-9 ">Cancel</button>
                                </a>
                            </div>
                        </form>



                        <!--end::Form-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('form').submit(function () {
            <?php

            if(Auth::guard('seller')->check())
            {
                Auth::guard('seller')->logout();

            } else if(Auth::guard('buyer')->check())
            {
                Auth::guard('buyer')->logout();
            } else if(Auth::guard('admin')->check())
            {
                Auth::guard('admin')->logout();
            }
            ?>


        })
    </script>


    <!-- ABout Us Last Slider End here -->






@stop




