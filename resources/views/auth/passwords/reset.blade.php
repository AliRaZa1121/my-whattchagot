@extends('layout.auth')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/pages/login/classic/login-1.css?v=7.0.4') }}">
@endsection

<?php
use Illuminate\Support\Facades\Crypt;
?>

@section('content')
<div class="d-flex flex-column flex-root">
    <!--begin::Login-->
    <div class="login login-1 login-signin-on d-flex flex-column flex-lg-row flex-row-fluid bg-white" id="kt_login">
        <!--begin::Aside-->
        <div class="login-aside d-flex flex-row-auto bgi-size-cover bgi-no-repeat p-10 p-lg-10"
            style="background-color: #252525">
            <!--begin: Aside Container-->
            <div class="d-flex flex-row-fluid flex-column justify-content-between">
                <!--begin: Aside header-->
                <center>
                    <a href="#" class="flex-column-auto mt-5">
                        <img src="{{ asset('media/logos/logo.png') }}" class="max-h-100px" alt="" />
                    </a>
                </center>

                <!--end: Aside header-->
                <!--begin: Aside content-->
                <div class="flex-column-fluid d-flex flex-column justify-content-center">
                    <h3 class="font-size-h1 mb-5 text-white">Welcome to Whattchagot!</h3>
                </div>
                <!--end: Aside content-->
                <!--begin: Aside footer for desktop-->
                <div class="d-none flex-column-auto d-lg-flex justify-content-between mt-10">
                    <div class="opacity-70 font-weight-bold text-white">©
                        <?php echo date('Y') ?> <a href="{{route('index')}}">Whattchagot</a>
                    </div>
                </div>
                <!--end: Aside footer for desktop-->
            </div>
            <!--end: Aside Container-->
        </div>
        <!--begin::Aside-->
        <!--begin::Content-->
        <div class="flex-row-fluid d-flex flex-column position-relative p-7 overflow-hidden">
            <!--begin::Content header-->


            <div
                class="position-absolute top-0 right-0 text-right mt-5 mb-15 mb-lg-0 flex-column-auto justify-content-center py-5 px-10 cancel_login">
                <span class="font-weight-bold text-dark-50">Already have an account?</span>
                <a href="{{route('login')}}" class="font-weight-bold ml-2 kt_login_signup_cancel"
                    id="kt_login_signup_cancel2">Sign In!</a>
            </div>
            <!--end::Content header-->
            <!--begin::Content body-->



            <!--end::Signup-->
            <!--begin::Forgot-->
            <div class="login-form login-forgot" style="display: block; margin:auto">
                <div class="text-center mb-10 mb-lg-20">
                    <h3 class="font-size-h1">Forgotten Password ?</h3>
                    <p class="text-muted font-weight-bold">Change your password</p>
                </div>

                <!--begin::Form-->
                <form class="form" method="post" action="{{ route('password.update') }}" autocomplete="off"
                    id="kt_login_forgot_form">
                    @csrf
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="form-group">
                        <input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg font-size-h6"
                            type="email" placeholder="Email" name="email" value="{{$email}}" readonly required />
                    </div>

                    <div class="form-group">
                        <input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg font-size-h6"
                            type="password" placeholder="password" name="password" required />
                    </div>
                    <div class="form-group">
                        <input id="password-confirm" type="password"
                            class="form-control form-control-solid h-auto py-7 px-6 rounded-lg font-size-h6"
                            placeholder="confirm password" name="password_confirmation" required />
                    </div>

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if (session('status'))
                    <p class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </p>
                    @endif
                    <div class="form-group d-flex flex-wrap flex-center">
                        <button type="submit" id="kt_login_signup_submit"
                            class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4">Submit</button>

                        <a href="{{route('login')}}">
                            <button type="button" id=""
                                class="btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-4">Cancel</button>
                        </a>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Forgot-->
        </div>
        <!--end::Content body-->
        <!--begin::Content footer for mobile-->
        <div
            class="d-flex d-lg-none flex-column-auto flex-column flex-sm-row justify-content-between align-items-center mt-5 p-5">
            <div class="text-dark-50 font-weight-bold order-2 order-sm-1 my-2">© 2020 Metronic</div>
            <div class="d-flex order-1 order-sm-2 my-2">
                <a href="#" class="text-dark-75 text-hover-primary">Privacy</a>
                <a href="#" class="text-dark-75 text-hover-primary ml-4">Legal</a>
                <a href="#" class="text-dark-75 text-hover-primary ml-4">Contact</a>
            </div>
        </div>
        <!--end::Content footer for mobile-->
    </div>
    <!--end::Content-->
</div>
<!--end::Login-->
</div>


@endsection

@section('scripts')

<script>




</script>
<script src="{{asset('js/pages/custom/login/login-general.js?v=7.0.4')}}"></script>
@endsection