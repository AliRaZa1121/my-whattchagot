@extends('layouts.app2')

@section('content')
    <!-- Full With banner -->
    <?php
    use Illuminate\Support\Facades\Crypt;
    ?>
    <style>
        .active-type {
            background-color: #3e4296 !important;
            color: #fff !important;
            border-color: #3e4296 !important;
        }

    </style>



@include('layout.nav_header.nav_slider_bar.nav_slider_bar')


   <!-- Full With banner -->
<div class="full-width-banner about-us-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Top Trades
                </h2>
            </div>

        </div>
    </div>

</div>



                        <!--trade Products Row Start here-->
                        <div class="trending_products profile-page-trade top-traders-page footer-top-trade" id="product-main-products">
                            <div class="container">
                                <div class="row">

                                    <div class="trending_products_wrapper">

                                        <div class="asasa ">
                                            <div class="item">

                                                @foreach($trades as $key)

                                                    <div class="trending_single_wrapper">
                                                        <div class="trending_single_product">
                                                            <img src="{{isset($key->product_images->image) ? asset('/storage/'.$key->product_images->image): asset('/storage/product/default.jpg')}}">

                                                            <h3>{{$key->products->name ?? ''}}</h3>
                                                            <div class="trade-product-content">
                                                                <p>{{ substr(($key->products->description ?? ''), 0 ,50) }}</p>
                                                            </div>
                                                            @if($key->flags != 'trade')
                                                            <span class="ts_price">${{$key->products->price ?? ''}}</span>
                                                            @endif
                                                        </div>
                                                        <div class="tr_single_overlay">

                                                            <div class="main_slider_btns tr_single_btns best_selling_btns">

                                                                    <a href="{{url('product-details').'/'.$key->product_id }}"><i class="fa fa-info" aria-hidden="true"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>


                                                @endforeach
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>

    </div>




@stop





