@extends('layouts.app2')
@php
use App\Models\Setting;
    $setting = Setting::first();
@endphp
@section('content')
    <!-- Full With banner -->
    <?php
    use Illuminate\Support\Facades\Crypt;

    ?>

    <!--Progile Page Badge End here-->

    <!--auction detail slider section start here-->
    <!-- current auction detail section start here-->

    @include('layout.nav_header.nav_slider_bar.nav_slider_bar')
    <div class="current-auction-detail-main container">
        <div class="row">
            <div class="col-md-6 col-lg-6">
                <div class="preview-pic tab-content slider-for product-single-for-slider">
                    <div class="tab-pane active" id="pic-1"><img
                                src="{{isset($products->product_images->image) ? asset('/storage/'.$products->product_images->image): asset('/storage/default.jpg')}}"/>
                    </div>
                    @if(isset($products->product_images_many) && !empty($products->product_images_many))
                        @foreach($products->product_images_many as $images)
                            <div class="tab-pane" id="num{{ $images->id }}"><img
                                        src="{{asset('/storage/'.$images->image)}}"/></div>
                        @endforeach
                    @endif
                </div>
                <ul class="preview-thumbnail nav nav-tabs slider-nav product-signle-slider">
                    <div>
                    {{-- <li class="active"><a data-target="#pic-1" data-toggle="tab"><img
                                    src="{{isset($products->product_images->image) ? asset('/storage/'.$products->product_images->image): asset('/storage/default.jpg')}}"/></a>
                    </li> --}}
                      @if(isset($products->product_images_many) && !empty($products->product_images_many))
                        @foreach($products->product_images_many as $key => $images)
                            <li class="{{ $key == 0 ? 'active':''}}"><a data-target="#num{{ $images->id }}" data-toggle="tab">
                                <img src="{{asset('/storage/'.$images->image)}}"/></a>
                            </li>
                        @endforeach
                     @endif
                    </div>
                </ul>


            </div>

            <div class="col-md-6 col-lg-6">
                <div class="auction-liosting-detail-seller-info">
                    <h2>Seller information</h2>
                    <div class="auction-liosting-detail-seller-info-inner">
                        <div class="seller-info-inner-thumbnail">
                            <div class="seller-thumbnail">
                                <img src="<?php   echo isset(user_image($products->user_id)->image) ? asset('/storage/' . user_image($products->user_id)->image) : url('/images/auction-detail-seller-img.png') ?>">
                            </div>
                            <div class="seller-txt">
                                <h3>
                                    <a href="{{ url('profile') }}/{{ $products->users->id}}">{{ $products->users->name }}</a>
                                </h3>
                                <!-- <p>Member Since 05 Jan 2021</p> -->
                            </div>
                        </div>
                        @if(Auth()->guard('buyer')->check())
                            <form method="post"
                                  action="{{route('chat_alreadylogin')}}">
                                @csrf
                                <input type="hidden"
                                       name="product_id"
                                       class="product_id"
                                       value="{{base64_encode($products->id)}}">
                                <input type="hidden" name="receiver_id"
                                       class="price"
                                       value="{{base64_encode($products->user_id)}}">

                                <button class="detail-contact-seller after-login-chat" type="submit">Chat With Seller
                                </button>
                            </form>

                        @else
                            <a class="detail-contact-seller"
                               href="{{route('buyer_login_id',['seller'=>Crypt::encrypt($products->user_id),'product'=>Crypt::encrypt($products->id)])}}">Chat
                                With Seller</a>

                        @endif

                    </div>
                </div>

                <div class="current-acc-detail-title">
                    <!-- <div class="porduct-uploaded-time">
                        <p><i class="fas fa-clock"></i>Uploaded On 20-04-2021</p>
                    </div> -->

                    <h2>{{$products->name}}</h2>
                    <div class="auction-details-reviews">
                        <div class="auction_star_rating slider_star_rating">
                            <div class="rate">
                                @for($i=1;$i<=5;$i++)
                                    <input type="radio" id="star{{ $i }}" name="rate" value="{{ $i }}"
                                           @if(round($reviewavg)>=$i) checked @endif>
                                    <label for="star{{ $i }}" class="@if(round($reviewavg)>=$i) start_select  @endif"
                                           title="text">{{ $i }} stars</label>
                                @endfor
                            </div>
                            <div class="auction_total_reviews slider_total_reviews">
                                <a href="#">{{ count($reviewbycount)  }} Review(s)</a>
                            </div>
                        </div>

                        <div class="single-products-share">
                            <div class="latest-update">
                                <!-- <p>Last Updated:</p> <a href="#">Amazon</a> -->
                            </div>
                        </div>
                    </div>
                    <p class="auction-product-des">{{$products->short_details}}</p>

                    <div class="sizee">
                        <p>Size:</p>
                        <form class="boxed">
                            @if(!empty($products->size_values))
                                @php $countsz=0 @endphp
                                @foreach($products->size_values as $size_values)
                                    <input type="radio" id="{{  $size_values->size }}" name="product_size"
                                           value="{{  $size_values->size }}" @if($countsz==0)checked @endif>
                                    <label for="{{  $size_values->size }}">{{  $size_values->size }}</label>
                                    @php $countsz++; @endphp
                                @endforeach
                            @endif
                        </form>

                    </div>


                   <div class="sizesss">
                         <p>Color:</p>
                        <div class="custom-radios">
                            @if(!empty($products->colors))
                                @php $count=0 @endphp
                                @foreach($products->colors as $colordata)
                                    <div>
                                        <input type="radio" id="color-{{  $colordata->color }}" name="color"
                                               value="{{  $colordata->color }}" @if($count==0)checked @endif>
                                        <label for="color-{{  $colordata->color }}">
                                <span style="background-color:{{  $colordata->color }}">
                                  <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/check-icn.svg"
                                       alt="Checked Icon"/>
                                </span>
                                        </label>
                                    </div>
                                    @php $count++; @endphp
                                @endforeach
                            @endif

                        </div>
                    </div>

                    <div class="bidding-price-auc-single product-single-qu-lbl for-mob-cn">
                        <h2>Price: ${{ $products->price}}</h2>
                    </div>


                    {{-- <div class="woo-commerce-btn single-product-page-woo">
                        <button class="inci dec" onclick="decrement()">-</button>
                        <input id=demoInput type=number min=1 max=9999999 value="1">
                        <button class="inci incc" onclick="increment()">+</button>

                    </div> --}}

                    <div class="current-acc-place-bid product-single-page-cart for-mob-cn">
                        @if(Auth()->guard('buyer')->check())
                            <form class="ajax_add_cart">
                                @csrf
                                <input type="hidden"
                                       name="product_id"
                                       class="product_id"
                                       value="{{base64_encode($products->id)}}">
                                <input type="hidden" name="price" class="price"
                                       value="{{base64_encode($products->price)}}">

                                <button type="submit" class="after-login-submit">Add To Cart</button>
                            </form>

                        @else
                            <a href="{{route('add_check_out',['seller'=>Crypt::encrypt($products->user_id),'product'=>Crypt::encrypt($products->id),'price'=>Crypt::encrypt($products->price),'last_segment'=>request()->segment(count(request()->segments()))])}}">Add
                                To Cart</a>

                        @endif


                    </div>
                    <div class="wishlist-single for-mob-cn">
                        <a href="javascript:;" class="wishlist_ajax"
                           data-id="{{$products->id}}"><i
                                    class="far fa-heart"></i></a>
                    </div>


                </div>
            </div>

        </div>
    </div>

    <!-- current auction detail section End here-->

    <!-- auction details slider section end here-->



    <!--- Product SIngle Page Rating Row Start here-->
    <div class="product-single-rating-row">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="ratting-inner-box">
                        <h4>{{ round($reviewavg) }} (Rating)</h4>
                        <div class="auction_star_rating slider_star_rating">
                            <div class="rate">
                                @for($i=1;$i<=5;$i++)
                                    <input type="radio" id="star{{ $i }}" name="rate" value="{{ $i }}"
                                           @if(round($reviewavg)>=$i) checked @endif>
                                    <label for="star{{ $i }}" class="@if(round($reviewavg)>=$i) start_select  @endif"
                                           title="text">{{ $i }} stars</label>
                                @endfor
                            </div>
                            <div class="auction_total_reviews slider_total_reviews">
                                {{ $reviewall->count() }} Review(s)
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="ratting-inner-box">
                        <h4>@if(!empty($reviewsum)) {{ round(($reviewsum/($reviewall->count()*5))*100) }} @else 0 @endif
                            %</h4>
                        <h6>Of Customers Are Satisfied From Our Products</h6>
                    </div>
                </div>
                <div class="col-md-4">
                    <?php
                    $getstar5 = getreview($products->id, 5);
                    $getstar4 = getreview($products->id, 4);
                    $getstar3 = getreview($products->id, 3);
                    $getstar2 = getreview($products->id, 2);
                    $getstar1 = getreview($products->id, 1);
                    $total = $reviewbycount->count();
                    ?>
                    <div class="ratting-inner-box last-rat-right">

                        <div class="animated-progress progress-blue">

                            <span data-progress="@if(!empty($getstar5))  {{   $getstar5->rate_count / ($total /100) }}  @else 0 @endif" style="width: @if(!empty($getstar5))  {{   $getstar5->rate_count / ($total /100) }}%  @else 0 @endif"></span>
                            <h5 class="span-cla notranslate">@if(!empty($getstar5))  {{  $getstar5->rate_count }} @else
                                    0 @endif</h5>
                        </div>

                        <div class="animated-progress progress-green">
                            <span data-progress="@if(!empty($getstar4))  {{   $getstar4->rate_count / ($total /100) }}  @else 0 @endif" style="width: @if(!empty($getstar4))  {{   $getstar4->rate_count / ($total /100) }}%  @else 0 @endif"></span>
                            <h5 class="span-cla notranslate">@if(!empty($getstar4)) {{  $getstar4->rate_count }} @else
                                    0 @endif</h5>
                        </div>
                        <div class="animated-progress progress-purple">
                            <span data-progress="@if(!empty($getstar3))  {{   $getstar3->rate_count / ($total /100) }}  @else 0 @endif" style=" width: @if(!empty($getstar3))  {{   $getstar3->rate_count / ($total /100) }}%  @else 0 @endif"></span>
                            <h5 class="span-cla notranslate">@if(!empty($getstar3)) {{  $getstar3->rate_count }} @else
                                    0 @endif</h5>
                        </div>
                        <div class="animated-progress progress-red">
                            <span data-progress="@if(!empty($getstar2))  {{   $getstar2->rate_count / ($total /100) }}  @else 0 @endif" style="width: @if(!empty($getstar2))  {{   $getstar2->rate_count / ($total /100) }}%  @else 0 @endif"></span>
                            <h5 class="span-cla notranslate">@if(!empty($getstar2)) {{  $getstar2->rate_count }}  @else
                                    0 @endif</h5>
                        </div>
                        <div class="animated-progress progress-ss">
                            <span data-progress="@if(!empty($getstar1))  {{   $getstar1->rate_count / ($total /100) }}  @else 0 @endif" style="width: @if(!empty($getstar1))  {{   $getstar1->rate_count / ($total /100) }}  @else 0 @endif"></span>
                            <h5 class="span-cla notranslate">@if(!empty($getstar1)) {{  $getstar1->rate_count }} @else
                                    0 @endif</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Product Single Page rating Row End herer-->




    <!-- Auction Products details tabs section-->
    <!-- prdocut description -->
    <div class="container products-details">
        <div class="product-inner-wrapper">
            <div class="product-btn">

            </div>

            <div class="col-md-12 product-info">
                <ul id="myTab" class="nav nav-tabs nav_tabs">

                    <li class="active"><a href="#service-one" class="active show" data-toggle="tab">Description</a></li>
                    <li><a href="#service-two" data-toggle="tab">Shipping Information</a></li>
                    <li><a href="#service-three" data-toggle="tab">Review</a></li>
                    <li><a href="#service-four" data-toggle="tab">Return Policy</a></li>


                </ul>
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade active show" id="service-one">

                        <section class="auction-product-info-deta">
                            <p>{{ $products->description }}</p>
                        </section>

                    </div>
                    <div class="tab-pane fade" id="service-two">
                    <p>{{$setting->shipping_deatils}}</p>
                    </div>

                    <div class="tab-pane fade" id="service-three">

                        <div class="bg-white rounded shadow-sm p-4 mb-4 restaurant-detailed-ratings-and-reviews">
                            {{-- <a href="#" class="btn btn-outline-primary btn-sm float-right">Top Rated</a> --}}
                            <h5 class="mb-1">All Ratings and Reviews </h5>
                            @if(!empty($reviewall) &&  count($reviewall)!=0)
                                @foreach ($reviewall as $reviewdata)
                                    <div class="reviews-members pt-4 pb-4">
                                        <div class="media">
                                            <a href="#"><img alt="Generic placeholder image"
                                                             src="<?php   echo isset(user_image($reviewdata->user_id)->image) ? asset('/storage/' . user_image($reviewdata->user_id)->image) : url('/images/auction-detail-seller-img.png') ?>"
                                                             class="mr-3 rounded-pill"></a>
                                            <div class="media-body">
                                                <div class="reviews-members-header">
                                                    <div class="auction_star_rating slider_star_rating">

                                                        <h6 class="mb-1"><a class="text-black"
                                                                            href="{{ url('profile') }}/{{ $reviewdata->user_id}}">{{ $reviewdata->users->name }}</a>
                                                        </h6>
                                                        <p class="text-gray">{{ $reviewdata->created_at }}</p>


                                                        <div class="auction_star_rating slider_star_rating pro-auc-de">
                                                            <div class="rate">
                                                                @for($i=1;$i<=5;$i++)
                                                                    <input type="radio" id="star{{ $i }}" name="rate" value="{{ $i }}"
                                                                           @if(round($reviewavg)>=$i) checked @endif>
                                                                    <label for="star{{ $i }}" class="@if(round($reviewavg)>=$i) start_select  @endif"
                                                                           title="text">{{ $i }} stars</label>
                                                                @endfor
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="reviews-members-body">
                                                        <p>{{ $reviewdata->comment }}</p>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <p>No Review found</p>
                            @endif
                        </div>

                    </div>


                    <div class="tab-pane fade" id="service-four">


                    </div>

                </div>

            </div>


        </div>
    </div>
    <!--product description-->

    <!-- Auction Products Details Tabs Section end here-->
    <style>
        .trending_products_slider {
            display: block;
        }
    </style>


    <!--trade Products Row Start here-->
    <div class="trending_products profile-page-trade product-details-page-top-sell" id="top-selling-products">
        <div class="container">
            <div class="row">
                <!--top products Start here-->
                <div class="trending_products_wrapper top-products-profile">
                    <div class="tr_p_head">

                        <h3>TOP SELLING PRODUCTS</h3>
                    </div>
                    <div class="trending_products_slider owl-carousel owl-theme">

                        @if(!empty($sellers))
                            <?php $product_count = count($sellers);   $loop_num = $product_count = round(count($sellers)); $no = 4;  ?>
                            @foreach($sellers as $sellersdata)

                                <?php
                                if ($no == 4) {
                                    $no = 0;
                                    echo "<div class='item'>";
                                }
                                ?>
                                <div class="trending_single_wrapper">
                                    <div class="trending_single_product">
                                        <!-- <div class="ts_label">New</div> -->
                                        <img src="{{isset($sellersdata->product_images->image) ? asset('/storage/'.$sellersdata->product_images->image): asset('/storage/default.jpg')}}">
                                        <div class="auction_wishlist tr_s_wishlist">
                                            <a href="javascript:;" class="wishlist_ajax"
                                               data-id="{{$sellersdata->id}}"><i
                                                        class="far fa-heart"></i></a>
                                        </div>
                                        <h3>{{ $sellersdata->name }}</h3>
                                        <div class="auction_star_rating slider_star_rating">
                                            <!-- <div class="rate">
                                               <input type="radio" id="star1" name="rate" value="1">
                                               <label for="star1" title="text">1 stars</label>
                                               <input type="radio" id="star2" name="rate" value="2">
                                               <label for="star2" title="text">2 stars</label>
                                               <input type="radio" id="star3" name="rate" value="3">
                                               <label for="star3" title="text">3 stars</label>
                                               <input type="radio" id="star4" name="rate" value="4">
                                               <label for="star2" title="text">4 stars</label>
                                               <input type="radio" id="star5" name="rate" value="5">
                                               <label for="star5" title="text">5 star</label>
                                           </div> -->

                                        </div>

                                        <div class="trade-product-content">
                                            <p>{{$sellersdata->short_details}}</p>
                                        </div>
                                    </div>
                                    <div class="tr_single_overlay">

                                        <div class="main_slider_btns tr_single_btns">
                                            <a href="{{$sellersdata->flags == 'trade' ? route('trading-single',$key->id) : url('product-details').'/'.$sellersdata->id }}">View
                                                Product</a>

                                        </div>
                                    </div>
                                </div>




                                <?php if ($no == 3) {
                                    echo '</div>';
                                    $no++;
                                } else {
                                    $no++;
                                }?>

                            @endforeach
                        @endif
                    </div>
                </div>

                <!--Top Products End here-->

            </div>
        </div>
    </div>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script>
        jQuery(".nav_sidebar").click(function () {
            jQuery(".side_cat_navbar").slideToggle("slow", function () {
                // Animation complete.
            });
        });

        jQuery("#client-stories .trending_products_slider").owlCarousel({
            loop: true,
            items: 3,
            margin: 10,
            dots: false,
            nav: true,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            autoplay: true,
            autoplayTimeout: 3000,
            responsiveClass: true,
        });

        jQuery(".about-last-slider-wrapper .auction_slider_wrapper").owlCarousel({
            loop: true,
            items: 1,
            margin: 10,
            dots: true,
            nav: false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            autoplay: false,
            autoplayTimeout: 3000,
            responsiveClass: true,
        });

        function increment() {
            document.getElementById('demoInput').stepUp();
        }

        function decrement() {
            document.getElementById('demoInput').stepDown();
        }

    </script>
    <script>
        function openPage(pageName, elmnt, color) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablink");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].style.backgroundColor = "";
            }
            document.getElementById(pageName).style.display = "block";
            elmnt.style.backgroundColor = color;
        }

        // Get the element with id="defaultOpen" and click on it
        document.getElementById("defaultOpen").click();
    </script>

    <script>
        jQuery('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: true,
            asNavFor: '.slider-nav'
        });
        jQuery('.slider-nav').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            vertical: true,
            asNavFor: '.slider-for',
            dots: false,
            arrows: false,
            focusOnSelect: true
        });

        jQuery('a[data-slide]').click(function (e) {
            e.preventDefault();
            var slideno = $(this).data('slide');
            jQuery('.slider-nav').slick('slickGoTo', slideno - 1);
        });


        jQuery("#auction-details-biding .best_selling_slider").owlCarousel({
            loop: true,
            items: 1,
            margin: 10,
            dots: false,
            nav: true,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            autoplay: true,
            autoplayTimeout: 3000,
            responsiveClass: true,
        });
        jQuery("#auction-you-like .best_selling_slider").owlCarousel({
            loop: true,
            items: 1,
            margin: 10,
            dots: false,
            nav: true,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            autoplay: true,
            autoplayTimeout: 3000,
            responsiveClass: true,
        });


        jQuery("#top-selling-products .trending_products_slider").owlCarousel({
            loop: true,
            items: 1,
            margin: 10,
            dots: false,
            nav: true,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            autoplay: true,
            autoplayTimeout: 3000,
            responsiveClass: true,
        });

        jQuery("#products-you-can-trade .trending_products_slider").owlCarousel({
            loop: true,
            items: 1,
            margin: 10,
            dots: false,
            nav: true,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            autoplay: false,
            autoplayTimeout: 3000,
            responsiveClass: true,
        });

        jQuery(".animated-progress span").each(function () {
            jQuery(this).animate(
                {
                    width: jQuery(this).attr("data-progress") + "%",
                },
                1000
            );
            jQuery(this).text(jQuery(this).attr("data-progress") + "%");
        });
    </script>




    <!--trade Products Row End here-->
@stop





