
<head>
    <title>Laravel Ajax jquery Validation Tutorial</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</head>
<body>
<form method="POST" id="form" action="{{url('/sender')}}">
	@csrf
<input type="text" id="text" name="text">
<input type="submit" name="">

</form>

 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>

   <script type="text/javascript">

    $('#form').on('submit',function(event){
        event.preventDefault();

        var text = $('#text').val();

        $.ajax({
          url: "{{url('/sender')}}",
          type:"POST",
          data:{
            "_token": "{{ csrf_token() }}",
            text:text,
         
          },
          success:function(response){
            console.log(response);
          },
         });
        });
      </script>