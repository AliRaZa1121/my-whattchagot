@extends('layouts.app2')

@section('content')
    <!-- Full With banner -->
    <?php
    use Illuminate\Support\Facades\Crypt;

    ?>
    <style>
        .active-type {
            background-color: #3e4296 !important;
            color: #fff !important;
            border-color: #3e4296 !important;
        }

        /*[aria-current] span {*/
        /*    font-weight: bold;*/
        /*    background-color: skyblue !important;*/
        /*    color: white !important;*/
        /*}*/


    </style>

@php $type=""; @endphp
@if(isset($_GET['type']))
    @php $type=$_GET['type'];@endphp
@endif


@include('layout.nav_header.nav_slider_bar.nav_slider_bar')


    <div class="profile-page-badge product-page-badges">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="profile-badge-txt">
                        <ul>
                            <li>Home</li>
                            <li>Products</li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

    </div>


    <div class="slider_nav_row">
        <div class="container">
            <div class="row">
                <div class="side_cat_navbar">
                    <div class="accordion" id="faq">
                        @if(!empty($categories))
                            @foreach($categories as $category)
                                <div class="card">
                                    <div class="card-header" id="{{ $category->id }}">
                                        <a href="#" class="btn btn-header-link collapsed {{request()->cat}}" data-toggle="collapse"
                                           data-target="#faq{{ $category->id }}"
                                           aria-expanded="true"
                                           aria-controls="faq{{ $category->id }}">{{ $category->name }}</a>
                                    </div>

                                    <div id="faq{{ $category->id }}" class="collapse"
                                         aria-labelledby="{{ $category->id }}" data-parent="#faq">
                                        <div class="card-body">
                                            <ul>
                                                @if(!empty($category->categories))
                                                    @foreach($category->categories as $subcat)
                                                        <li><a href="{{ url('shop/products') }}?type={{$type}}&&sub_cat={{$subcat->id}}">{{ $subcat->name }}</a></li>
                                                    @endforeach
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="product-page-banner">
                    {{-- <img src="./images/product-page-banner.jpg"> --}}
                    <img src="{{asset(App\Helpers\Banner::get_banner_images('products', 'top'))}}">
                </div>
            </div>
        </div>
    </div>

    <!-- Product Page Main Row wrapper-->
    <div class="product-page-main-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-3">

                    <div class="filter-head">
                        <h3>FILTER BY</h3>
                    </div>
                    <nav class="filter-by" role="navigation">
                        <div class="container">

                            <div class="panel-group" id="accordion">

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                Categories
                                            </a>
                                        </h4>
                                    </div><!--/.panel-heading -->
                                    <div id="collapseOne" class="panel-collapse collapse arrow-hide in show">
                                        <div class="panel-body">
                                            <div class="panel-group" id="nested11">

                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a href="{{ url('shop/products') }}?type={{$type}}">
                                                                ALL
                                                            </a>
                                                        </h4>
                                                    </div><!--/.panel-heading -->

                                                </div><!-- /.panel -->
                                                @foreach($categories as $cat)
                                                    <div class="panel panel-default {{ $cat->id==request()->cat ? 'cat_active' : ''}}">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a href="{{ url('shop/products') }}?type={{$type}}&&cat={{$cat->id}}">
                                                                    {{$cat->name}}
                                                                </a>
                                                            </h4>
                                                        </div><!--/.panel-heading -->

                                                    </div><!-- /.panel -->
                                                @endforeach


                                            </div><!-- /.panel-group -->
                                        </div><!--/.panel-body -->
                                    </div><!--/.panel-collapse -->
                                </div><!-- /.panel -->

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="" data-toggle="collapse" data-parent="#accordion"
                                               href="#collapseTwo">
                                                Brands

                                            </a>
                                        </h4>
                                    </div><!--/.panel-heading -->
                                    <div id="collapseTwo" class="panel-collapse arrow-hide collapse show">
                                        <div class="panel-body">

                                            <!-- nested -->


                                            <div class="panel-group" id="nested">

                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#nested"
                                                               href="#nested-collapseOne">
                                                                ALL
                                                            </a>
                                                        </h4>
                                                    </div><!--/.panel-heading -->
                                                    @foreach($brand as $brands)
                                                        <div class="panel panel-default {{ $brands->id==request()->brand ? 'cat_active' : ''}}">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a
                                                                            href="{{ url('shop/products') }}?type={{$type}}&&brand={{$brands->id}}">
                                                                        {{$brands->name}}
                                                                    </a>
                                                                </h4>
                                                            </div><!--/.panel-heading -->

                                                        </div><!-- /.panel -->
                                                    @endforeach
                                                </div><!-- /.panel -->


                                            </div><!-- /.panel-group -->


                                            <!-- nested -->


                                        </div><!--/.panel-body -->
                                    </div><!--/.panel-collapse -->
                                </div><!-- /.panel -->


                            </div><!-- /.panel-group -->

                        </div><!-- /.container -->
                    </nav>
                        @php
                        if(isset($_GET['min_value']) && isset($_GET['max_value']))
                        {
                        $minvalue=$_GET['min_value'];
                        $maxvalue=$_GET['max_value'];
                        }else{
                        $minvalue=25000;
                        $maxvalue=50000;
                        }
                        $getcategory ="All";
                        if(isset($_GET['cat']) && !empty($_GET['cat']))
                        {
                        $getcategory =$categories_name->name;
                        }
                        if(isset($_GET['sub_cat']) && !empty($_GET['sub_cat']))
                        {
                            $getcategory =$sub_categories_name->name;
                        }
                        @endphp
                    <form class="form" id="form" method="GET"  action="{{ url('shop/products') }}" >
                    <div class="price-filterss">
                        <h2>Price Filter</h2>
                        <div class="range-slider">
                            <input value="{{$minvalue}}" min="1"  max="120000" step="5" type="range" name="min_value"/>
                            <input value="{{$maxvalue}}" min="1"   max="120000" step="5" type="range" name="max_value"/>
                            <span>
                        <input type="number" id="price1" value="{{$minvalue}}" min="1" max="120000"/>to
                        <input type="number" value="{{$maxvalue}}" min="1" max="120000" /></span>
                                <svg width="100%" height="24">
                                    <line x1="4" y1="0" x2="300" y2="0" stroke="#444" stroke-width="12"
                                          stroke-dasharray="1 28"></line>
                                </svg>
                            </div>

                            <!-- <div id="slider-range"></div> -->

                        </div>
                        <input type="hidden" name="type" value="{{$type}}">
                        <!-- <input type="hidden" name="min-value" value="" id="min-value">
                        <input type="hidden" name="max-value" value="" id="max-value"> -->
                        <button type="submit" class="btn btn-primary font-weight-bolder">Submit</button>
                    </form>
                    <div class="prouct-page-add">
                        {{-- <img src="./images/product-page-side-add.png"> --}}
                        <img src="{{asset(App\Helpers\Banner::get_banner_images('products', 'left-1'))}}">
                    </div>
                    <div class="prouct-page-add">
                        {{-- <img src="./images/product-page-side-add.png"> --}}
                        <img src="{{asset(App\Helpers\Banner::get_banner_images('products', 'left-2'))}}">
                    </div>

                </div>
                <div class="col-md-9">
                    <div class="product-main-page">
                        <div class="fashion-heading">
                            <h2>{{ $getcategory }}</h2>
                        </div>
                        <div class="products-filter-btns">
                            <a href="{{ url('/shop/products') }}?type=list" @if($type=="list") class="active-type" @endif style="display: inline-block;">All Listings</a>
                            <a href="{{ url('/shop/products') }}?type=selling" @if($type=="selling") class="active-type" @endif style="display: inline-block;">Selling Products</a>
                            <a href="{{ url('/shop/products') }}?type=trade" @if($type=="trade") class="active-type" @endif style="display: inline-block;">Trade Products</a>
                            <a href="{{ url('/shop/products') }}?type=auction" @if($type=="auction") class="active-type" @endif style="display: inline-block;">Auction Products</a>

                        </div>
                        <div class="row sorting-div">
                            <div class="col-md-6 sorting-col1">
                                <div class="all-show-items same-line-style">
                                    <p>Showing {{ $products->total()}}</p>
                                </div>

                            </div>

                            <div class="col-md-6 sorting-col1">
                                <div class="sort-drop-down same-line-style">
                                    <p>Sort By:</p>
                                    <div class="border-u">
                                        <form class="form" id="form" method="GET" action="{{ url('shop/products') }}">
                                            <input type="hidden" name="type" value="{{$type}}">
                                            <select class="form-control1 form-control-sm" name="orderby"
                                                    id="categories_for_redirection" onchange="this.form.submit()">
                                                <option value="">Select</option>
                                                <option value="asc"
                                                        @if(isset($_GET['orderby']) && $_GET['orderby']=="asc"))
                                                        selected @endif>Asc
                                                </option>
                                                <option value="desc"
                                                        @if(isset($_GET['orderby']) && $_GET['orderby']=="desc"))
                                                        selected @endif>Desc
                                                </option>
                                            </select>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--trade Products Row Start here-->
                        <div class="trending_products profile-page-trade" id="product-main-products">
                            <div class="container">
                                <div class="row">

                                    <div class="trending_products_wrapper">

                                        <div class="asasa ">
                                            <div class="item">

                                                @foreach($products as $key)

                                                @php
                                                switch (isset($key->product) ? $key->flags : 'N/A') {
                                                    case('trade'):
                                                     $route = route('trading-single', $key->id);
                                                    break;
                                                      case("auction"):
                                                       $route = route('auction-single', $key->id);
                                                        break;   
                                                          case("selling"):
                                                           $route = route('product-details', $key->id);
                                                            break;
                                                                default:
                                                                    $route = '#';
                                                                    break;
                                                }
                                                @endphp
                                                 
                                                    <div class="trending_single_wrapper">
                                                        <div class="trending_single_product">
                                                            <img src="{{isset($key->product_images->image) ? asset('/storage/'.$key->product_images->image): asset('/storage/product/default.jpg')}}">
                                                            <div class="auction_wishlist tr_s_wishlist">
                                                                <a href="javascript:;" class="wishlist_ajax"
                                                                   data-id="{{$key->id}}"><i
                                                                            class="far fa-heart"></i></a>
                                                            </div>
                                                                <?php
                                                                $review_val = reviewbycount($key->id);
                                                                if(!empty($review_val)){
                                                                $review_data = $review_val['rate']/$review_val['rate_count'];
                                                                }else{
                                                                    $review_data=0;
                                                                }
                                                                ?>
                                                            <h3>{{$key->name}}</h3>
                                                            <div class="auction_star_rating slider_star_rating">
                                                                <div class="rate">
                                                                    @for($i=1;$i<=5;$i++)
                                                                        <input type="radio" id="star{{ $i }}" name="rate" value="{{ $i }}"
                                                                               @if( $review_data>=$i) checked @endif>
                                                                        <label for="star{{ $i }}" class="@if( $review_data>=$i) start_select  @endif"
                                                                               title="text">{{ $i }} stars</label>
                                                                    @endfor
                                                                </div>

                                                            </div>
                                                            <div class="trade-product-content">
                                                                <p>{{ substr($key->description, 0 ,50) }}</p>
                                                            </div>
                                                            @if($key->flags != 'trade')
                                                            <span class="ts_price">${{$key->price}}</span>
                                                            @endif
                                                        </div>
                                                        <div class="tr_single_overlay">

                                                            <div class="main_slider_btns tr_single_btns best_selling_btns">
                                                                @auth
                                                                @if($key->flags == 'selling' && auth()->user()->user_type === "user")
                                                                <form class="ajax_add_cart">
                                                                    @csrf
                                                                    <input type="hidden"
                                                                           name="product_id"
                                                                           class="product_id"
                                                                           value="{{base64_encode($key->id)}}">
                                                                    <input type="hidden" name="price" class="price"
                                                                           value="{{base64_encode($key->price)}}">
                                                                    <button type="submit">Add To Cart</button>
                                                                </form>
                                                                @endif    
                                                                @endauth
                                                                                     
                                                                <a href="{{ $route }}">
                                                                    <i class="fa fa-info" aria-hidden="true"></i>
                                                                </a>

                                                            </div>
                                                        </div>
                                                    </div>
                                                


                                                @endforeach
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>


                        {!! $products->withQueryString()->links() !!}

                    </div>
                </div>
            </div>
        </div>

    </div>
    <!--trade Products Row Start here-->
    <div class="trending_products profile-page-trade" id="top-selling-products">
        <div class="container">
            <div class="row">
                <!--top products Start here-->
                <div class="trending_products_wrapper top-products-profile">
                    <div class="tr_p_head">
                        <h3>TOP SELLING PRODUCTS</h3>
                    </div>
                    <div class="trending_products_slider owl-carousel owl-theme">

                        @if(!empty($sellers))
                            <?php $product_count = count($sellers);   $loop_num =$product_count = round(count($sellers)); $no=4;  ?>
                            @foreach($sellers as $sellersdata)

                                <?php
                                if($no==4){
                                    $no=0;
                                    echo "<div class='item'>";
                                }
                                ?><div class="trending_single_wrapper">
                                    <div class="trending_single_product">
                                        <!-- <div class="ts_label">New</div> -->
                                        <img src="{{isset($sellersdata->product_images->image) ? asset('/storage/'.$sellersdata->product_images->image): asset('/storage/default.jpg')}}">
                                        <div class="auction_wishlist tr_s_wishlist">
                                            <a href="javascript:;" class="wishlist_ajax"
                                               data-id="{{$sellersdata->id}}"><i
                                                        class="far fa-heart"></i></a>
                                        </div>
                                        <h3>{{ $sellersdata->name }}</h3>
                                        <div class="auction_star_rating slider_star_rating">
                                            <!-- <div class="rate">
                                               <input type="radio" id="star5" name="rate" value="5">
                                               <label for="star5" title="text">5 stars</label>
                                               <input type="radio" id="star4" name="rate" value="4">
                                               <label for="star4" title="text">4 stars</label>
                                               <input type="radio" id="star3" name="rate" value="3">
                                               <label for="star3" title="text">3 stars</label>
                                               <input type="radio" id="star2" name="rate" value="2">
                                               <label for="star2" title="text">2 stars</label>
                                               <input type="radio" id="star1" name="rate" value="1">
                                               <label for="star1" title="text">1 star</label>
                                           </div> -->

                                        </div>

                                        <div class="trade-product-content">
                                            <p>{{substr($sellersdata->short_details,0,50)}}</p>

                                        </div>
                                    </div>
                                    <div class="tr_single_overlay">

                                        <div class="main_slider_btns tr_single_btns">
                                            <a href="{{$sellersdata->flags == 'trade' ? route('trading-single',$key->id) : url('product-details').'/'.$sellersdata->id }}">View Product</a>

                                        </div>
                                    </div>
                                </div>




                                <?php if($no==3){ echo '</div>'; $no++; } else { $no++; }?>

                            @endforeach
                        @endif
                    </div>
                </div>

                <!--Top Products End here-->

            </div>
        </div>
    </div>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

{{--        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>--}}
    <script>




        (function () {

            var parent = document.querySelector(".range-slider");
            if (!parent) return;

            var
                rangeS = parent.querySelectorAll("input[type=range]"),
                numberS = parent.querySelectorAll("input[type=number]");

            rangeS.forEach(function (el) {
                el.oninput = function () {
                    var slide1 = parseFloat(rangeS[0].value),
                        slide2 = parseFloat(rangeS[1].value);

                    if (slide1 > slide2) {
                        [slide1, slide2] = [slide2, slide1];
                        // var tmp = slide2;
                        // slide2 = slide1;
                        // slide1 = tmp;
                    }

                    numberS[0].value = slide1;
                    numberS[1].value = slide2;
                }
            });

            numberS.forEach(function (el) {
                el.oninput = function () {
                    var number1 = parseFloat(numberS[0].value),
                        number2 = parseFloat(numberS[1].value);

                    if (number1 > number2) {
                        var tmp = number1;
                        numberS[0].value = number2;
                        numberS[1].value = tmp;
                    }

                    rangeS[0].value = number1;
                    rangeS[1].value = number2;
                    jQuery("#min-value").val("£" + number1);
                    jQuery("#max-value").val("£" + number1);
                    jQuery("#price1").val("£" + number1);

                }
            });

        })();



         /*** Trade Page Slider */
    jQuery("#top-selling-products .trending_products_slider").owlCarousel({
        loop: true,
        items: 1,
        margin: 10,
        dots: false,
        nav: true,
        navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
        autoplay: true,
        autoplayTimeout: 9000,
        responsiveClass: true,
        autoplaySpeed: 2000,
        responsive: {
            0: {
                items: 1,
                autoHeight: true,
                nav: true
            },
            600: {
                items: 1,
                autoHeight: true,
                nav: false
            },
            1000: {
                items: 1,
                nav: true,
                autoHeight: false
            }
        }
    });


    /** Trade pagee slider end here  */

    </script>
@stop





