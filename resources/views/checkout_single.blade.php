@extends('layouts.app2')

@section('content')
<script type="text/javascript" src="https://js.stripe.com/v3/"></script>
    <style>
        .panel-title {
            display: inline;
            font-weight: bold;
        }
        .display-table {
            display: table;
        }
        .display-tr {
            display: table-row;
        }
        .display-td {
            display: table-cell;
            vertical-align: middle;
            width: 61%;
        }
        p.text-sm.text-gray-700.leading-5 {
            font-size: 14px;
        }
        span.relative.z-0.inline-flex.shadow-sm.rounded-md span.relative.inline-flex.items-center.px-4.py-2.-ml-px.text-sm.font-medium.text-gray-500.bg-white.border.border-gray-300.cursor-default.leading-5 {
            font-size: 12px !important;
            padding: 9px 14px !important;
        }
        nav.flex.items-center.justify-between svg {
            width: 14px !important;
            height: 11px !important;
            position: relative;
            right: -3px;
        }
        span.relative.z-0.inline-flex.shadow-sm.rounded-md span.relative.inline-flex.items-center.px-2.py-2.text-sm.font-medium.text-gray-500.bg-white.border.border-gray-300.cursor-default.rounded-l-md.leading-5 {
            padding: 5px 8px !important;
            line-height: 0 !important;
            position: relative;
            top: 2px;
        }
        span.relative.z-0.inline-flex.shadow-sm.rounded-md a.relative.inline-flex.items-center.px-4.py-2.-ml-px.text-sm.font-medium.text-gray-700.bg-white.border.border-gray-300.leading-5.hover\:text-gray-500.focus\:z-10.focus\:outline-none.focus\:ring.ring-gray-300.focus\:border-blue-300.active\:bg-gray-100.active\:text-gray-700.transition.ease-in-out.duration-150 {
            font-size: 12px !important;
            padding: 9px 13px !important;
        }
        span.relative.z-0.inline-flex.shadow-sm.rounded-md a.relative.inline-flex.items-center.px-2.py-2.text-sm.font-medium.text-gray-500.bg-white.border.border-gray-300.rounded-l-md.leading-5.hover\:text-gray-400.focus\:z-10.focus\:outline-none.focus\:ring.ring-gray-300.focus\:border-blue-300.active\:bg-gray-100.active\:text-gray-500.transition.ease-in-out.duration-150 {
            padding: 5px 5px !important;
            position: relative;
            top: 2px;
        }
        span.relative.z-0.inline-flex.shadow-sm.rounded-md span.relative.inline-flex.items-center.px-2.py-2.-ml-px.text-sm.font-medium.text-gray-500.bg-white.border.border-gray-300.cursor-default.rounded-r-md.leading-5 {
            padding: 4px 7px !important;
            position: relative;
            top: 2px;
            line-height: 0 !important;
        }
        nav.flex.items-center.justify-between svg {
            width: 13px !important;
            height: 11px !important;
            position: relative;
            right: 0;
        }
        span.relative.z-0.inline-flex.shadow-sm.rounded-md a.relative.inline-flex.items-center.px-2.py-2.-ml-px.text-sm.font-medium.text-gray-500.bg-white.border.border-gray-300.rounded-r-md.leading-5.hover\:text-gray-400.focus\:z-10.focus\:outline-none.focus\:ring.ring-gray-300.focus\:border-blue-300.active\:bg-gray-100.active\:text-gray-500.transition.ease-in-out.duration-150 {
            padding: 5px 9px !important;
            position: relative;
            top: 2px;
        }
        span.relative.inline-flex.items-center.px-2.py-2.text-sm.font-medium.text-gray-500.bg-white.border.border-gray-300.cursor-default.rounded-l-md.leading-5 svg.w-5.h-5 {
            right: -2px;
        }
        .bottom-pagination {
            margin: 20px 0 30px 0;
        }

          /**
            * The CSS shown here will not be introduced in the Quickstart guide, but shows
            * how you can use CSS to style your Element's container.
            */
            .StripeElement {
            box-sizing: border-box;

            height: 40px;
            width: 100%;
            padding: 10px 12px;

            border: 1px solid transparent;
            border-radius: 4px;
            background-color: #e6ebf1;

            box-shadow: 0 1px 3px 0 #e6ebf1;
            -webkit-transition: box-shadow 150ms ease;
            transition: box-shadow 150ms ease;
        }

        .StripeElement--focus {
            box-shadow: 0 1px 3px 0 #cfd7df;
        }

        .StripeElement--invalid {
            border-color: #fa755a;
        }

        .StripeElement--webkit-autofill {
            background-color: #fefde5 !important;
        }

        #card-errors {
            color: orangered
        }
    </style>
    <!--checkout Page Start here-->
    @include('layout.nav_header.nav_slider_bar.nav_slider_bar')
    <section class="checkoutForm">
        <div class="container">
            <div class="row">
                <div class="col-md-4 order-md-2 mb-4">
                    <h4 class="d-flex justify-content-between align-items-center mb-3">
                        <span class="text-dark">Your Cart</span>

                    </h4>


                    <p>Shipment price depend upon to your area code</p>
                    <ul class="list-group mb-3">
                        
                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                <div>
                                    <h6 class="my-0">{{$checkout->products->name}}</h6>
                                    <small class="text-muted">QTY: {{$checkout->qty}}</small>
                                    <small class="text-muted price-class">Price: ${{ $checkout_total }}</small>
                                    <small class="text-muted">Type: {{$checkout->type }}</small><br>
                                    <small class="text-muted">Shipping price: $<span id="shipment_price">0</span></small><br>
                                    <small class="text-muted">Trader Shipping price: $<span id="trader_shipment_price">0</span></small>
                                </div>
                            </li>

                          


                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h6 class="my-0">Total</h6>
                            </div>
                            <span class="text-muted" id="total_amount">${{$checkout_total}}</span>
                        </li>


                    </ul>

                  
                </div>
                <div class="col-md-8 order-md-1">
                    <h4 class="mb-3">Billing Address</h4>
                    @if(Session::has('msg'))
                    <p class="alert alert-danger">{{ Session::get('msg') }}</p>
                    @endif
                    <form role="form" action="{{ route('checkout.single.post') }}" method="post" id="checkout_form" 
                          data-cc-on-file="false"
                          data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"
                          id="payment-form">
                        @csrf
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="firstName">First Name</label>
                                <input type="text" name="billing_firstname"  class="form-control" id="firstName" placeholder="" value="" required>
                                <div class="invalid-feedback" >
                                    Valid first name is required.

                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="lastName">Last Name</label>
                                <input type="text" class="form-control" id="lastName" name="billing_lastname" placeholder="" value="" required>
                                <div class="invalid-feedback">
                                    Valid last name is required.
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="checkout_id" value="{{ $checkout->id }}">

                        <div class="mb-3">
                            <label for="email">Email <span class="text-muted"> (Optional)</span></label>
                            <input type="email" class="form-control" id="email" name="billing_email" placeholder="you@example.com">
                            <div class="invalid-feedback">
                                Please enter a valid email address for shipping updates.
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="phone">Phone <span class="text-muted">(Optional)</span></label>
                            <input type="number" class="form-control" id="phone" name="billing_phone" placeholder="">
                            <div class="invalid-feedback">
                                Please enter a valid phone number  for shipping updates.
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="address">Address</label>
                            <input type="text" class="form-control" id="address" name="billing_address" placeholder="1234 Main St" required>
                            <div class="invalid-feedback" >
                                Please enter your shipping address.
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="address2">Address 2 <span class="text-muted">(Optional)</span></label>
                            <input type="text" class="form-control" name="billing_address2" id="address2" placeholder="Apartment or suite">
                        </div>

                        <div class="row">
                            <div class="col-md-5 mb-3">
                                <label for="country">Country</label>
                                <select class="custom-select d-block w-100" name="billing_country" id="country" required>
                                    <option value="USA" selected>United States</option>
                                </select>
                                <div class="invalid-feedback">
                                    Please select a valid country.
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="state">State</label>
                                <select class="custom-select d-block w-100" name="billing_state" id="state" required>
                                    <option value="">Choose...</option>
                                    <option value="AL">Alabama</option>
                                    <option value="AK">Alaska</option>
                                    <option value="AZ">Arizona</option>
                                    <option value="AR">Arkansas</option>
                                    <option value="CA">California</option>
                                    <option value="CO">Colorado</option>
                                    <option value="CT">Connecticut</option>
                                    <option value="DE">Delaware</option>
                                    <option value="DC">District Of Columbia</option>
                                    <option value="FL">Florida</option>
                                    <option value="GA">Georgia</option>
                                    <option value="HI">Hawaii</option>
                                    <option value="ID">Idaho</option>
                                    <option value="IL">Illinois</option>
                                    <option value="IN">Indiana</option>
                                    <option value="IA">Iowa</option>
                                    <option value="KS">Kansas</option>
                                    <option value="KY">Kentucky</option>
                                    <option value="LA">Louisiana</option>
                                    <option value="ME">Maine</option>
                                    <option value="MD">Maryland</option>
                                    <option value="MA">Massachusetts</option>
                                    <option value="MI">Michigan</option>
                                    <option value="MN">Minnesota</option>
                                    <option value="MS">Mississippi</option>
                                    <option value="MO">Missouri</option>
                                    <option value="MT">Montana</option>
                                    <option value="NE">Nebraska</option>
                                    <option value="NV">Nevada</option>
                                    <option value="NH">New Hampshire</option>
                                    <option value="NJ">New Jersey</option>
                                    <option value="NM">New Mexico</option>
                                    <option value="NY">New York</option>
                                    <option value="NC">North Carolina</option>
                                    <option value="ND">North Dakota</option>
                                    <option value="OH">Ohio</option>
                                    <option value="OK">Oklahoma</option>
                                    <option value="OR">Oregon</option>
                                    <option value="PA">Pennsylvania</option>
                                    <option value="RI">Rhode Island</option>
                                    <option value="SC">South Carolina</option>
                                    <option value="SD">South Dakota</option>
                                    <option value="TN">Tennessee</option>
                                    <option value="TX">Texas</option>
                                    <option value="UT">Utah</option>
                                    <option value="VT">Vermont</option>
                                    <option value="VA">Virginia</option>
                                    <option value="WA">Washington</option>
                                    <option value="WV">West Virginia</option>
                                    <option value="WI">Wisconsin</option>
                                    <option value="WY">Wyoming</option>

                                </select>
                                <div class="invalid-feedback">
                                    Please provide a valid state.
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="billing_zip">Zip</label>
                                <input type="number" maxlength="5" class="form-control" id="billing_zip" name="billing_zip" placeholder="" required>
                                <div class="invalid-feedback">
                                    Zip code required.
                                </div>
                                <div class="billing_zip_wrong" id="billing_zip_wrong" style="display: none; color: red">
                                    Zip code is not found.
                                </div>
                            </div>
                        </div>
                        <hr class="mb-4">

                        <div class="custom-control custom-checkbox">
                            <input type="checkbox"
                                   class="custom-control-input" {{ isset($shipping->id) ? 'checked' : '' }} id="shipping-address" value="1" name="shipping_checked">
                            <label class="custom-control-label" for="shipping-address">Ship to different address?</label>
                            <div id="shippingAddress">
                                <div class="row">
                                    <div class="col-md-6 mb-3">
                                        <label for="shipping_firstname">First Name</label>
                                        <input type="text" class="form-control" id="shipping_firstname" placeholder="" value="{{ isset($shipping->firstname) ? $shipping->firstname : '' }}"
                                               required  name="shipping_firstname">
                                        <div class="invalid-feedback">
                                            Valid first name is required.
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label for="shipping_lastname">Last Name</label>
                                        <input type="text" class="form-control" id="shipping_lastname" placeholder="" value="{{ isset($shipping->lastname) ? $shipping->lastname : '' }}"
                                               required name="shipping_lastname">
                                        <div class="invalid-feedback">
                                            Valid last name is required.
                                        </div>
                                    </div>
                                </div>



                                <div class="mb-3">
                                    <label for="shipping_email">Email <span class="text-muted"> (Optional)</span></label>
                                    <input type="email" class="form-control" id="shipping_email" placeholder="you@example.com" value="{{ isset($shipping->email) ? $shipping->email : '' }}" >
                                    <div class="invalid-feedback" name="shipping_email">
                                        Please enter a valid email address for shipping updates.
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label for="shipping_phone">Phone <span class="text-muted"> (Optional)</span></label>
                                    <input type="number" class="form-control" id="shipping_phone" name="shipping_phone" value="{{ isset($shipping->phone) ? $shipping->phone : '' }}">
                                    <div class="invalid-feedback">
                                        Please enter a valid phone number  for shipping updates.
                                    </div>
                                </div>

                                <div class="mb-3">
                                    <label for="shipping_address">Address</label>
                                    <input type="text" class="form-control" id="shipping_address" placeholder="1234 Main St"
                                           required name="shipping_address" value="{{ isset($shipping->address) ? $shipping->address : '' }}">
                                    <div class="invalid-feedback">
                                        Please enter your shipping address.
                                    </div>
                                </div>

                                <div class="mb-3">
                                    <label for="shipping_address2">Address 2 <span class="text-muted"> (Optional)</span></label>
                                    <input type="text" class="form-control" id="shipping_address2"
                                           placeholder="Apartment or suite" name="shipping_address2" value="{{ isset($shipping->address_optional) ? $shipping->address_optional : '' }}">
                                </div>

                                <div class="row">
                                    <div class="col-md-5 mb-3">
                                        <label for="shipping_country">Country</label>
                                        <select class="custom-select d-block w-100" name="shipping_country" id="shipping_country" required>
                                            {{-- <option value="">Choose...</option> --}}
                                            <option {{ isset($shipping->country) ? $shipping->country=="United States" ? 'selected' : '' : '' }}>United States</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please select a valid country.
                                        </div>
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label for="shipping_state">State</label>
                                        <select class="custom-select d-block w-100" id="shipping_state" name="shipping_state" required>
                                            <option value="">Choose...</option>
                                            <option value="AL">Alabama</option>
                                            <option value="AK">Alaska</option>
                                            <option value="AZ">Arizona</option>
                                            <option value="AR">Arkansas</option>
                                            <option value="CA">California</option>
                                            <option value="CO">Colorado</option>
                                            <option value="CT">Connecticut</option>
                                            <option value="DE">Delaware</option>
                                            <option value="DC">District Of Columbia</option>
                                            <option value="FL">Florida</option>
                                            <option value="GA">Georgia</option>
                                            <option value="HI">Hawaii</option>
                                            <option value="ID">Idaho</option>
                                            <option value="IL">Illinois</option>
                                            <option value="IN">Indiana</option>
                                            <option value="IA">Iowa</option>
                                            <option value="KS">Kansas</option>
                                            <option value="KY">Kentucky</option>
                                            <option value="LA">Louisiana</option>
                                            <option value="ME">Maine</option>
                                            <option value="MD">Maryland</option>
                                            <option value="MA">Massachusetts</option>
                                            <option value="MI">Michigan</option>
                                            <option value="MN">Minnesota</option>
                                            <option value="MS">Mississippi</option>
                                            <option value="MO">Missouri</option>
                                            <option value="MT">Montana</option>
                                            <option value="NE">Nebraska</option>
                                            <option value="NV">Nevada</option>
                                            <option value="NH">New Hampshire</option>
                                            <option value="NJ">New Jersey</option>
                                            <option value="NM">New Mexico</option>
                                            <option value="NY">New York</option>
                                            <option value="NC">North Carolina</option>
                                            <option value="ND">North Dakota</option>
                                            <option value="OH">Ohio</option>
                                            <option value="OK">Oklahoma</option>
                                            <option value="OR">Oregon</option>
                                            <option value="PA">Pennsylvania</option>
                                            <option value="RI">Rhode Island</option>
                                            <option value="SC">South Carolina</option>
                                            <option value="SD">South Dakota</option>
                                            <option value="TN">Tennessee</option>
                                            <option value="TX">Texas</option>
                                            <option value="UT">Utah</option>
                                            <option value="VT">Vermont</option>
                                            <option value="VA">Virginia</option>
                                            <option value="WA">Washington</option>
                                            <option value="WV">West Virginia</option>
                                            <option value="WI">Wisconsin</option>
                                            <option value="WY">Wyoming</option>

                                        </select>
                                        <div class="invalid-feedback">
                                            Please provide a valid state.
                                        </div>
                                    </div>
                                    <div class="col-md-3 mb-3">
                                        <label for="shipping_zip">Zip</label>
                                        <input type="number" maxlength="5" class="form-control"  name="shipping_zip" value="{{ isset($shipping->zip) ? $shipping->zip : '' }}" id="zip" placeholder="" required>
                                        <div class="invalid-feedback">
                                            Zip code required.
                                        </div>
                                        <div class="zip_wrong" id="zip_wrong" style="display: none; color: red">
                                            Zip code is not found.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="mb-4">

                        <h4 class="mb-3">Payment</h4>
                        <div class="payment-radioss">
                             <input type="radio" name="payment_method" value="old" {{ ($card !== null) ? 'checked' :  'disabled' }} >
                             <label for="css">Your Account</label><br>
                             <input type="radio" id="javascript" name="payment_method" value="new" {{ ($card == null) ? 'checked' :  '' }}>
                             <label for="javascript">New Card</label>
                        </div>
                        <hr>

                    <div class="payment-wrapper old-card">
                        @if($card !== null)
                        <div class='form-row row'>
                            <div class='col-xs-12 col-md-4 form-group'>
                                <label class='control-label'>Card Number</label>
                                <input autocomplete='off'  class='form-control' disabled value="{{ $card->brand .' || *********'. $card->last4 }}">
                            </div>
                            <div class='col-xs-12 col-md-4 form-group'>
                                <label class='control-label'>Expiration Month</label> <input autocomplete='off'  class='form-control' disabled value="{{ $card->exp_month }}">
                            </div>
                            <div class='col-xs-12 col-md-4 form-group'>
                                <label class='control-label'>Expiration Year</label><input autocomplete='off'  class='form-control' disabled value="{{ $card->exp_year }}">
                            </div>
                        </div>
                        @endif

                    </div>



                    <!-- second payment method -->

                    <div class="payment-wrapper new-card">
                        <div class="row">
                            <div class="col-sm-12">
                                <label for="card-element">
                                    Credit or debit card
                                </label>
                                <div id="card-element">
                                    <!-- A Stripe Element will be inserted here. -->
                                </div>

                                <!-- Used to display Element errors. -->
                                <div id="card-errors" role="alert"></div>
                            </div>
                        </div>
            

                    </div>


                    <input type='hidden' name='stripeToken' id="stripe_token" />


                        <div class='form-row row'>
                            <div class='col-md-12 error form-group hide'>
                                <div class='alert-danger alert' id="error_stripe" style="display: none">Please correct the errors and try
                                    again.</div>
                            </div>
                        </div>
                        <input name="amount" type="hidden"  id="total_input_amount" value="{{($checkout_total)}}">

                        <div class="row pay-now-checkout">
                            <div class="col-md-12">
                                <button class="btn btn-primary btn-lg btn-block"  id="pay_now" type="submit">Pay Now</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>

    <script>
        $('.number_only').bind('keyup paste', function(){
            this.value = this.value.replace(/[^0-9]/g, '');
        });
    </script>
    
    <script type="text/javascript">


        // Create a Stripe client.
        // Issue on route cache
        var stripe = Stripe("{{ env('STRIPE_KEY') }}");


        // Create an instance of Elements.
        var elements = stripe.elements();

        // Custom styling can be passed to options when creating an Element.
        // (Note that this demo uses a wider set of styles than the guide below.)
        var style = {
            base: {
                color: '#32325d',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        };

        // Create an instance of the card Element.
        var card = elements.create('card', {
            style: style
        });

        // Add an instance of the card Element into the `card-element` <div>.
        card.mount('#card-element');
        // Handle real-time validation errors from the card Element.
        card.on('change', function(event) {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });


     


        $(Document).ready(function (){
           
            if($('input[name="payment_method"]:checked').val() == "old"){
               $(".old-card").show();
               $(".new-card").hide();
            }
            else{
               $(".old-card").hide();
               $(".new-card").show();
            }
        
        });
            

            $('#billing_zip').change(function (){

                var zip =$('#zip').val();
                var checkout_total = "{{ $checkout_total }}";
                var total = 0;

                if(zip== ''){
                var zip =$('#billing_zip').val();
                var checkout = "{{ $checkout->id }}";
                var path = `{{ url('/') }}`
                $('#pay_now').attr('disabled' , true);
                $.ajax({
                    type:     "get",
                    cache:    false,
                    url:      `${path}/shipping_ajax/single/${checkout}/${zip}`,
                    error: function (request, error) {
                        $('.billing_zip_wrong').show();
                        $('#pay_now').attr('disabled' , true);
                    },
                    success: function (data) {
                            
                            let rate = parseFloat(data.one_way['Rate']);
                            var shiping_input = $('#shipment_price').html(rate);
                            total = parseFloat(rate);
                            console.log(total);

                            if(data.two_way != ""){
                            let rate_two = parseFloat(data.two_way['Rate']);
                            var shiping_input = $('#trader_shipment_price').html(rate_two);
                            total =  total + rate_two;
                            }    

                           
                        var total = parseFloat(checkout_total) + total;
                        $('#pay_now').attr('disabled' , false);
                        $('#total_input_amount').val(total.toFixed(2));
                        $('#total_amount').html('$'+total.toFixed(2));

                            $('.billing_zip_wrong').hide();
                            $('.zip_wrong').hide();
                    }
                });

                }

            });
            $('#zip').change(function (){
                var zip =$('#zip').val();
                var checkout = "{{ $checkout->id }}";
                var path = `{{ url('/') }}`
                var checkout_total = "{{ $checkout_total }}";
                $('#pay_now').attr('disabled' , true);
                if(zip!== ''){
                    $.ajax({
                    type:     "get",
                    cache:    false,
                    url:      `${path}/shipping_ajax/single/${checkout}/${zip}`,
                    error: function (request, error) {
                        $('.billing_zip_wrong').show();
                        $('#pay_now').attr('disabled' , true);
                    },
                    success: function (data) {
                        let rate = parseFloat(data.one_way['Rate']);
                            var shiping_input = $('#shipment_price').html(rate);
                            total = parseFloat(rate);
                            console.log(total);

                            if(data.two_way != ""){
                            let rate_two = parseFloat(data.two_way['Rate']);
                            var shiping_input = $('#trader_shipment_price').html(rate_two);
                            total =  total + rate_two;
                            }    

                           
                        var total = parseFloat(checkout_total) + total;
                        $('#pay_now').attr('disabled' , false);
                        $('#total_input_amount').val(total.toFixed(2));
                        $('#total_amount').html('$'+total.toFixed(2));

                            $('.billing_zip_wrong').hide();
                            $('.zip_wrong').hide();
                    }
                   });
                }
            });


            if ($('#shipping-address').is(':checked')) {
                $('#shippingAddress input').prop('disabled',false);
                $('#shippingAddress select').prop('disabled',false);
                $("#shippingAddress").show();
            }else{
                $('#shippingAddress input').prop('disabled',true);
                $('#shippingAddress select').prop('disabled',true);

                $("#shippingAddress").hide();
            }

        $('#shipping-address').click(function (){
            if ($('#shipping-address').is(':checked')) {
                $('#shippingAddress input').prop('disabled',false);
                $('#shippingAddress select').prop('disabled',false);
                $("#shippingAddress").show();
            }else{
                $('#shippingAddress input').prop('disabled',true);
                $('#shippingAddress select').prop('disabled',true);
                $("#shippingAddress").hide();
            }
        });
   
        $("input[name=payment_method]").change(function(){
            if($(this).val() == "old"){
               $(".old-card").show();
               $(".new-card").hide();
            //    $('#checkout_form').removeClass("require-validation");
            }
            else{
                $(".old-card").hide();
               $(".new-card").show();
            //    $('#checkout_form').addClass("require-validation");
            }
        });

         // Handle form submission.
         var form = document.getElementById('checkout_form');
        form.addEventListener('submit', function(event) {
            event.preventDefault();
            $('#pay_now').attr("disabled", "disabled");
            if($('input[name="payment_method"]:checked').val() == "new"){
                stripe.createToken(card).then(function(result) {
                if (result.error) {
                    // Inform the user if there was an error.
                    var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;
                } else {
                    // Send the token to your server.
                    stripeTokenHandler(result.token);
                }
               });
            }
            else{
                document.getElementById('checkout_form').submit();
                // Submit the form
                form.submit();
            }
          
        });

        // Submit the form with the token ID.
        function stripeTokenHandler(token) {
            // Insert the token ID into the form so it gets submitted to the server
            var form = document.getElementById('checkout_form');
            document.getElementById('stripe_token').value = token.id;
            document.getElementById('checkout_form').submit();
            // Submit the form
            form.submit();
        }


    
    </script>
    <!--checkout page end here -->
    
   


@stop






