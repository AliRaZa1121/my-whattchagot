@extends('layouts.app2')

@section('content')
    <script src="https://js.stripe.com/v3/"></script>

<style>
    .form-group {
        margin-bottom: 1rem;
        position: relative;
    }
    .form-group i {
        position: absolute;
        right: 20px;
        top: 10px;
    }
    .face-icon {
        position: relative;
    }

    .face-icon:after {content: '\f082';position: absolute;top: 6px;right: 190px;font-family: 'Font Awesome 5 Brands';font-size: 16px;}
    .google-icon {
        position: relative;
    }

    .google-icon:after {content: '\f0d5';position: absolute;right: 184px;font-family: 'Font Awesome 5 Brands';font-size: 16px;}



        /**
            * The CSS shown here will not be introduced in the Quickstart guide, but shows
            * how you can use CSS to style your Element's container.
            */
        .StripeElement {
            box-sizing: border-box;

            height: 40px;
            width: 100%;
            padding: 10px 12px;

            border: 1px solid transparent;
            border-radius: 4px;
            background-color: #e6ebf1;

            box-shadow: 0 1px 3px 0 #e6ebf1;
            -webkit-transition: box-shadow 150ms ease;
            transition: box-shadow 150ms ease;
        }

        .StripeElement--focus {
            box-shadow: 0 1px 3px 0 #cfd7df;
        }

        .StripeElement--invalid {
            border-color: #fa755a;
        }

        .StripeElement--webkit-autofill {
            background-color: #fefde5 !important;
        }

        #card-errors {
            color: orangered
        }

        .modal {
            background-color: #00000069;
        }

        .modal-content {
        margin-top: 22% !important;
         }

    </style>





    <!-- Full With banner -->
    <div class="full-width-banner about-us-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Profile</h2>
                </div>

            </div>
        </div>

    </div>


    <!-- Our Brands Section end here -->


    <!-- About Us Last Slider Start here -->
    <div class="about-last-slider-wrapper login-div-page-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session('failed'))
                        <div class="alert alert-danger">
                            {{ session('failed') }}
                        </div>
                    @endif
                </div>
                <div class="col-12">

                    <div class="login-form login-signup">
                        <div class="text-center mb-10 mb-lg-20">
                            <h3 class="font-size-h1">Profile</h3>
                            <p class="text-muted font-weight-bold">Enter your details to activate your account</p>
                        </div>
                        <!--begin::Form-->
                        <form class="form" method="post" action="{{route('profile.update', $user->id)}}" id="signup_form">
                            @csrf
                            <div class="form-group">
                                <input class="form-control " type="text"
                                       placeholder="Full Name" name="fullname" autocomplete="off" value="{{$user->name}}" readonly/>
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="hidden"
                                       placeholder="Email" name="email" value="{{$user->email}}" autocomplete="off" readonly/>
                            </div>

                            <div class="form-group" id="seller_package_div" >
                                <select name="seller_package_id" id="seller_package_id"
                                    class="form-control form-control-solid" >
                                    <option value="">Select Package</option>
                                    @foreach ($packages as $pkg)
                                        <option value="{{ $pkg->id }}">
                                            @if($pkg->type === "paid")
                                            {{  $pkg->name .' Package: $' . $pkg->price . ' ' . 'for' . ' ' . $pkg->duration . ' Months' }}
                                            @else
                                            {{ $pkg->name .' Package:  $' . $pkg->price . ' ' . 'for life time'  }}
                                            @endif
                                        </option>
                                    @endforeach
                                </select>

                            </div>
                            <div class="form-group">
                                <input class="form-control " type="text"
                                       placeholder="Zip Code" name="zipcode" id="zipcode" autocomplete="off"/>
                            </div>

                            <input type="hidden" name="nonce" id="stripe_token" value="">

                            <div class="form-group">
                                <label class="checkbox mb-0">
                                    <input type="checkbox" required name="agree"/>&nbsp;I Agree the
                                    <a href="{{ url('terms-and-condition') }}">terms and conditions</a>.
                                    <span></span></label>
                            </div>

                            <div class="form-group d-flex flex-wrap flex-center">
                                <button type="submit" id=""
                                        class="btn btn-primary login-btn">Submit
                                </button>

                            </div>
                        </form>

                        <!--end::Form-->
                    </div>
                </div>
            </div>
        </div>

          <div class="modal fade" id="stripe" tabindex="-1" role="dialog" aria-labelledby="stripeLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3>Add your card in stripe</h3>
                    <hr>
                    <form class="container" id="stripe-payment-form">
                        <div class="row">
                            <div class="col-sm-12">
                                <label for="card-element">
                                    Credit or debit card
                                </label>
                                <div id="card-element">
                                    <!-- A Stripe Element will be inserted here. -->
                                </div>

                                <!-- Used to display Element errors. -->
                                <div id="card-errors" role="alert"></div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12">
                                <button class="btn btn-primary pay-btn">Pay</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>



    </div>


@stop


@section('scripts')
    <script>

    $('#user_type_select').on('change', function()
        {
            if (this.value == "Seller") {
                $("#seller_package_div").show();
            }
            else if (this.value == "Buyer"){
                $("#seller_package_div").hide();
            }

        });

    $('#signup_form').on('submit', function(e) {
            e.preventDefault();
            e.stopImmediatePropagation();
            const zipcode = $('#zipcode').val();


            $('.login-btn').attr("disabled", "disabled");

                $.ajax({
                        type:"get",
                        url:"{{route('check_zipcode')}}?zip="+zipcode,
                        success: function(res) {
                            if(res){
                                    $('#stripe').show();
                                    $('#stripe').addClass('show');
                            }else{
                                    alert('Invalid Zip Code!')
                                    
                            }
                            $('.login-btn').attr("disabled", false);
                        }
                    });
        });

          $('.close').click(function() {
            $('#stripe').hide();
            $('#stripe').removeClass('show');
        })

 // Create a Stripe client.
        // Issue on route cache
        var stripe = Stripe("{{ env('STRIPE_KEY') }}");


        // Create an instance of Elements.
        var elements = stripe.elements();

        // Custom styling can be passed to options when creating an Element.
        // (Note that this demo uses a wider set of styles than the guide below.)
        var style = {
            base: {
                color: '#32325d',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        };

        // Create an instance of the card Element.
        var card = elements.create('card', {
            style: style
        });

        // Add an instance of the card Element into the `card-element` <div>.
        card.mount('#card-element');
        // Handle real-time validation errors from the card Element.
        card.on('change', function(event) {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        // Handle form submission.
        var form = document.getElementById('stripe-payment-form');
        form.addEventListener('submit', function(event) {
            event.preventDefault();
            $('.pay-btn').addClass('spinner spinner-darker-success spinner-left mr-3');
            $('.pay-btn').attr("disabled", "disabled");
            stripe.createToken(card).then(function(result) {
                if (result.error) {
                    // Inform the user if there was an error.
                    var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;
                } else {
                    // Send the token to your server.
                    stripeTokenHandler(result.token);
                }
            });
        });

        // Submit the form with the token ID.
        function stripeTokenHandler(token) {
            // Insert the token ID into the form so it gets submitted to the server
            var form = document.getElementById('signup_form');
            document.getElementById('stripe_token').value = token.id
            document.getElementById('signup_form').submit();
            //   alert(token);

            // Submit the form
            form.submit();
        }


     </script>
@endsection

