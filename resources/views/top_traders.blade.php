@extends('layouts.app2')

@section('styles')

<style>
    section {
        padding: 60px 0;
    }

    section .section-title {
        text-align: center;
        color: #3e4296;
        margin-bottom: 50px;
        text-transform: uppercase;
    }

    #team .card {
        border: none;
        background: #ffffff;
    }


    .frontside .card,
    .backside .card {
        min-height: 312px;
    }


    .frontside .card .card-title,
    .backside .card .card-title {
        color: #3e4296 !important;
    }

    .frontside .card .card-body img {
        width: 120px;
        height: 120px;
        border-radius: 50%;
    }
</style>
@endsection
@section('content')
@include('layout.nav_header.nav_slider_bar.nav_slider_bar')

<!-- Full With banner -->
<div class="full-width-banner about-us-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Top Traders
                </h2>
            </div>

        </div>
    </div>

</div>


<!-- Full With Banner End here -->
<section id="team" class="pb-5">
    <div class="container">
        <h5 class="section-title h1">SELLERS</h5>
        <div class="row">
            @forelse ($traders as $products)
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                    <p><img class=" img-fluid"
                                            src="<?php   echo isset(user_image($products->user_id)->image) ? asset('/storage/' . user_image($products->user_id)->image) : url('/images/auction-detail-seller-img.png') ?>"
                                            alt="card image"></p>
                                    <h4 class="card-title">{{ $products->users->name }}</h4>
                                    {{-- <p class="card-text">This is basic card with image on top, title, description
                                        and button.</p> --}}
                                    <a href="{{ url('profile') }}/{{ $products->users->id}}"
                                        class="btn btn-primary btn-sm">View Profile</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            @empty
            <p>No Data Fonud</p>
            @endforelse


        </div>
    </div>
</section>



@stop
