@extends('layouts.app2')

@section('styles')

    <style type="text/css" media="all">
        span.Notes {
            font-size: 8pt;
            font-family: Arial;
            font-size: 0.9rem;
            color: rgb(0, 0, 0);
            background-color: rgb(255, 255, 128);
            border: 1px solid rgb(128, 128, 128);
            padding: 2px;
        }

        @media print {
            #pmnavbar {
                display: none;
            }
        }

        /* ------------------------------------------------------------
                Reset Styles (from meyerweb.com)
            ------------------------------------------------------------ */

        html,
        body,
        div,
        span,
        applet,
        object,
        iframe,
        h1,
        h2,
        h3,
        h4,
        h5,
        h6,
        p,
        blockquote,
        pre,
        abbr,
        acronym,
        address,
        big,
        cite,
        code,
        del,
        dfn,
        em,
        font,
        img,
        ins,
        kbd,
        q,
        s,
        samp,
        small,
        strike,
        strong,
        sub,
        sup,
        tt,
        var,
        dl,
        dt,
        dd,
        ol,
        ul,
        li,
        fieldset,
        form,
        label,
        legend,
        table,
        caption,
        tbody,
        tfoot,
        thead,
        tr,
        th,
        td {
            margin: 0;
            padding: 0;
            border: 0;
            outline: 0;
            font-weight: inherit;
            font-style: inherit;
            font-size: 1rem;
            font-family: inherit;
            vertical-align: baseline;
        }

        /* ------------------------------------------------------------
                NUMBER OF COLUMNS: Adjust #primaryNav li to set the number
                of columns required in your site map. The default is
                4 columns (25%). 5 columns would be 20%, 6 columns would
                be 16.6%, etc.
            ------------------------------------------------------------ */

        #primaryNav li {
            width: 25%;
        }

        #primaryNav li ul li {
            width: 100% !important;
        }

        #primaryNav.col1 li {
            width: 99.9%;
        }

        #primaryNav.col2 li {
            width: 50.0%;
        }

        #primaryNav.col3 li {
            width: 33.3%;
        }

        #primaryNav.col4 li {
            width: 25.0%;
        }

        #primaryNav.col5 li {
            width: 20.0%;
        }

        #primaryNav.col6 li {
            width: 16.6%;
        }

        #primaryNav.col7 li {
            width: 14.2%;
        }

        #primaryNav.col8 li {
            width: 12.5%;
        }

        #primaryNav.col9 li {
            width: 11.1%;
        }

        #primaryNav.col10 li {
            width: 9.99%;
        }

        #primaryNav.col11 li {
            width: 9.08%;
        }

        #primaryNav.col12 li {
            width: 8.32%;
        }

        #primaryNav.col13 li {
            width: 7.68%;
        }


        /* ------------------------------------------------------------
                General Styles
            ------------------------------------------------------------ */

        #sitemap {
            padding: 100px;
            background-color: #fff;
            margin-bottom: 220px;
            color: #000;
            font-size: 8pt;
            font-family: Arial;
        }

        a {
            overflow: hidden;
        }

        a:link {
            color: rgb(0, 0, 238);
        }

        a:visited {
            color: rgb(85, 26, 139);
        }

        .sitemap {
            margin: 0 0 40px 0;
            float: left;
            width: 100%;
        }

        h1 {
            font-weight: bold;
            text-transform: uppercase;
            font-size: 120%;
            margin: 0 0 5px 0;
        }

        h2 {
            font-size: 8pt;
            font-family: Arial;
            font-size: 80%;
            color: #555;
            margin: 0 0 20px 0;
        }

        ol,
        ul {
            list-style: none;
        }


        /* ------------------------------------------------------------
                Site Map Styles
            ------------------------------------------------------------ */

        /* --------	Top Level --------- */

        #primaryNav {
            margin: 0;
            float: left;
            width: 100%;
        }

        #primaryNav #home {
            display: block;
            float: none;
            background: #ffffff url('slickmap/L1-left.png') center bottom no-repeat;
            position: relative;
            z-index: 2;
            padding: 0 0 30px 0;
        }

        #primaryNav li {
            float: left;
            background: url('slickmap/L1-center.png') center top no-repeat;
            padding: 30px 0;
            margin-top: -30px;
        }

        #primaryNav li a {
            margin: 0 20px 0 0;
            padding: 10px 0;
            display: block;
            color: black;
            font-size: 8pt;
            font-family: Arial;
            text-align: center;
            background: #c3eafb url('slickmap/white-highlight.png') top left repeat-x;
            border: 2px solid #b5d9ea;
            border-radius: 5px;
            box-shadow: rgba(0, 0, 0, 0.5) 2px 2px 2px;
            /* FF 3.5+ */
        }

        #primaryNav li a:hover {
            background-color: #e2f4fd;
            border-color: #97bdcf;
        }

        #primaryNav li.last-child {
            background: url('slickmap/L1-right.png') center top no-repeat;
        }

        a:link:before,
        a:visited:before {
            /*content: " "attr(href)" ";*/
            display: block;
            text-transform: uppercase;
            font-size: 0.9rem;
            margin-bottom: 5px;
            word-wrap: break-word;
        }

        #primaryNav li a:link:before,
        #primaryNav li a:visited:before {
            color: #78a9c0;
        }

        /* --------	Second Level --------- */

        #primaryNav li li {
            width: 100%;
            clear: left;
            margin-top: 0;
            padding: 10px 0 0 0;
            background: url('slickmap/vertical-line.png') center bottom repeat-y;
        }

        #primaryNav li li a {
            background-color: #cee3ac;
            border-color: #b8da83;
        }

        #primaryNav li li a:hover {
            border-color: #94b75f;
            background-color: #e7f1d7;
        }

        #primaryNav li li.first-child {
            padding-top: 30px;
        }

        #primaryNav li li.last-child {
            background: url('slickmap/vertical-line.png') center bottom repeat-y;
        }

        #primaryNav li li a:link:before,
        #primaryNav li li a:visited:before {
            color: #8faf5c;
        }

        /* --------	Third Level --------- */

        #primaryNav li li ul {
            margin: 10px 0 0 0;
            width: 100%;
            float: right;
            padding: 9px 0 10px 0;
            background: #ffffff url('slickmap/L3-ul-top.png') center top no-repeat;
        }

        #primaryNav li li li {
            background: url('slickmap/L3-center.png') left center no-repeat;
            padding: 5px 0;
        }

        #primaryNav li li li a {
            background-color: #fff7aa;
            border-color: #e3ca4b;
            font-size: 0.7rem;
            padding: 5px 0;
            width: 80%;
            float: right;
        }

        #primaryNav li li li a:hover {
            background-color: #fffce5;
            border-color: #d1b62c;
        }

        #primaryNav li li li.first-child {
            padding: 15px 0 5px 0;
            background: url('slickmap/L3-li-top.png') left center no-repeat;
        }

        #primaryNav li li li.last-child {
            background: url('slickmap/L3-bottom.png') left center no-repeat;
        }

        #primaryNav li li li a:link:before,
        #primaryNav li li li a:visited:before {
            color: #ccae14;
            font-size: 0.8rem;
        }


        /* ------------------------------------------------------------
                Utility Navigation
            ------------------------------------------------------------ */

        #utilityNav {
            float: right;
            max-width: 50%;
            margin-right: 10px;
        }

        #utilityNav li {
            float: left;
            margin-bottom: 10px;
        }

        #utilityNav li a {
            margin: 0 10px 0 0;
            padding: 5px 10px;
            display: block;
            border: 2px solid #e3ca4b;
            font-size: 0.75rem;
            /*font-weight: bold;*/
            text-align: center;
            color: black;
            background: #fff7aa url('slickmap/white-highlight.png') top left repeat-x;
            border-radius: 5px;
            box-shadow: rgba(0, 0, 0, 0.5) 2px 2px 2px;
            /* FF 3.5+ */
        }

        #utilityNav li a:hover {
            background-color: #fffce5;
            border-color: #d1b62c;
        }

        #utilityNav li a:link:before,
        #utilityNav li a:visited:before {
            color: #ccae14;
            font-size: 0.75rem;
            margin-bottom: 3px;
        }

    </style>
@endsection


@section('content')
    @include('layout.nav_header.nav_slider_bar.nav_slider_bar')

    <!-- Full With banner -->
    <div class="full-width-banner about-us-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>SITEMAP</h2>
                </div>

            </div>
        </div>

    </div>


    <div id="sitemap">
        <h1>Do What Winners Do – Auction</h1>
        <h2>Site Map</h2>
        <div class="PrintingStyle" id="PrintStyleID" style="height:100%;width:100%">
            <ul id="primaryNav" class="col8">
                <li id="home"><a href="{{ route('index') }}">Home</a></li>
                <li class="first-child"><a href="{{ route('login') }}">Login </a></li>
                <li><a href="{{ route('login') }}">Buyer Login </a></li>
                <li><a href="{{ route('about-us') }}">About Us </a></li>
                <li><a href="{{ route('auction-list') }}">Auction </a></li>
                <li><a href="{{ route('products') }}?type=list">Products </a></li>
                <li><a href="{{ route('news') }}">News </a></li>
                <li><a href="{{ url('contact-us') }}">Contact Us
                    </a></li>
                <li><a href="{{ url('money-back-guarantee') }}">Money Back Guarantee
                    </a></li>
                <li><a href="{{ url('bidding-buying-help') }}">Bidding & Buying Help
                    </a></li>
                <li><a href="{{ url('stores') }}">Stores
                    </a></li>
                <li><a href="{{ url('whattchagot-local') }}">Whattchagot Local
                    </a></li>
                <li><a href="{{ url('start-selling') }}">Start Selling
                    </a></li>
                <li><a href="{{ url('learn-to-sell') }}">Learn To Sell
                    </a></li>
                <li><a href="{{ url('business-sellers') }}">Business Sellers
                    </a></li>
                <li><a href="{{ url('affiliates') }}">Affiliates
                    </a></li>
                <li><a href="{{ url('careers') }}">Careers
                    </a></li>
                <li><a href="{{ url('advertise-with-us') }}">Advertise With Us
                    </a></li>
                <li><a href="{{ url('help-center') }}">Help Center
                    </a></li>
                <li><a href="{{ url('seller-information-center') }}">Seller Information Center
                    </a></li>
                <li><a href="{{ url('terms-and-condition') }}">Terms & Conditions
                    </a></li>

                    <li><a href="{{ url('faqs') }}">FAQs
                    </a></li>
                <li><a href="{{ url('privacy-policy') }}">Privacy Policy
                    </a></li>


            </ul>
        </div>
    </div>







    <script>
        jQuery("#client-stories .trending_products_slider").owlCarousel({
            loop: true,
            items: 3,
            margin: 10,
            dots: false,
            nav: true,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            autoplay: true,
            autoplayTimeout: 3000,
            responsiveClass: true,
        });

        jQuery(".about-last-slider-wrapper .auction_slider_wrapper").owlCarousel({
            loop: true,
            items: 1,
            margin: 10,
            dots: true,
            nav: false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            autoplay: false,
            autoplayTimeout: 3000,
            responsiveClass: true,
        });
    </script>
@stop
