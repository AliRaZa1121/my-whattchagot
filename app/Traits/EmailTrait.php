<?php
namespace App\Traits;

use Mail;

trait EmailTrait
{
    public function sendMail($data, $view){
        try {
            Mail::send($view, $data, function ($message) use ($data) {
                $message->from(env('MAIL_FROM_ADDRESS'), env('APP_NAME'));
                $message->subject($data['subject'] ?? "Email From ".env('APP_NAME'));
                $message->to($data['email']);
            });
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function sendContactMail($data, $view){
        try {
            Mail::send($view, $data, function ($message) use ($data) {
                $message->from($data['email'], $data['name']);
                $message->to('info@smartysupply.com');
            });
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function sendEnquiryMail($data, $view){
        try {
            Mail::send($view, $data, function ($message) use ($data) {
                $message->from($data['fromEmail'], 'Smarty Supply User');
                $message->to($data['email']);
            });
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}
