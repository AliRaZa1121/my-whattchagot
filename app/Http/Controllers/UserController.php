<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\user_image;
use Illuminate\Support\Facades\Hash;
use App\Models\SellerPackage;
use Carbon\Carbon;
use App\Models\Warehouse;
use Illuminate\Support\Facades\DB;



class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit()
    {

        $user = User::find(auth()->user()->id);
        $user_image = user_image::where('user_id',auth()->user()->id)->first();
        return view('pages.admin.user.edit', compact('user','user_image'));
    }
    
    public function getusers()
    {
        $users = User::where('user_type', '!=', 'Admin')->latest()->get();
        return view('pages.admin.user.index', compact('users'));
    }

    public function profile_update(Request $required, $userid){
        
        $required->validate(
            [
                'fullname' => 'required',
                'email' => 'required|email',
                'nonce' => 'required',
                'seller_package_id' => 'required'
            ]
        );

        try {
            DB::beginTransaction();
            $pkg =  SellerPackage::find($required->seller_package_id);
            $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
            $customer = $stripe->customers->create([
                'source' => $required->nonce,
                'name' => $required->fullname,
                'email' => $required->email,
            ]);

          //  dd($customer);

            $card = $stripe->customers->retrieveSource(
                $customer['id'],
                $customer['default_source'],
              );

            if ($pkg->type == 'paid') {
              $stripe->charges->create([
                'amount' => $pkg->price * 100,
                'currency' => 'usd',
                'customer' => $customer['id'],
                'description' => $pkg->name .' '. ' Packge Purchasing by '.$required->fullname,
              ]);

            }
      
            //UPDATE USER PACKAGE
            $user_update = user::find($userid);
            $user_update->seller_package_id = $required->seller_package_id;
            $user_update->package_start_date = Carbon::now();
            $user_update->package_valid_till = Carbon::now()->addMonth($pkg->duration);
            $user_update->stripe_customer_id = $customer['id'];
            $user_update->update();

            //WAREHOUSE ENTRY
            $warehouse = new Warehouse;
            $warehouse->zip = $required->zipcode;
            $warehouse->email = $required->email;
            $warehouse->country = 'United States';
            $warehouse->user_id = $userid;
            $warehouse->save();

            DB::table('cards')->insert([
                'user_id' => $userid,
                'card_id' => $card['id'],
                'brand' => $card['brand'],
                'exp_month' => $card['exp_month'],
                'exp_year' => $card['exp_year'],
                'last4' => $card['last4'],
            ]);
            DB::commit();
            session()->flash('success', 'Your account has been created successfully.');
            return redirect('/');

            } catch (\Throwable $th) {
                DB::rollback();
                session()->flash('failed', $th->getMessage());
                return back()->with('failed', $th->getMessage());
            }
        return back()->withInput($required->only('email', 'remember'));

    }
   

    public function update_user( Request $request)
    {
        $request->validate([
            'banner_image' => 'required',
        ]);
        try {
            $user = User::find(auth()->user()->id);
            $user->name = $request->name;
            if(!empty($request->name)){
                $user->name = $request->name;
            }
            if(!empty($request->password)){
                $user->password = Hash::make($request->password);

            }
            if(!empty($request->email)){
                $user->email = $request->email;

            }
            $user->update();

            $user_image = user_image::where('user_id', $user->id)->first();
            if (empty($user_image)) {
                $user_image = new user_image;
                $user_image->user_id = auth()->user()->id;
                $user_image->image = $request->banner_image;
                $user_image->save();
            } else {
                $user_imageID = $user_image->id;
                $user_image = user_image::find($user_imageID);
                $user_image->user_id = auth()->user()->id;
                $user_image->image = $request->banner_image;
                $user_image->update();
            }


            DB::commit();
            return response()->json(['msg' => 'Your profile has been updated.']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['msg' => $e->getMessage()], 500);

        }
    }

    public function uploadAttachment(Request $request, $type)
    {
        $file2 = $request->files->get('banner')->getClientOriginalName();
        if (auth()->guard('buyer')->check()) {
            $guard = 'buyer';
        } else if (auth()->guard('admin')->check()) {
            $guard = 'admin';

        } else if (auth()->guard('seller')->check()) {
            $guard = 'seller';

        } else {
            return back();
        }
        $image = "banner";
        if ($type == 'banner') {
            $request->validate([
                'banner' => 'required|image',
            ]);
        } else {
            $request->validate([
                'image' => 'required|image',
            ]);
            $image = "image";
        }

        $fileName = $request->$image->store('public/' . $guard . '_profile');
        $fileName = Str::after($fileName, 'public/');
        return response(['msg' => 'Image has been uploaded successfully.', 'file_name' => $fileName, 'file2' => $file2]);
    }

}
