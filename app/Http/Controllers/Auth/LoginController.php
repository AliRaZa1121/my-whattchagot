<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Contracts\Auth\Authenticatable;
use Auth;
use Socialite;
use App\Models\SellerPackage;
use App\Models\User;
use App\Models\Warehouse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Models\chat;
use App\Models\checkout;
use App\Traits\EmailTrait;
use Carbon\Carbon;
use Stripe\Stripe;
use Session;
use Illuminate\Support\Facades\DB;


class LoginController extends Controller
{
    use EmailTrait;

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logoutUser');
        $this->middleware('guest:buyer')->except('logoutUser');
        $this->middleware('guest:seller')->except('logoutUser');
    }

    public function showLoginForm()
    {
        $data['packages'] = SellerPackage::all();
        return view('login',$data);
    }
    public function loginUser(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:3'
        ]);
        $user = User::where('email', $request->email)->first();

        if ($user) {
            if (Auth::attempt(['email' => $request->email, 'user_type' => $user->user_type, 'password' => $request->password], $request->get('remember'))) {
                Session::put('pas_key', $request->password);
                if (isset($request->product_id)) {
                    if (isset($request->check_out)) {

                        if ($request->check_out == 'checkout') {
                            $product_id = Crypt::decrypt($request->product_id);
                            $priceCrypt = Crypt::decrypt($request->price);

                            $userId = Auth::user()->id;
                            checkout::create([
                                'user_id' => $userId,
                                'product_id' => $product_id,
                                'price' => $priceCrypt,
                                'qty' => 1,

                            ]);
                            return redirect($request->last . '/');
                        } else {
                            Auth::logout();
                            session()->flash('failed', 'Doesn\'t open development tool.');
                            return back()->withInput($request->only('email', 'remember'));
                        }


                    } else {
                        chat::create([
                            'sender_id' => Auth::user()->id,
                            'receiver_id' => Crypt::decrypt($request->receiver_id),
                            'product_id' => Crypt::decrypt($request->product_id),
                            'message' => Crypt::decrypt($request->product_id),
                            'status' => 0,

                        ]);
                        return redirect('/dashboard');
                    }

                } else {
                    return redirect('/dashboard');

                }
            } else {
                session()->flash('failed', 'Sorry, credentials doesn\'t match.');
                return back()->withInput($request->only('email', 'remember'));
            }
        } else {
            session()->flash('failed', 'Sorry, credentials doesn\'t match.');
            return back()->withInput($request->only('email', 'remember'));
        }

    }

    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback()
    {
        try {
            $user = Socialite::driver('google')->user();

            $finduser = User::where('google_id', $user->id)->first();
            if ($finduser) {
                $finduser->update(['user_type'=> 'Buyer']);
                $guardName = "buyer";
                Auth::guard($guardName)->login($finduser);
                Session::put('pas_key', 'click123');

                if ($finduser->stripe_customer_id == null) {
                   return redirect()->route('profile.processing',$finduser)->with('failed', "Please Purchase Package / Add Your Card & become out customer");
                }
                return redirect()->intended('/' . $guardName);

            } else {

                $newUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'status' => 'Approved',
                    'user_type' => 'Buyer',
                    'google_id' => $user->id,
                    'password' => Hash::make('click123'),
                ]);
                Session::put('pas_key', 'click123');
                Auth::guard('buyer')->login($newUser);
                return redirect()->route('profile.processing',$newUser)->with('failed', "Please Purchase Package / Add Your Card & become out customer");

            }

        } catch (Exception $e) {
            return redirect('login')->with('failed', $e->getMessage());
        }
    }

    public function logoutUser(Request $request)
    {
        Auth::logout();
        Session::flush();
        return redirect()->route('login');
    }


    public function create(Request $required)
      {
            $validation = $required->validate(
                [
                    'fullname' => 'required',
                    'email' => 'required|email|unique:users',
                    'password' => 'required|min:3',
                    'cpassword' => 'required|min:3',
                    'nonce' => 'required',
                    'seller_package_id' => 'required'
                ]
            );
            try {
            DB::beginTransaction();
            $pkg =  SellerPackage::find($required->seller_package_id);
            $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
            $customer = $stripe->customers->create([
                'source' => $required->nonce,
                'name' => $required->fullname,
                'email' => $required->email,
            ]);

        

            $card = $stripe->customers->retrieveSource(
                $customer['id'],
                $customer['default_source'],
              );




            if ($pkg->type == 'paid') {

              $stripe->charges->create([
                'amount' => $pkg->price * 100,
                'currency' => 'usd',
                'customer' => $customer['id'],
                'description' => $pkg->name .' '. ' Packge Purchasing by '.$required->fullname,
              ]);

        }

       $user = User::create([
            'name' => $required->fullname,
            'email' => $required->email,
            'password' => hash::make($required->password),
            'user_type' => "Buyer",
            'seller_package_id' => $required->seller_package_id,
            'package_start_date' => Carbon::now(),
            'package_valid_till' => Carbon::now()->addMonth($pkg->duration),
            'status' => 'Approved',
            'stripe_customer_id' => $customer['id']
        ]);
            $warehouse = new Warehouse;
            $warehouse->zip = $required->zipcode;
            $warehouse->email = $required->email;
            $warehouse->country = 'United States';
            $warehouse->user_id = $user->id;
            $warehouse->save();

        DB::table('cards')->insert([
            'user_id' => $user->id,
            'card_id' => $card['id'],
            'brand' => $card['brand'],
            'exp_month' => $card['exp_month'],
            'exp_year' => $card['exp_year'],
            'last4' => $card['last4'],
        ]);
        DB::commit();
        session()->flash('success', 'Your account has been created successfully.');
        } catch (\Throwable $th) {
            Db::rollback();
            session()->flash('failed', $th->getMessage());
            return back()->with('failed', $th->getMessage());
        }

        return back()->withInput($required->only('email', 'remember'));
    }

    public function password_reset()
    {
        // return view('auth.forgotpassword');
        return view('auth.buyer.forgotpassword');
    }

    public function reset_password(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        $validation = $request->validate(
            [
                'email' => 'required|email',
            ]);
        if (!empty($user)) {
            $random_digit = mt_rand(00000000, 99999999);

            $user_update = user::find($user->id);
            $user_update->reset_code = $random_digit;
            $user_update->update();

            $data['name'] = $user->name;
            $data['email'] = $user->email;
            $data['bodytxt'] = 'Your password reset code is ' . $random_digit;

            $this->sendMail($data, 'emails.password_reset');
            return redirect('/reset-code/' . base64_encode($random_digit));

        } else {
            session()->flash('failed', 'Sorry, Email is not found.');
            return redirect('/reset-password/');

        }
    }

    public function password_reset_code_again($code)
    {
        $user = User::where('reset_code', base64_decode($code))->first();

        if (!empty($user)) {
            $random_digit = mt_rand(00000000, 99999999);

            $user_update = user::find($user->id);
            $user_update->reset_code = $random_digit;
            $user_update->update();
            $data['name'] = $user->name;
            $data['email'] = $user->email;
            $data['bodytxt'] = 'Your password reset code is ' . $random_digit;

            $this->sendMail($data, 'emails.password_reset');
            return redirect('/reset-code/' . base64_encode($random_digit));

        } else {
            session()->flash('failed', 'Sorry, Email is not found.');
            return redirect('/reset-password/');

        }
    }

    public function reset_code_check(Request $request)
    {
        $user = User::where('reset_code', $request->code)->first();


        if (!empty($user)) {

            return redirect('/new-password/' . Crypt::encrypt($user->reset_code));

        } else {
            session()->flash('failed', 'Sorry, Code is invalid.');
            return back();

        }
    }

    public function password_reset_code($code)
    {

        return view('auth.buyer.reset_code');


    }

    public function new_password($code)
    {
        return view('auth.buyer.new_password');


    }

    public function change_password(Request $request)
    {
        $validation = $request->validate(
            [
                'password' => 'required|min:3',

            ]
        );

        if ($request->password == $request->confirm) {
            session()->flash('success', 'Your password has been changed.');
            $user = User::where('reset_code', Crypt::decrypt($request->code))->first();
            $user_update = user::find($user->id);
            $user_update->password = hash::make($request->password);
            $user_update->reset_code = 0;
            $user_update->update();
            return redirect('login');

        } else {
            session()->flash('failed', "Sorry, passwords don't match.");
            return back();
        }


    }

    public function buyer_password_reset()
    {

        return view('auth.buyer.forgotpassword');


    }

    public function buyer_reset_password(Request $request)
    {
        $user = User::where('email', $request->email)->where('user_type','buyer')->first();

        $validation = $request->validate(
            [

                'email' => 'required|email',


            ]);
        if (!empty($user)) {
            $random_digit = mt_rand(00000000, 99999999);

            $user_update = user::find($user->id);
            $user_update->reset_code = $random_digit;
            $user_update->update();


            $data['name'] = $user->name;
            $data['email'] = $user->email;
            $data['bodytxt'] = 'Your password reset code is ' . $random_digit;

            $this->sendMail($data, 'emails.password_reset');
            return redirect('/buyer-reset-code/' . base64_encode($random_digit));

        } else {
            session()->flash('failed', 'Sorry, Email is not found.');
            return back();

        }
    }

    public function buyer_password_reset_code_again($code)
    {
        $user = User::where('reset_code', base64_decode($code))->first();


        if (!empty($user)) {
            $random_digit = mt_rand(00000000, 99999999);

            $user_update = user::find($user->id);
            $user_update->reset_code = $random_digit;
            $user_update->update();
            $data['name'] = $user->name;
            $data['email'] = $user->email;
            $data['bodytxt'] = 'Your password reset code is ' . $random_digit;

            $this->sendMail($data, 'emails.password_reset');
            return redirect('/buyer-reset-code/' . base64_encode($random_digit));

        } else {
            session()->flash('failed', 'Sorry, Email is not found.');
            return redirect('/buyer-reset-password/');

        }
    }

    public function buyer_reset_code_check(Request $request)
    {
        $user = User::where('reset_code', $request->code)->first();


        if (!empty($user)) {


            return redirect('/buyer-new-password/' . Crypt::encrypt($user->reset_code));

        } else {
            session()->flash('failed', 'Sorry, Code is invalid.');
            return back();

        }
    }

    public function buyer_password_reset_code($code)
    {

        return view('auth.buyer.reset_code');


    }

    public function buyer_new_password($code)
    {
        return view('auth.buyer.new_password');


    }

    public function buyer_change_password(Request $request)
    {
        $validation = $request->validate(
            [

                'password' => 'required|min:3',



            ]
        );

        if ($request->password == $request->confirm) {
            session()->flash('success', 'Your password has been changed.');
            $user = User::where('reset_code', Crypt::decrypt($request->code))->first();
            $user_update = user::find($user->id);
            $user_update->password = hash::make($request->password);
            $user_update->reset_code = 0;
            $user_update->update();
            return redirect('buyer_login');


        } else {
            session()->flash('failed', "Sorry, passwords don't match.");
            return back();
        }


    }

    public function checkZip($zip)
    {
        try {
            $input_xml = rawurlencode('<RateV4Request USERID="798KODER4897">
            <Revision>2</Revision>

            <Package ID="101">
            <Service>PRIORITY</Service>
            <ZipOrigination>'.$zip.'</ZipOrigination>
            <ZipDestination>99501</ZipDestination>
            <Pounds>0</Pounds>
            <Ounces>1</Ounces>
            <Container>RECTANGULAR</Container>
            <Size>LARGE</Size>
            <Width>5</Width>
            <Length>5</Length>
            <Height>5</Height>
            <Girth>0</Girth>
            <Value>1</Value>
            <SpecialServices>
            <SpecialService>1</SpecialService>
            </SpecialServices>
            </Package>

            </RateV4Request>');
             $url =  "https://secure.shippingapis.com/ShippingAPI.dll?API=RateV4&xml=" . $input_xml;


            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
            curl_setopt( $ch , CURLOPT_SSL_VERIFYPEER , false );
            curl_setopt( $ch , CURLOPT_SSL_VERIFYHOST , false );
            $data = curl_exec($ch);
            curl_close($ch);
            $array_data = json_decode(json_encode(simplexml_load_string($data)), true);

                if(isset($array_data['Package']['Postage'])){
                    return 400;
                }else{
                    return 404;
                }
            }
            catch (\Throwable $th) {
              return $th->getMessage();
            }

    }



}
