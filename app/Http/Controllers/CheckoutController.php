<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//use Illuminate\Support\Facades\Crypt;
use App\Models\checkout;
use App\Models\order;
use App\Models\order_item;
use App\Models\shipping;
use App\Models\billing;
use App\Models\wishlist;
use App\Models\size_value;
use App\Models\color;
use App\Models\product;
use App\Models\Warehouse;
use App\Models\User;

use Session;
use DB;

use Stripe;
use App\Traits\EmailTrait;


class CheckoutController extends Controller
{
    use EmailTrait;

    public function checkout_ajax(Request $request)
    {

        $product_id = $request->product_id;
        $price = $request->price;
        $price_type = $request->price_type;
        $input = $request->all();
        $product = product::where([
            'user_id' => auth()->user()->id,
            'id' => base64_decode($product_id),
        ])->first();
        if ($product == null) {
            if (!array_key_exists('color', $input)) {
                $product_colors = color::where('product_id', base64_decode($product_id))->first();
                $input['color'] = $product_colors->color;
            }
            if (!array_key_exists('size', $input)) {
                $product_size = size_value::where('product_id', base64_decode($product_id))->first();
                $input['size'] = $product_size->size;
            }

            $checkout_update = checkout::where([
                ['user_id', auth()->user()->id],
                ['product_id', base64_decode($product_id)],
                ['color', $input['color']],
                ['size', $input['size']]
            ])->first();
            if (empty($checkout_update)) {
                checkout::create([
                    'user_id' => auth()->user()->id,
                    'product_id' => base64_decode($product_id),
                    'price' => base64_decode($price),
                    'qty' => 1,
                    'color' => $input['color'],
                    'size' => $input['size'],

                ]);
            } else {
                $checkout = checkout::find($checkout_update->id);
                $checkout->update([
                    'qty' => $checkout_update->qty + 1,

                ]);
            }

            $checkout_item = checkout::where([
                'user_id' => auth()->user()->id,
                'type' => 'selling'
                ])->count();

            return response()->json(['message' => 'product is added on your bill', 'cart_item' => $checkout_item, 'code' => 200]);
        }
        else {
            return 422;

        }
    }

    public function checkCart($user_id)
    {
        $checkout_update = checkout::where('user_id', $user_id)->get();
        foreach ($checkout_update as $key => $value) {
            $product = product::where('id', $value->product_id)->first(['id', 'flags']);
            if ($product->flags === "trade" || $product->flags === "request-trade") {
                return false;
            }
        }
        return true;
    }

    public function checkout_chat_ajax(Request $request)
    {
        $product_id = $request->product_id;
        $price = $request->price;
        $user_id = $request->user_id;
        $checkout_update = checkout::where('user_id', $user_id)->where('product_id', $product_id)->first();

        if (empty($checkout_update)) {
            checkout::create([
                'user_id' => $user_id,
                'product_id' => $product_id,
                'price' => $price,
                'qty' => 1,

            ]);
            return response()->json(['message' => 'product is added on your bill']);
        } else {
            $checkout = checkout::find($checkout_update->id);
            $checkout->update([

                'price' => $price,
                'qty' => $checkout_update->qty + 1,

            ]);
            return response()->json(['message' => 'product is added on your bill']);
        }


        return response()->json(['message' => 'product is added on your bill']);
    }

    public function checkout_ajax_qty(Request $request)
    {
        $product_id = $request->product_id;


        $checkout = checkout::find($request->id);
        $checkout->update([

            'qty' => $request->qty,

        ]);
        return $request->qty;
        //            return response()->json(['message'=> 'product is added on your bill']);


    }

    public function checkout_delete($id)
    {
        checkout::where('id', $id)->delete();

        return back()->withErrors(['Product has been removed']);
    }

    public function stripePost(Request $request)
    {
        if (Auth()->guard('seller')->check()) {
            $role = 'seller';
        } else if (Auth()->guard('buyer')->check()) {
            $role = 'buyer';
        } else if (Auth()->guard('admin')->check()) {
            $role = 'admin';
        }
        // dd($request->all());
        DB::beginTransaction();
        try {
        $ship = shipping::where('user_id', auth()->guard($role)->user()->id)->first();
        if (!empty($ship)) {
            $shipping = shipping::findOrFail($ship->id);
            if (isset($request->shipping_checked)) {
                $shipping->firstname = $request->shipping_firstname;
                $shipping->lastname = $request->shipping_lastname;
                $shipping->email = $request->shipping_email;
                $shipping->phone = $request->shipping_phone;
                $shipping->address = $request->shipping_address;
                $shipping->address_optional = isset($request->shipping_address2) ? $request->shipping_address2 : ' ';
                $shipping->country = $request->shipping_country;
                $shipping->state = $request->shipping_state;
                $shipping->zip = $request->shipping_zip;
                $shipping->user_id = auth()->guard($role)->user()->id;
            } else {
                $shipping->firstname = $request->billing_firstname;
                $shipping->lastname = $request->billing_lastname;
                $shipping->email = $request->billing_email;
                $shipping->phone = $request->billing_phone;
                $shipping->address = $request->billing_address;
                $shipping->address_optional = isset($request->billing_address2) ? $request->billing_address2 : ' ';
                $shipping->country = $request->billing_country;
                $shipping->state = $request->billing_state;
                $shipping->zip = $request->billing_zip;
                $shipping->user_id = auth()->guard($role)->user()->id;
            }
            $shipping->save();
        } else {
            $shipping = new shipping;
            if (isset($request->shipping_checked)) {
                $shipping->firstname = $request->shipping_firstname;
                $shipping->lastname = $request->shipping_lastname;
                $shipping->email = $request->shipping_email;
                $shipping->phone = $request->shipping_phone;
                $shipping->address = $request->shipping_address;
                $shipping->address_optional = isset($request->shipping_address2) ? $request->shipping_address2 : ' ';
                $shipping->country = $request->shipping_country;
                $shipping->state = $request->shipping_state;
                $shipping->zip = $request->shipping_zip;
                $shipping->user_id = auth()->guard($role)->user()->id;
            } else {
                $shipping->firstname = $request->billing_firstname;
                $shipping->lastname = $request->billing_lastname;
                $shipping->email = $request->billing_email;
                $shipping->phone = $request->billing_phone;
                $shipping->address = $request->billing_address;
                $shipping->address_optional = isset($request->billing_address2) ? $request->billing_address2 : ' ';
                $shipping->country = $request->billing_country;
                $shipping->state = $request->billing_state;
                $shipping->zip = $request->billing_zip;
                $shipping->user_id = auth()->guard($role)->user()->id;
            }
            $shipping->save();
        }

        $billing = new billing;
        $billing->firstname = $request->billing_firstname;
        $billing->lastname = $request->billing_lastname;
        $billing->email = $request->billing_email;
        $billing->phone = $request->billing_phone;
        $billing->address = $request->billing_address;
        $billing->address_optional = isset($request->billing_address2) ? $request->billing_address2 : ' ';
        $billing->country = $request->billing_country;
        $billing->state = $request->billing_state;
        $billing->zip = $request->billing_zip;
        $billing->user_id = auth()->guard($role)->user()->id;
        $billing->save();
        $checkout = checkout::where('user_id', auth()->guard($role)->user()->id)->get();
        $no = 1;
        $shiptotal = 0;
        foreach ($checkout as $key) {
            $shpping_price = $this->usps($shipping->zip, $key->products->user_id, $key->products->width, $key->products->length, $key->products->height, $key->product_id);
            $order_item = new order_item;
            $order_item->product_id = $key->product_id;
            $order_item->price = $key->price;
            $order_item->qty = $key->qty;
            $order_item->shipping_price = $shpping_price;
            $order_item->seller_id = $key->products->user_id;
            $order_item->user_id = auth()->guard($role)->user()->id;
            $order_item->current_order = 0;
            $order_item->order_id = 0;
            $order_item->color = $key->color;
            $order_item->size = $key->size;
            $order_item->save();
            $shiptotal += $shpping_price;
            $selleremail = User::where('id', $key->products->user_id)->first();
            $buyreemail = User::where('id', auth()->guard($role)->user()->id)->first();
            $adminemail = User::where('id', 1)->first();
            //Seller Email/
            if ($no == 1) {
                $data['name'] = $selleremail->name;
                $data['email'] = $selleremail->email;
                $data['bodytxt'] = "$buyreemail->name  purchased a product name:" . $key->products->name . " QTY: " . $key->qty . " price: " . $key->price;
                $this->sendMail($data, 'emails.actions_send');
                //buyer Email/
                $data['name'] = $buyreemail->name;
                $data['email'] = $buyreemail->email;
                $data['bodytxt'] = "you ordered: " . $key->products->name . " QTY: " . $key->qty . " price: " . $key->price;
                $this->sendMail($data, 'emails.actions_send');
                // Admin Email/
                $data['name'] = $adminemail->name;
                $data['email'] = $adminemail->email;
                $data['bodytxt'] = "$buyreemail->name  purchased a product " . $key->products->name . " QTY: " . $key->qty . " price: " . $key->price;
                $this->sendMail($data, 'emails.actions_send');
                $no = 0;
            }

            // "your order is : "$key->products->name ."QTY: ".$key->qty;."price: ".$key->price;

        }
        $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
        if ($request->has('stripeToken') && $request->stripeToken !== null && $request->payment_method === "new") {
            $result_strip =  $stripe->charges->create([
                'amount' => (($request->amount)) * 100,
                'currency' => 'usd',
                'source' => $request->stripeToken,
                'description' =>  "Billing Name:" . $request->billing_firstname . ' ' . $request->billing_lastname,
            ]);
        } else {
            if (auth()->guard($role)->user()->stripe_customer_id) {
                $result_strip = $stripe->charges->create([
                    'amount' => (($request->amount)) * 100,
                    'currency' => 'usd',
                    'customer' =>  auth()->guard($role)->user()->stripe_customer_id,
                    'description' =>  "Billing Name:" . $request->billing_firstname . ' ' . $request->billing_lastname,
                ]);

                if (isset($result_strip['id']) && $result_strip['status'] !== "succeeded") {
                    DB::rollback();
                    return redirect()->back()->with(['msg' => 'payment not charged, please use new card']);
                }
            } else {
                DB::rollback();
                return redirect()->back()->with(['msg' => 'Card not found, please use new card']);
            }
        }


        $order_loop = order_item::where('user_id', auth()->guard($role)->user()->id)
            ->where('current_order', 0)->groupBy('seller_id')->get();
        foreach ($order_loop as $key2) {
            $order = new order;
            $order->total_amount = 0;
            $order->total_qty = 0;
            $order->total_qty = 0;
            //                $order->charge_id =;
            $order->shipping_id = $shipping->id;
            $order->billing_id = $billing->id;
            $order->user_id = auth()->guard($role)->user()->id;
            $order->seller_id = $key2->seller_id;
            $order->save();
            order_item::where('user_id', auth()->guard($role)->user()->id)
                ->where('current_order', 0)->where('seller_id', $key2->seller_id)
                ->update(['current_order' => 1, 'order_id' => $order->id, 'charge_id' => $result_strip->id]);
            $order_sum = order_item::where('order_id', $order->id)->get();
            $total_qty = 0;
            $total_amount = 0;
            foreach ($order_sum as $key_sum) {

                $total_qty = $total_qty + $key_sum->qty;
                $total_amount = $total_amount + ($key_sum->price * $key_sum->qty);
            }

            $orderupdate = order::where('id', $order->id)->update(['total_qty' => $total_qty, 'total_amount' => $total_amount]);
        }

        checkout::where('user_id', auth()->guard($role)->user()->id)->delete();

        DB::commit();
        Session::flash('success', 'Payment successful!');
        return view('thank-you-cart');

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['msg' => $e->getMessage()], 500);
        }

    }
    public function check_zipcode(Request $request){
        // dd($request->all());
        try {
            //code...

            $input_xml = rawurlencode('<RateV4Request USERID="798KODER4897">
            <Revision>2</Revision>

            <Package ID="101">
            <Service>PRIORITY</Service>
            <ZipOrigination>'.$request->zip.'</ZipOrigination>
            <ZipDestination>99501</ZipDestination>
            <Pounds>0</Pounds>
            <Ounces>1</Ounces>
            <Container>RECTANGULAR</Container>
            <Size>LARGE</Size>
            <Width>5</Width>
            <Length>5</Length>
            <Height>5</Height>
            <Girth>0</Girth>
            <Value>1</Value>
            <SpecialServices>
            <SpecialService>1</SpecialService>
            </SpecialServices>
            </Package>

            </RateV4Request>');
             $url =  "https://secure.shippingapis.com/ShippingAPI.dll?API=RateV4&xml=" . $input_xml;


            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
            curl_setopt( $ch , CURLOPT_SSL_VERIFYPEER , false );
            curl_setopt( $ch , CURLOPT_SSL_VERIFYHOST , false );
            $data = curl_exec($ch);
            curl_close($ch);
            $array_data = json_decode(json_encode(simplexml_load_string($data)), true);

                if(isset($array_data['Package']['Postage'])){
                    return true;
                }else{
                    return false;
                }
            } catch (\Throwable $th) {
            return $th->getMessage();

            }
    }
    public function wishlist_ajax(Request $request)
    {
        if (auth()->check()) {
            $json = array();
            $product_id = $request->id;
            $user_id = auth()->user()->id;
            $checkout_update = wishlist::where('user_id', $user_id)->where('product_id', $product_id)->first();
            if (empty($checkout_update)) {
                wishlist::create([
                    'user_id' => $user_id,
                    'product_id' => $product_id,
                ]);
                $porduct = product::where('id',$product_id)->first(['id','user_id','name']);
                $buyer = User::where('id',$user_id)->first(['id','name']);
                isset($porduct->users) ? $this->sendEmailForWishlist($buyer,$porduct) : false;
                $json['type'] = 'success';
                $json['message'] = "Product is added on your wish list";
                return $json;
            } else {
                $json['type'] = 'error';
                $json['message'] = "This product already in your wish list";
                return $json;
            }
        } else {
            $json['type'] = 'error';
            $json['message'] = "Please login your account first";
            return $json;
        }
    }

    private function sendEmailForWishlist($buyer,$product){
        try {
            $data['name'] = $product->users->name;
            $data['email'] = $product->users->email;
            $data['bodytxt'] = "Mr/Mrs: ".$buyer->name." added your product " .$product->name. " in his/her wishlist items.";
            $this->sendMail($data, 'emails.actions_send');
        } catch (\Throwable $th) {
            return false;
        }
    }

    function wishlist()
    {
        $product = product::join('wishlists', 'wishlists.product_id', 'products.id')->where('wishlists.user_id', auth()->user()->id)->where('products.status', 'Approved')->select('products.*', 'wishlists.id as wishlistID')->get();
        return view('wishlist', compact('product'));
    }

    function wishlish_delete($id)
    {
        $trip = wishlist::findorfail($id);
        $trip->delete();
        return back()->withErrors(['msg' => 'Wishlist item deleted successfully!!']);
    }

    public function  shipping_ajax(Request $request)
    {

        if (request()->get('twoway')) {
        }
        $checkout = checkout::where('user_id', auth()->guard('buyer')->user()->id)->get();

        $call_data = [];
        $array_counter  = 0;

        foreach ($checkout as $key) {
            $product = product::where('id', $key->product_id)->first();
            $zip = request()->get('zip');
            if (request()->get('twoway')) {
                $product = product::where('id', $request->product_id)->first();
            }

            $warehouse = Warehouse::where('user_id', $product->user_id)->first();
            $warehouse_zip = 99501;
            if ($product->price >= 200) {
                $warehouse_zip = 99501;
            } else {
                if (!$zip) {
                    $warehouse_zip = isset($warehouse->zip) ? $warehouse->zip : 99501;
                }
                $warehouse_zip = $zip;
            }

            $input_xml = rawurlencode('<RateV4Request USERID="798KODER4897">
        <Revision>2</Revision>

        <Package ID="' . $key->id . '">
        <Service>PRIORITY</Service>
        <ZipOrigination>' . $request->zip . '</ZipOrigination>
        <ZipDestination>' . $warehouse_zip . '</ZipDestination>
        <Pounds>0</Pounds>
        <Ounces>1</Ounces>
        <Container>RECTANGULAR</Container>
        <Size>LARGE</Size>
        <Width>' . $product->width / 2.54 . '</Width>
        <Length>' . $product->length / 2.54 . '</Length>
        <Height>' . $product->height / 2.54 . '</Height>
        <Girth>0</Girth>
        <Value>1</Value>
        <SpecialServices>
        <SpecialService>1</SpecialService>
        </SpecialServices>
        </Package>

        </RateV4Request>');
            $url =  "https://secure.shippingapis.com/ShippingAPI.dll?API=RateV4&xml=" . $input_xml;


            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            $data = curl_exec($ch);
            curl_close($ch);
            $array_data = json_decode(json_encode(simplexml_load_string($data)), true);
            $rate = $array_data['Package']['Postage'];


            $call_data['Rate'][$array_counter] = $rate['Rate'];
            $call_data['id'][$array_counter] =  $key->id;

            $array_counter++;
        }

        return $call_data;
    }





    public function checkoutSingle(Request $request)
    {
        if (Auth()->guard('seller')->check()) {
            $role = 'seller';
        } else if (Auth()->guard('buyer')->check()) {
            $role = 'buyer';
        } else if (Auth()->guard('admin')->check()) {
            $role = 'admin';
        }
        $checkout = checkout::findOrFail($request->checkout_id);
        DB::beginTransaction();
        try {
        $shipping = shipping::where('user_id', auth()->guard($role)->user()->id)->first();
        if (!empty($shiping)) {
            if (isset($request->shipping_checked)) {
                $shipping->firstname = $request->shipping_firstname;
                $shipping->lastname = $request->shipping_lastname;
                $shipping->email = $request->shipping_email;
                $shipping->phone = $request->shipping_phone;
                $shipping->address = $request->shipping_address;
                $shipping->address_optional = isset($request->shipping_address2) ? $request->shipping_address2 : ' ';
                $shipping->country = $request->shipping_country;
                $shipping->state = $request->shipping_state;
                $shipping->zip = $request->shipping_zip;
                $shipping->user_id = auth()->guard($role)->user()->id;
            } else {
                $shipping->firstname = $request->billing_firstname;
                $shipping->lastname = $request->billing_lastname;
                $shipping->email = $request->billing_email;
                $shipping->phone = $request->billing_phone;
                $shipping->address = $request->billing_address;
                $shipping->address_optional = isset($request->billing_address2) ? $request->billing_address2 : ' ';
                $shipping->country = $request->billing_country;
                $shipping->state = $request->billing_state;
                $shipping->zip = $request->billing_zip;
                $shipping->user_id = auth()->guard($role)->user()->id;
            }
            $shipping->save();
        } else {
            $shipping = new shipping;
            if (isset($request->shipping_checked)) {
                $shipping->firstname = $request->shipping_firstname;
                $shipping->lastname = $request->shipping_lastname;
                $shipping->email = $request->shipping_email;
                $shipping->phone = $request->shipping_phone;
                $shipping->address = $request->shipping_address;
                $shipping->address_optional = isset($request->shipping_address2) ? $request->shipping_address2 : ' ';
                $shipping->country = $request->shipping_country;
                $shipping->state = $request->shipping_state;
                $shipping->zip = $request->shipping_zip;
                $shipping->user_id = auth()->guard($role)->user()->id;
            } else {
                $shipping->firstname = $request->billing_firstname;
                $shipping->lastname = $request->billing_lastname;
                $shipping->email = $request->billing_email;
                $shipping->phone = $request->billing_phone;
                $shipping->address = $request->billing_address;
                $shipping->address_optional = isset($request->billing_address2) ? $request->billing_address2 : ' ';
                $shipping->country = $request->billing_country;
                $shipping->state = $request->billing_state;
                $shipping->zip = $request->billing_zip;
                $shipping->user_id = auth()->guard($role)->user()->id;
            }
            $shipping->save();
        }

            $billing = new billing;
            $billing->firstname = $request->billing_firstname;
            $billing->lastname = $request->billing_lastname;
            $billing->email = $request->billing_email;
            $billing->phone = $request->billing_phone;
            $billing->address = $request->billing_address;
            $billing->address_optional = isset($request->billing_address2) ? $request->billing_address2 : ' ';
            $billing->country = $request->billing_country;
            $billing->state = $request->billing_state;
            $billing->zip = $request->billing_zip;
            $billing->user_id = auth()->guard($role)->user()->id;
            $billing->save();

            $checking = 1;
            $shpping_price = $this->shippingAjaxSingle($checkout->id,$shipping->zip,$checking);
            $order_item = new order_item;
            $order_item->product_id = $checkout->product_id;
            $order_item->price = $checkout->price;
            $order_item->qty = $checkout->qty;
            $order_item->shipping_price = $shpping_price;
            $order_item->seller_id = $checkout->products->user_id;
            $order_item->user_id = auth()->guard($role)->user()->id;
            $order_item->current_order = 0;
            $order_item->order_id = 0;
            $order_item->color = $checkout->color;
            $order_item->size = $checkout->size;
            $order_item->save();

            $selleremail = User::where('id', $checkout->products->user_id)->first();
            $buyreemail = User::where('id', auth()->guard($role)->user()->id)->first();
            $adminemail = User::where('id', 1)->first();
            //Seller Email/

            $data['name'] = $selleremail->name;
            $data['email'] = $selleremail->email;
            $data['bodytxt'] = "$buyreemail->name  purchase a product name:" . $checkout->products->name . " QTY: " . $checkout->qty . " price: " . $checkout->price;
            $this->sendMail($data, 'emails.actions_send');
            //buyer Email/
            $data['name'] = $buyreemail->name;
            $data['email'] = $buyreemail->email;
            $data['bodytxt'] = "your order is : " . $checkout->products->name . " QTY: " . $checkout->qty . " price: " . $checkout->price;
            $this->sendMail($data, 'emails.actions_send');
            // Admin Email/
            $data['name'] = $adminemail->name;
            $data['email'] = $adminemail->email;
            $data['bodytxt'] = "$buyreemail->name  purchase a product " . $checkout->products->name . " QTY: " . $checkout->qty . " price: " . $checkout->price;
            $this->sendMail($data, 'emails.actions_send');


        $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
        if ($request->has('stripeToken') && $request->stripeToken !== null && $request->payment_method === "new") {
            $result_strip =  $stripe->charges->create([
                'amount' => (($checkout->price + $shpping_price)) * 100,
                'currency' => 'usd',
                'source' => $request->stripeToken,
                'description' =>  "Billing Name:" . $request->billing_firstname . ' ' . $request->billing_lastname,
            ]);
        } else {
            if (auth()->guard($role)->user()->stripe_customer_id) {
                $result_strip = $stripe->charges->create([
                    'amount' => (($checkout->price + $shpping_price)) * 100,
                    'currency' => 'usd',
                    'customer' =>  auth()->guard($role)->user()->stripe_customer_id,
                    'description' =>  "Billing Name:" . $request->billing_firstname . ' ' . $request->billing_lastname,
                ]);

                if (isset($result_strip['id']) && $result_strip['status'] !== "succeeded") {
                    DB::rollback();
                    return redirect()->back()->with(['msg' => 'payment not charged, please use new card']);
                }
            } else {
                DB::rollback();
                return redirect()->back()->with(['msg' => 'Card not found, please use new card']);
            }
        }



            $order = new order;
            $order->total_amount = ($checkout->price + $shpping_price);
            $order->total_qty = 1;
            $order->total_qty = 1;
            $order->shipping_id = $shipping->id;
            $order->billing_id = $billing->id;
            $order->user_id = auth()->guard($role)->user()->id;
            $order->seller_id = $selleremail->id;
            $order->save();

            $order_item->update(['current_order' => 1, 'order_id' => $order->id, 'charge_id' => $result_strip->id]);


        $checkout->delete();
        DB::commit();
        return redirect()->route('checkout.list')->with('alert-success', 'Payment successful! Thank You for shopping');
    }

        catch (\Exception $e) {
            DB::rollback();
            dd($e);
            return response()->json(['msg' => $e->getMessage()], 500);
        }

    }



    public function  shippingAjaxSingle($checkout_id,$zip , $checking = null)
    {

        $checkout = checkout::find($checkout_id);
        $product = product::where('id', $checkout->product_id)->first();
        $seller_warehouse = Warehouse::where('user_id', $product->user_id)->first();
        $rate = [];
        if ($checkout->type !== "auction" && $product->price > 200) {
            $admin_wrehouse = Warehouse::where('user_id', 1)->first();
            $rate['one_way'] = $this->Shipment($checkout ,$product,$seller_warehouse->zip, $admin_wrehouse->zip);
            $rate['two_way'] = $this->Shipment($checkout ,$product,$admin_wrehouse->zip, $zip);
        }
        else{
            $rate['one_way'] = $this->Shipment($checkout ,$product,$seller_warehouse->zip, $zip);
            $rate['two_way'] = [];
        }

        if($checking != null){
           $total = 0;
           $rate_1 = (!empty($rate['one_way'])) ? $rate['one_way']['Rate']: 0;
           $rate_2 = (!empty($rate['two_way'])) ? $rate['two_way']['Rate']: 0;
           return $rate_1 + $rate_2;
        }

        return $rate;

    }

    public function Shipment($checkout ,$product, $ZipOrigination,$ZipDestination)
    {

        $input_xml = rawurlencode('<RateV4Request USERID="798KODER4897">
        <Revision>2</Revision>

        <Package ID="' . $checkout->id . '">
        <Service>PRIORITY</Service>
        <ZipOrigination>' .$ZipOrigination .'</ZipOrigination>
        <ZipDestination>' . $ZipDestination . '</ZipDestination>
        <Pounds>0</Pounds>
        <Ounces>1</Ounces>
        <Container>RECTANGULAR</Container>
        <Size>LARGE</Size>
        <Width>' . $product->width / 2.54 . '</Width>
        <Length>' . $product->length / 2.54 . '</Length>
        <Height>' . $product->height / 2.54 . '</Height>
        <Girth>0</Girth>
        <Value>1</Value>
        <SpecialServices>
        <SpecialService>1</SpecialService>
        </SpecialServices>
        </Package>
        </RateV4Request>');
        $url =  "https://secure.shippingapis.com/ShippingAPI.dll?API=RateV4&xml=" . $input_xml;


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $data = curl_exec($ch);
        curl_close($ch);
        $array_data = json_decode(json_encode(simplexml_load_string($data)), true);

       return $array_data['Package']['Postage'];

    }




    public function usps($user_zip, $product_user_id, $width, $length, $height, $product_id)
    {

        $product = product::where('id', $product_user_id)->first();

        $warehouse = Warehouse::where('user_id', $product_id)->first();

        $warehouse_zip = isset($warehouse->zip) ? $warehouse->zip : 99501;

        $input_xml = rawurlencode('<RateV4Request USERID="798KODER4897">
        <Revision>2</Revision>

        <Package ID="' . rand(10, 100) . '">
        <Service>PRIORITY</Service>
        <ZipOrigination>' . $user_zip . '</ZipOrigination>
        <ZipDestination>' . $warehouse_zip . '</ZipDestination>
        <Pounds>0</Pounds>
        <Ounces>1</Ounces>
        <Container>RECTANGULAR</Container>
        <Size>LARGE</Size>
        <Width>' . $width / 2.54 . '</Width>
        <Length>' . $length / 2.54 . '</Length>
        <Height>' . $height / 2.54 . '</Height>
        <Girth>0</Girth>
        <Value>2</Value>
        <SpecialServices>
        <SpecialService>1</SpecialService>
        </SpecialServices>
        </Package>

        </RateV4Request>');

        //
        $url =  "https://secure.shippingapis.com/ShippingAPI.dll?API=RateV4&xml=" . $input_xml;


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $data = curl_exec($ch);
        curl_close($ch);
        $array_data = json_decode(json_encode(simplexml_load_string($data)), true);
        $rate = $array_data['Package']['Postage'];






        return $rate['Rate'];
    }





}
