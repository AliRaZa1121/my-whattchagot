<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormBuilderController extends Controller
{
    public function AffiliateQuoteTripDetailsApend(Request $request)
    {
        if (isset($request->turn) && $request->turn != "") {
            return view('pages.affiliate.booking._TripDetails', ['turn' => $request->turn]);
        }
        return abort(500, 'Turn Required');
    }
}
