<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\RequestTradeProduct;
use App\Models\product;
use App\Models\trade_price;
use App\Models\trading_product;
use App\Models\category;
use App\Models\sub_category;
use App\Models\product_image;
use DB;
use App\Models\color;
use App\Models\order_item;
use App\Models\size_value;
use App\Models\Brand;
use App\Models\make_trade;
use App\Models\make_trade_images;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;


class RequestTradeProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $request_trade = product::join('make_trade', 'make_trade.product_id', 'products.id')->select('make_trade.*', 'make_trade.id as maketrade_id',   'make_trade.status as maketrade_status', 'products.*', 'products.id as id')
        ->where('products.user_id', auth()->user()->id)
        ->where('products.flags', 'request-trade')
        ->get();

        $trade_price = trade_price::first();
        // dd($request_trade,auth()->user()->id);

        return view('pages.admin.product.request-trading.index', compact('request_trade','trade_price'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $brand = Brand::all();
        $category = category::all();
        $sub_category = sub_category::all();
        $trade_product = product::find($id);
        return view('pages.admin.product.request-trading.create', compact('category', 'sub_category', 'brand','trade_product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $trade_product = product::findOrFail($request->product_id);
        $request->validate([
            'name' => 'required',
            'price' => 'required|numeric|min:1|not_in:0',
            'category_id' => 'required',
            'sub_category_id' => 'required',
            'length_size' => 'required|max:50|numeric',
            'height_size' => 'required|max:50|numeric',
            'width_size' => 'required|max:50|numeric',
            // 'start_time' => 'required',
            // 'end_time' => 'required',
            // 'start_date'      => 'required|date|before:end_date',
            // 'end_date'        => 'date|after:start_date',
            'brand' => 'required',
            'banner_image' => 'required',
            'short_details' => 'required',
            'price' => 'required|numeric'
        ]);

        $data = $request->except('_token');


        $data['length'] = $data['length_size'];
        $data['height'] = $data['height_size'];
        $data['width'] = $data['width_size'];
        $data['brand_id'] = $data['brand'];
        $data['flags'] = 'request-trade';
        $data['user_id'] = auth()->user()->id;
        $data['status'] = "Approved";

        DB::beginTransaction();
        try {
         $product = product::create($data);
         $seller = product::find($request->product_id);
         $make_trade =   make_trade::create([
                'product_id' => $product->id,
                'request_product_id' =>  $request->product_id,
                'user_id' =>  $data['user_id'],
                'seller_id' => $seller->user_id,
                'title' => $request->name,
                'description' => $request->short_details,
                'color' => $request->color,
                'size' => $request->size_value,

            ]);

            $imagecount = isset($request->banner_image) ? count($request->banner_image) : 0;
            if ($imagecount > 0) {
                for ($i = 0; $i < $imagecount; $i++) {
                    product_image::create([
                        'product_id' => $product->id,
                        'image' => $request->banner_image[$i],
                        'type' => 'request-trade'

                    ]);
                }

            }
            DB::commit();
            return response()->json(['msg' => 'Trade Request inserted successfully.']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['msg' => $e->getMessage()], 500);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RequestTradeProduct  $requestTradeProduct
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = category::all();
        $sub_category = sub_category::all();
        $product = product::join('make_trade', 'make_trade.product_id', 'products.id')->select('make_trade.*', 'make_trade.id as maketrade_id', 'make_trade.description as short_details' , 'products.*', 'products.id as id')->where('products.id', $id)->first();
        $brand = Brand::all();
        return view('pages.admin.product.request-trading.show', compact('category', 'sub_category', 'product','brand'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RequestTradeProduct  $requestTradeProduct
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = category::all();
        $sub_category = sub_category::all();
        $product = product::join('make_trade', 'make_trade.product_id', 'products.id')->select('make_trade.*', 'make_trade.id as maketrade_id', 'make_trade.description as short_details' , 'products.*', 'products.id as id')->where('products.id', $id)->first();
        $brand = Brand::all();

        return view('pages.admin.product.request-trading.edit', compact('category', 'sub_category', 'product','brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RequestTradeProduct  $requestTradeProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'price' => 'required|numeric|min:1|not_in:0',
            'category_id' => 'required',
            'sub_category_id' => 'required',
            'length_size' => 'required|max:50|numeric',
            'height_size' => 'required|max:50|numeric',
            'width_size' => 'required|max:50|numeric',
            'brand' => 'required',
            'banner_image' => 'required',
            'short_details' => 'required',
            'price' => 'required|numeric',
        ]);
        $data = $request->except('_token');

        $data['brand_id'] = $data['brand'];
        $data['flags'] = 'request-trade';
        $data['user_id'] = auth()->user()->id;
        $data['status'] = "Approved";
        $data['length'] = $data['length_size'];
        $data['height'] = $data['height_size'];
        $data['width'] = $data['width_size'];

        DB::beginTransaction();
        try {
            $product = product::findOrFail($request->id);

            $product->update($data);
            $make_trade = make_trade::where('product_id', $request->id)->first();
            $make_trade->update([
                'title' => $request->name,
                'description' => $request->short_details,
                'color' => $request->color,
                'size' => $request->size_value
            ]);

            $imagecount = isset($request->banner_image) ? count($request->banner_image) : 0;

             product_image::where('product_id',$product->id)->delete();
            //  dd($imagecount);
                if ($imagecount > 0) {
                    for ($i = 0; $i < $imagecount; $i++) {
                        product_image::create([
                            'product_id' => $product->id,
                            'image' => $request->banner_image[$i],
                            'type' => 'request-trade'

                        ]);

                    }
            }



            DB::commit();
            return response()->json(['msg' => 'Product Update successfully.']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['msg' => $e->getMessage()], 500);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RequestTradeProduct  $requestTradeProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            product_image::where('product_id',$id)->delete();
            make_trade::where('request_product_id',$id)->delete();
            product::find($id)->delete();
            return redirect()->back()->with('alert-success', "Request product deleted!");
        } catch (\Throwable $th) {
            return redirect()->back()->with('alert-danger', $th->getMessage());

        }
    }

    public function uploadAttachment(Request $request, $type)
    {
        $file2 = $request->files->get('banner')->getClientOriginalName();


            $request->validate([
                'banner' => 'required|image',
            ]);
            $image = "image";

        $fileName = $request->$image->store('public/product');
        $fileName = Str::after($fileName, 'public/');
        return response(['msg' => 'Image has been successfully uploaded.', 'file_name' => $fileName, 'file2' => $file2]);
    }
}
