<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\product;
use Illuminate\Support\Str;
use App\Models\sub_category;
use App\Models\category;
use App\Models\Brand;
use App\Models\ProductDetail;
use App\Models\product_image;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
class ProductController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        if (auth()->user()->user_type === "admin") {
            $products = product::where('flags','!=','request-trade')->latest()->get();
        } else {
            $products = product::where('user_id', auth()->user()->id)->where('flags','!=','request-trade')->latest()->get();
        }
        return view('pages.admin.product.index', compact('products'));
    }


    public function edit($id)
    {
        $category = category::all();
        $product = product::where('products.id', $id)->first();
        $sub_category = sub_category::where('id',$product->sub_category_id)->get();
        $brand = Brand::all();

        return view('pages.admin.product.edit', compact('category', 'sub_category', 'product', 'brand'));
    }

    public function show($id)
    {
        $category = category::all();
        $product = product::where('products.id', $id)->first();
        $sub_category = sub_category::where('id',$product->sub_category_id)->get();
        $brand = Brand::all();
        return view('pages.admin.product.show', compact('category', 'sub_category', 'product', 'brand'));
    }
    public function create()
    {
        $category = category::all();
        $sub_category = sub_category::all();
        $brand = Brand::all();
        return view('pages.admin.product.create', compact('category', 'sub_category', 'brand'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'price' => 'required|numeric|gt:0',
            'category_id' => 'required',
            'sub_category_id' => 'required',
            'length_size' => 'required|max:50|numeric',
            'height_size' => 'required|max:50|numeric',
            'width_size' => 'required|max:50|numeric',
            'brand' => 'required',
            'banner_image' => 'required',
            'color' => 'required',
            'size_value' => 'required',
            'short_details' => 'required',
        ]);
        $data = $request->except('_token');
        $data['length'] = $data['length_size'];
        $data['height'] = $data['height_size'];
        $data['width'] = $data['width_size'];
        $data['brand_id'] = $data['brand'];
        $data['size'] = $data['size_value'];

        if($request->flags === "auction"){
            $request->validate([
                'start_date'      => 'required|date_format:Y-m-d H:i|after:yesterday',
                'auction_end'        => 'required|gt:0|max:7',
            ]);
            $data['end_date'] = Carbon::parse($data['start_date'])->addDays($data['auction_end']);
        }
        else if($request->start_date !== null || $request->end_date !== null){
            $request->validate([
                'start_date'      => 'required|date_format:Y-m-d H:i',
                'end_date'        => 'required|date_format:Y-m-d H:i|after:start_date',
            ]);
        }
        else{
            $data['start_date'] = Carbon::now();
            $data['end_date'] = Carbon::now()->addYear(5);
        }


        $data['user_id'] = auth()->user()->id;

        DB::beginTransaction();
        try {
            $product = product::create($data);
            $data['product_id'] = $product->id;
            ProductDetail::create($data);
            $imagecount = isset($request->banner_image) ? count($request->banner_image) : 0;
            if ($imagecount > 0) {
                for ($i = 0; $i < $imagecount; $i++) {
                    product_image::create([
                        'product_id' => $product->id,
                        'image' => $request->banner_image[$i],
                    ]);
                }
            }
            DB::commit();
            return response()->json(['msg' => 'Product inserted successfully.']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['msg' => $e->getMessage()], 500);
        }
    }
    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'price' => 'required|numeric|gt:0',
            'category_id' => 'required',
            'sub_category_id' => 'required',
            'length_size' => 'required|max:50|numeric',
            'height_size' => 'required|max:50|numeric',
            'width_size' => 'required|max:50|numeric',
            'brand' => 'required',
            'banner_image' => 'required',
            'color' => 'required',
            'size_value' => 'required',
            'short_details' => 'required',
        ]);
        $data = $request->except(['_token','_method']);
        $data['length'] = $data['length_size'];
        $data['height'] = $data['height_size'];
        $data['width'] = $data['width_size'];
        $data['brand_id'] = $data['brand'];
        $data['size'] = $data['size_value'];
        if($request->flags === "auction"){
            $request->validate([
                'start_date'      => 'required|date_format:Y-m-d H:i|after:yesterday',
                'auction_end'        => 'required|gt:0|max:7',
            ]);
            $data['end_date'] = Carbon::parse($data['start_date'])->addDays($data['auction_end']);
        }
        else if($request->start_date !== null || $request->end_date !== null){
            $request->validate([
                'start_date'      => 'required|date_format:Y-m-d H:i',
                'end_date'        => 'required|date_format:Y-m-d H:i|after:start_date',
            ]);
        }
        else{
            $data['start_date'] = Carbon::now();
            $data['end_date'] = Carbon::now()->addYear(5);
        }

        DB::beginTransaction();
        try {
            $product = product::findOrFail($id);
            $product->update($data);
            $details = ProductDetail::where('product_id',$id)->first();
            $details->update($data);
            $imagecount = count($request->banner_image);
            $images = product_image::where('product_id', $product->id)->get();
            foreach ($images as $key => $value) {
                if (File::exists(public_path('storage/'.$value->image))) {
                    unlink(public_path('storage/'.$value->image));
                }
                $value->delete();
            }
            if ($imagecount > 0) {
                for ($i = 0; $i < $imagecount; $i++) {
                    product_image::create([
                        'product_id' => $product->id,
                        'image' => $request->banner_image[$i],
                    ]);
                }
            }
            DB::commit();
            return response()->json(['msg' => 'Product Update successfully.']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['msg' => $e->getMessage()], 500);
        }
    }


    public function uploadAttachment(Request $request, $type)
    {
        $file2 = $request->files->get('banner')->getClientOriginalName();
        $image = "banner";
        if ($type == 'banner') {
            $request->validate([
                'banner' => 'required|image',
            ]);
        } else {
            $request->validate([
                'image' => 'required|image',
            ]);
            $image = "image";
        }
        $fileName = $request->$image->store('public/product');
        $fileName = Str::after($fileName, 'public/');
        return response(['msg' => 'Image has been successfully uploaded.', 'file_name' => $fileName, 'file2' => $file2]);
    }


    public function updateStatus(product $Product, $status)
    {
        try {
            $Product->update(['status' => $status]);
        } catch (\Throwable $th) {
            return back()->with('alert-danger', $th->getMessage());
        }

        return back()->with('alert-success', "Your product is {$status} successfully.");
    }
}
