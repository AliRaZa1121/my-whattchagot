<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\order_item;
use App\Models\SellerPackage as SP;
use App\Models\product;
use App\Models\contactus;
use App\Models\Newsletter;
use App\Models\checkout;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getcontactus(){

        $contactus = contactus::get();
         return view('pages.admin.contactus.index', compact('contactus'));
    }

    public function contactsdelete($contactid){
        $del = contactus::destroy($contactid);
        if($del){
        return redirect()->back()->with('message', 'Deleted Successfully');
        }else{
        return redirect()->back()->with('message', 'Error Occured');
        }
    }

    public function index(Request $request)
    {
        $pkg = [];
        $count_pending = [];
        $wishlistCount = "";
        if($request->has('code')){
            $input = $request->all();
            $this->StripeAccountConnect($input);
        }
        if (Auth()->guard('seller')->check()) {
            $auctionCount = order_item::join('products', 'products.id', 'order_items.product_id')->where('flags', 'auction')->where('order_items.seller_id', Auth()->guard('seller')->user()->id)->count();
            $tradingCount = order_item::join('products', 'products.id', 'order_items.product_id')->where('flags', 'trade')->where('order_items.seller_id', Auth()->guard('seller')->user()->id)->count();
            $sellerCount = order_item::join('products', 'products.id', 'order_items.product_id')->where('flags', 'seller')->where('order_items.seller_id', Auth()->guard('seller')->user()->id)->count();
         
            if(!empty(auth()->user()->seller_package_id)){
                $pkg = SP::find(auth()->user()->seller_package_id);
                if(!empty($pkg)){
                    $from   = auth()->user()->package_start_date;
                    $to = auth()->user()->package_valid_till;

                    $pkg_seller_count  =  product::where('user_id', auth()->user()->id)->where('flags', 'seller')->whereBetween('created_at', [$from, $to])->count();
                    $pkg_auction_count  =   product::where('user_id', auth()->user()->id)->where('flags', 'auction')->whereBetween('created_at', [$from, $to])->count();
                    $pkg_trade_count  =  product::where('user_id', auth()->user()->id)->where('flags', 'trade')->whereBetween('created_at', [$from, $to])->count();


                    return view('pages.admin.dashboard', compact('tradingCount', 'auctionCount', 'sellerCount','pkg','pkg_seller_count','pkg_auction_count','pkg_trade_count'));

                }
            }
        }

        else if (Auth()->guard('buyer')->check()) {
            $auctionCount = order_item::join('products', 'products.id', 'order_items.product_id')->where('flags', 'auction')->where('order_items.user_id', Auth()->guard('buyer')->user()->id)->count();
            $tradingCount = order_item::join('products', 'products.id', 'order_items.product_id')->where('flags', 'trade')->where('order_items.user_id', Auth()->guard('buyer')->user()->id)->count();
            $sellerCount = order_item::join('products', 'products.id', 'order_items.product_id')->where('flags', 'seller')->where('order_items.user_id', Auth()->guard('buyer')->user()->id)->count();
            $wishlistCount = product::join('wishlists', 'wishlists.product_id', 'products.id')->where('wishlists.user_id', auth()->guard('buyer')->user()->id)->where('products.status', 'Approved')->select('products.*', 'wishlists.id as wishlistID')->count();
        } else {
            $auctionCount = order_item::join('products', 'products.id', 'order_items.product_id')->where('flags', 'auction')->count();
            $tradingCount = order_item::join('products', 'products.id', 'order_items.product_id')->where('flags', 'trade')->count();
            $sellerCount = order_item::join('products', 'products.id', 'order_items.product_id')->where('flags', 'seller')->count();
            $wishlistCount= '';
            $count_pending = [
                'pending_tradingCount'  => order_item::join('products', 'products.id', 'order_items.product_id')->where('order_items.status' ,'Pending')->where('products.flags', 'trade')->count(),
                'pending_sellerCount'  => order_item::join('products', 'products.id', 'order_items.product_id')->where('order_items.status' ,'Pending')->where('products.flags', 'seller')->count(),
                'pending_auctionCount' => order_item::join('products','products.id', 'order_items.product_id')->where('order_items.status','Pending')->where('products.flags','auction')->count(),
            ];

        }

        return view('pages.admin.dashboard', compact('wishlistCount','tradingCount', 'auctionCount', 'sellerCount', 'pkg','count_pending'));
    }

    public function StripeAccountConnect($input)
    {
        try {
            $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
            $acc = $stripe->accounts->retrieve(
            $input['code'],
            []
            );
            User::find(Auth()->guard('seller')->user()->id)->update(['stripe_flag' => 1 , 'strip_account_id' => $acc['id']]);
        } catch (\Throwable $th) {
            User::find(Auth()->guard('seller')->user()->id)->update(['stripe_flag' => 1]);
        }

    }

    public function Newsletterindex()
    {
        $data['newsletter'] = Newsletter::all();
        return view('pages.admin.newsletter.index',$data);
    }


    public function checkoutList()
    {
        $data['checkouts'] = checkout::where('user_id' , auth()->guard('buyer')->user()->id)
        ->where('type' ,'!=','selling')
        ->with(['products_images'])->get();

        return view('pages.admin.checkout.index',$data);
    }


}

