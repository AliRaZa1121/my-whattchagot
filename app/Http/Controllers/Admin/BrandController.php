<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Brand;
use DB;
use Illuminate\Support\Str;


class BrandController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $categories = Brand::latest()->get();
        return view('pages.admin.brand.index', compact('categories'));
    }

    public function create()
    {
        return view('pages.admin.brand.create');
    }
    public function edit($id)
    {
        $brand = Brand::where('id',$id)->first();
        return view('pages.admin.brand.edit',compact('brand'));
    }
    public function destroy($id)
    {
        $trip = Brand::findorfail($id);
        $trip->delete();
        return response()->json(['msg' => 'Sub Category deleted successfully!!']);
    }

    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
           'banner_image' => 'required'
        ]);

        $data = $request->except('_token');

        DB::beginTransaction();
        try {
            $Category = Brand::create($data);

            DB::commit();
            return response()->json(['msg' => 'Brand has been inserted successfully'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['msg' => $e->getMessage()], 500);

        }
    }
    public function update(Request $request,$id)
    {
        $category = Brand::find($id);

        $request->validate([
            'name' => 'required'
        ]);

        $data = $request->except('_token');

        DB::beginTransaction();
        try {
            $category->update($data);

            DB::commit();
            return response()->json(['msg' => 'Brand has been inserted successfully'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['msg' => $e->getMessage()], 500);

        }
    }

    public function uploadAttachment(Request $request, $type)
    {
        $image = "banner";
        if ($type == 'banner') {
            $request->validate([
                'banner' => 'required|image',
            ]);
        } else {
            $request->validate([
                'image' => 'required|image',
            ]);
            $image = "image";
        }

        $fileName = $request->$image->store('public/brands');
        $fileName = Str::after($fileName, 'public/');
        return response(['msg' => 'Image uploaded successfully!!', 'file_name' => $fileName]);
    }


}
