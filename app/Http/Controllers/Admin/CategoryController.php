<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Models\category;
use DB;


class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $categories = category::latest()->get();
        return view('pages.admin.category.index', compact('categories'));
    }

    public function create()
    {
        return view('pages.admin.category.create');
    }
    public function edit($id)
    {
        $category = category::where('id',$id)->first();
        return view('pages.admin.category.edit',compact('category'));
    }

    public function store(Request $request)
    {


        $request->validate([
            'name' => 'required',
//            'banner_image' => 'required'
        ]);

        $data = $request->except('_token');

        DB::beginTransaction();
        try {
            $Category = category::create($data);

            DB::commit();
            return response()->json(['msg' => 'Category has been inserted successfully.'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['msg' => $e->getMessage()], 500);

        }
    }
    public function update(Request $request,$id)
    {
        $category = category::find($id);

        $request->validate([
            'name' => 'required'
        ]);

        $data = $request->except('_token');

        DB::beginTransaction();
        try {
            $category->update($data);

            DB::commit();
            return response()->json(['msg' => 'Category has been inserted successfully.'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['msg' => $e->getMessage()], 500);

        }
    }
    public function uploadAttachment(Request $request, $type)
    {
   
        $image = "banner";
        if ($type == 'banner') {
            $request->validate([
                'banner' => 'required|image',
            ]);
        } else {
            $request->validate([
                'image' => 'required|image',
            ]);
            $image = "image";
        }

        $fileName = $request->$image->store('public/category');
        $fileName = Str::after($fileName, 'public/');
        return response(['msg' => 'image uploaded successfully!!', 'file_name' => $fileName]);
    }
    public function destroy($id)
    {
        $trip = category::findorfail($id);
        $trip->delete();
        return response()->json(['msg' => 'Subcategory has been deleted successfully.']);
    }
}
