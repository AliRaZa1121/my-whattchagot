<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\seller_product;
use App\Models\product;
use App\Models\category;
use App\Models\sub_category;
use App\Models\product_image;
use App\Models\User;
use App\Models\color;
use App\Models\size_value;
use App\Models\checkout;
use DB;
use App\Models\Brand;
use App\Models\make_trade;
use App\Models\make_trade_images;
use App\Models\SellerPackage;
use App\Models\Transaction;
use Carbon\Carbon;
use App\Traits\EmailTrait;
//use Illuminate\Support\Facades\Hash;

class SellerProductController extends Controller
{
    use EmailTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('seller-pkg')->only(['create', 'store']);
    }

    public function index()
    {
        if (Auth()->guard('seller')->check()) {
            $role = 'seller';
        } else if (Auth()->guard('buyer')->check()) {
            $role = 'buyer';
        }

        $sellers = product::join('seller_products', 'seller_products.product_id', 'products.id')->select('seller_products.*', 'seller_products.id as seller_id', 'products.*', 'products.id as id')->latest('products.created_at')->where('products.status', '!=', 'Deleted');

        if (isset($role)) {
            $sellers = $sellers->where('products.user_id', Auth()->guard($role)->user()->id)->get();
        } else {
            $sellers = $sellers->get();
        }

        return view('pages.admin.product.seller.index', compact('sellers'));
    }

    public function edit($id)
    {
        $category = category::all();
        $sub_category = sub_category::all();
        $product = product::join('seller_products', 'seller_products.product_id', 'products.id')->select('seller_products.*', 'seller_products.id as auction_id', 'products.*')->where('products.id', $id)->first();
        $brand = Brand::all();

        return view('pages.admin.product.seller.edit', compact('category', 'sub_category', 'product', 'brand'));
    }

    public function show($id)
    {
        $category = category::all();
        $sub_category = sub_category::all();
        $product = product::join('seller_products', 'seller_products.product_id', 'products.id')->select('seller_products.*', 'seller_products.id as auction_id', 'products.*')->where('products.id', $id)->first();
        $brand = Brand::all();
        return view('pages.admin.product.seller.show', compact('category', 'sub_category', 'product', 'brand'));
    }
    public function create()
    {
        $category = category::all();
        $sub_category = sub_category::all();
        $brand = Brand::all();

        return view('pages.admin.product.seller.create', compact('category', 'sub_category', 'brand'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'price' => 'required|numeric|gt:0',
            'category_id' => 'required',
            'sub_category_id' => 'required',
            'length_size' => 'required|max:50|numeric',
            'height_size' => 'required|max:50|numeric',
            'width_size' => 'required|max:50|numeric',
            // 'start_time' => 'required',
            // 'end_time' => 'required',
            'brand' => 'required',
            // 'start_date'      => 'required|date|before:end_date',
            // 'end_date'        => 'date|after:start_date',
            'banner_image' => 'required',
            // 'color' => 'required',
            // 'size_value' => 'required',
            'short_details' => 'required',
        ]);
        $data = $request->except('_token');
        $data['start_date'] = Carbon::now()->format('m/d/Y');
        $data['end_date'] = Carbon::now()->addYear(5)->format('m/d/Y');
        $data['length'] = $data['length_size'];
        $data['height'] = $data['height_size'];
        $data['width'] = $data['width_size'];
        $data['brand_id'] = $data['brand'];
        $data['user_id'] = auth()->user()->id;
        $data['flags'] = 'seller';
        $data['status'] = "Approved";
        if ($request->color !== null) {
            $color = explode(",", $request->color);
        }
        else{
            $color = ["Black"];
        }
        if ($request->size_value !== null) {
            $size_value = explode(",", $request->size_value);
        }
        else{
            $size_value = ["S"];
        }



        DB::beginTransaction();
        try {
            $product = product::create($data);
            seller_product::create([
                'product_id' => $product->id,
                'brand' => $request->brand,
                'start_date' => $data['start_date'],
                'start_time' => Carbon::now()->format('g:i A'),
                'end_date' =>   $data['end_date'],
                'end_time' => Carbon::now()->format('g:i A'),
                'short_details' => $request->short_details,
            ]);

            for ($i = 0; $i < count($color); $i++) {
                color::create([
                    'product_id' => $product->id,
                    'color' => $color[$i],

                ]);
            }
            for ($i = 0; $i < count($size_value); $i++) {
                size_value::create([
                    'product_id' => $product->id,
                    'size' => $size_value[$i],
                ]);
            }

            $imagecount = isset($request->banner_image) ? count($request->banner_image) : 0;

            if ($imagecount > 0) {
                for ($i = 0; $i < $imagecount; $i++) {
                    product_image::create([
                        'product_id' => $product->id,
                        'image' => $request->banner_image[$i],

                    ]);
                }
            }


            DB::commit();
            return response()->json(['msg' => 'Product inserted successfully.']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['msg' => $e->getMessage()], 500);
        }
    }
    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'price' => 'required|numeric|gt:0',
            'category_id' => 'required',
            'sub_category_id' => 'required',
            'length_size' => 'required|max:50|numeric',
            'height_size' => 'required|max:50|numeric',
            'width_size' => 'required|max:50|numeric',
            // 'start_time' => 'required',
            // 'end_time' => 'required',
            // 'start_date'      => 'required|date|before:end_date',
            // 'end_date'        => 'date|after:start_date',
            'brand' => 'required',
            'banner_image' => 'required',
            'color' => 'required',
            'size_value' => 'required',
            'short_details' => 'required',

        ]);
        $data = $request->except('_token');
        // $data['start_date'] = $data['start_date'];
        // $data['end_date'] = $data['end_date'];
        $data['length'] = $data['length_size'];
        $data['height'] = $data['height_size'];
        $data['width'] = $data['width_size'];
        $data['brand_id'] = $data['brand'];
        if ($request->color !== null) {
            $color = explode(",", $request->color);
        }
        else{
            $color = ["Black"];
        }
        if ($request->size_value !== null) {
            $size_value = explode(",", $request->size_value);
        }
        else{
            $size_value = ["S"];
        }

        DB::beginTransaction();
        try {
            $product = product::findOrFail($id);
            $product->update($data);
            seller_product::where('product_id', $id)->update([
                'product_id' => $product->id,
                'brand' => $request->brand,
                // 'start_date' => $request->start_date,
                // 'start_time' => $request->start_time,
                // 'end_date' => $request->end_date,
                // 'end_time' => $request->end_time,
                'short_details' => $request->short_details,
            ]);
            size_value::where('product_id', $product->id)->delete();
            color::where('product_id', $product->id)->delete();

            for ($i = 0; $i < count($color); $i++) {
                color::create([
                    'product_id' => $product->id,
                    'color' => $color[$i],

                ]);
            }
            for ($i = 0; $i < count($size_value); $i++) {
                size_value::create([
                    'product_id' => $product->id,
                    'size' => $size_value[$i],

                ]);
            }

            $imagecount = count($request->banner_image);

            product_image::where('product_id', $product->id)->delete();
            if ($imagecount > 0) {
                for ($i = 0; $i < $imagecount; $i++) {
                    product_image::create([
                        'product_id' => $product->id,
                        'image' => $request->banner_image[$i],

                    ]);
                }
            }


            DB::commit();
            return response()->json(['msg' => 'Product Update successfully.']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['msg' => $e->getMessage()], 500);
        }
    }
   
    public function updateStatus(User $user, $status)
    {
        $user->update(['status' => $status]);
        return redirect()->back()->with('alert-success', "Status marked as {$status} successfully.");
    }
    public function maketrade()
    {

        if (Auth()->guard('seller')->check()) {
            $make_trade = make_trade::where('seller_id' , auth()->user()->id)->where('status' ,'!=' , 'Cancel')->where('status' ,'!=' , 'Decline')
            ->groupBy(['request_product_id','user_id','status'])->with('product_images', 'trade_with_products')->latest()->paginate(2);
            return view('pages.admin.product.maketrade.index', compact('make_trade'));
        } else {
            $make_trade = make_trade::where('status' ,'!=' , 'Cancel')->where('status' ,'!=' , 'Decline')->with('product_images', 'trade_with_products')->groupBy(['request_product_id','user_id','status'])->latest()->get();
            return view('pages.admin.product.maketrade.adminindex', compact('make_trade'));
        }

    }

    public function maketradeDetail($request_product_id,$user_id,$status)
    {
        $make_trade = make_trade::where([
           'request_product_id' => $request_product_id,
           'user_id' => $user_id,
           'status' => $status
        ])->with('product_images', 'products')->latest()->get();
        return view('pages.admin.product.maketrade.details', compact('make_trade'));

    }
    public function updateStatus_trade($request_product_id,$user_id, $status)
    {
        try {
         $make_trade = make_trade::where([
            'request_product_id' => $request_product_id,
            'user_id' => $user_id
         ])->with('trade_with_products', 'products')->latest()->get();
        $trade_with = isset($make_trade[0]->trade_with_products) ? $make_trade[0]->trade_with_products : [];

        if (!empty($trade_with)) {
            if ($status === "Accept") {

                if ($trade_with->price >= 200) {
                    // Buyer
                    $buyer = $this->checkPackage($make_trade[0]->user_id, $trade_with->price);
                    $this->tradingProcessHolding($buyer['amount'], $trade_with, $status, $buyer['user'], $make_trade);

                    // Seller
                    $seller = $this->checkPackage($make_trade[0]->seller_id , $trade_with->price);
                    $this->tradingProcessHolding($seller['amount'], $trade_with, $status, $seller['user'], $make_trade);
                } else {
                    // Buyer
                    $buyer = $this->checkPackage($make_trade[0]->user_id , $trade_with->price);
                    $this->tradingProcess($buyer['amount'], $trade_with, $status, $buyer['user'], $make_trade);

                    // Seller
                    $seller = $this->checkPackage($make_trade[0]->seller_id , $trade_with->price);
                    $this->tradingProcess($seller['amount'], $trade_with, $status, $seller['user'], $make_trade);
                }
                $make_trade = make_trade::where('request_product_id' , $request_product_id)->where('user_id','!=', $user_id)
                ->update(['status' => 'Cancel']);
                // return redirect()->back()->with('alert-success', "Status marked as {$status} successfully.");

            }
            else {
                foreach ($make_trade as $key => $data) {
                    $data->update(['status' => $status]);
                }
            }
            $this->EmailNotify($status,$make_trade[0]->user_id);
            return redirect()->back()->with('alert-success', "Status marked as {$status} successfully.");

        } else {
            abort(404);
        }

    } catch (\Throwable $th) {
        return redirect()->back()->with('alert-danger', $th->getMessage());
    }

    }

    public function EmailNotify($status,$user_id)
    {
         try {
            $buyer =User::where('id', $user_id)->first();
            $buyreemail=$buyer->email;
            $buyrename=$buyer->name;
            $body= "<b>Hello '$buyrename'!</b> Your Trade request has been '$status'";
            $data['name']= $buyrename;
            $data['email']= $buyreemail;
            $data['bodytxt']= $body;
            $this->sendMail($data, 'emails.actions_send');
            return true;
         } catch (\Throwable $th) {
            return false;
         }

    }

    public function checkPackage($id ,$trade_with_price)
    {

        $user = User::where('id', $id)->first(['id', 'user_type', 'stripe_customer_id', 'package_valid_till','seller_package_id']);
        $status = false;
        $pkg = SellerPackage::find($user->seller_package_id);
        if(!empty($pkg) &&  $user->package_valid_till > Carbon::now() && $pkg->type === "paid"){
            $status = true;
            $amount = ($trade_with_price * 6) / 100;
        }
        else{
            $amount = ($trade_with_price * 12) / 100;
        }

        return ['user' => $user, 'status' => $status , 'amount' => $amount];

}

    public function tradingProcessHolding($tax_ammount, $trade_with, $status, $user, $make_trade)
    {

        $buyer_products_price = 0;
        $diff_value = 0;
        if ($trade_with->trade_with_amount == 1) {

        foreach ($make_trade as $key => $value) {
            $seller_product_price = $trade_with->price;
            $buyer_products_price += $value->products->price;
        }
        $diff_value = $this->compare_prices($seller_product_price,$buyer_products_price);
        }

        $role = $user->user_type === "Buyer" ? 'Buyer' : 'Seller';
        $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
        $stripeCharge = $stripe->charges->create([
            'amount' => 200 * 100,
            'currency' => 'usd',
            'customer' => $user->stripe_customer_id,
            'description' => 'Holding amount of trade from '. $role .' of  Product Id: ' . $trade_with->id,
        ]);
        $pro['id'] = [];

        if ($user->user_type === "Buyer") {
            foreach ($make_trade as $key => $data) {
                $data->update(['status' => $status]);
                array_push($pro['id'] ,$data->product_id);
            }
            $additional_amount = 0;
            if ($diff_value > 0) {
                $additional_amount = $diff_value;
            }

            $checkout  = new checkout;
            $checkout->product_id = $make_trade[0]->request_product_id;
            $checkout->user_id = $user->id;
            $checkout->price = $tax_ammount + $additional_amount;
            $checkout->size = $make_trade[0]->size;
            $checkout->color = $make_trade[0]->color;
            $checkout->type = "trade";
            $checkout->qty = 1;
            $checkout->save();

            product::find($make_trade[0]->request_product_id)->update(['status' => 'Trade-Confirmed']);
        } else {
            $additional_amount = 0;

            foreach ($make_trade as $key => $data) {
                $data->update(['status' => $status]);
                array_push($pro['id'],$data->request_product_id);

                if ($diff_value < 0) {
                    $additional_amount = $diff_value/$make_trade->count();
                }

            $checkout  = new checkout;
            $checkout->product_id = $data->product_id;
            $checkout->user_id = $user->id;
            $checkout->price =  ($tax_ammount/$make_trade->count()) + abs($additional_amount);
            $checkout->size = $data->size;
            $checkout->color = $data->color;
            $checkout->type = "request-trade";
            $checkout->qty = 1;
            $checkout->save();

        }

    }

        $charge = $stripeCharge;
        $transaction = new Transaction;
        $transaction->user_id = $user->id;
        $transaction->type = 'trade-holding';
        $transaction->product_id = json_encode($pro['id']);
        $transaction->charge_id = $charge['id'];
        $transaction->details = json_encode($charge);
        $transaction->status = 'pending';
        $transaction->save();


}

    public function tradingProcess($tax_ammount, $trade_with, $status, $user, $make_trade)
    {

        $buyer_products_price = 0;
        $diff_value = 0;
    if ($trade_with->trade_with_amount == 1) {
        foreach ($make_trade as $key => $value) {
            $seller_product_price = $trade_with->price;
            $buyer_products_price += $value->products->price;
        }
        $diff_value = $this->compare_prices($seller_product_price,$buyer_products_price);

    }

        if ($user->user_type === "Buyer") {
            foreach ($make_trade as $key => $data) {
                $data->update(['status' => $status]);
            }
            $additional_amount = 0;
            if ($diff_value > 0) {
                $additional_amount = $diff_value;
            }


            $checkout  = new checkout;
            $checkout->product_id = $make_trade[0]->request_product_id;
            $checkout->user_id = $user->id;
            $checkout->price = $tax_ammount + $additional_amount;
            $checkout->size = $make_trade[0]->size;
            $checkout->color = $make_trade[0]->color;
            $checkout->type = "trade";
            $checkout->qty = 1;
            $checkout->save();

            product::find($trade_with->id)->update(['status' => 'Trade-Confirmed']);

        } else {
            $additional_amount = 0;
            foreach ($make_trade as $key => $data) {
            $data->update(['status' => $status]);
            if ($diff_value < 0) {
                $additional_amount = $diff_value/$make_trade->count();
            }


            $checkout  = new checkout;
            $checkout->product_id = $data->product_id;
            $checkout->user_id = $user->id;
            $checkout->price = ($tax_ammount/$make_trade->count()) + abs($additional_amount);
            $checkout->size = $data->size;
            $checkout->color = $data->color;
            $checkout->type = "request-trade";
            $checkout->qty = 1;
            $checkout->save();
          }

      }

  }


  public function compare_prices($seller_product_price, $buyer_products_price){

    return (int)$seller_product_price - (int)$buyer_products_price;
    // if return in minus value it mean returned value will add in seller cart
    // if return in plus value it mean returned value will add in buyer cart
    // else diff value will be zero
}

}
