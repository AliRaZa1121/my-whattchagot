<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\trading_product;
use App\Models\product;
use App\Models\category;
use App\Models\sub_category;
use App\Models\product_image;
use DB;
use App\Models\color;
use App\Models\order_item;
use App\Models\size_value;
use App\Models\Brand;
use App\Models\trade_price;
use App\Models\trading_return_expectation;
use Session;
use Illuminate\Support\Carbon;

class TradingProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('seller-pkg')->only(['create','store']);
    }

    public function index()
    {
        if (Auth()->guard('seller')->check()) {
            $role = 'seller';
        } else if (Auth()->guard('buyer')->check()) {
            $role = 'buyer';

        } else if (Auth()->guard('admin')->check()) {


        }
        $trade_price = trade_price::first();
        $tradings = product::join('trading_products', 'trading_products.product_id', 'products.id')->select('trading_products.*', 'trading_products.id as trading_id', 'products.*')->latest('products.created_at')->where('products.status', '!=', 'Deleted');
        if (isset($role)) {
            $tradings = $tradings->where('products.user_id', Auth()->guard($role)->user()->id)->get();
        } else {
            $tradings = $tradings->get();
        }

        return view('pages.admin.product.trading.index', compact('tradings', 'trade_price'));
    }

    public function edit($id)
    {
        $brand = Brand::all();
        $category = category::all();
        $sub_category = sub_category::all();
        $product = product::join('trading_products', 'trading_products.product_id', 'products.id')->select('trading_products.*', 'trading_products.id as auction_id', 'products.*')->where('products.id', $id)->first();

        return view('pages.admin.product.trading.edit', compact('category', 'sub_category', 'product', 'brand'));
    }

    public function show($id)
    {
        $brand = Brand::all();
        $category = category::all();
        $sub_category = sub_category::all();
        $product = product::join('trading_products', 'trading_products.product_id', 'products.id')->select('trading_products.*', 'trading_products.id as auction_id', 'products.*')->where('products.id', $id)->first();

        return view('pages.admin.product.trading.show', compact('category', 'sub_category', 'product', 'brand'));
    }


    public function create()
    {
        $brand = Brand::all();
        $category = category::all();
        $sub_category = sub_category::all();
//        $sub_category = sub_category::all();

        return view('pages.admin.product.trading.create', compact('category', 'sub_category', 'brand'));
    }

    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'price' => 'required|numeric|min:1|not_in:0',
            'category_id' => 'required',
            'sub_category_id' => 'required',
            'length_size' => 'required|max:50|numeric',
            'height_size' => 'required|max:50|numeric',
            'width_size' => 'required|max:50|numeric',
            // 'start_time' => 'required',
            // 'end_time' => 'required',
            // 'start_date'      => 'required|date|before:end_date',
            // 'end_date'        => 'date|after:start_date',
            'brand' => 'required',
            'banner_image' => 'required',
            'short_details' => 'required',

        ]);
        $data = $request->except('_token');
        $data['start_date'] = Carbon::now()->format('m/d/Y');
        $data['end_date'] = Carbon::now()->addYear(5)->format('m/d/Y');
        $data['length'] = $data['length_size'];
        $data['height'] = $data['height_size'];
        $data['width'] = $data['width_size'];
        $data['brand_id'] = $data['brand'];
        $data['flags'] = 'trade';
        $data['status'] = "Approved";

        $data['user_id'] = auth()->user()->id;
        if ($request->color !== null) {
            $color = explode(",", $request->color);
        }
        else{
            $color = ["Black"];
        }
        if ($request->size_value !== null) {
            $size_value = explode(",", $request->size_value);
        }
        else{
            $size_value = ["S"];
        }

        DB::beginTransaction();
        try {
            $product = product::create($data);
            trading_product::create([
                'product_id' => $product->id,
                'brand' => $request->brand,
                'start_date' => $data['start_date'],
                'start_time' => Carbon::now()->format('g:i A'),
                'end_date' =>   $data['end_date'],
                'end_time' => Carbon::now()->format('g:i A'),
//                'price' => $request->price,
                'short_details' => $request->short_details,
            ]);

            for ($j = 0; $j < count($color); $j++) {
                color::create([
                    'product_id' => $product->id,
                    'color' => $color[$j],

                ]);
            }
            for ($i = 0; $i < count($size_value); $i++) {
                size_value::create([
                    'product_id' => $product->id,
                    'size' => $size_value[$i],

                ]);
            }

            $image_imagecount = isset($request->image) ? count($request->image) : 0;
            if ($image_imagecount > 0) {
                for ($i = 0; $i < $image_imagecount; $i++) {
                    trading_return_expectation::create([
                        'product_id' => $product->id,
                        'image' => $request->image[$i],

                    ]);

                }


            }

            $imagecount = isset($request->banner_image) ? count($request->banner_image) : 0;
            if ($imagecount > 0) {
                for ($i = 0; $i < $imagecount; $i++) {
                    product_image::create([
                        'product_id' => $product->id,
                        'image' => $request->banner_image[$i],

                    ]);

                }


            }
            DB::commit();
            return response()->json(['msg' => 'Product inserted successfully.']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['msg' => $e->getMessage()], 500);

        }
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'banner_image' => 'required',

            'name' => 'required',
            'price' => 'required|numeric|min:1|not_in:0',
            'category_id' => 'required',
            'sub_category_id' => 'required',
            'length_size' => 'required|max:50|numeric',
            'height_size' => 'required|max:50|numeric',
            'width_size' => 'required|max:50|numeric',
            // 'start_time' => 'required',
            // 'start_date'      => 'required|date|before:end_date',
            // 'end_date'        => 'date|after:start_date',
            // 'end_time' => 'required',
            'brand' => 'required',

            'color' => 'required',
            'size_value' => 'required',
            'short_details' => 'required',
        ]);
        $data = $request->except('_token');
//        $data['price'] = 0;

        $data['length'] = $data['length_size'];
        $data['height'] = $data['height_size'];
        $data['width'] = $data['width_size'];
        $data['brand_id'] = $data['brand'];
        if ($request->color !== null) {
            $color = explode(",", $request->color);
        }
        else{
            $color = ["Black"];
        }
        if ($request->size_value !== null) {
            $size_value = explode(",", $request->size_value);
        }
        else{
            $size_value = ["S"];
        }
        DB::beginTransaction();
        try {
            $product = product::find($id);
            $product->update($data);
            trading_product::where('product_id', $id)->update([
                'product_id' => $product->id,
                'brand' => $request->brand,
                'short_details' => $request->short_details,
            ]);
            size_value::where('product_id', $product->id)->delete();
            color::where('product_id', $product->id)->delete();

            for ($i = 0; $i < count($color); $i++) {
                color::create([
                    'product_id' => $product->id,
                    'color' => $color[$i],

                ]);
            }
            for ($i = 0; $i < count($size_value); $i++) {
                size_value::create([
                    'product_id' => $product->id,
                    'size' => $size_value[$i],

                ]);
            }

            $imagecount = count($request->banner_image);

            product_image::where('product_id', $product->id)->delete();
            if ($imagecount > 0) {
                for ($i = 0; $i < $imagecount; $i++) {
                    product_image::create([
                        'product_id' => $product->id,
                        'image' => $request->banner_image[$i],

                    ]);

                }


            }

            trading_return_expectation::where('product_id', $product->id)->delete();

            $image_imagecount = isset($request->image) ? count($request->image) : 0;
            if ($image_imagecount > 0) {
                for ($i = 0; $i < $image_imagecount; $i++) {
                    trading_return_expectation::create([
                        'product_id' => $product->id,
                        'image' => $request->image[$i],

                    ]);

                }


            }
            DB::commit();
            return response()->json(['msg' => 'Product Update Successfully']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['msg' => $e->getMessage()], 500);

        }

    }

    public function stripePost(Request $request)
    {

//        $request->;
        if (Auth()->guard('seller')->check()) {
            $role = 'seller';
        } else if (Auth()->guard('buyer')->check()) {
            $role = 'buyer';

        } else if (Auth()->guard('admin')->check()) {
            $role = 'admin';

        }
        DB::beginTransaction();
        try {

//            trading_product::where('product_id', $request->product_id)->update(['payment' => 'Completed']);
//            $product = product::find($request->product_id);
            $stripe = new \Stripe\StripeClient(
                env('STRIPE_SECRET')
            );
            $result_strip= $stripe->charges->create([
                'amount' => ($request->amount) * 100,
                'currency' => 'usd',
                'source' => $request->stripeToken,
                'description' => "Trade Product seller paid by Vendor" . auth()->guard($role)->user()->name,
            ]);
            $order_item = order_item::find($request->order_items_id);
                $order_item->update(['status'=>'Shipping','seller_charge_id' => $result_strip->id,'payment_flag' => 1,'seller_charge_price' => $request->amount]);


            DB::commit();

            Session::flash('success', "Paid successfully");
            return back()->withErrors(['msg' => "Paid successfully"]);

        } catch (\Exception $e) {
            DB::rollback();
            print_r( $e->getMessage());
            die;
            return back()->withErrors(['msg' => $e->getMessage()], 500);
        }

    }
    public function trade_price()
    {
        $trade_price = trade_price::first();
        return view('pages.admin.product.trading.price_edit', compact( 'trade_price'));
    }

    public function trade_update(Request $request,$id)
    {

        $data = $request->except('_token');

        DB::beginTransaction();
        try {
            $trade_price = trade_price::find($id);
            $trade_price->update($data);

            DB::commit();
            return response()->json(['msg' => 'Product Update Successfully!!']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['msg' => $e->getMessage()], 500);

        }

    }

}
