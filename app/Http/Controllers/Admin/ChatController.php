<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\chat;
use App\Models\shipping;
use App\Models\billing;
use App\Models\order;
use App\Models\emoji;
use DB;


class ChatController extends Controller
{
    public function index(){
        $chat = chat::where('receiver_id',auth()->user()->id)->Orwhere('sender_id',auth()->user()->id)->groupBy('product_id')->latest()->get();
        return view('pages.admin.chat.chat',compact('chat'));
    }

    public function admin_chat(){
        $id = array();
        $chat =  DB::select('SELECT * FROM `chats` WHERE `sender_id`=`receiver_id` or `receiver_id`=`sender_id` or`sender_id`=`sender_id` or`receiver_id`=`receiver_id` GROUP by `product_id`');
            foreach($chat as $key){
                $id[] = $key->id;
            }
        $chat = chat::whereIn('id',$id)->latest()->get();
        return view('pages.admin.chat.chat_admin',compact('chat'));
    }

    public function ajax_save_message(Request $request){
       $messge = $request->message;

        $emoji = emoji::get();
        foreach ($emoji as $key ){
            $emoji_colon = ":".str_replace('.png','',$key->name).":";
            $messge = str_replace($emoji_colon,'<img src="../packs/basic/images/'.$key->name.'" alt=":'.$key->name.':">', $messge);

        }
        $data = str_replace("world","Peter","Hello world!");
        $chatsave = chat::create([
            'sender_id' => $request->sender_id,
            'receiver_id' => $request->receiver_id,
            'product_id' => $request->product_id,
            'message' => $messge,
            'status' => 0,

        ]);

        echo json_encode($chatsave);

    }
    public function ajax_show_message(Request $request){

        $receiver_id =$request->receiver_id;
        $sender_id =$request->sender_id;
        $chat = chat::where('chats.product_id',$request->product_id)
            ->Where(function($query) use ($receiver_id, $sender_id){
                $query->where('chats.receiver_id',$receiver_id)
                    ->Orwhere('chats.receiver_id',$sender_id)->Orwhere('chats.sender_id',$receiver_id)->Orwhere('chats.sender_id',$sender_id);
            })->join('products','products.id','chats.product_id')->select('chats.*','products.*','products.created_at AS product_date')->latest('chats.created_at')->limit(1)->get();

        echo json_encode($chat);

    }

    public function ajax_get_message_admin(Request $request){
        $receiver_id =$request->receiver_id;
        $sender_id =$request->sender_id;

        $chat = chat::where('chats.product_id',$request->product_id)
            ->Where(function($query) use ($receiver_id, $sender_id){
                $query->where('chats.receiver_id',$receiver_id)
                    ->Orwhere('chats.receiver_id',$sender_id)->Orwhere('chats.sender_id',$receiver_id)->Orwhere('chats.sender_id',$sender_id);
            })->join('products','products.id','chats.product_id')
            ->select('chats.*','products.*','products.created_at AS product_date')
            ->orderBy('chats.created_at', 'asc')->get();
        DB::table('chats')->where('chats.product_id',$request->product_id)
            ->Where(function($query) use ($receiver_id, $sender_id){
                $query->where('chats.receiver_id',$receiver_id)
                    ->Orwhere('chats.receiver_id',$sender_id)->Orwhere('chats.sender_id',$receiver_id)->Orwhere('chats.sender_id',$sender_id);
            })->update(['status' =>1]);
        echo json_encode($chat);
    }

    public function ajax_get_message(Request $request){
        $receiver_id =$request->receiver_id;
        $sender_id =$request->sender_id;
        $sql =  "SELECT * FROM `chats` JOIN `products` ON chats.product_id = products.id where chats.product_id = $request->product_id and( (chats.sender_id = '$sender_id' and chats.receiver_id = $receiver_id ) or (chats.sender_id = $receiver_id and chats.receiver_id = '$sender_id'))";
        $chat =  DB::select(DB::raw($sql));

        echo json_encode($chat);
    }

    public function chat_alreadylogin(Request $request){
        if(Auth()->guard('buyer')->user()->id != base64_decode($request->receiver_id)){
            chat::create([
            'sender_id' =>  Auth()->guard('buyer')->user()->id,
            'receiver_id' => base64_decode($request->receiver_id),
            'product_id' => base64_decode($request->product_id),
            'message' => base64_decode($request->product_id),
            'status' => 0,

        ]);
        return redirect('buyer/buyer_chat');
    }
    else{
        return redirect('buyer')->with('alert-danger', "You can not chat on your own products");
    }

    }

   


}
