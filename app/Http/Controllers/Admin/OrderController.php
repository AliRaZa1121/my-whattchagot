<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\order;
use App\Models\order_item;
use App\Models\billing;
use App\Models\shipping;


class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $order = order_item::latest();
        if (auth()->user()->user_type === "admin") {
            $order = $order->get();
        }
        else{
            $order = $order->where('seller_id', auth()->user()->id)->get();
        }
        return view('pages.admin.order.index', compact('order'));

    }
    public function order_print()
    {
        return view('pages.admin.order.print');
    }

    public function ajax_get_billing_address(Request $request){

        $order= order::where('id',$request->id)->first();
        $billing= billing::where('id',$order->billing_id)->first();
        echo json_encode($billing);
    }
    public function ajax_get_shipping(Request $request){
        $order= order::where('id',$request->id)->first();
        $shipping= shipping::where('id',  $order->shipping_id)->first();
        echo json_encode($shipping);
    }
}
