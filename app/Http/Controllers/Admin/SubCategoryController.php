<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\sub_category;
use App\Models\category;
use DB;

class SubCategoryController extends Controller
{
    public function index()

    {
        $subCategories = sub_category::latest('sub_categories.created_at')->join('categories','categories.id','sub_categories.category_id')->select('sub_categories.*','categories.name as cat_name','categories.id as cat_id')->get();
        return view('pages.admin.sub_category.index', compact('subCategories'));
    }

    public function create()
    {
        $category = category::all();

        return view('pages.admin.sub_category.create', compact('category'));
    }

    public function edit($id)
    {
        $category = category::all();
        $sub_category = sub_category::where('id', $id)->first();
        return view('pages.admin.sub_category.edit', compact('sub_category','category'));
    }

    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'category_id' => 'required'
        ]);

        $data = $request->except('_token');

        DB::beginTransaction();
        try {
            $sub_category = sub_category::create($data);

            DB::commit();
            return response()->json(['msg' => 'Category inserted successfully.'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['msg' => $e->getMessage()], 500);

        }
    }
    public function update(Request $request,$id)
    {
        $sub_category =   sub_category::find($id);

        $request->validate([
            'name' => 'required',
            'category_id' => 'required'
        ]);

        $data = $request->except('_token');

        DB::beginTransaction();
        try {
            $sub_category->update($data);

            DB::commit();
            return response()->json(['msg' => 'Category inserted successfully.'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['msg' => $e->getMessage()], 500);

        }
    }

    public function destroy($id)
    {
        $trip = sub_category::findorfail($id);
        $trip->delete();
        return response()->json(['msg' => 'Sub Category deleted successfully.']);
    }

    public function GetCategories($id)
    {
        $data = sub_category::where('category_id',$id)->get();
        return $data;
    }
}
