<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\PageBanner;
use Illuminate\Support\Str;
use DB;

class PageBannerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        
        $banners = PageBanner::latest()->get();
        return view('pages.admin.page_banners.index', compact('banners'));
    }

    public function create()
    {
        return view('pages.admin.page_banners.create');
    }
    public function edit($id)
    {
        $PageBanner = PageBanner::where('id',$id)->first();
        return view('pages.admin.page_banners.edit',compact('PageBanner'));
    }
    public function destroy($id)
    {
        $banner = PageBanner::findorfail($id);
        $banner->delete();
        return response()->json(['msg' => 'Banner deleted successfully!!']);
    }

    public function store(Request $request)
    {

        $request->validate([
            'page' => 'required',
            'position' => 'required',
            'image_url' => 'required | url',
            'banner_image' => 'required',
        ]);

        $data = $request->except('_token');
        DB::beginTransaction();
        try {
        
            PageBanner::create($data);

            DB::commit();
            return response()->json(['msg' => 'Banner has been inserted successfully'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['msg' => $e->getMessage()], 500);

        }
    }
    public function update(Request $request,$id)
    {
        $PageBanner = PageBanner::find($id);

        $request->validate([
            'page' => 'required',
            'position' => 'required',
            'image_url' => 'required | url',
        ]);

        $data = $request->except('_token');
        if($request->banner_image == null){
            unset($data['banner_image']);
        }

        DB::beginTransaction();
        try {
            $PageBanner->update($data);

            DB::commit();
            return response()->json(['msg' => 'Brand has been updated successfully'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['msg' => $e->getMessage()], 500);

        }
    }

    public function uploadAttachment(Request $request, $type)
    {
        $image = "banner";
        if ($type == 'banner') {
            $request->validate([
                'banner' => 'required|image',
            ]);
        } else {
            $request->validate([
                'image' => 'required|image',
            ]);
            $image = "image";
        }

        $fileName = $request->$image->store('public/page_banners');
        $fileName = Str::after($fileName, 'public/');
        return response(['msg' => 'image uploaded successfully!!', 'file_name' => $fileName]);
    }
  

}
