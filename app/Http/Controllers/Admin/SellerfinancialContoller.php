<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\seller_product;
use App\Models\product;
use App\Models\User;
use App\Models\Transfer;
use App\Models\order_item;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use DB;
use Stripe;


class SellerfinancialContoller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {


        $sellers = User::where('user_type', 'seller')->get();


        return view('pages.admin.financial_statement.index', compact('sellers'));

    }

    public function show($id, Request $request)
    {

        $seller = User::where('user_type', 'seller')->where('id', $id)->first();
        $order_details = order_item::where('seller_id', $id)->whereIn('product_id', function ($query) use ($request) {
            $query->select('id')
                ->from(with(new product)->getTable())
                ->where('flags', $request->product_flag);
        });
        $filter = $request->except(['product_flag']);

        if (isset($filter)) {
            for ($i = 0; $i < count($filter); $i++) {
                $filter_key = array_keys($filter);

                $order_details = $order_details->where($filter_key[$i], $filter[$filter_key[$i]]);
            }


        }
        $order_details = $order_details->get();
        $id = $id;
        return view('pages.admin.financial_statement.show', compact('seller', 'order_details', 'id'));

    }

    public function connect(Request $request)
    {

        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $client = new Client(['base_uri' => 'https://connect.stripe.com/oauth/']);
        $request_client = $client->request("POST", "token", [
            'form_params' => [
                'client_secret' => getenv('STRIPE_SECRET'),
                'code' => $request->code,
                'grant_type' => 'authorization_code'
            ]
        ]);
        $stripe_user_id = json_decode($request_client->getBody()->getContents());
        $user = User::find(auth()->guard('seller')->user()->id);
        $user->strip_account_id = $stripe_user_id->stripe_user_id;
        $user->stripe_flag = 1;
        $user->update();
        return redirect()->route('login');

    }

    public function transfer($id)
    {
        $order_item = order_item::where('id',$id)->first();
        $user = User::where('id',$order_item->seller_id)->first();
        DB::beginTransaction();
        try {

        $transfer_amount=($order_item->price+$order_item->shipping_price)-$order_item->seller_charge_price;
            $stripe =  new \Stripe\StripeClient(env('STRIPE_SECRET'));
//            dd($stripe);
            $trasfer_stripe=  $stripe->transfers->create([
            'amount' => $transfer_amount*100,
            'currency' => 'usd',
            'destination' => $user->strip_account_id,
            'transfer_group' => 'ORDER_'.$id,
        ]);
//            dd($trasfer_stripe);
            $transfer_table = new Transfer;
            $transfer_table->stripe_transfer_id = $trasfer_stripe->id;
            $transfer_table->amount  =$transfer_amount;
            $transfer_table->seller_id = $user->id;
            $transfer_table->save();
            $order_item_update = order_item::find($order_item->id);
            $order_item_update->stripe_transfer_id= 1;
            $order_item_update->update();



            DB::commit();
            return back()->with('alert-success',"Payment has been transferred.");
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['msg' => $e->getMessage()], 500);

        }
        print_r($order_item);
        print_r($user);


    }


}
