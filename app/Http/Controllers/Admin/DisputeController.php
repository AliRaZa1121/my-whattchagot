<?php

namespace App\Http\Controllers\admin;

use App\Models\Dispute;
use App\Models\order_item;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DB;


class DisputeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function list()
    {

        if(Auth()->guard('seller')->check()) {
            $data['disputes'] = Dispute::where('seller_id',auth()->user()->id)->get();
        }
        else{
            $data['disputes'] = Dispute::all();
        }
        return view('pages.admin.disputes.index',$data);
    }

     public function index($order_id)
    {

        if (Auth()->guard('buyer')->check()) {
            $data['disputes'] = Dispute::where('user_id',auth()->user()->id)->where('order_id',$order_id)->get();
        }
        elseif (Auth()->guard('seller')->check()) {
            $data['disputes'] = Dispute::where('seller_id',auth()->user()->id)->where('order_id',$order_id)->get();
        }
        else{
            $data['disputes'] = Dispute::where('order_id',$order_id)->get();
        }
        return view('pages.admin.disputes.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($order_id)
    {
        $data['dispute'] = Dispute::where('user_id',auth()->user()->id)->where('order_id',$order_id)->with(['seller_name'])->first();
        $data['order_id'] = $order_id;
        return view('pages.admin.disputes.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        try {
            DB::beginTransaction();
            $dispute = Dispute::where('order_id',$request->order_id)->where('user_id',$request->user_id)->first();
            if($dispute !== null){
                $dispute->message = $request->message;
                $dispute->save();
                DB::commit();
                return response()->json(['msg' => 'Message has been changed.', 'code' => 200]);
            }
            else{
                $input = $request->all();
                $input['seller_id'] = order_item::where('order_id',$request->order_id)->pluck('seller_id')->first();
                unset($input['_token']);
                Dispute::create($input);
                DB::commit();
                return response()->json(['msg' => 'Dispute has been added.', 'code' => 200]);

            }

        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json(['msg' => $th->getMessage(), 'code' => 500]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Dispute  $dispute
     * @return \Illuminate\Http\Response
     */
    public function show(Dispute $dispute)
    {
        $data['dispute'] = $dispute; 
        return view('pages.admin.disputes.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Dispute  $dispute
     * @return \Illuminate\Http\Response
     */
    public function edit(Dispute $dispute)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Dispute  $dispute
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dispute $dispute)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Dispute  $dispute
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dispute $dispute)
    {
        //
    }

    public function ChangeStatus(Dispute $dispute, $status)
    {
        // dd($dispute,$status);
        try {
            DB::beginTransaction();
            if($status === 'refunded') {
             $order = order_item::where('order_id', $dispute->order_id)->get();                      
                $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
                  $stripe->refunds->create([
                    'charge' => $order[0]['charge_id'],
                  ]);
                  $dispute->status = 'refunded';
                  $dispute->save();

                  $notification = 'Payment has been refunded';
                  foreach ($order as $key => $value) {
                    $value->update(['status' => 'dispute']);
                  }
                  
            }
            elseif($status === 'processing'){
                $dispute->status = 'processing';
                $dispute->save();
                $notification = 'Dispute status has been changed';
            }
            elseif($status === 'cancel'){
                $dispute->status = 'cancel';
                $dispute->save();
                $notification = 'Dispute status has been changed';
            }
            DB::commit();
            return redirect()->back()->with('alert-success', $notification);
        }
         catch (\Throwable $th) {
            $notification = array(
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            );
            DB::rollback();
            return redirect()->back()->with('alert-danger', $th->getMessage());
        }

        
    }
}
