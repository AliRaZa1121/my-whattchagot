<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Testimonial;
use DB;
use Illuminate\Support\Str;



class TestimonialController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['testimonials'] = Testimonial::latest()->get();
        return view('pages.admin.testimonials.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.testimonials.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
           'name' => 'required|max:255',
           'banner_image' => 'required',
           'description' => 'required|max:255'
        ]);

        $data = $request->except('_token');

        DB::beginTransaction();
        try {
            Testimonial::create($data);

            DB::commit();
            return response()->json(['msg' => 'Testimonial has been inserted successfully.'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['msg' => $e->getMessage()], 500);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function show(Testimonial $testimonial)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $testimonial = Testimonial::where('id',$id)->first();
        return view('pages.admin.testimonials.edit',compact('testimonial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $testimonial = Testimonial::find($id);

        $request->validate([
            'name' => 'required|max:255',
            //    'banner_image' => 'required',
               'description' => 'required|max:255'
            ]);
        $data = $request->except('_token');
        DB::beginTransaction();
        try {
            $testimonial->update($data);
            DB::commit();
            return response()->json(['msg' => 'Testimonial has been inserted successfully'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['msg' => $e->getMessage()], 500);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $trip = Testimonial::findorfail($id);
        $trip->delete();
        // return response()->json(['msg' => 'Testimonial deleted successfully!!']);
        return redirect()->back()->with('alert-success', "Testimonial has been deleted");
    }

    public function uploadAttachment(Request $request, $type)
    {
        $image = "banner";
        if ($type == 'banner') {
            $request->validate([
                'banner' => 'required|image',
            ]);
        } else {
            $request->validate([
                'image' => 'required|image',
            ]);
            $image = "image";
        }

        $fileName = $request->$image->store('public/testimonials');
        $fileName = Str::after($fileName, 'public/');
        return response(['msg' => 'Image uploaded successfully!!', 'file_name' => $fileName]);
    }
}
