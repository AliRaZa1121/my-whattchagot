<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\auction_product;
use App\Models\product;
use App\Models\category;
use App\Models\sub_category;
use App\Models\product_image;
use App\Models\Brand;
use Carbon\Carbon;
use DB;

class AuctionProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('seller-pkg')->only(['create','store']);
    }

    public function index()
    {
        if (Auth()->guard('seller')->check()) {
            $role = 'seller';
        } else if (Auth()->guard('buyer')->check()) {
            $role = 'buyer';

        }

        $auctions = product::join('auction_products', 'auction_products.product_id', 'products.id')->select('auction_products.*','auction_products.id as auction_id', 'products.*')->latest('products.created_at')->where('products.status','!=','Deleted');
        if (isset($role)) {
            $auctions = $auctions->where('products.user_id', Auth()->guard($role)->user()->id)->get();
        }else{
            $auctions = $auctions->get();
        }
        return view('pages.admin.product.auction.index', compact('auctions'));
    }

    public function edit($id)
    {
    //    echo $id;
        $category = category::all();
        $sub_category = sub_category::all();
        $product = product::join('auction_products', 'auction_products.product_id', 'products.id')->with('product_images_many')->select('auction_products.*','auction_products.id as auction_id', 'products.*')->where('products.id', $id)->first();
// dd( $product );
        $brand = Brand::all();
        return view('pages.admin.product.auction.edit', compact('category', 'sub_category', 'product','brand'));
    }
    public function show($id)
    {
        $brand = Brand::all();
        $category = category::all();
        $sub_category = sub_category::all();
        $product = product::join('auction_products', 'auction_products.product_id', 'products.id')->select('auction_products.*','auction_products.id as auction_id', 'products.*')->where('products.id', $id)->first();

        return view('pages.admin.product.auction.show', compact('category', 'sub_category', 'product','brand'));
    }


    public function create()
    {
        $category = category::all();
        $sub_category = sub_category::all();
        $brand = Brand::all();

        return view('pages.admin.product.auction.create', compact('category', 'sub_category','brand'));
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'banner_image' => 'required',
            'name' => 'required',
            'price' => 'required|numeric|gt:0',
            'category_id' => 'required',
            'sub_category_id' => 'required',
            'length_size' => 'required|max:50|numeric',
            'height_size' => 'required|max:50|numeric',
            'width_size' => 'required|max:50|numeric',
            'start_time' => 'required',
            'auction_end' => 'required|numeric|max:7',
            // 'end_time' => 'required',
            'start_date'      => 'required|date',
            // 'end_date'        => 'date|after:start_date',
            'brand' => 'required',
            'short_details' => 'required',
        ]);


        $data = $request->except('_token');
        $data['start_date'] = $data['start_date'];
        $data['end_date'] = Carbon::parse($data['start_date'])->addDays($data['auction_end'])->format("m/d/Y");
        $data['end_time'] = $data['start_time'];
        $data['length'] = $data['length_size'];
        $data['height'] = $data['height_size'];
        $data['width'] = $data['width_size'];
        $data['brand_id'] = $data['brand'];
        $data['flags'] = 'auction';
        $data['status'] = "Approved";
        
        $data['user_id'] = auth()->user()->id;

        DB::beginTransaction();
        try {
            $product = product::create($data);
            auction_product::create([
                'product_id' => $product->id,
                'brand' => $request->brand,
                'start_date' => $request->start_date,
                'start_time' => $request->start_time,
                'end_date' => $data['end_date'],
                'end_time' => $data['end_time'],
                'short_details' => $request->short_details,
                'bid_price_update' => $request->price,
            ]);

            $imagecount= count($request->banner_image);

            if ($imagecount > 0) {
                for ($i = 0; $i < $imagecount; $i++) {
                    product_image::create([
                        'product_id' => $product->id,
                        'image' => $request->banner_image[$i],

                    ]);
                }
            }


            DB::commit();
            return response()->json(['msg' => 'Product has been inserted successfully.']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['msg' => $e->getMessage()], 500);

        }
    }
    public function update($id,Request $request)
    {
        $request->validate([
            'banner_image' => 'required',
            'name' => 'required',
            'price' => 'required|numeric|gt:0',
            'category_id' => 'required',
            'sub_category_id' => 'required',
            'length_size' => 'required|max:50|numeric',
            'height_size' => 'required|max:50|numeric',
            'width_size' => 'required|max:50|numeric',
            'start_time' => 'required',
            'auction_end' => 'required|numeric|max:7',
            // 'end_time' => 'required',
            'start_date'      => 'required|date',
            // 'end_date'        => 'date|after:start_date',
            'brand' => 'required',
            'short_details' => 'required',
        ]);

        $data = $request->except('_token','_method');
        $data['start_date'] = $data['start_date'];
        // $data['end_date'] = $data['end_date'];

        $data['end_date'] = Carbon::parse($data['start_date'])->addDays($data['auction_end'])->format("m/d/Y");
        $data['end_time'] = $data['start_time'];

        $data['length'] = $data['length_size'];
        $data['height'] = $data['height_size'];
        $data['width'] = $data['width_size'];
        $data['brand_id'] = $data['brand'];
        DB::beginTransaction();
        try {

            $product = product::find($id);
            $product->update($data);
            auction_product::where('product_id',$id)->update([
                'product_id' => $product->id,
                'brand' => $request->brand,
                'start_date' => $request->start_date,
                'start_time' => $request->start_time,
                'end_date' => $data['end_date'],
                'end_time' => $data['end_time'],
                'short_details' => $request->short_details,
                'end_auction' => 0,
                'bid_price_update' => $request->price,
            ]);

            $imagecount= count($request->banner_image);

            product_image::where('product_id',$product->id)->delete();
            if ($imagecount > 0) {
                for ($i = 0; $i < $imagecount; $i++) {
                    product_image::create([
                        'product_id' => $product->id,
                        'image' => $request->banner_image[$i],

                    ]);

                }


            }

            DB::commit();
            return response()->json(['msg' => 'Product update successfully.']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['msg' => $e->getMessage()], 500);

        }
    }

}
