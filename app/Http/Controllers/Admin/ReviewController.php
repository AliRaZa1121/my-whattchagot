<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\order;
use App\Models\review;
use App\Models\order_item;
use DB;
class ReviewController extends Controller
{
    public function index(){
        $order = order_item::latest('order_items.created_at')->leftJoin('reviews','order_items.id','=','reviews.order_item_id')->select('order_items.*','reviews.comment','reviews.rate')->where('status','Delivered')->where('order_items.user_id', auth()->guard('buyer')->user()->id)->get();

        return view('pages.admin.review.index', compact('order'));
    }
    public function store(Request $request)
    {
//        dd($request);


        $request->validate([
            'comment' => 'required',
            'rate' => 'required',

        ]);
        $data = $request->except('_token');

        $data['user_id'] = auth()->user()->id;

        DB::beginTransaction();
        try {
        review::create([
                'product_id' => $request->product_id,
                'user_id' => Auth()->guard('buyer')->user()->id,
                'seller_id' => $request->seller_id,
                'order_item_id' => $request->order_id,
                'comment' => $request->comment,
                'rate' => $request->rate,
            ]);



            DB::commit();
            return back()->with('alert-success',"Your review is submitted");
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['msg' => $e->getMessage()], 500);

        }
    }
}
