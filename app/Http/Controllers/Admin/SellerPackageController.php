<?php

namespace App\Http\Controllers\admin;


use App\Models\SellerPackage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Stripe\Stripe;
use Illuminate\Support\Carbon;


class SellerPackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = 'Packages';
        $data['packages'] = SellerPackage::where('type','paid')->get();

        return view('pages.admin.seller_packages.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = 'Create Package';
        return view('pages.admin.seller_packages.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:100',
            'duration' => 'required|gt:0|max:48',
            'price' => 'required|gt:0',
        ]);

        $input =$request->all();
        unset($input['_token']);

        try{
            DB::beginTransaction();
            SellerPackage::create($input);
            $notification = array(
                'message' => 'Seller Package created!',
                'alert-type' => 'success'
            );
            DB::commit();
        }
        catch (\Throwable $th) {
            $notification = array(
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            );
            DB::rollback();
            return redirect()->back()->with($notification);
        }

        return redirect()->route('seller.packages')->with($notification);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SellerPackage  $sellerPackage
     * @return \Illuminate\Http\Response
     */
    public function show(SellerPackage $sellerPackage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SellerPackage  $sellerPackage
     * @return \Illuminate\Http\Response
     */
    public function edit(SellerPackage $sellerPackage)
    {
        $data['page_title'] = 'Edit Package';
        $data['package'] = $sellerPackage;
        return view('pages.admin.seller_packages.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SellerPackage  $sellerPackage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SellerPackage $sellerPackage)
    {
        $this->validate($request, [
            'name' => 'required|max:100',
            'duration' => 'required|gt:0|max:48',
            'price' => 'required|gt:0',
        ]);

        $input =$request->all();
        unset($input['_token']);

        try{
            DB::beginTransaction();
            $sellerPackage->update($input);
            $notification = array(
                'message' => 'Seller updated!',
                'alert-type' => 'success'
            );
            DB::commit();
        }
        catch (\Throwable $th) {
            $notification = array(
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            );
            DB::rollback();
            return redirect()->back()->with($notification);
        }

        return redirect()->route('seller.packages')->with($notification);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SellerPackage  $sellerPackage
     * @return \Illuminate\Http\Response
     */
    public function destroy(SellerPackage $sellerPackage)
    {
        $sellerPackage->delete();
        return response()->json(['msg' => 'Package deleted successfully.']);
    }

    public function PackageRenew()
    {
        $data['packages'] = SellerPackage::all();
        $data['user'] = auth()->user();
        return view('pages.admin.seller_packages.renew',$data);
    }

    public function PackageRenewUpdate(Request $request)
    {
        try {
            $pkg =  SellerPackage::find($request->seller_package_id);

            $stripe = new \Stripe\StripeClient(
                env('STRIPE_SECRET')
              );
              $stripe->charges->create([
                'amount' => $pkg->price * 100,
                'currency' => 'usd',
                'source' => $request->nonce,
                'description' => $pkg->name .' '. ' Package Purchasing by '.$request->fullname,
              ]);

              $user = User::find(auth()->user()->id);

              $user->update([
                'seller_package_id' => $request->seller_package_id,
                'package_start_date' => Carbon::now(),
                'package_valid_till' => Carbon::now()->addMonth($pkg->duration)
              ]);

        }
        catch (\Throwable $th) {
            return redirect()->back()->with('alert-danger', $th->getMessage());
        }

        return redirect()->back()->with('alert-success', 'Package Purchased!');

    }
}
