<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\product;
use App\Models\order_item;
use Illuminate\Support\Str;
use App\Models\Auction_bit_list;
use App\Models\make_trade;
use App\Models\SellerPackage;
use App\Models\User;
use App\Models\Transaction;
use Carbon\Carbon;
use App\Traits\EmailTrait;
//use Stripe;

class OrderItemController extends Controller
{
    use EmailTrait;
    // public function updateStatus(order_item $order_item, $status)
    // {

    //     if ($status == 'Cancel') {
    //         $this->cancelOrder($order_item, $status);
    //         $this->EmailUpdate($order_item);
    //         return back()->with('alert-success', "order has been canceled");
    //     } else if ($status == 'Shipping') {
    //         $order_item->update(['status' => 'Shipping', 'seller_charge_price' => ($order_item->price * $order_item->qty) * 0.12]);
    //         $this->EmailUpdate($order_item);
    //         return back()->with('alert-success', "Product is shipped successfully");
    //     } else if ($status == 'Delivered') {
    //         $product = product::findOrFail($order_item->product_id);
    //             if ($product->flags === "trade" || $product->flags === "request-trade") {
    //                 $this->refundHoldingAmmount($order_item, $status);
    //                 $product->update(['status' => 'Solded']);
    //             }
    //             else{
    //                 $order_item->update(['status' => $status]);
    //             }
    //             return back()->with('alert-success', "Your Product is {$status} successfully");
    //     } else {
    //         $order_item->update(['status' => $status]);
    //         $this->EmailUpdate($order_item);
    //         return back()->with('alert-success', "Your Product is {$status} successfully");
    //     }


    // }

    public function updateStatus(order_item $order_item, $status)
    {
        if ($status == 'Cancel') {
           $return_msg = $this->cancelOrder($order_item , $status);
        }
        else if ($status == 'Shipping') {
            $return_msg = $this->shipOrder($order_item , $status);        
        }
        else{
            $order_item->update(['status' => $status]);
            $return_msg = ['alert-success', "Order has been updated successfully"];   
        }
        $this->EmailUpdate($order_item);

        return back()->with($return_msg[0],$return_msg[1]);


    }

    public function shipOrder($order_item , $status)
    {

        try {
           $user = User::find($order_item->seller_id);
           $pkg = SellerPackage::find($user->seller_package_id);
           if ($pkg->type == 'paid'){
               $amount = ($order_item->price * 6)/100;
           }
           else{
            $amount = ($order_item->price * 12)/100;
           }

         $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
         $charge = $stripe->charges->create([
            'amount' => $amount * 100,
            'currency' => 'usd',
            'customer' => $user->stripe_customer_id,
            'description' => 'Whattchagot selling fee by '.$user->name,
          ]);
          if ($charge['status'] == "succeeded") {
                $order_item->update(['status' => $status]);

                $transaction = new Transaction();
                $transaction->type = 'selling-fee';
                $transaction->charge_id = $charge['id'];
                $transaction->details = json_encode($charge);
                $transaction->status = 'complete';
                $transaction->order_id = $order_item->order_id;
                $transaction->save();
            return ['alert-success', "Order has been shipped successfully"];
          }
          else{
            return ['alert-danger', "Your account have insufficient balance"];
          }

        } catch (\Throwable $th) {
            return ['alert-danger', $th->getMessage()];
        }
    }

    public function cancelOrder($order_item , $status)
    {
           try {
            $start = Carbon::parse($order_item->created_at);
            $today = Carbon::parse(now());
            $diff = $start->diffInDays($today);
            if($diff > 5){
                return ['alert-danger', "Can not cancel the order after 5 days"];
            }   
            $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
            $chargeId = $order_item->charge_id;
            $refundAmount = (($order_item->price * $order_item->qty) + $order_item->shipping_price) - ((($order_item->price * $order_item->qty) + $order_item->shipping_price) * 0.045);
            $refund = $stripe->refunds->create(['amount' => number_format($refundAmount, 1) * 100, 'charge' => $chargeId]);
            if ($refund['status'] == "succeeded") {
                $order_item->update(['status' => $status, 'refund_flag' => 1]);
                return ['alert-success', "Order has been cancelled successfully"];
            }
            else{
                return ['alert-danger', "Refund process failed!! please contact with Admin"];
            }
           } catch (\Throwable $th) {
            return ['alert-danger', $th->getMessage()];
           }
    }

    public function orderStatusDelivery($order_item,$status)
    {
        $product = product::findOrFail($order_item->product_id);
        if ($product->flags === "trade" || $product->flags === "request-trade") {
            $this->refundHoldingAmmount($order_item, $status);
            $product->update(['status' => 'Solded']);
        }
        else{
            $order_item->update(['status' => $status]);
        }
        return back()->with('alert-success', "Your Product is {$status} successfully");
                
    }

    public function refundHoldingAmmount($order_item , $status)
    {
        try {
            $transaction = Transaction::where('product_id', 'like', '%' . $order_item->product_id . '%')->where('status' , 'pending')->where('type' , 'trade-holding')->first();
            if (!empty($transaction)) {
                        $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
                        $refund =  $stripe->refunds->create([
                            'charge' => $transaction->charge_id,
                        ]);

                        if($refund['status'] == "succeeded") {
                            $transaction->update(['status' => 'refunded']);
                        }
                    }

              $order_item->update(['status' => 'Delivered', 'delivered_date' => Carbon::now()]);

            } catch (\Throwable $th) {
                return back()->with('alert-danger', $th->getMessage());
        }
    }

    public function EmailUpdate($order_item)
    {
        try {
            $data['status_msg'] = "";
            switch ($order_item->status) {
                case 'Cancel':
                    $data['status_msg'] = "Your Order has been canceled";
                    break;
                    case 'In Process':
                        $data['status_msg'] = "Your Order has shipped Your order will be delivered within the next few days";
                        break;
                        case 'Shipping':
                            $data['status_msg'] = "Your Order is in process, will let you know within the next few days";
                            break;
                default:
                $data['status_msg'] = "Your Order has been updated, for more details please visit whattchagot website";
                    break;
            }
            $user = User::where('id',$order_item->user_id)->first();
            $data['order_item'] = $order_item;
            $data['name'] = $user->name;
            $data['email'] = $user->email;
            $this->sendMail($data,'emails.orderupdate');
        } catch (\Throwable $th) {
            return false;
        }
    }

    public function order_print($id)
    {

        return view('order_invoice');
    }

    public function bid_list()
    {
        $bitlist = Auction_bit_list::with('products', 'bidprice')->where('user_id', Auth()->guard('buyer')->user()->id)->orderBy('id', 'DESC')->get();
        return view('pages.admin.bid_list.index', compact('bitlist'));
    }

    public function bid_list_seller()
    {
        $bitlist = Auction_bit_list::with('products', 'bidprice')->where('saller_id', Auth()->guard('seller')->user()->id)->latest('created_at')->get();
        return view('pages.admin.bid_list.index', compact('bitlist'));
    }

    public function bid_list_admin()
    {
        $bitlist = Auction_bit_list::with('products')->latest('created_at')->get();
        return view('pages.admin.bid_list.index', compact('bitlist'));
    }
}
