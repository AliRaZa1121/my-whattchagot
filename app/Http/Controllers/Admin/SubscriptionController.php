<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\product;
use App\Models\order_item;
use Illuminate\Support\Str;
use App\Models\Subscription;
use Carbon\Carbon;
use DB;

//use Stripe;

class SubscriptionController extends Controller
{
    public function index(){
        $subscriptions =  Subscription::latest()->get();
        return view('pages.admin.subscription.index',compact('subscriptions'));

    }
    public function create(){
        return view('pages.admin.subscription.create');
    }
    public function store(Request $request){


        DB::beginTransaction();
        try{
            $subscription = Subscription::create($request->all());

            Subscription::where('id','!=',$subscription->id)->update(['status'=>'Decline']);

            DB::commit();
            return response()->json(['msg' => 'Category inserted successfully.'], 200);

        }catch(\Exception $e)
        {
            DB::rollback();
            return response()->json(['msg' => $e->getMessage()], 500);

        }


    }
}
