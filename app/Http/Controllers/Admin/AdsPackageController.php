<?php

namespace App\Http\Controllers\admin;

use App\Helpers\Banner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AdsPackages;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Session;
use App\Models\product;
use App\Models\BannerImage;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;

class AdsPackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $data['page_title'] = 'Packages';
        $data['adPackages'] = AdsPackages::all();

        return view('pages.admin.ad-packages.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = 'Create  Packages';
        $data['adPackage'] = new AdsPackages;

        return view('pages.admin.ad-packages.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'price' => 'required',
            'description' => 'nullable',
            'type' => 'required',
            'days' => 'required',
        ]);

        $input = $request->all();
        unset($input['_token']);
        try {
            DB::beginTransaction();

            $adPackage = AdsPackages::create($input);

            $notification = array(
                'message' => 'Package created!',
                'alert-type' => 'success'
            );
            DB::commit();
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Something went wrong.',
                'alert-type' => 'error'
            );
            DB::rollback();
        }
        return redirect()->route('ad-packages')->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\AdsPackages $adsPackages
     * @return \Illuminate\Http\Response
     */
    public function show(AdsPackages $adsPackages)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\AdsPackages $adsPackages
     * @return \Illuminate\Http\Response
     */
    public function edit($id,AdsPackages $adsPackages)
    {

        $data['adPackage'] =AdsPackages::where('id',$id)->first();
        return view('pages.admin.ad-packages.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\AdsPackages $adsPackages
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $request, AdsPackages $adsPackages)
    {

        $request->validate([
            'name' => 'sometimes|required',
            'price' => 'sometimes|required',
            'description' => 'nullable',
            'type' => 'sometimes|required',
            'days' => 'sometimes|required',
        ]);

        $input = $request->all();
        unset($input['_token']);
        try {
            DB::beginTransaction();

            $adPackage = $adsPackages->where('id',$id)->update($input);

            $notification = array(
                'message' => 'Package updated!',
                'alert-type' => 'success'
            );

            DB::commit();

        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Something Went Wrong!',
                'alert-type' => 'error'
            );
            DB::rollback();
        }

        return redirect()->route('ad-packages')->with($notification);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\AdsPackages $adsPackages
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $trip = AdsPackages::findorfail($id);
        $trip->delete();
        return response()->json(['msg' => 'Package is deleted successfully!!']);
    }

    public function AdvertiseBanner()
    {
        if (auth()->user()->user_type === "admin") {
            $data['banners']= BannerImage::latest()->get();
        }
        else{
            $data['banners']= BannerImage::where('user_id',auth()->user()->id)->latest()->get();
        }
        return view('pages.admin.ad-packages.banners_list', $data);
    }
    
    public function myAdvertiseBannerCreate()
    {
        $data['packages']= AdsPackages::get();
        $data['products']= product::where('user_id', auth()->user()->id)->where('flags','!=','request-trade')->latest()->get(['id','name']);
        return view('pages.admin.ad-packages.create_banner', $data);
    }

    public function myAdvertiseBannerStore(Request $request)
    {
        $request->validate([
            'package_id' => 'required',
            'product_id' => 'required',
            'banner_image' => 'required',
            'start_date' => 'required',
        ]);
        
        $data = $request->except('_token');
        try {
            $user = User::findOrFail($data['user_id']);
            $package = AdsPackages::findOrFail($data['package_id']);
            $data['end_date'] = Carbon::parse($data['start_date'])->addDays($package->days);
            DB::beginTransaction();
            $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
            $charge = $stripe->charges->create([
                'amount' => $package->price * 100,
                'currency' => 'usd',
                'customer' => $user->stripe_customer_id,
                'description' => $user->name.' purchased banner package'
            ]);
            if ($charge['status'] == "succeeded") {
                $banner = BannerImage::create($data);
                $transaction = new Transaction;
                $transaction->user_id = $user->id;
                $transaction->type = 'advertisment';
                $transaction->charge_id = $charge['id'];
                $transaction->details = json_encode($charge);
                $transaction->save();
                DB::commit();
            }
            else{
                return response()->json(['msg' => "Your card have insufficient balance"], 400);
            }
            return response()->json(['msg' => 'Advertisment has been purchased successfully.'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['msg' => $e->getMessage()], 500);
        }
       
    }

    
}
