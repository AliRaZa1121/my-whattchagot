<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Warehouse;
use DB;


class WarehouseController extends Controller
{
    public function index(){
        $warehouse= Warehouse::where('user_id',auth()->user()->id)->first();
        return view('pages.admin.warehouse.create', compact('warehouse'));
    }

    public function SellerWearhouse()
    {
        $warehouse= Warehouse::where('user_id','!=',auth()->user()->id)->with(['seller'])->get();

        return view('pages.admin.warehouse.list', compact('warehouse'));
    }

    public function store(Request $request){
        if (Auth()->guard('seller')->check()) {
            $role = 'seller';
        } else if (Auth()->guard('admin')->check()) {
            $role = 'admin';

        }else if (Auth()->guard('buyer')->check()) {
            $role = 'buyer';
        }

        $input = $request->all();

        unset($input['_token']);
        try {
            DB::beginTransaction();
            $warehouse= Warehouse::where('user_id',auth()->guard($role)->user()->id)->first();
            if(empty($warehouse)){
                $input['user_id'] =auth()->guard($role)->user()->id;
                $warehouse = Warehouse::create($input);
                $notification = array(
                    'message' => 'Warehouse has been created.',
                    'alert-type' => 'success',
                    'status' => 200
                );
            }else{
                $warehouse = Warehouse::find($warehouse->id);
                $warehouse->update($input);
                $notification = array(
                    'message' => 'Warehouse has been updated.',
                    'alert-type' => 'success',
                    'status' => 200
                );
            }



            DB::commit();
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Something Went Wrong!',
                'alert-type' => 'error',
                'status' => 500
            );
            DB::rollback();
        }
        return back();

    }
}
