<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Models\product;
use App\Models\order_item;
use Illuminate\Support\Str;
use App\Models\Auction_bit_list;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $data['page_title'] = 'Packages';
        $data['packages'] = Package::all();

        return view('pages.admin.packages.list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = 'Create Package';
        $data['package'] = new Package;
        return view('pages.admin.packages.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validatePackage($request);

        $input = $request->all();

        foreach($input as $key => $value)
        {
            if($value == "on") {
                $input[$key] = 1;
            }
        }

        unset($input['_token']);
        try {
            DB::beginTransaction();

            $package = Package::create($input);
            $notification = array(
                'message' => 'Package created!',
                'alert-type' => 'success',
                'status' => 200
            );
            DB::commit();
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Something Went Wrong!',
                'alert-type' => 'error',
                'status' => 500
            );
            DB::rollback();
        }

        return response()->json(
            [
                'redirectUrl' => '/packages',
                'message' => $notification['message'],
                'alert-type' => $notification['alert-type']
            ], $notification['status']
        );


//        return redirect('packages')->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Package $package)
    {
        $data['page_title'] = 'Edit Package';
        $data['package'] = $package;
        return view('pages.admin.packages.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Package $package)
    {

        $this->validatePackage($request, 'edit');


        $input = $request->all();

        foreach($input as $key => $value)
        {
            if($value == "on") {
                $input[$key] = 1;
            }
        }

        unset($input['_token']);
        try {
            DB::beginTransaction();

            $package = $package->update($input);
            $notification = array(
                'message' => 'Package updated!',
                'alert-type' => 'success',
                'status' => 200
            );
            DB::commit();
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Something Went Wrong!',
                'alert-type' => 'error',
                'status' => 500
            );
            DB::rollback();
        }

        return response()->json(
            [
                'redirectUrl' => '/packages',
                'message' => $notification['message'],
                'alert-type' => $notification['alert-type']
            ], $notification['status']
        );

//        return redirect('packages')->with($notification);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Package $package)
    {
        $package->delete();
        $notification = array(
            'message' => 'Package deleted!',
            'alert-type' => 'success'
        );
        return redirect('packages')->with($notification);

    }

    protected function validatePackage(Request $request, string $method = 'create')
    {
            $rules = [
                'name' => 'required',
                'monthly_price' => 'required',
                'yearly_price' => 'required',
//                'monthly_plan_price' => 'required',
//                'yearly_plan_price' => 'required',
                'description' => 'required',
                'type' => 'required',
                'categories' => 'required',
                'images' => 'required',
                'menu_and_attachment' => 'required',
                'video' => 'required',
                'coupons' => 'required',
                'events' => 'required'
            ];

            if ($method == 'edit') {
                $rules['name'] = 'sometimes|required';
                $rules['monthly_price'] = 'sometimes|required';
                $rules['yearly_price'] = 'sometimes|required';
//                $rules['monthly_plan_price'] = 'sometimes|required';
//                $rules['yearly_plan_price'] = 'sometimes|required';
                $rules['description'] = 'sometimes|required';
                $rules['type'] = 'sometimes|required';
                $rules['categories'] = 'sometimes|required';
                $rules['images'] = 'sometimes|required';
                $rules['menu_and_attachment'] = 'sometimes|required';
                $rules['video'] = 'sometimes|required';
                $rules['coupons'] = 'sometimes|required';
                $rules['events'] = 'sometimes|required';
            }

            $customMessages = [
                'images.required' => 'Image count for package is required.',
                'video.required' => 'Video count for package is required.',
                'coupons.required' => 'Coupons count for package is required',
                'events.required' => 'Events count for package is required.'
            ];

            $this->validate($request, $rules, $customMessages);
    }
}
