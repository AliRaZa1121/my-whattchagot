<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\product;
use App\Models\category;
use App\Models\contactus;
use App\Models\auction_product;
use App\Models\sub_category;
use App\Models\checkout;
use App\Models\shipping;
use App\Models\Brand;
use App\Models\Newsletter;
use App\Models\trading_product;
use Carbon\Carbon;
use App\Models\review;
use DB;
use Validator;
use App\Models\make_trade;
use App\Models\make_trade_images;
use App\Models\Auction_bit_list;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Notification;
use App\Notifications\auctionsend;
use App\Traits\EmailTrait;
use App\Models\User;
use App\Models\BannerImage;
use App\Models\Testimonial;
use App\Models\Warehouse;
use Session;
use App\Models\SellerPackage;
use function Ramsey\Uuid\v6;

class HomeController extends Controller
{
    use EmailTrait;

    public function home()
    {

        $now = Carbon::now()->format('Y-m-d H:i:s');
        $bannerimages = BannerImage::where('start_date', '<', $now)->where('end_date','>', $now)->get();

        $products_trade = product::join('product_details', 'product_details.product_id', 'products.id')
        ->select('product_details.*', 'product_details.id as detail_id', 'products.*')
        ->where(['products.flags'=>'trade', 'products.status' => 'Approved'])
        ->where('product_details.start_date', '<', $now)->where('product_details.end_date','>', $now)
        ->inRandomOrder()->take(20)->get();

        $selling = product::join('product_details', 'product_details.product_id', 'products.id')
        ->select('product_details.*', 'product_details.id as detail_id', 'products.*')
        ->where(['products.flags'=>'selling', 'products.status' => 'Approved'])
        ->where('product_details.start_date', '<', $now)->where('product_details.end_date','>', $now)
        ->inRandomOrder()->take(20)->get();

        $auctions = product::join('product_details', 'product_details.product_id', 'products.id')
        ->select('product_details.*', 'product_details.id as detail_id', 'products.*')
        ->where(['products.flags'=>'auction', 'products.status' => 'Approved'])
        ->where('product_details.start_date', '>', $now)
        ->inRandomOrder()->take(20)->get();

        $live_auction = product::join('product_details', 'product_details.product_id', 'products.id')
         ->select('product_details.*', 'product_details.id as detail_id', 'products.*')
         ->where(['products.flags'=>'auction', 'products.status' => 'Approved'])
         ->where('product_details.start_date','<', $now)->where('product_details.end_date','>', $now)
         ->inRandomOrder()->take(5)->get();

        return view('index', compact('products_trade','selling','auctions','live_auction','bannerimages'));
    }




    public function profileProcess(User $user)
    {
        if($user->stripe_customer_id !== null){
            return redirect('');
        }
        $data['packages'] = SellerPackage::all();
        $data['user'] = $user;
        return view('profile_process',$data);
    }

    public function categories()
    {
        $categories = category::latest()->with('categories')->where('deleted_at', null)->get();

        return view('categories', compact('categories'));
    }

    public function news()
    {
        $categories = category::latest()->with('categories')->where('deleted_at', null)->get();
        return view('news', compact('categories'));
    }

    public function news_single()
    {
        $categories = category::latest()->with('categories')->where('deleted_at', null)->get();
        return view('news-single', compact('categories'));
    }

    public function contact_us()
    {
        $brands   = Brand::all();
        $categories = category::latest()->with('categories')->where('deleted_at', null)->get();
        return view('contact-us', compact('categories', 'brands'));
    }

    public function contact_us_submit(Request $request)
    {

        $request->validate([
            'name' => 'required', 'email' => 'required | email',
            'phone' => 'required', 'category' => 'required',
            'message' => 'required | max:1000',
        ]);

        $formdata = $request->except('_token');

        $contactus = new contactus;
        $contactus->create($formdata);
        return redirect()->back()->with('message', 'Form Submitted Succuessfully');
    }


    public function profile($id)
    {
        $categories = category::latest()->with('categories')->where('deleted_at', null)->get();
        $user = User::where('id', $id)->first();
        if (empty($user)) {
            return redirect('/');
        }
        $products = product::where('user_id', $id)->first();
        $products_trade = product::join('trading_products', 'trading_products.product_id', 'products.id');
        $products_trade = $products_trade->select('trading_products.*', 'trading_products.id as trading_id', 'products.*')
            ->latest('products.created_at')
            ->where('products.user_id', $id)
            ->where('products.status', '=', 'Approved')->get();
        $sellers = product::join('seller_products', 'seller_products.product_id', 'products.id')->select('seller_products.*', 'seller_products.id as seller_id', 'products.*', 'products.id as id')
            ->latest('products.created_at')
            ->where('products.user_id', $id)
            ->where('products.status', '=', 'Approved')->get();
        $reviewavg = review::where('seller_id', $id)->avg('rate');
        $reviewsum = review::where('seller_id', $id)->sum('rate');
        $reviewall = review::where('seller_id', $id)->get();
        return view('profile', compact('user', 'products', 'products_trade', 'sellers', 'categories', 'reviewavg', 'reviewsum', 'reviewall'));
    }

    public function about_us()
    {
        $now = Carbon::now()->format('Y-m-d H:i:s');
        $testimonials = Testimonial::latest()->get();
        $products_trade = product::join('product_details', 'product_details.product_id', 'products.id')
        ->select('product_details.*', 'product_details.id as detail_id', 'products.*')
        ->where(['products.flags'=>'trade', 'products.status' => 'Approved'])
        ->where('product_details.start_date', '<', $now)->where('product_details.end_date','>', $now)
        ->inRandomOrder()->take(20)->get();
        return view('about-us', compact('testimonials', 'products_trade'));
    }

    public function auction(Request $request)
    {

        $categories = category::latest()->with('categories')->where('deleted_at', null)->get();
        $perpage = 10;
        if (isset($request->perpage) && !empty($request->perpage)) {
            $perpage = $request->perpage;
        }

        $auctions = product::join('auction_products', 'auction_products.product_id', 'products.id')->where('products.status', 'Approved');

        $auctions = $auctions->where('auction_products.end_auction', 0);
        if (isset($request->category) && !empty($request->category)) {
            $category = $request->category;
            $auctions = $auctions->where('products.category_id', $category);
        }
        if (isset($request->sub_category) && !empty($request->sub_category)) {
            $sub_category = $request->sub_category;
            $auctions = $auctions->where('products.sub_category_id', $sub_category);
        }
        if (isset($request->brand) && !empty($request->brand)) {
            $brand = $request->brand;
            $auctions = $auctions->where('products.brand_id', $brand);
        }

        $auctions = $auctions->select('auction_products.*', 'auction_products.id as auction_id', 'products.*');
        $auctions = $auctions->latest('products.created_at')->where('products.status', '!=', 'Deleted');
        if (isset($request->sort) && !empty($request->sort)) {
            $auctions = $auctions->orderBy('products.id', $request->sort);
        }
        $auctions = $auctions->paginate($perpage);
        $auctions = $auctions->withQueryString();
        $auctions = $this->checkAuctionList($auctions);
        $auctions->total();
        $category = category::latest()->with('categories')->get();
        $sub_category = sub_category::get();
        $brand = Brand::get();
        return view('auction', compact('category', 'sub_category', 'auctions', 'brand', 'categories'));
    }

    public function checkAuctionList($auctions){
        $now = Carbon::now();
        foreach($auctions  as $key => $auc){
           $auction_date = Carbon::parse($auc->start_date.' '. $auc->start_time);
           $auction_end_date =  Carbon::parse($auc->end_date.' '. $auc->end_time);
           if($auction_date > $now || $now > $auction_end_date){
               unset($auctions[$key]);
           }
        }
        return $auctions;
    }



    public function auction_list($id)
    {
        if(Auth()->guard('buyer')->user()){
            $user_id = Auth()->guard('buyer')->user()->id;
        }
        else if(Auth()->guard('seller')->user()){
            $user_id = Auth()->guard('seller')->user()->id;
        }
        else{
            return redirect('/admin');
        }
        $categories = category::latest()->with('categories')->where('deleted_at', null)->get();

        $auctions = product::join('auction_products', 'auction_products.product_id', 'products.id')->where('products.status', 'Approved');
        $auctions = $auctions->where('products.id', $id);
        // $auctions = $auctions->where('auction_products.end_date', '>=', date('m/d/Y'));
        $auctions = $auctions->where('products.user_id', '!=', $user_id);
        $auctions = $auctions->select('auction_products.*', 'auction_products.id as auction_id', 'products.*');
        $auctions = $auctions->where('products.status', '!=', 'Deleted')->first();
        if(!$this->checkAuctionDetail($auctions)){
            return redirect()->back();
        }
        if (empty($auctions)) {
            return redirect()->route('seller_auction.show',$id);
        }
        $categorydata = product::join('auction_products', 'auction_products.product_id', 'products.id')->where('products.status', 'Approved');
        // $categorydata = $categorydata->where('products.id','!=',$id);
        // $categorydata = $categorydata->where('products.category_id',$auctions->category_id);
        $categorydata = $categorydata->select('auction_products.*', 'auction_products.id as auction_id', 'products.*');
        $categorydata = $categorydata->where('products.status', '!=', 'Deleted')->get();
        $acutioncount = Auction_bit_list::where('product_id', $id)->get();
        $reviewavg = review::where('product_id', $id)->avg('rate');
        $reviewsum = review::where('product_id', $id)->sum('rate');
        $reviewall = review::where('product_id', $id)->get();
        return view('auction-list', compact('auctions', 'categorydata', 'acutioncount', 'categories', 'reviewavg', 'reviewsum', 'reviewall'));
    }

    public function checkAuctionDetail($auc){
        $now = Carbon::now();
        $auction_date = Carbon::parse($auc->start_date.' '. $auc->start_time);
        $auction_end_date =  Carbon::parse($auc->end_date.' '. $auc->end_time);

        if($auction_date > $now || $now > $auction_end_date){
           return false;
        }
        return true;
    }
    public function trading_detail($id)
    {
        if(Auth()->guard('buyer')->user()){
            $user_id = Auth()->guard('buyer')->user()->id;
        }
        else if(Auth()->guard('seller')->user()){
            $user_id = Auth()->guard('seller')->user()->id;
        }
        else{
            return redirect('/admin');
        }

        $categories = category::latest()->with('categories')->where('deleted_at', null)->get();


        $products = product::join('trading_products', 'trading_products.product_id', 'products.id')->where('products.status', 'Approved');
        $products = $products->where('products.id', $id);
        $products = $products->where('products.user_id', '!=', $user_id);
        $products = $products->select('trading_products.*', 'trading_products.id as trading_id', 'products.*')
            ->latest('products.created_at')
            ->where('products.status', '!=', 'Deleted')->first();
        if (empty($products)) {
            return redirect()->route('seller_trading.show',$id);
        }
        $reviewavg = review::where('product_id', $id)->avg('rate');
        $reviewsum = review::where('product_id', $id)->sum('rate');
        $reviewall = review::where('product_id', $id)->get();
        $item = "";
        $reviewbycount = review::where('product_id', $id)->get();

        //dump($products);
        $sellers = product::join('seller_products', 'seller_products.product_id', 'products.id')->select('seller_products.*', 'seller_products.id as seller_id', 'products.*', 'products.id as id')
            ->latest('products.created_at')
            ->where('products.status', '!=', 'Deleted')->where('products.status', 'Approved')->get();
        return view('trading_details', compact('products', 'reviewavg', 'reviewall', 'reviewsum', 'reviewbycount', 'sellers', 'categories'));
    }

    public function post_make_trade(Request $request)
    {


        $validator = Validator::make($request->all(), [

            'title' => 'required',
            'description' => 'required',
            'image' => 'required|image',

        ]);
        $request->except('_token');
        if ($validator->fails()) {
            $response['error'] = 1;
            $response['errors'] = $validator->errors();
            //d($response);
            return json_encode($response);
        }
        DB::beginTransaction();
        try {
            $data['title'] = $request->title;
            $data['size'] = $request->size;
            $data['color'] = $request->color;
            $data['description'] = $request->description;
            $data['product_id'] = $request->product_id;
            $data['user_id'] =  Auth()->guard('buyer')->user()->id;
            $data['seller_id'] = $request->seller_id;

            $make_trade = make_trade::create($data);


            $file2 = $request->files->get('image')->getClientOriginalName();
            $fileName = $request->image->store('public/product');
            $fileName = Str::after($fileName, 'public/');
            $image_data['make_trade_id'] = $make_trade->id;
            $image_data['image'] = $fileName;
            $make_trade = make_trade_images::create($image_data);
            DB::commit();
            $response['success'] = 1;
            $response['msg'] = ('product inserted Successfully!!');
            //d($response);
            return json_encode($response);
        } catch (\Exception $e) {
            DB::rollback();
            $response['error'] = 1;
            $response['msg'] = ($e->getMessage());
            return json_encode($response);
        }
    }
    public function products(Request $request)
    {
        $type = "";
        $tradings = "";
        $products = "";
        $category = "";
        $brand = "";
        $orderby = "";
        $categories_name = "";
        $keyword = "";
        $sub_cat = "";
        $sub_categories_name = "";
        $now = Carbon::now()->format('Y-m-d H:i:s');

        if (isset($request->keyword) && !empty($request->keyword)) {
            $keyword = $request->keyword;
        }
        if (isset($request->type) && !empty($request->type)) {
            $type = $request->type;
        }
        if (isset($request->cat) && !empty($request->cat)) {
            $category = $request->cat;
            $categories_name = category::where('id', $category)->first();
        }
        if (isset($request->sub_cat) && !empty($request->sub_cat)) {
            $sub_cat = $request->sub_cat;
            $sub_categories_name = sub_category::where('id', $sub_cat)->first();
        }
        if (isset($request->brand) && !empty($request->brand)) {
            $brand = $request->brand;
        }
        if (isset($request->max_value) && !empty($request->max_value)) {
            $max_price = (int)$request->max_value;
            $min_price = (int)$request->min_value;
        }
        if (isset($request->orderby) && !empty($request->orderby)) {
            $orderby = $request->orderby;
        }
        if ($type == "list" || (!isset($request->type) && empty($request->type))) {
            $products = product::join('product_details', 'product_details.product_id', 'products.id')
            ->where('products.status', 'Approved')
            ->select('product_details.*', 'product_details.id as product_id', 'products.*')
            ->where('products.flags', '!=','request-trade')
            ->where('product_details.start_date', '<', $now)->where('product_details.end_date','>', $now);

            if (!empty($keyword)) {
                $products = $products->where('products.name', 'LIKE', "%$keyword%");
            }
            if (!empty($sub_cat)) {
                $products = $products->where('products.sub_category_id', $sub_cat);
            }
            if (!empty($category)) {
                $products = $products->where('products.category_id', $category);
            }
            if (!empty($brand)) {
                $products = $products->where('products.brand_id', $brand);
            }
            if (!empty($max_price)) {
                $products = $products->whereBetween('products.price', [$min_price, $max_price]);
            }
            if (isset($request->orderby) && !empty($request->orderby)) {
                $products = $products->orderBy('products.id',  $orderby);
            }

            $products =  $products->paginate(9)->withQueryString();
        }


        if ($type == "selling") {
            $products = product::join('product_details', 'product_details.product_id', 'products.id')
            ->where('products.status', 'Approved')
            ->where('products.flags', $type)
            ->where('product_details.start_date', '<', $now)->where('product_details.end_date','>', $now);
            if (!empty($keyword)) {
                $products = $products->where('products.name', 'LIKE', "%$keyword%");
            }
            if (!empty($category)) {
                $products = $products->where('products.category_id', $category);
            }
            if (!empty($sub_cat)) {
                $products = $products->where('products.sub_category_id', $sub_cat);
            }
            if (!empty($brand)) {
                $products = $products->where('products.brand_id', $brand);
            }
            if (!empty($max_price)) {
                $products = $products->whereBetween('products.price', [$min_price, $max_price]);
            }
            $products = $products->select('product_details.*', 'product_details.id as product_id', 'products.*')
                ->latest('products.created_at')
                ->where('products.status', 'Approved');
            if (isset($request->orderby) && !empty($request->orderby)) {
                $products = $products->orderBy('products.id',  $orderby);
            }
            $products = $products->paginate(9)->withQueryString();
        }

        if ($type == "trade") {
            $products = product::join('product_details', 'product_details.product_id', 'products.id')
            ->where('products.status', 'Approved')
            ->where('products.flags', $type);
            if (!empty($keyword)) {
                $products = $products->where('products.name', 'LIKE', "%$keyword%");
            }
            if (!empty($category)) {
                $products = $products->where('products.category_id', $category);
            }
            if (!empty($sub_cat)) {
                $products = $products->where('products.sub_category_id', $sub_cat);
            }
            if (!empty($brand)) {
                $products = $products->where('products.brand_id', $brand);
            }
            if (!empty($max_price)) {
                $products = $products->whereBetween('products.price', [$min_price, $max_price]);
            }
            $products = $products->select('product_details.*', 'product_details.id as trading_id', 'products.*')
                ->latest('products.created_at')
                ->where('products.status', 'Approved');
            if (isset($request->orderby) && !empty($request->orderby)) {
                $products = $products->orderBy('products.id',  $orderby);
            }
            $products = $products->paginate(9)->withQueryString();
        }


        if ($type == "auction") {
            $products = product::join('product_details', 'product_details.product_id', 'products.id')
            ->where('products.status', 'Approved')
            ->where('products.flags', $type)
            ->where('product_details.start_date', '<', $now)->where('product_details.end_date','>', $now);
            if (!empty($keyword)) {
                $products = $products->where('products.name', 'LIKE', "%$keyword%");
            }
            if (!empty($category)) {
                $products = $products->where('products.category_id', $category);
            }
            if (!empty($sub_cat)) {
                $products = $products->where('products.sub_category_id', $sub_cat);
            }
            if (!empty($brand)) {
                $products = $products->where('products.brand_id', $brand);
            }
            if (!empty($max_price)) {
                $products = $products->whereBetween('products.price', [$min_price, $max_price]);
            }
            $products = $products->select('product_details.*', 'product_details.id as product_id', 'products.*')
                ->latest('products.created_at')
                ->where('products.status', 'Approved');
            if (isset($request->orderby) && !empty($request->orderby)) {
                $products = $products->orderBy('products.id',  $orderby);
            }
            $products = $products->paginate(9)->withQueryString();
        }


        $categories = category::latest()->with('categories')->where('deleted_at', null)->get();
        $brand = Brand::get();
        $sellers = product::join('product_details', 'product_details.product_id', 'products.id')
        ->select('product_details.*', 'product_details.id as seller_id', 'products.*', 'products.id as id')
        ->latest('products.created_at')
        ->where('product_details.start_date', '<', $now)->where('product_details.end_date','>', $now)
        ->where('products.status', 'Approved')
        ->where('products.flags', 'selling')
        ->inRandomOrder()->take(20)->get();

        if($products === ""){
            abort(404);
        }


        return view('products', compact('products', 'tradings', 'categories', 'brand', 'categories_name', 'sellers', 'sub_categories_name'));
    }


    public function newArrival(Request $request)
    {
        $type = "";
        $tradings = "";
        $products = "";
        $category = "";
        $brand = "";
        $orderby = "";
        $categories_name = "";
        $keyword = "";
        $sub_cat = "";
        $sub_categories_name = "";
        $now = Carbon::now()->format('Y-m-d H:i:s');
        $past = Carbon::now()->subDays(30)->format('Y-m-d H:i:s');

        if (isset($request->keyword) && !empty($request->keyword)) {
            $keyword = $request->keyword;
        }
        if (isset($request->type) && !empty($request->type)) {
            $type = $request->type;
        }
        if (isset($request->cat) && !empty($request->cat)) {
            $category = $request->cat;
            $categories_name = category::where('id', $category)->first();
        }
        if (isset($request->sub_cat) && !empty($request->sub_cat)) {
            $sub_cat = $request->sub_cat;
            $sub_categories_name = sub_category::where('id', $sub_cat)->first();
        }
        if (isset($request->brand) && !empty($request->brand)) {
            $brand = $request->brand;
        }
        if (isset($request->max_value) && !empty($request->max_value)) {
            $max_price = (int)$request->max_value;
            $min_price = (int)$request->min_value;
        }
        if (isset($request->orderby) && !empty($request->orderby)) {
            $orderby = $request->orderby;
        }
        if ($type == "list" || (!isset($request->type) && empty($request->type))) {
            $products = product::join('product_details', 'product_details.product_id', 'products.id')
            ->where('products.status', 'Approved')
            ->select('product_details.*', 'product_details.id as product_id', 'products.*')
            ->where('products.flags', '!=','request-trade')->where('products.created_at', '>',$past)
            ->where('product_details.start_date', '<', $now)->where('product_details.end_date','>', $now);

            if (!empty($keyword)) {
                $products = $products->where('products.name', 'LIKE', "%$keyword%");
            }
            if (!empty($sub_cat)) {
                $products = $products->where('products.sub_category_id', $sub_cat);
            }
            if (!empty($category)) {
                $products = $products->where('products.category_id', $category);
            }
            if (!empty($brand)) {
                $products = $products->where('products.brand_id', $brand);
            }
            if (!empty($max_price)) {
                $products = $products->whereBetween('products.price', [$min_price, $max_price]);
            }
            if (isset($request->orderby) && !empty($request->orderby)) {
                $products = $products->orderBy('products.id',  $orderby);
            }

            $products =  $products->paginate(9)->withQueryString();
        }


        if ($type == "selling") {
            $products = product::join('product_details', 'product_details.product_id', 'products.id')
            ->where('products.status', 'Approved')
            ->where('products.flags', $type)->where('products.created_at', '>',$past)
            ->where('product_details.start_date', '<', $now)->where('product_details.end_date','>', $now);
            if (!empty($keyword)) {
                $products = $products->where('products.name', 'LIKE', "%$keyword%");
            }
            if (!empty($category)) {
                $products = $products->where('products.category_id', $category);
            }
            if (!empty($sub_cat)) {
                $products = $products->where('products.sub_category_id', $sub_cat);
            }
            if (!empty($brand)) {
                $products = $products->where('products.brand_id', $brand);
            }
            if (!empty($max_price)) {
                $products = $products->whereBetween('products.price', [$min_price, $max_price]);
            }
            $products = $products->select('product_details.*', 'product_details.id as product_id', 'products.*')
                ->latest('products.created_at')
                ->where('products.status', 'Approved');
            if (isset($request->orderby) && !empty($request->orderby)) {
                $products = $products->orderBy('products.id',  $orderby);
            }
            $products = $products->paginate(9)->withQueryString();
        }

        if ($type == "trade") {
            $products = product::join('product_details', 'product_details.product_id', 'products.id')
            ->where('products.status', 'Approved')->where('products.created_at', '>',$past)
            ->where('products.flags', $type);
            if (!empty($keyword)) {
                $products = $products->where('products.name', 'LIKE', "%$keyword%");
            }
            if (!empty($category)) {
                $products = $products->where('products.category_id', $category);
            }
            if (!empty($sub_cat)) {
                $products = $products->where('products.sub_category_id', $sub_cat);
            }
            if (!empty($brand)) {
                $products = $products->where('products.brand_id', $brand);
            }
            if (!empty($max_price)) {
                $products = $products->whereBetween('products.price', [$min_price, $max_price]);
            }
            $products = $products->select('product_details.*', 'product_details.id as trading_id', 'products.*')
                ->latest('products.created_at')
                ->where('products.status', 'Approved');
            if (isset($request->orderby) && !empty($request->orderby)) {
                $products = $products->orderBy('products.id',  $orderby);
            }
            $products = $products->paginate(9)->withQueryString();
        }


        if ($type == "auction") {
            $products = product::join('product_details', 'product_details.product_id', 'products.id')
            ->where('products.status', 'Approved')->where('products.created_at', '>',$past)
            ->where('products.flags', $type)
            ->where('product_details.start_date', '<', $now)->where('product_details.end_date','>', $now);
            if (!empty($keyword)) {
                $products = $products->where('products.name', 'LIKE', "%$keyword%");
            }
            if (!empty($category)) {
                $products = $products->where('products.category_id', $category);
            }
            if (!empty($sub_cat)) {
                $products = $products->where('products.sub_category_id', $sub_cat);
            }
            if (!empty($brand)) {
                $products = $products->where('products.brand_id', $brand);
            }
            if (!empty($max_price)) {
                $products = $products->whereBetween('products.price', [$min_price, $max_price]);
            }
            $products = $products->select('product_details.*', 'product_details.id as product_id', 'products.*')
                ->latest('products.created_at')
                ->where('products.status', 'Approved');
            if (isset($request->orderby) && !empty($request->orderby)) {
                $products = $products->orderBy('products.id',  $orderby);
            }
            $products = $products->paginate(9)->withQueryString();
        }


        $categories = category::latest()->with('categories')->where('deleted_at', null)->get();
        $brand = Brand::get();


        if($products === ""){
            abort(404);
        }


        return view('new-arrival', compact('products', 'tradings', 'categories', 'brand', 'categories_name',  'sub_categories_name'));

}

    public function topTraders()
    {
        $data['traders'] = product::where('flags', 'trade')->groupBy('user_id')->limit(9)->get();
        return view('top_traders', $data);
    }

    public function topTrades()
    {
        $data['trades'] = make_trade::groupBy("product_id")->orderByRaw("COUNT(product_id) DESC")->limit(9)->get();
        return view('top_trades', $data);
    }

    public function newsletterSubmit(Request $request)
    {
        try {
            $newsletter = Newsletter::where('email', $request->email)->first();
            if (!empty($newsletter)) {
                return 422;
            }
            $data = $request->all();
            Newsletter::create($data);
            return 200;
        } catch (\Throwable $th) {
            return 500;
        }
    }

    public function buyer_login()
    {
        if (Auth()->guard('buyer')->check()) {
            $guard = 'buyer';
        } else if (Auth()->guard('seller')->check()) {
            $guard = 'seller';
        } else if (Auth()->guard('admin')->check()) {
            $guard = 'admin';
        } else {
            return redirect()->route('login');
        }
        return redirect('/' . $guard);
    }



    public function buyer_signup()
    {
        return view('buyer_signup');
    }
    public function product_details($id)
    {
        if(Auth()->guard('buyer')->user()){
            $user_id = Auth()->guard('buyer')->user()->id;
        }
        else if(Auth()->guard('seller')->user()){
            $user_id = Auth()->guard('seller')->user()->id;
        }
        else{
            return redirect('/admin');
        }

        $categories = category::latest()->with('categories')->where('deleted_at', null)->get();

        $products = product::where('id',$id);
        $products = $products->where('products.user_id', '!=', $user_id)->first();
        if (empty($products)) {
            return redirect()->route('seller_seller.show',$id);
        }
        $reviewavg = review::where('product_id', $id)->avg('rate');
        $reviewsum = review::where('product_id', $id)->sum('rate');
        $reviewall = review::where('product_id', $id)->get();
        $item = "";
        $reviewbycount = review::where('product_id', $id)->select(DB::raw('count(*) as rate_count,reviews.*'))
            ->groupBy('rate')
            ->orderBy('rate', 'asc')
            ->get();

        //dump($products);
        $sellers = product::latest('products.created_at')
            ->where('products.status', 'Approved')->where('products.flags', 'seller')->get();

        return view('product-details', compact('products', 'reviewavg', 'reviewall', 'reviewsum', 'reviewbycount', 'categories', 'sellers'));
    }

    public function checkout()
    {
        $categories = category::latest()->with('categories')->where('deleted_at', null)->get();

        if (Auth()->guard('seller')->check()) {
            $role = 'seller';
        } else if (Auth()->guard('buyer')->check()) {
            $role = 'buyer';
        } else if (Auth()->guard('admin')->check()) {
            $role = 'admin';
        }
        if (isset($role)) {
            $checkout = checkout::where('user_id', auth()->guard($role)->user()->id)->paginate(9)->withQueryString();
            $checkout_total_query = checkout::with('products')->where('user_id', auth()->guard($role)->user()->id)->get();
            $checkout_total = 0;
            $zip = 0;
            $products = null;
            $arr = [];

            foreach ($checkout_total_query as $key) {
                $checkout_total = $checkout_total + ($key->price * $key->qty);
                if($key->products->flags == 'trade'){

                    if((int) $key->products->price >= 200){
                        $checkuser_id = checkout::with('products')->where('product_id', $key->products->id)->first();
                        $w =  Warehouse::where('user_id', $checkuser_id->user_id )->first();
                        $products = $checkuser_id->products->id;
                        $zip = $w->zip;
                        array_push($arr,['zip' => $zip, 'products' => $products, 'checkout_id' => $key->id]);

                    }
                }
            }
            $arr = json_encode($arr);
            if($checkout_total == 0){
                return view('empty_checkout');
            }
            // $shipping = shipping::where('user_id', auth()->guard($role)->user()->id)->first();
            $shipping = [];
            $card = DB::table('cards')->where('user_id',auth()->guard($role)->user()->id)->first();
            return view('checkout', compact('checkout', 'checkout_total', 'shipping', 'categories','card', 'zip', 'products','arr'));
        } else {
            return redirect('/buyer_login');
        }
    }

    public function checkoutSingle($id)
    {
        $categories = category::latest()->with('categories')->where('deleted_at', null)->get();

        if (Auth()->guard('seller')->check()) {
            $role = 'seller';
        } else if (Auth()->guard('buyer')->check()) {
            $role = 'buyer';
        } else if (Auth()->guard('admin')->check()) {
            $role = 'admin';
        }
        if (isset($role)) {
            $checkout = checkout::where([
                'id' => $id,
                'user_id'=> auth()->guard($role)->user()->id
            ])
            ->with(['products'])
            ->first();

            $checkout_total = $checkout->price * $checkout->qty;
            $card = DB::table('cards')->where('user_id',auth()->guard($role)->user()->id)->first();
            return view('checkout_single', compact('checkout', 'checkout_total', 'categories','card'));
        }
        else {
            return redirect('/buyer_login');
        }
    }

    public function cart()
    {

        $categories = category::latest()->with('categories')->where('deleted_at', null)->get();

        if (Auth()->guard('seller')->check()) {
            $role = 'seller';
        } else if (Auth()->guard('buyer')->check()) {
            $role = 'buyer';
            $checkout = checkout::where([
            'user_id' => auth()->guard($role)->user()->id,
            'type' => 'selling'
            ])->with(['products_images'])->paginate(9)->withQueryString();
            $checkout_total_query = checkout::where([
                'user_id' => auth()->guard($role)->user()->id,
                'type' => 'selling'
                ])->get();
            $checkout_total = 0;
            foreach ($checkout_total_query as $key) {
                $checkout_total = $checkout_total + ($key->price * $key->qty);
            }
            return view('cart', compact('checkout', 'checkout_total', 'categories'));

        } else if (Auth()->guard('admin')->check()) {
            $role = 'admin';
        }
        return redirect('login');
    }


    // public function tradeCart()
    // {
    //     $categories = category::latest()->with('categories')->where('deleted_at',null)->get();
    //     if(Auth()->guard('seller')->check()) {
    //         $role = 'seller';
    //         $checkout = checkout::where('user_id',auth()->guard($role)->user()->id)->with(['products_images'])->paginate(9)->withQueryString();
    //         $checkout_total_query = checkout::where('user_id',auth()->guard($role)->user()->id)->get();
    //         $checkout_total=0;
    //         foreach($checkout_total_query as $key){
    //             $checkout_total=$checkout_total+($key->price*$key->qty);
    //         }
    //         return view('cart',compact('checkout','checkout_total' ,'categories'));
    //     }
    //     else{
    //         abort(404);
    //     }
    // }

    public function GetCategories($id)
    {
        $data = sub_category::where('category_id',$id)->get();
        return $data;
    }

    public function add_bid(Request $request)
    {

        $json = array();
        $data = array();
        $price = $request->price;
        $product_id = $request->product_id;
        $product_detail = product::where('id', $product_id)->first();

        if (Auth()->guard('buyer')->check() != 1) {
            $json['type'] = 'error';
            $json['message'] = "You are not authorize to perform this action";
        } else {
            $auctiondaata = auction_product::where('product_id', $product_id)->first();
            if (!empty($auctiondaata)) {
                if ($auctiondaata->bid_count != 0 && $price == $product_detail->price) {
                    $json['type'] = 'error';
                    $json['message'] = "Price must be greater than actual price";
                } else if ($auctiondaata->bid_price_update != "" && $price <= $auctiondaata->bid_price_update) {
                    $json['type'] = 'error';
                    $json['message'] = "Price must be greater than actual price";
                } else {

                    $sellaremail = $product_detail->users->email;
                    $sellarname = $product_detail->users->name;
                    $biddername = Auth()->guard('buyer')->user()->name;
                    $url = url('/auction-list/') . '/' . $product_id;
                    $body = "<b>$biddername</b> bid on your product with price  <b>$price</b>  <a href='$url'>product detail</a>";

                    $bid_count = $auctiondaata->bid_count + 1;
                    $data = array('bid_price_update' => $price, 'bid_count' => $bid_count, 'bid_last_user_id' => Auth()->guard('buyer')->user()->id);
                    auction_product::where('product_id', $product_id)->update($data);
                    $auctionuser = array('product_id' => $product_id, 'saller_id' => $product_detail->user_id, 'user_price' => $price, 'user_id' => Auth()->guard('buyer')->user()->id);
                    Auction_bit_list::create($auctionuser);
                    $data['name'] = $sellarname;
                    $data['email'] = $sellaremail;
                    $data['bodytxt'] = $body;
                     $this->sendMail($data, 'emails.actions_send');
                    $json['type'] = 'success';
                    $json['message'] = "successfully bids on product";
                }
            }
        }
        return  $json;
    }
    public function getsub(Request $request)
    {
        $id = $request->id;
        if (!empty($id)) {
            $sub_category = sub_category::where('category_id', $id)->get();
            if (!empty($sub_category)) {
                return json_encode($sub_category);
            }
        }
    }

    public function Sitemap()
    {
        return view('sitemap');
    }

    public function PrivacyPolicy()
    {
        return view('privacy');
    }


    public function switchUser($type, $id)
    {
        $user = User::find($id);
        if ($type === "buyer") {
            Auth::guard('seller')->logout();
            $user->update(['user_type' => "Buyer"]);
            $pass = Session::get('pas_key');

            if (Auth::guard("buyer")->attempt(['email' => $user->email, 'password' => $pass])) {
                return redirect()->route('buyer_dashboard');
            } else {
                return redirect()->route('login');
            }
        }
        elseif ($type === "seller") {
            Auth::guard('buyer')->logout();
            $user->update(['user_type' => "Seller"]);
            $pass = Session::get('pas_key');

            if (Auth::guard("seller")->attempt(['email' => $user->email, 'password' => $pass])) {
                return redirect()->route('seller_dashboard');
            } else {
                return redirect()->route('login');
            }
        } else {
            return redirect()->back()->with('alert-danger', "Error occured in switching role!");
        }
    }

    public function AuctionCron()
    {
        try {
            $auction_winer = product::join('auction_products', 'auction_products.product_id', 'products.id');
            $auction_winer = $auction_winer->where('auction_products.end_auction', 0);
            $auction_winer = $auction_winer->select('auction_products.*', 'auction_products.id as auction_id', 'products.*');
            $auction_winer = $auction_winer->where('products.status', '!=', 'Deleted')->get();
            foreach ($auction_winer as $key => $value) {
                $date = $value->end_date . ' ' . $value->end_time;
                $check = $this->checkDate($date);
                if ($check == true) {
                    unset($auction_winer[$key]);
                }
            }

            foreach ($auction_winer as $winer) {
                $checkoutdata = array(
                    'user_id' => $winer->bid_last_user_id,
                    'product_id' => $winer->product_id,
                    'price' => $winer->bid_price_update,
                    'qty' => 1,
                    'type' => 'auction',
                    'color' => 'N/A',
                    'size' => 'N/A'
                );

                $checkout = checkout::create($checkoutdata);
                auction_product::where('product_id', $winer->product_id)->update(['end_auction' => 1]);
            }
        } catch (\Throwable $th) {
            return $th->getMessage();
            return 404;
        }

        return 1;
    }

    public function checkDate($date)
    {
        $date =  strtotime($date);
        $differenceInDays = Carbon::now()->diffInDays(Carbon::parse($date), false);
        $differenceInHours = Carbon::now()->diffInHours(Carbon::parse($date), false);
        $differenceInMinutes = Carbon::now()->diffInMinutes(Carbon::parse($date), false);

        if ($differenceInDays > 0) {
            return true;
        } elseif ($differenceInHours > 0) {
            return true;
        } elseif ($differenceInMinutes > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function cardRetrive()
    {
        $stripe = new \Stripe\StripeClient(
            env('STRIPE_SECRET')
          );
        $users = User::where('stripe_customer_id' , '!=' , null)->get();
        foreach($users as $u){
            $customer = $stripe->customers->retrieve(
                $u->stripe_customer_id,
                []
              );
             $card = $stripe->customers->retrieveSource(
                $u->stripe_customer_id,
                $customer['default_source'],
              );

            DB::table('cards')->insert([
                'user_id' => $u->id,
                'card_id' => $card['id'],
                'brand' => $card['brand'],
                'exp_month' => $card['exp_month'],
                'exp_year' => $card['exp_year'],
                'last4' => $card['last4'],
            ]);
        }


    }
}
