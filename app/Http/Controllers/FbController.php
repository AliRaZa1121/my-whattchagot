<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Models\User;
use Validator;
use Socialite;
use Exception;
use Auth;

class FbController extends Controller
{
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function facebookSignin()
    {
        try {

            $user = Socialite::driver('facebook')->user();
            $finduser = User::where('facebook_id', $user->id)->first();

            if($finduser){
                $user_type = $finduser->user_type;
                $guardName = "";
                switch ($user_type) {
                    case 'Admin':
                        $guardName = "admin";
                        break;
                    case 'Seller':
                        $guardName = "seller";
                        break;
                    case 'Buyer':
                        $guardName = "buyer";
                        break;

                }

                Auth::guard($guardName)->login($finduser);

//                 return redirect($finduser->user_type.'/home');
                return redirect()->intended('/' . $guardName);

            }else{
                $newUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'status' => 'Approved',
                    'user_type' => 'Buyer',
                    'facebook_id'=> $user->id
                ]);

                Auth::guard('buyer')->login($newUser);

                return redirect('buyer/');
            }

        } catch (Exception $exception) {
            dd($exception->getMessage());
        }
    }
}