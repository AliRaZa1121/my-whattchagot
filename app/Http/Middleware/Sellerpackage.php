<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\SellerPackage as SP;
use App\Models\product;
use Illuminate\Support\Facades\DB;


class Sellerpackage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!empty(auth()->user()->seller_package_id)) {
            $pkg = SP::find(auth()->user()->seller_package_id);
            $from   = auth()->user()->package_start_date;
            $to = auth()->user()->package_valid_till;
            if (!empty($pkg)) {
                $url = $request->url();
                switch ($url) {
                    case str_contains($url, 'seller_seller'):
                        $sp = product::where('user_id', auth()->user()->id)->where('flags', 'seller')->whereBetween('created_at', [$from, $to])->count();
                        if ($sp < $pkg->seller_product_quantity) {
                            return $next($request);
                        } else {
                            return redirect()->back()->with('alert-danger', "Seller Product Package Value Out of reach");
                        }
                        break;

                    case str_contains($url, 'seller_auction'):
                        $sp = product::where('user_id', auth()->user()->id)->where('flags', 'auction')->whereBetween('created_at', [$from, $to])->count();
                        if ($sp < $pkg->auction_product_quantity) {
                            return $next($request);
                        } else {
                            return redirect()->back()->with('alert-danger', "Auction Product Package Value Out of reach");
                        }
                        break;

                    case str_contains($url, 'seller_trading'):
                        $sp = product::where('user_id', auth()->user()->id)->where('flags', 'trade')->whereBetween('created_at', [$from, $to])->count();
                        if ($sp < $pkg->trade_product_quantity) {
                            return $next($request);
                        } else {
                            return redirect()->back()->with('alert-danger', "Trade Product Package Value Out of reach");
                        }
                        break;

                    default:
                        return redirect()->back()->with('alert-danger', "Error occured in route");
                        break;
                }
            } else {
                return redirect()->back()->with('alert-danger', "Your current package has been deleted by administrator, Please renew your package");
            }
        }
        else {
            return redirect()->back()->with('alert-danger', "Please purchase seller package");
        }
        return $next($request);
    }
}
