<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\product;
use App\Models\category;
use App\Models\auction_product;
use App\Models\sub_category;
use App\Models\checkout;
use App\Models\shipping;
use App\Models\Brand;
use App\Models\trading_product;
use Carbon\Carbon;
use App\Models\review;
use DB;
use Validator;
use App\Models\make_trade;
use App\Models\make_trade_images;
use App\Models\Auction_bit_list;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Notification;
use App\Notifications\auctionsend;
use App\Traits\EmailTrait;
use App\Models\User;

class AuctionWinner extends Command
{
    use EmailTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auctionwinner:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $auction_winer = product::join('auction_products', 'auction_products.product_id', 'products.id');
        $auction_winer = $auction_winer->where('auction_products.end_auction', 0);
        $auction_winer = $auction_winer->select('auction_products.*', 'auction_products.id as auction_id', 'products.*');
        $auction_winer = $auction_winer->where('products.status', '!=', 'Deleted')->get();
        foreach ($auction_winer as $key => $value) {
            $date = $value->end_date . ' ' . $value->end_time;
            $check = $this->checkDate($date);
            if ($check == true) {
                unset($auction_winer[$key]);
            }
        }


        foreach ($auction_winer as $winer) {
            $checkoutdata = array(
                'user_id' => $winer->bid_last_user_id,
                'product_id' => $winer->product_id,
                'price' => $winer->bid_price_update,
                'qty' => 1,
                'type' => 'auction',
                'color' => 'N/A',
                'size' => 'N/A'
            );

            $checkout = checkout::create($checkoutdata);
            auction_product::where('product_id', $winer->product_id)->update(['end_auction' => 1]);
        

         $buyer =User::where('id',$winer->bid_last_user_id)->first();
         $buyreemail=$buyer->email;
         $buyrename=$buyer->name;
         $url=url('/checkout/');
         $body="<b>Congratulations!</b> you win the bid    <a href='$url'>click here to checkout</a>";
         $data['name']= $buyrename;
         $data['email']= $buyreemail;
         $data['bodytxt']= $body;
         $this->sendMail($data, 'emails.actions_send');
        }
    
    }

    public function checkDate($date)
    {
        $date =  strtotime($date);
        $differenceInDays = Carbon::now()->diffInDays(Carbon::parse($date), false);
        $differenceInHours = Carbon::now()->diffInHours(Carbon::parse($date), false);
        $differenceInMinutes = Carbon::now()->diffInMinutes(Carbon::parse($date), false);

        if ($differenceInDays > 0) {
            return true;
        } elseif ($differenceInHours > 0) {
            return true;
        } elseif ($differenceInMinutes > 0) {
            return true;
        } else {
            return false;
        }
    }

    
}
