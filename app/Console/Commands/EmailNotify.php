<?php

namespace App\Console\Commands;

use App\Models\checkout;
use Illuminate\Console\Command;
use App\Traits\EmailTrait;
use App\Models\User;
class EmailNotify extends Command
{
    use EmailTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:notify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $checkout = checkout::groupBy('user_id')->get();
            foreach ($checkout as $key => $value) {
                $user = User::find($value->user_id);
                $data['name'] = $user->name;
                $data['email'] = $user->email;
                $data['bodytxt'] = "Some products are in your cart, please check it out!";
                $this->sendMail($data, 'emails.actions_send');
            }
        } catch (\Throwable $th) {
            //throw $th;
        }
        // return Command::SUCCESS;
    }
}
