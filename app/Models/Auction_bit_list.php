<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\make_trade_images;
use App\Models\product;
use App\Models\User;
use App\Models\auction_product;

class Auction_bit_list extends Model
{
    use HasFactory;
    protected $table='auction_bit_list';
    protected $guarded = [];

    public function products()
    {
        return $this->hasOne(product::class,'id','product_id');
    }
    public function seller_name()
    {
        return $this->hasOne(User::class,'id','saller_id');
    }
    public function buyer_name()
    {
        return $this->hasOne(User::class,'id','user_id');
    }
    public function bidprice()
    {
        return $this->hasOne(auction_product::class,'product_id','product_id');
    }


}
