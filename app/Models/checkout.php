<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\product;
use App\Models\product_image;

class checkout extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','product_id','price','qty','color','size','type'];

    function products(){
        return $this->hasOne(product::class,'id','product_id');
    }
    function products_images(){
        return $this->hasOne(product_image::class,'product_id','product_id');
    }

}
