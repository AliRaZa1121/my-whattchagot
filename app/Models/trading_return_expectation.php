<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class trading_return_expectation extends Model
{
    use HasFactory;
    protected $fillable = ['product_id','image'];
}
