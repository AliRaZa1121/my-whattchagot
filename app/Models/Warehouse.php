<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    use HasFactory;
    protected $fillable = ['user_id','email','phone','address','country','state','zip'];

    public function seller()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
}
