<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\category;
use Illuminate\Database\Eloquent\SoftDeletes; //add this line


class sub_category extends Model
{
    use HasFactory;
    use SoftDeletes; //add this line


    protected $fillable = ['name','category_id'];


}
