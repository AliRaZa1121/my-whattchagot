<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SellerPackage extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'price',
        'password',
        'duration',
        'seller_product_quantity',
        'auction_product_quantity',
        'trade_product_quantity',
    ];

    protected $guarded = [

    ];
    public function user(){
        return $this->hasMany('App\User', 'seller_package_id');
    }

}
