<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dispute extends Model
{
    use HasFactory;

    protected $fillable = [

        'order_id',
        'name',
        'email',
        'message',
        'status',
        'seller_id',
        'user_id'
    ];

    public function seller_name()
    {
        return $this->belongsTo(User::class,'seller_id','id');
    }

    public function buyer_name()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class,'order_id','id');
    }


}
