<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; //add this line


class Brand extends Model
{
    use HasFactory;
    use SoftDeletes; //add this line

    protected $table = "brands";

    protected $fillable=['name','banner_image'];
}
