<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
    use SoftDeletes;
    protected $guarded = ['id'];

    public function subscription(){
        return $this->hasMany('App\Subscription', 'package_id');
    }

    public function user(){
        return $this->hasMany('App\User', 'seller_package_id');
    }

    public function onetimepayments() {
        return $this->hasMany('App\OneTimePayment', 'package_id');
    }
}
