<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class make_trade_images extends Model
{
    use HasFactory; 
    protected $table='make_trade_images';
    protected $guarded = [];  
}
