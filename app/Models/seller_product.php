<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class seller_product extends Model
{
    use HasFactory;
    protected $fillable = ['product_id', 'brand', 'start_date',
        'start_time',
        'end_date',
        'short_details',
        'end_time'];
    public $timestamps = false;
}
