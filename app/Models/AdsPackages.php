<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdsPackages extends Model
{

    use SoftDeletes;
    protected $guarded = ['id'];
    protected $table = "ads_packages";
}
