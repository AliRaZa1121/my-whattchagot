<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product;


class product_image extends Model
{
    use HasFactory;
    protected $fillable = ['product_id', 'image'];
    public $timestamps = false;

    public function products()
    {
        return $this->belongsTo(product::class);
    }
}
