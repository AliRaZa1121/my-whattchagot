<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\make_trade_images;
use App\Models\Product;
use App\Models\User;
use App\Models\auction_product;

class BannerImage extends Model
{
    use HasFactory;
    protected $table='banner_image';
    protected $guarded = [];

    public function product()
    {
        return $this->hasOne(product::class,'id','product_id');
    }
    public function user()
    {
        return $this->hasOne(User::class,'id','user_id');
    }
    public function package()
    {
        return $this->hasOne(AdsPackages::class,'id','package_id');
    }

    public function product_details()
    {
        return $this->hasOne(ProductDetail::class,'product_id','product_id');
    }

}
