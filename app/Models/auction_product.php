<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class auction_product extends Model
{
    use HasFactory;
    protected $fillable = ['product_id', 'brand', 'start_date', 'bid_price_update',
        'start_time',
        'short_details',
        'end_date',
        'end_time'];
    public $timestamps = false;
}
