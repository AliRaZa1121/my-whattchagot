<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\seller_product;
use App\Models\product_image;
use App\Models\sub_category;
use App\Models\category;
use App\Models\color;
use App\Models\size_value;
use App\Models\Brand;
use App\Models\User;
use App\Models\trading_return_expectation;
use Illuminate\Database\Eloquent\SoftDeletes;
class product extends Model
{
    use SoftDeletes;
    use HasFactory;

    protected $fillable = [
        'name',
        'price',
        'category_id',
        'sub_category_id',
        'brand_id',
        'length',
        'height',
        'width',
        'quantity',
        'user_id',
        'flags',
        'status',
        'trade_with_amount'
    ];
    public function seller_products()
    {
        return $this->hasOne(seller_product::class);
    }
    public function product_images()
    {
        return $this->hasOne(product_image::class);
    }
    public function product_images_many()
    {
        return $this->hasMany(product_image::class);
    }


    public function request_trade_images()
    {
        return $this->hasOne(product_image::class);
    }
    public function request_trade_images_many()
    {
        return $this->hasMany(product_image::class);
    }


    public function trading_return_expectations()
    {
        return $this->hasMany(trading_return_expectation::class);
    }


    public function sub_categories()
    {
        return $this->hasOne(sub_category::class,'id','sub_category_id');
    }
    public function categories()
    {
        return $this->hasOne(category::class,'id','category_id');
    }
    public function brand()
    {
        return $this->hasOne(Brand::class,'id','brand_id');
    }
    public function colors()
    {
        return $this->hasMany(color::class,'product_id','id');
    }  public function size_values()
    {
        return $this->hasMany(size_value::class,'product_id','id');
    }
    public function users()
    {
        return $this->hasOne(User::class,'id','user_id');
    }


    public function details()
    {
        return $this->hasOne(ProductDetail::class,'product_id','id');
    }

}
