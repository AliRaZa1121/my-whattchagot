<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\product;
use App\Models\User;

class chat extends Model
{
    use HasFactory;

    protected $fillable = ['sender_id',
        'receiver_id',
        'product_id',
        'message',
        'status'];

    public function products()
    {
        return $this->hasOne(product::class,'id','product_id');
    }
    public function users_receiver()
    {
        return $this->hasOne(User::class, 'id', 'receiver_id');
    }

        public function users_sender()
    {
        return $this->hasOne(User::class,'id','sender_id');
    }
    public function sub_categories()
    {
        return $this->hasOne(sub_category::class,'id','sub_category_id');
    }
    public function categories()
    {
        return $this->hasOne(category::class,'id','category_id');
    }

  

}
