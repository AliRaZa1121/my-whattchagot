<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_id',
        'start_date',
        'end_date',
        'bid_price_update',
        'bid_count',
        'bid_last_user_id',
        'auction_end',
        'size',
        'color',
        'description',
        'policy',
        'short_details',
    ];
    protected $guarded = [

    ];

    public function product()
    {
        return $this->hasOne(product::class);
    }
}
