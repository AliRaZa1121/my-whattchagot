<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; //add this line


class contactus extends Model
{
    use HasFactory;
    use SoftDeletes; //add this line

    protected $table = "contact_us";
    protected $guarded = [];

    // public function categories()
    // {
    //     return $this->hasMany(sub_category::class);
    // }
}
