<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\make_trade_images;
use App\Models\product;

class make_trade extends Model
{
    use HasFactory;
    protected $table='make_trade';
    protected $guarded = [];

    public function product_images()
    {
        return $this->hasOne(make_trade_images::class);
    }
    public function products()
    {
        return $this->hasOne(product::class,'id','product_id');
    }

    public function trade_with_products()
    {
        return $this->hasOne(product::class,'id','request_product_id');
    }


    public function seller()
    {
        return $this->hasOne(User::class,'id','seller_id');
    }

    public function buyer()
    {
        return $this->hasOne(User::class,'id','user_id');
    }





}
