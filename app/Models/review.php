<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\order_item;
use App\Models\User;


class review extends Model
{
    use HasFactory;

    protected $fillable = ['product_id','user_id','seller_id','order_item_id','comment','rate'];

    public function users()
    {
        return $this->hasOne(User::class,'id','user_id');
    }

}
