<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\user_image;
use App\Models\SellerPackage;



class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'user_type',
        'seller_package_id',
        'package_start_date',
        'package_valid_till',
        'google_id',
        'facebook_id',
        'strip_account_id',
        'stripe_flag',
        'status',
        'stripe_customer_id'
    ];

    protected $guarded = [

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getUserTypeFormatedAttribute()
    {
        $type=$this->user_type;
        return $type;
    }
    public function user_image(){
        return $this->hasOne(user_image::class,'id','user_id');
    }

    public function packages(){
        return$this->hasOne(SellerPackage::class,'id', 'seller_package_id');
    }
}
