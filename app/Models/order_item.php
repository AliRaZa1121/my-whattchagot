<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\product;
use App\Models\User;
use App\Models\review;

class order_item extends Model
{
    use HasFactory;

    protected $fillable = [

        'product_id',
        'user_id',
        'price',
        'qty',
        'shipping_price',
        'order_id',
        'seller_id',
        'current_order',
        'status',
        'charge_id',
        'refund_flag',
        'seller_charge_id',
        'payment_flag',
        'seller_charge_price',
        'stripe_transfer_id',
        'delivered_date',
        'color',
        'size'
    ];

    public function products()
    {
        return $this->hasOne(product::class,'id','product_id');
    }
    public function coustomer_name()
    {
        return $this->hasOne(User::class,'id','user_id');
    }
    public function seller_name()
    {
        return $this->hasOne(User::class,'id','seller_id');
    }
}
