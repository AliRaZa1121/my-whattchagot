<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; //add this line


class PageBanner extends Model
{
    use HasFactory;
    use SoftDeletes; //add this line

    protected $table = "page_images";
    protected $guarded = [];

 
}
