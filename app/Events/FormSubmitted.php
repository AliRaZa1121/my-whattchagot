<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class FormSubmitted implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    public $text;
    public $sender_id;
    public $receiver_id;
    public $product_id;
    public $sender_name;
    public function __construct(  $text,  $sender_id,  $receiver_id,  $product_id,$sender_name)
    {
        $this->text =$text;
        $this->sender_id =$sender_id;
        $this->receiver_id =$receiver_id;
        $this->product_id =$product_id;
        $this->sender_name =$sender_name;

    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {

        return new Channel('watchagot');
    }
    public function broadcastAs()
    {
        return new Channel('form-submitted');
    }
}
