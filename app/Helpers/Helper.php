<?php
use App\Models\User;
use App\Models\review;
use App\Models\category;
use App\Models\user_image;
use App\Models\order_item;
use App\Models\product;
use App\Models\Subscription;
use App\Models\checkout;
use App\Models\Setting;
use App\Models\make_trade;


if (!function_exists('user_name')) {

    function user_name($id)
    {
        return user::where('id',$id)->first();


    }
}
if (!function_exists('getreview')) {

    function getreview($id,$star)
    {
      return  $reviewbycount=review::where('product_id',$id)->where('rate',$star)->select(DB::raw('count(*) as rate_count,reviews.*'))
        ->groupBy('rate')
        ->orderBy('rate', 'asc')
        ->first();
    }

}




if (!function_exists('user_image')) {

    function user_image($id)
    {
        return  $user_image=user_image::where('user_id',$id)->first();
    }

}
if (!function_exists('getreviewbyproduct')) {

    function getreviewbyproduct($id)
    {
      return  $reviewbycount=review::where('product_id',$id)->get();
    }

}
if (!function_exists('getcategories')) {

    function getcategories()
    {
      return  $categories = category::latest()->with('categories')->get();

    }

}
if (!function_exists('reviewbycount')) {

    function reviewbycount($id)
    {
      return    $reviewbycount=review::where('product_id',$id) ->select(DB::raw('count(*) as rate_count,reviews.*'))
          ->groupBy('rate')
          ->orderBy('rate', 'asc')
          ->first();
    }

}


if (!function_exists('reviewbycount_user')) {

    function reviewbycount_user($id)
    {
        return    $reviewbycount=review::where('seller_id',$id) ->select(DB::raw('count(*) as rate_count,reviews.*'))
            ->groupBy('rate')
            ->orderBy('rate', 'asc')
            ->first();
    }

}
if (!function_exists('Order_finanicial_amount')) {

    function Order_finanicial_amount($id,$type)
    {
        $price=order_item::where('seller_id',$id)->where('status','Delivered')->whereIn('product_id', function($query) use ($type){
            $query->select('id')
                ->from(with(new product)->getTable())
                ->where('flags', $type);
        })->sum('price');
        $shipping_price=order_item::where('seller_id',$id)->where('status','Delivered')->whereIn('product_id', function($query) use ($type){
            $query->select('id')
                ->from(with(new product)->getTable())
                ->where('flags', $type);
        })->sum('shipping_price');
        $seller_charge_price=order_item::where('seller_id',$id)->where('status','Delivered')->whereIn('product_id', function($query) use ($type){
            $query->select('id')
                ->from(with(new product)->getTable())
                ->where('flags', $type);
        })->sum('seller_charge_price');
        return ($price+$shipping_price)-$seller_charge_price;
    }

}
if (!function_exists('Order_finanicial_amount')) {

    function Order_finanicial_amount($id,$type)
    {
        $price=order_item::where('seller_id',$id)->where('status','Delivered')->whereIn('product_id', function($query) use ($type){
            $query->select('id')
                ->from(with(new product)->getTable())
                ->where('flags', $type);
        })->sum('price');
        $shipping_price=order_item::where('seller_id',$id)->where('status','Delivered')->whereIn('product_id', function($query) use ($type){
            $query->select('id')
                ->from(with(new product)->getTable())
                ->where('flags', $type);
        })->sum('shipping_price');
        $seller_charge_price=order_item::where('seller_id',$id)->where('status','Delivered')->whereIn('product_id', function($query) use ($type){
            $query->select('id')
                ->from(with(new product)->getTable())
                ->where('flags', $type);
        })->sum('seller_charge_price');
        return ($price+$shipping_price)-$seller_charge_price;
    }

}
if (!function_exists('Order_finanicial_amount_price')) {

    function Order_finanicial_amount_price($id,$type)
    {
        return $price=order_item::where('seller_id',$id)->where('status','Delivered')->whereIn('product_id', function($query) use ($type){
            $query->select('id')
                ->from(with(new product)->getTable())
                ->where('flags', $type);
        })->sum('price');

    }

}
if (!function_exists('Order_finanicial_amount_shipping')) {

    function Order_finanicial_amount_shipping($id,$type)
    {

        return  $shipping_price=order_item::where('seller_id',$id)->whereIn('product_id', function($query) use ($type){
            $query->select('id')
                ->from(with(new product)->getTable())
                ->where('flags', $type);
        })->where('status','Delivered')->sum('shipping_price');

    }

}
if (!function_exists('Order_finanicial_amount_whatchagot_charges')) {

    function Order_finanicial_amount_whatchagot_charges($id,$type)
    {

        return $seller_charge_price=order_item::where('seller_id',$id)->where('status','Delivered')->whereIn('product_id', function($query) use ($type){
            $query->select('id')
                ->from(with(new product)->getTable())
                ->where('flags', $type);
        })->sum('seller_charge_price');
    }

}
if (!function_exists('number_of_subscription')) {

    function number_of_subscription($id)
    {

        return $number_of_subscription=User::where('subscription_id',$id)->count('id');
    }

}


if (!function_exists('get_cart_count')) {

   function get_cart_count()
    {
        $checkout_item = checkout::where([
            'user_id' => auth()->user()->id,
            'type' => 'selling'
            ])->count();
        return $checkout_item;
    }

}

if (!function_exists('get_setting')) {

    function get_setting()
     {
         $setting = Setting::first();
         return $setting;
     }

 }


if (!function_exists('maketradeDetail')) {

    function maketradeDetail($request_product_id,$user_id,$status)
    {
        $make_trade = make_trade::where([
           'request_product_id' => $request_product_id,
           'user_id' => $user_id,
           'status' => $status
        ])->with('product_images', 'products')->latest()->get();
        return $make_trade;

    }
 }

