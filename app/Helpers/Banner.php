<?php

namespace App\Helpers;


use App\Models\PageBanner;


class Banner
{

    public static function  get_banner_images($page, $position)
    {
         $image = PageBanner::where(['page'=> $page, 'position' => $position])->first();
         if($image){
            return 'storage/'.$image->banner_image;
        }
        else{
             return 'images/placeholder-image.png';
         }
        
    }

    public static function  get_banner_url($page, $position)
    {
         $url = PageBanner::where(['page'=> $page, 'position' => $position])->first();
         if($url){
            return $url->image_url;
         }else{
            return route('index');
         }
    }
   
}



